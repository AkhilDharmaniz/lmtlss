//
//  BeverageSubCategoryField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 10/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct BeverageSubCategoryField {
    
    public var option_id: Int?
    public var option: String?
    public var description: String?
    public var example: String?
    public var selected: Int?
    
    public init(option_id: Int?, option: String?, description: String?, example: String?, selected: Int?) {
        self.option_id = option_id
        self.option = option
        self.description = description
        self.example = example
        self.selected = selected
    }
}
