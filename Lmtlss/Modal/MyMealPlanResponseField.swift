//
//  MyMealPlanResponseField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 11/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct MyMealPlanResponseField {

    public var meal_name: String?
    public var a_food_item: [MyMealPlanField]
    public var food_item: [MyMealPlanField]
    public var selected: Bool?
    public var swap: Int?
    public var food_item_total: FoodItemTotal?
    public var a_food_item_total: FoodItemTotal?
    public var recipe_data: RecipeField?
    public var swapCount: Int?
    public var mark_meal_as_done: String?
    public var meal_plan_no: String?
    public var select_meal: Int?
    public var other_meal: Int?
    public var meal_id: Int?
    public init(meal_name: String?, a_food_item: [MyMealPlanField], food_item: [MyMealPlanField], selected: Bool?, swap: Int?,food_item_total: FoodItemTotal?,a_food_item_total: FoodItemTotal?,recipe_data: RecipeField?,swapCount: Int?,mark_meal_as_done: String?,meal_plan_no: String?,select_meal: Int?,other_meal: Int?,meal_id: Int?) {
        self.meal_name = meal_name
        self.a_food_item = a_food_item
        self.food_item = food_item
        self.selected = selected
        self.swap = swap
        self.food_item_total = food_item_total
        self.a_food_item_total = a_food_item_total
        self.recipe_data = recipe_data
        self.swapCount = swapCount
        self.mark_meal_as_done = mark_meal_as_done
        self.meal_plan_no = meal_plan_no
        self.select_meal = select_meal
        self.other_meal = other_meal
        self.meal_id = meal_id
    }
}
