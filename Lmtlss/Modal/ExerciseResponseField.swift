//
//  ExerciseResponseField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 24/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct ExerciseResponseField {

    public var option_name: String?
    public var exerciseList: [ExerciseField]
    public var selected: Bool?

    public init(option_name: String?, exerciseList: [ExerciseField], selected: Bool?) {
        self.option_name = option_name
        self.exerciseList = exerciseList
        self.selected = selected
    }


}
