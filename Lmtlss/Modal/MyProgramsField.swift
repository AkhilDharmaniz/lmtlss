//
//  MyProgramsField.swift
//  Lmtlss
//
//  Created by apple on 20/10/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct MyProgramsField {

    public var created_at: String?
    public var date: String?
    public var id: String?
    public var meal: [MyProgramsCategoryField]
    public var order_id: String?
    public var order_status: String?
    public var plan_id: String?
    public var plan_price: String?
    public var title: String?
    public var updated_at: String?
    public var user_id: String?
    public var selectedState: Bool

    public init(created_at: String?, date: String?, id: String?,meal: [MyProgramsCategoryField],order_id: String?,order_status: String?,plan_id: String?,plan_price: String?,title: String?,updated_at: String?,user_id: String?,selectedState: Bool) {
        self.created_at = created_at
        self.date = date
        self.id = id
        self.meal = meal
        self.order_id = order_id
        self.order_status = order_status
        self.plan_id = plan_id
        self.plan_price = plan_price
        self.title = title
        self.updated_at = updated_at
        self.user_id = user_id
        self.selectedState = selectedState
    }
}
