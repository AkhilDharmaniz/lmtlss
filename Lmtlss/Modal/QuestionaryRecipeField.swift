//
//  QuestionaryRecipeField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 30/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
public struct QuestionaryRecipeField {

    public var option: String?
    public var option_id: Int?
    public var selected: Int?
    public var description: String?
    public var example: String?
    public var img: String?
    public var thumbnail_img: String?

    public init(option: String?, option_id: Int?, selected: Int?, description: String?, example: String?, img: String, thumbnail_img: String?) {
        self.option = option
        self.option_id = option_id
        self.selected = selected
        self.description = description
        self.example = example
        self.img = img
        self.thumbnail_img = thumbnail_img
    }
}

