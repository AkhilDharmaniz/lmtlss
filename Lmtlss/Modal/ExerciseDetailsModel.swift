//
//  ExerciseDetailsModel.swift
//  Lmtlss
//
//  Created by mac on 09/02/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import Foundation

public struct ExerciseDetailsModel {
    public var av_reps: String?
    public var av_weight: String?
    public var date: String?
    public var sets: String?
    public var volume: String?

    public init(av_reps: String?,av_weight: String?,date: String?,sets: String?,volume: String?) {
        self.av_reps = av_reps
        self.av_weight = av_weight
        self.date = date
        self.sets = sets
        self.volume = volume
    }
}
