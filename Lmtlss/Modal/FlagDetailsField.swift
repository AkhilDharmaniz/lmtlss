//
//  FlagDetailsField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct FlagDetailsField{

    public var name: String?
    public var unicode: String?
    public var code: String?
    public var emoji: String?

    public init(name: String?, unicode: String?, code: String?, emoji: String?) {
        self.name = name
        self.unicode = unicode
        self.code = code
        self.emoji = emoji
    }


}
