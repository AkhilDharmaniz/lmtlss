//
//  QuestionaryField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 23/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
public struct QuestionaryField {

    public var option: String?
    public var option_id: Int?
    public var selected: Int?
    public var description: String?
    public var example: String?
    public var title: String?

    public init(option: String?, option_id: Int?, selected: Int?, description: String?, example: String?, title: String?) {
        self.option = option
        self.option_id = option_id
        self.selected = selected
        self.description = description
        self.example = example
        self.title = title
    }


}
