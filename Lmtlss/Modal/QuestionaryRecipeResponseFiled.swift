//
//  QuestionaryRecipeResponseFiled.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 30/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct QuestionaryRecipeResponseFiled {

    public var name: String?
    public var recipe: [QuestionaryRecipeField]
    public var selected: Bool?
    public var id: String?

    public init(name: String?, recipe: [QuestionaryRecipeField], selected: Bool?, id: String?) {
        self.name = name
        self.recipe = recipe
        self.selected = selected
        self.id = id
    }


}
