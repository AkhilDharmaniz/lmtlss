//
//  BeverageQuestionnaireField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 10/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct BeverageQuestionnaireField {
    
    public var high: [BeverageCategoryField]
    public var low: [BeverageCategoryField]
  
    public init(high: [BeverageCategoryField], low: [BeverageCategoryField]) {
        self.high = high
        self.low = low
    }
}
