//
//  MealPlanField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct MealPlanField {

    public var food_name: String?
    public var id: Int?
    public var selected: Int?
    public var selectedLocal: Int?


    public init(food_name: String?, id: Int?, selected: Int?, selectedLocal: Int?) {
        self.food_name = food_name
        self.id = id
        self.selected = selected
        self.selectedLocal = selectedLocal

    }
}
