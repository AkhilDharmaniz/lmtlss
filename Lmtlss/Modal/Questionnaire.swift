//
//  Questionnaire.swift
//  Lmtlss
//
//  Created by apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation



enum QuestionnaireScreensTypes{
    case Form
    case BMR
    case PhysicalActivity
    case Lifestyle
    case Cardio
    case basedProgram
    case TrainDays
    case Muscles
    case Exercises
    case Ambition
    case LoseFat
//    case GainWeight
    case Prefer
    case PreferBeverage
    case BeverageSelection
    case PreferMeal
    case CaloriesDistribute
    case LargerMeal
    case CustomizeMealPlan
    case IncorporateRecipe
    case ReplaceRecipe
    case SelectRecipe
    case results
}


struct PhysicalActivity{
    var title: String
    var subtitle: String
    var selected: Bool
}


struct LifestyleActivity{
    var title: String
    var subtitle: String
    var selected: Bool
}


struct Options{
    var option: String
    var selected: Bool
}
