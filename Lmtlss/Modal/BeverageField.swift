//
//  BeverageField.swift
//  Lmtlss
//
//  Created by apple on 21/04/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct BeverageField {
    public var calorie_category: String?
    public var calories: String?
    public var carb: String?
    public var created_at: String?
    public var dietary_fibre: String?
    public var drink_calories: String?
    public var drink_carbs: String?
    public var drink_dietary_fibre: String?
    public var drink_fat: String?
    public var drink_id: Int?
    public var drink_name: String?
    public var drink_protein: String?
    public var drink_serving_unit: String?
    public var drink_serving_size: String?
    public var fat: String?
    public var id: Int?
    public var meal_id: String?
    public var meal_no: String?
    public var protein: String?
    public var serving_quantity: String?
    public var serving_size: String?
    public var serving_unit: Int?
    public var sub_category: Int?
    public var sub_category_name: String?
    public var updated_at: String?
    public var user_id: Int?

    public init(calorie_category: String?,calories: String?,carb: String?,created_at: String?,dietary_fibre: String?, drink_calories: String?, drink_carbs: String?, drink_dietary_fibre: String?, drink_fat: String?, drink_id: Int?, drink_name: String?, drink_protein: String?, drink_serving_unit:String?,drink_serving_size: String?,fat: String?,id: Int?,meal_id: String?,meal_no: String?,protein: String?,serving_quantity: String?,serving_size: String?,serving_unit: Int?,sub_category: Int?,sub_category_name: String?,updated_at: String?,user_id: Int?) {
        self.calorie_category = calorie_category
        self.calories = calories
        self.carb = carb
        self.created_at = created_at
        self.dietary_fibre = dietary_fibre
        self.drink_calories = drink_calories
        self.drink_carbs = drink_carbs
        self.drink_dietary_fibre = drink_dietary_fibre
        self.drink_fat = drink_fat
        self.drink_id = drink_id
        self.drink_name = drink_name
        self.drink_protein = drink_protein
        self.drink_serving_unit = drink_serving_unit
        self.drink_serving_size = drink_serving_size
        self.fat = fat
        self.id = id
        self.meal_id = meal_id
        self.meal_no = meal_no
        self.protein = protein
        self.serving_quantity = serving_quantity
        self.serving_size = serving_size
        self.serving_unit = serving_unit
        self.sub_category = sub_category
        self.sub_category_name = sub_category_name
        self.updated_at = updated_at
        self.user_id = user_id
    }
}
