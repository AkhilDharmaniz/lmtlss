//
//  PersonalInfoField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 23/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct PersonalInfoField{

    public var order_number: String?
    public var name: String?
    public var username: String?

    public init(order_number: String?, name: String?, username: String?) {
        self.order_number = order_number
        self.name = name
        self.username = username
    }


}
