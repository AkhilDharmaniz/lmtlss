//
//  MuscleListModel.swift
//  Lmtlss
//
//  Created by mac on 04/02/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import Foundation

public struct MuscleListModel {
    public var created_at: String?
    public var gender: String?
    public var id: Int?
    public var muscle_group_name: String?
    public var muscle_group_status: Int?
    public var updated_at: String?
    public var selected : Bool?
    public init(created_at: String?,gender: String?,id: Int?,muscle_group_name: String?, muscle_group_status: Int?, updated_at: String?,selected : Bool?) {
        self.created_at = created_at
        self.gender = gender
        self.id = id
        self.muscle_group_name = muscle_group_name
        self.muscle_group_status = muscle_group_status
        self.updated_at = updated_at
        self.selected = selected
    }
}
