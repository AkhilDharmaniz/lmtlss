//
//  RecipeField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct RecipeField {

    public var calories: String?
    public var carb: String?
    public var fat: String?
    public var created_at: String?
    public var ingredients: [[String:Any]]?
    public var method: [String]?
    public var protein: String?
    public var recipe_id: String?
    public var recipe_image: String?
    public var recipe_name: String?
    public var serves: Int?
    public var tip: String?
    public var topping: String?
    public var updated_at: String?
    public var per_serve_calories: String?
    public var per_serve_carb: String?
    public var per_serve_fat: String?
    public var per_serve_protein: String?
    public var recipe_serve: String?
    public var total_serve_calories: String?
    public var total_serve_carb: String?
    public var total_serve_fat: String?
    public var total_serve_protein: String?
    public var recipe_weight: String?
    public var weight: String?
    public var recipe_thumbnail_image: String?
    
    public init(calories: String?, carb: String?, fat: String?, created_at: String?, ingredients: [[String:Any]]?, protein: String?, recipe_id: String?, recipe_image: String?, recipe_name: String?, serves: Int?, tip: String?, topping: String?, updated_at: String?, method: [String]?,per_serve_calories: String?,per_serve_carb: String?,per_serve_fat: String?,per_serve_protein: String?,recipe_serve: String?,total_serve_calories: String?,total_serve_carb: String?,total_serve_fat: String?,total_serve_protein: String?,recipe_weight: String?,weight:String?,recipe_thumbnail_image: String?) {
        self.calories = calories
        self.carb = carb
        self.fat = fat
        self.created_at = created_at
        self.ingredients = ingredients
        self.method = method
        self.protein = protein
        self.recipe_id = recipe_id
        self.recipe_image = recipe_image
        self.recipe_name = recipe_name
        self.serves = serves
        self.tip = tip
        self.topping = topping
        self.updated_at = updated_at
        self.per_serve_calories = per_serve_calories
        self.per_serve_carb = per_serve_carb
        self.per_serve_fat = per_serve_fat
        self.per_serve_protein = per_serve_protein
        self.recipe_serve = recipe_serve
        self.total_serve_calories = total_serve_calories
        self.total_serve_carb = total_serve_carb
        self.total_serve_fat = total_serve_fat
        self.total_serve_protein = total_serve_protein
        self.recipe_weight = recipe_weight
        self.weight = weight
        self.recipe_thumbnail_image = recipe_thumbnail_image
    }
}
