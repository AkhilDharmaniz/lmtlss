//
//  MyMealPlanField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 11/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct MyMealPlanField {

    public var food_carb: String?
    public var food_fat: String?
    public var food_item_id: Int?
    public var food_name: String?
    public var food_protien: String?
    public var food_cal: String?
    public var serving_size: String?
    public var serving_unit: String?

    public init(food_carb: String?,food_fat: String?, food_item_id: Int?,food_name: String?, food_protien: String?,food_cal: String,serving_size: String?,serving_unit: String?) {
        self.food_carb = food_carb
        self.food_fat = food_fat
        self.food_item_id = food_item_id
        self.food_name = food_name
        self.food_protien = food_protien
        self.food_cal = food_cal
        self.serving_size = serving_size
        self.serving_unit = serving_unit
    }
}
