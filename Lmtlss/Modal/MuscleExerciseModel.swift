//
//  MuscleExerciseModel.swift
//  Lmtlss
//
//  Created by mac on 05/02/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import Foundation

public struct MuscleExerciseModel {
    public var av_reps: String?
    public var av_weight: String?
    public var date: String?
    public var exercise_name: String?
    public var exercise_video_thumbnail: String?
    public var exercise_video_url: String?
    public var sets: String?
    public var volume: String?
    public var selected : Bool?
    public var exercise_id: Int?

    public init(av_reps: String?,av_weight: String?,date: String?,exercise_name: String?, exercise_video_thumbnail: String?, exercise_video_url: String?,sets: String?,volume: String?,selected : Bool?,exercise_id: Int?) {
        self.av_reps = av_reps
        self.av_weight = av_weight
        self.date = date
        self.exercise_name = exercise_name
        self.exercise_video_thumbnail = exercise_video_thumbnail
        self.exercise_video_url = exercise_video_url
        self.sets = sets
        self.volume = volume
        self.selected = selected
        self.exercise_id = exercise_id
    }
}
