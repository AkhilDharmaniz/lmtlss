//
//  SupplementField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct SupplementField {

    public var created_at: String?
    public var id: String?
    public var image: String?
    public var status: String?
    public var supplement_description: String?
    public var supplement_name: String?
    public var updated_at: String?
    public var url: String?

    public init(created_at: String?, id: String?, image: String?, status: String?, supplement_description: String?, supplement_name: String?, updated_at: String?, url:String?) {
        self.created_at = created_at
        self.id = id
        self.image = image
        self.status = status
        self.supplement_description = supplement_description
        self.supplement_name = supplement_name
        self.updated_at = updated_at
        self.url = url
    }
}
