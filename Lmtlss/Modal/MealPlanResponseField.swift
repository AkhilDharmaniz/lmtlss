//
//  MealPlanResponseField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 08/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct MealPlanResponseField {

    public var name: String?
    public var id: Int?
    public var alternate_meal: [MealPlanField]
    public var meal: [MealPlanField]
    public var selected: Bool?
    public var enanled: Bool?

    public init(name: String?,id: Int?, alternate_meal: [MealPlanField], meal: [MealPlanField], selected: Bool?, enabled: Bool?) {
        self.name = name
        self.id = id
        self.alternate_meal = alternate_meal
        self.meal = meal
        self.selected = selected
        self.enanled = enabled
    }
}

