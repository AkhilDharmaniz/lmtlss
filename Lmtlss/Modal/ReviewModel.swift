//
//  ReviewModel.swift
//  Lmtlss
//
//  Created by apple on 11/04/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import Foundation
struct ReviewModel {
    public var title: String?
    public var detail: [ReviewDetails]
    public init(dict: [String:Any]) {
        self.title = dict["title"] as? String ?? ""
        var daysTRArr = [ReviewDetails]()
        if let daysTRList = dict["detail"] as? [[String:Any]]{
            for daysTR in daysTRList{
                var detailsArr = [DetailsModel]()
                if let detailsList = daysTR["detail"] as? [[String:Any]]{
                    for details in detailsList{
                        detailsArr.append(DetailsModel(name: details["name"] as? String ?? "", weight: details["weight"] as? String ?? "", meal_no: details["meal_no"] as? String ?? ""))
                    }
                }
                daysTRArr.append(ReviewDetails(title: daysTR["title"] as? String ?? "", detail: detailsArr))
            }
        }
        self.detail = daysTRArr
        
    }
}

// MARK: - ResponseData
struct ReviewDetails {
    public var title: String?
    public var detail: [DetailsModel]
    public init(title: String?,detail: [DetailsModel]) {
        self.title = title
        self.detail = detail
    }
}

// MARK: - Detail
struct DetailsModel {
    public var name: String?
    public var weight: String?
    public var meal_no: String?
    public init(name: String?,weight: String?,meal_no: String?) {
        self.name = name
        self.weight = weight
        self.meal_no = meal_no
    }

}
