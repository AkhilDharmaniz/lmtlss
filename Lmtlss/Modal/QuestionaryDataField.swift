//
//  QuestionaryDataField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 23/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation
public struct QuestionaryDataField {

    public var personalInformation: PersonalInfoField?
    public var bodyFat: [QuestionaryField]?
    public var bodyType: [QuestionaryField]?
    public var dietaryRestrictions: [QuestionaryField]?
    public var dietaryIntolerances: [QuestionaryField]?
    public var allergies: [QuestionaryField]?
    public var snackDessertPreference: [QuestionaryField]?
    public var activityLevel: [QuestionaryField]?
    public var trainingTime: [QuestionaryField]?
    public var trainDays: [QuestionaryField]?
    public var trainingExperience: [QuestionaryField]?
    public var bodyWeight: [QuestionaryField]?
    public var goal: [QuestionaryField]?

    /** consumption */
    public var alcoholConsumption: [QuestionaryField]?
    public var injuries: [String]?

    public init(personalInformation: PersonalInfoField?, bodyFat: [QuestionaryField]?, bodyType: [QuestionaryField]?, dietaryRestrictions: [QuestionaryField]?, dietaryIntolerances: [QuestionaryField]?, allergies: [QuestionaryField]?, snackDessertPreference: [QuestionaryField]?, activityLevel: [QuestionaryField]?, trainingTime: [QuestionaryField]?, trainDays: [QuestionaryField]?, trainingExperience: [QuestionaryField]?, alcoholConsumption: [QuestionaryField]?, injuries: [String]?, bodyWeight: [QuestionaryField]?, goal: [QuestionaryField]? ) {
        self.personalInformation = personalInformation
        self.bodyFat = bodyFat
        self.bodyType = bodyType
        self.dietaryRestrictions = dietaryRestrictions
        self.dietaryIntolerances = dietaryIntolerances
        self.allergies = allergies
        self.snackDessertPreference = snackDessertPreference
        self.activityLevel = activityLevel
        self.trainingTime = trainingTime
        self.trainDays = trainDays
        self.trainingExperience = trainingExperience
        self.alcoholConsumption = alcoholConsumption
        self.injuries = injuries
        self.bodyWeight = bodyWeight
        self.goal = goal
    }

}
