//
//  ForumField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 11/01/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct ForumField {
    public var caption: String?
    public var created_at: String?
    public var id: Int?
    public var photo: String?
    public var title: String?
    public var type: String?
    public var updated_at: String?
    public var url: String?
    public var video: String?

    public init(caption: String?, created_at: String?, id: Int?, photo: String?, title: String?, type: String?, updated_at: String?,url: String?,video: String?) {
        self.caption = caption
        self.created_at = created_at
        self.id = id
        self.photo = photo
        self.title = title
        self.type = type
        self.updated_at = updated_at
        self.url = url
        self.video = video
    }
}
