//
//  MyProgramsCategoryField.swift
//  Lmtlss
//
//  Created by apple on 20/10/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct MyProgramsCategoryField {
    
    public var name: String?
    public var url: String?
    public init(name: String?, url: String?) {
        self.name = name
        self.url = url
    }
}
