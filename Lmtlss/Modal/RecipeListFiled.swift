//
//  RecipeListFiled.swift
//  Lmtlss
//
//  Created by apple on 14/03/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import Foundation

import Foundation

public struct RecipeListFiled {

    public var catgory_name: String?
    public var all_recipe: [RecipeField]
    public var selected: Bool?

    public init(catgory_name: String?, all_recipe: [RecipeField], selected: Bool?) {
        self.catgory_name = catgory_name
        self.all_recipe = all_recipe
        self.selected = selected
    }


}
