//
//  MainMealPlanField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 11/06/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct MainMealPlanField {
    
    public var protein: [MealPlanResponseField]
    public var carb: [MealPlanResponseField]
    public var fat: [MealPlanResponseField]
    public var condiments: [MealPlanResponseField]
    public var selected : Int?
    public init(protein: [MealPlanResponseField], carb: [MealPlanResponseField],fat: [MealPlanResponseField],condiments:[MealPlanResponseField], selected: Int?) {
        self.protein = protein
        self.carb = carb
        self.fat = fat
        self.condiments = condiments
        self.selected = selected
    }
}
