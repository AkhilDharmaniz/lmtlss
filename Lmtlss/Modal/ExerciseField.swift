//
//  ExerciseField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 24/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct ExerciseField {

    public var exercise_name: String?
    public var id: Int?
    public var selected: Int?

    public init(exercise_name: String?, id: Int?, selected: Int?) {
        self.exercise_name = exercise_name
        self.id = id
        self.selected = selected
    }


}
