//
//  VideoListField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 04/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct VideoListField {
    public var category: String?
    public var created_at: String?
    public var fav_status: String?
    public var id: String?
    public var sub_category: String?
    public var title: String?
    public var updated_at: String?
    public var video_thumbnail: String?
    public var video_url: String?

    public init(category: String?, created_at: String?,fav_status: String?,id: String?, sub_category: String?, title: String?, updated_at: String?, video_thumbnail: String?, video_url: String?) {
        self.category = category
        self.created_at = created_at
        self.fav_status = fav_status
        self.id = id
        self.sub_category = sub_category
        self.title = title
        self.updated_at = updated_at
        self.video_thumbnail = video_thumbnail
        self.video_url = video_url
    }
}
