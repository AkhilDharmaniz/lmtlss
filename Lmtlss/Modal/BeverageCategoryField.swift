//
//  BeverageCategoryField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 10/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct BeverageCategoryField {

    public var drink_category_name: String?
    public var drink_items: [BeverageSubCategoryField]
    public var selectedState: Bool

    public init(drink_category_name: String?, drink_items: [BeverageSubCategoryField],selectedState: Bool) {
        self.drink_category_name = drink_category_name
        self.drink_items = drink_items
        self.selectedState = selectedState
    }
}
