//
//  FoodItemTotal.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 17/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct FoodItemTotal {
   public var total_cal: String
   public var total_carb : String
   public var total_fat : String
   public var total_protein : String
   public init(total_cal: String, total_carb : String,total_fat : String,total_protein : String) {
        self.total_cal = total_cal
        self.total_carb = total_carb
        self.total_fat = total_fat
        self.total_protein = total_protein
    }
}
