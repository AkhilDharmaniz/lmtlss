//
//  SupportDataModel.swift
//  Lmtlss
//
//  Created by apple on 10/09/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation
import UIKit
public struct SupportDataModel {
    public var id: Int?
    public var sender_id: Int?
    public var receiver_id: Int?
    public var room_id: String?
    public var message: String?
    public var document: String?
    public var seen: Int?
    public var role: Int?
    public var is_draft: Int?
    public var is_schedule: Int?
    public var schedule_date: String?
    public var schedule_time: String?
    public var message_time: String?
    public var created_at: String?
    public var updated_at: String?
    public var thumbnail: UIImage?
    public var thumbnail_img: String?
    public var time: Float?
    public var playTime: String?
    public var minTime: Float?
    public var maxTime: Float?
    public var isPlaying: Bool?
    
    public init(id: Int?, sender_id: Int?, receiver_id: Int?, room_id: String?, message: String?,document: String?,seen: Int?,role: Int?,is_draft: Int?,is_schedule: Int?,schedule_date: String?,schedule_time: String?,message_time: String?,created_at: String?,updated_at: String?,thumbnail: UIImage?,thumbnail_img: String?,time: Float?, playTime: String?, minTime: Float?, maxTime: Float?,isPlaying: Bool?) {
        self.id = id
        self.sender_id = sender_id
        self.receiver_id = receiver_id
        self.room_id = room_id
        self.message = message
        self.document = document
        self.seen = seen
        self.role = role
        self.is_draft = is_draft
        self.is_schedule = is_schedule
        self.schedule_date = schedule_date
        self.schedule_time = schedule_time
        self.message_time = message_time
        self.created_at = created_at
        self.updated_at = updated_at
        self.thumbnail = thumbnail
        self.thumbnail_img = thumbnail_img
        self.time = time
        self.playTime = playTime
        self.minTime = minTime
        self.maxTime = maxTime
        self.isPlaying = isPlaying
    }
}
//
