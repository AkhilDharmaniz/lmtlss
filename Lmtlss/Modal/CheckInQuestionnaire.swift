//
//  CheckInQuestionnaire.swift
//  Lmtlss
//
//  Created by apple on 21/07/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

enum CheckInQuestionnaireScreensTypes{
    case CurrentWeight
    case TargetWeight
    case NewTargetWeight
    case uploadImages
    case PhysicalActivity
    case Lifestyle
    case Cardio
    case TrainingProgram
    case basedProgram
    case TrainDays
    case Muscles
    case Exercises
//    case Ambition
    case LoseFat
//    case GainWeight
    case Prefer
    case PreferBeverage
    case BeverageSelection
    case AmendMeal
    case ChangeMeal
    case PreferMeal
    case CaloriesDistribute
    case LargerMeal
    case CustomizeMealPlan
    case IncorporateRecipe
    case ReplaceRecipe
    case SelectRecipe
    case results
}
