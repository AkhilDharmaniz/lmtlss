//
//  VideoTypesField.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 11/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import Foundation

public struct VideoTypesField {
    public var color: String?
    public var created_at: String?
    public var id: Int?
    public var image: String?
    public var name: String?
    public var updated_at: String?
    public var selected: Bool?

    public init(color: String?, created_at: String?, id: Int?, image: String?, name: String?, updated_at: String?,selected: Bool?) {
        self.color = color
        self.created_at = created_at
        self.id = id
        self.image = image
        self.name = name
        self.updated_at = updated_at
        self.selected = selected
    }
}
