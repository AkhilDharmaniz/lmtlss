//
//  NotificationsListModel.swift
//  Lmtlss
//
//  Created by apple on 25/11/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

public struct NotificationsListModel {
    public var id: Int?
    public var user_id: Int?
    public var title: String?
    public var message: String?
    public var status: Int?
    public var seen: Int?
    public var push_type: Int?
    public var creation_at: String?

    public init(id: Int?,user_id: Int?,title: String?, message: String?, status: Int?,seen: Int?, push_type: Int?, creation_at: String?) {
        self.id = id
        self.user_id = user_id
        self.title = title
        self.message = message
        self.status = status
        self.seen = seen
        self.push_type = push_type
        self.creation_at = creation_at
    }
}
