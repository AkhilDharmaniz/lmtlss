//
//  CustomHeaderView.swift
//  Lmtlss
//
//  Created by apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
protocol CustomHeaderViewDelegate:class {
    func openSideMenu()
    func searchBtnTapped()
}


class CustomHeaderView: UIView {
    var delegate:CustomHeaderViewDelegate?
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var menuBtn: UIButton!
    @IBOutlet weak var searchImage: UIImageView!
    
    @IBOutlet weak var logoImg: UIImageView!
    @IBOutlet weak var menuImage: UIImageView!
    @IBOutlet weak var searchBtn: UIButton!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLbl: UILabel!
    var isSupport = false
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    override init(frame: CGRect) {
        super.init(frame: frame)
        let subView: UIView = loadViewFromNib()
        subView.frame = self.bounds
        //label1.text = "123" //would cause error
        addSubview(subView)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadge(notification:)), name: Notification.Name("updateBadge"), object: nil)
    }
    
    func loadViewFromNib() -> UIView {
        let view: UIView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
        return view
    }
    
    @IBAction func menuBtnTapped(_ sender: UIButton) {
        if self.isSupport == true{
            self.badgeView.isHidden = true
            self.isSupport = false
        }
        self.delegate?.openSideMenu()
    }
    
    @IBAction func searchBtnTapped(_ sender: UIButton) {
        self.delegate?.searchBtnTapped()
    }
    @objc func updateBadge(notification: Notification) {
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            badgeView.isHidden = searchImage.image == #imageLiteral(resourceName: "supportB") ? false : true
            badgeLbl.text = searchImage.image == #imageLiteral(resourceName: "supportB") ?  support_count : ""
        }
    }
}
