//
//  QuestionnaireLabelOptionCell.swift
//  Lmtlss
//
//  Created by apple on 23/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class QuestionnaireLabelOptionCell: UICollectionViewCell {

    @IBOutlet weak var optionLbl: UILabel!
    @IBOutlet weak var optionImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
