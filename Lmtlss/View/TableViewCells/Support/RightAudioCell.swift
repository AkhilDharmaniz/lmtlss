//
//  RightAudioCell.swift
//  Lmtlss
//
//  Created by apple on 26/08/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import AVFoundation

class RightAudioCell: UITableViewCell,AVAudioPlayerDelegate {

    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var playSlider: AudioSlider!
    @IBOutlet weak var playTimeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
}
