//
//  LeftAudioCell.swift
//  Lmtlss
//
//  Created by apple on 06/09/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class LeftAudioCell: UITableViewCell {

    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var playBtn: UIButton!
    @IBOutlet weak var playSlider: AudioSlider!
    @IBOutlet weak var playTimeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
