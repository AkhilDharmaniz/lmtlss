//
//  rightTextCell.swift
//  Lmtlss
//
//  Created by apple on 26/08/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class RightTextCell: UITableViewCell {

    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var msgTV: UITextView!
    @IBOutlet weak var timeLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        messageView.layer.cornerRadius = 8
        if #available(iOS 11, *) {
            messageView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner ,.layerMinXMinYCorner]
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
