//
//  MyProgramsCategoryCell.swift
//  Lmtlss
//
//  Created by apple on 20/10/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class MyProgramsCategoryCell: UITableViewCell {

    @IBOutlet weak var mealLbl: UILabel!
    @IBOutlet weak var downloadBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
