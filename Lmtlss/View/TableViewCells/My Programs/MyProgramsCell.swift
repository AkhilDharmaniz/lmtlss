//
//  MyProgramsCell.swift
//  Lmtlss
//
//  Created by apple on 20/10/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
protocol MyProgramsProtocol{
    func tappedAt(obj: MyProgramsCategoryField, cat: MyProgramsField)
}
class MyProgramsCell: UITableViewCell {

    @IBOutlet weak var planNameLbl: UILabel!
    @IBOutlet weak var planTimeLbl: UILabel!
    @IBOutlet weak var arrowImg: UIImageView!
    @IBOutlet weak var arrowBtn: UIButton!
    @IBOutlet weak var mealsTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var rightArrowImg: UIImageView!
    var delegate:MyProgramsProtocol?
    var category: MyProgramsField?
    var didSelectIndex:((_ name:String, _ url:String) -> Void)?
    var downloadPDF:((_ name:String, _ url:String) -> Void)?
    var subCats:[MyProgramsCategoryField] = []{
        didSet{
            mealsTableView.reloadData()
        }
    }
    var selectedCat:((MyProgramsField,MyProgramsCategoryField,Int,Bool)->Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        mealsTableView.register(UINib(nibName: "MyProgramsCategoryCell", bundle: nil), forCellReuseIdentifier: "Cell")
        mealsTableView.delegate = self
        mealsTableView.dataSource = self
        mealsTableView.separatorStyle = .none
    }
    func configureCategoryCell(data: MyProgramsField){
        self.category = data
        self.subCats = data.selectedState ? data.meal : []
   //     borderView.isHidden = !data.selectedState
        planNameLbl.text = data.title
        planTimeLbl.text = data.date
        arrowImg.isHidden = data.selectedState ? false : true
        rightArrowImg.isHidden = data.selectedState ? true : false
   //     arrowImg.image = UIImage(named: data.selectedState ? "arrow_down" : "ic_arrow_r")
        let height = data.selectedState ? (Double(data.meal.count) * 45.0+15) : 0.0
        tableViewHeight.constant = CGFloat(height)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

       
    }
    
}
extension MyProgramsCell: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyProgramsCategoryCell
        cell.mealLbl.text = subCats[indexPath.row].name
        cell.downloadBtn.tag = indexPath.row
        cell.downloadBtn.addTarget(self, action: #selector(downloadBtnAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 45
    }
    @objc func downloadBtnAction(_ sender: UIButton){
        downloadPDF?(subCats[sender.tag].name ?? "", subCats[sender.tag].url ?? "")
       
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        didSelectIndex?(subCats[indexPath.row].name ?? "", subCats[indexPath.row].url ?? "")
    }
}
