//
//  NotificationTableCell.swift
//  Lmtlss
//
//  Created by Vivek Dharmani on 16/11/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class NotificationTableCell: UITableViewCell {

    @IBOutlet weak var lblDesp: UILabel!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var iconImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
