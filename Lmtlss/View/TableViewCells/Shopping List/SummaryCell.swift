//
//  SummaryCell.swift
//  Lmtlss
//
//  Created by apple on 11/04/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit

class SummaryCell: UITableViewCell {

    @IBOutlet weak var lineLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var servingSizeLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
