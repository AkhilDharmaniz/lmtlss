//
//  ReviewCell.swift
//  Lmtlss
//
//  Created by apple on 11/04/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {

    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var summaryTableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    var reviewDetails = [ReviewDetails]()
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        summaryTableView.delegate = self
        summaryTableView.dataSource = self
//        if #available(iOS 15.0, *) {
//            UITableView.appearance().sectionHeaderTopPadding = CGFloat(0)
//        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state

    }
    func setupData(data: [ReviewDetails]){

        self.reviewDetails = data
        self.summaryTableView.reloadData()
        var height = 9.0
        for data in self.reviewDetails{
            var rowHeight = 0.0
            rowHeight = Double(50 + data.detail.count * 60)
            height = height + rowHeight
        }
        self.tableViewHeight.constant = height
        
    }
}
//MARK:- TableView Delegate Method(s)
extension ReviewCell: UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.reviewDetails.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviewDetails[section].detail.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: SummaryCell! = tableView.dequeueReusableCell(withIdentifier: "SummaryCell") as? SummaryCell
        if cell == nil {
            tableView.register(UINib(nibName: "SummaryCell", bundle: nil), forCellReuseIdentifier: "SummaryCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "SummaryCell") as? SummaryCell
        }
        cell.titleLbl.text = reviewDetails[indexPath.section].detail[indexPath.row].name
        cell.servingSizeLbl.text = "Serving size: \(reviewDetails[indexPath.section].detail[indexPath.row].weight ?? "")" 
        if indexPath.row == reviewDetails[indexPath.section].detail.count - 1 {
            cell.lineLbl.isHidden = true
        }else{
            cell.lineLbl.isHidden = false
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
            var cell: MealsSectionCell! = tableView.dequeueReusableCell(withIdentifier: "MealsSectionCell") as? MealsSectionCell
            if cell == nil {
                tableView.register(UINib(nibName: "MealsSectionCell", bundle: nil), forCellReuseIdentifier: "MealsSectionCell")
                cell = tableView.dequeueReusableCell(withIdentifier: "MealsSectionCell") as? MealsSectionCell
            }
        cell.mealTitleLbl.text = self.reviewDetails[section].title
            return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
}
