//
//  BeverageCategoryCell.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 10/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
protocol BeverageSelectCategoryProtocol{
    func tappedAt(obj: BeverageSubCategoryField, cat: BeverageCategoryField)
}
class BeverageCategoryCell: UITableViewCell {

    @IBOutlet weak var dropBtn: UIButton!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var borderView: UIImageView!
    @IBOutlet weak var subCatTableView: UITableView!
    @IBOutlet weak var catNameLbl: UILabel!
    @IBOutlet weak var dropDownImageView: UIImageView!
    var delegate:BeverageSelectCategoryProtocol?
    var category: BeverageCategoryField?
    var subCats:[BeverageSubCategoryField] = []{
        didSet{
            subCatTableView.reloadData()
        }
    }
    var selectedCat:((BeverageCategoryField,BeverageSubCategoryField,Int,Bool)->Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        subCatTableView.register(UINib(nibName: "BeverageSubCategoryCell", bundle: nil), forCellReuseIdentifier: "cell")
        subCatTableView.delegate = self
        subCatTableView.dataSource = self
        subCatTableView.separatorStyle = .none
    }
    func configureCategoryCell(data: BeverageCategoryField){
        self.category = data
        self.subCats = data.selectedState ? data.drink_items : []
   //     borderView.isHidden = !data.selectedState
        catNameLbl.text = data.drink_category_name
        dropDownImageView.image = UIImage(named: data.selectedState ? "arrow_up" : "arrow_down")
        let height = data.selectedState ? (Double(data.drink_items.count) * 55.0) : 0.0
        tableViewHeightConstraint.constant = CGFloat(height)
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension BeverageCategoryCell: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return subCats.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BeverageSubCategoryCell
        cell.subCatLbl.text = subCats[indexPath.row].option
        cell.bgLbl.backgroundColor = subCats[indexPath.row].selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .clear
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if subCats[indexPath.row].selected == 1{
//            subCats[indexPath.row].selected = 0
//            subCatTableView.reloadData()
            guard let cat = self.category else {return}
                selectedCat?(cat,subCats[indexPath.row],indexPath.row,false)
        }else{
        guard let cat = self.category else {return}
            selectedCat?(cat,subCats[indexPath.row],indexPath.row,true)
        }
    }
}
