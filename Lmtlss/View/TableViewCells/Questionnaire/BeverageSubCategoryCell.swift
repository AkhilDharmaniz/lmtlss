//
//  BeverageSubCategoryCell.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 10/05/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit

class BeverageSubCategoryCell: UITableViewCell {
    @IBOutlet weak var subCatLbl: UILabel!
    @IBOutlet weak var bgLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
