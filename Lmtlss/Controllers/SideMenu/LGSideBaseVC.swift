//
//  LGSideBaseVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 20/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import LGSideMenuController

class LGSideBaseVC: LGSideMenuController {
var pushType = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        leftViewPresentationStyle = .slideAbove
        leftViewWidth = UIScreen.main.bounds.size.width * 0.8
        leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(leftViewWidth, leftViewWidth);
    }
}
