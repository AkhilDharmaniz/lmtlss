//
//  SupportVC.swift
//  Lmtlss
//
//  Created by apple on 26/08/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import MobileCoreServices
import Alamofire
import SDWebImage
import AVKit
import AVFoundation
import SocketIO
import IQKeyboardManagerSwift

class SupportVC: UIViewController {

    @IBOutlet weak var chatTableView: UITableView!
    @IBOutlet weak var sendMsgView: UIView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var msgTV: UITextView!
    @IBOutlet weak var tableViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var msgTVHeight: NSLayoutConstraint!
    @IBOutlet var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var sendBtn: LoadingButton!
    @IBOutlet weak var sendImg: UIImageView!
    @IBOutlet weak var addMediaBtn: LoadingButton!
    @IBOutlet weak var addMediaImg: UIImageView!
    @IBOutlet weak var noDataLbl: UILabel!

    let imagePicker = UIImagePickerController()
    var msgArr = [SupportDataModel]()
    var roomId = ""
    var profileImg = ""
    var fromDelegate = false
    
    var playerItem:AVPlayerItem?
    var player:AVPlayer?
    var slider: UISlider?
    var urlStr = ""
    var selectedIndex = 0
    var isFinished = false
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        getChatData()
    }
    func configureUI(){
        let isReply = UserDefaults.standard.value(forKey: "isReply") as? Int
        if isReply != 3{
            self.tableViewBottomConstraint.constant = -68
            self.sendMsgView.isHidden = true
        }
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Support"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.isHidden = true
        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.delegate = self
        myCustomView.isSupport = true
        self.navView.addSubview(myCustomView)
        chatTableView.delegate = self
        chatTableView.dataSource = self
        msgTV.delegate = self
        IQKeyboardManager.shared.disabledDistanceHandlingClasses.append(SupportVC.self)
        IQKeyboardManager.shared.disabledToolbarClasses = [SupportVC.self]
         NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        chatTableView.addGestureRecognizer(tap)
    }
    
    // MARK:- Socket Method(s)
    func configureSocket(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            let socketConnectionStatus = SocketManger.shared.socket.status
            switch socketConnectionStatus {
            case SocketIOStatus.connected:
                print("socket connected")
                SocketManger.shared.socket.emit("ConncetedChat", self.roomId)
                self.newMessageSocketOn()
            case SocketIOStatus.connecting:
                print("socket connecting")
            case SocketIOStatus.disconnected:
                print("socket disconnected")
                SocketManger.shared.socket.connect()
                self.connectSocketOn()
                self.newMessageSocketOn()
            case SocketIOStatus.notConnected:
                print("socket not connected")
                SocketManger.shared.socket.connect()
                self.connectSocketOn()
                self.newMessageSocketOn()
            }
            
        }
    }
    func connectSocketOn(){
        SocketManger.shared.onConnect {
            SocketManger.shared.socket.emit("ConncetedChat", self.roomId)
            print("connected")
            
        }
    }
    func newMessageSocketOn(){
        SocketManger.shared.handleNewMessage { (message) in
            self.msgArr.append(SupportDataModel(id: message["id"] as? Int ?? 0, sender_id: message["sender_id"] as? Int ?? 0, receiver_id: message["receiver_id"] as? Int ?? 0, room_id: message["room_id"] as? String ?? "", message: message["message"] as? String ?? "", document: message["document"] as? String ?? "", seen: message["seen"] as? Int ?? 0, role: message["role"] as? Int ?? 0, is_draft: message["is_draft"] as? Int ?? 0, is_schedule: message["is_schedule"] as? Int ?? 0, schedule_date: message["schedule_date"] as? String ?? "", schedule_time: message["schedule_time"] as? String ?? "", message_time: message["message_time"] as? String ?? "", created_at: message["created_at"] as? String ?? "", updated_at: message["updated_at"] as? String ?? "", thumbnail: UIImage(color: .lightGray), thumbnail_img: message["thumbnail_img"] as? String ?? "",time: 0,playTime: "00:00",minTime: 0,maxTime: 0,isPlaying: false))
            self.chatTableView.reloadData()
           self.scrollToEnd()
        }
    }
    
    // MARK:- Keyboard Handling Method
    
    @objc func handleKeyboardNotification(_ notification: Notification) {
        if let userInfo = notification.userInfo {
            
            let keyboardFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            if UIDevice.current.iPhoneX{
                bottomConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height-view.safeAreaInsets.bottom : 0
                
            }else{
                bottomConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height : 0
            }
             //   bottomConstraint?.constant = isKeyboardShowing ? keyboardFrame!.height : 0
            UIView.animate(withDuration: 0.1, animations: { () -> Void in
                self.view.layoutIfNeeded()
                self.scrollToEnd()
            })
        }
    }
    // MARK:- Loading Button Method(s)
    func SendStopMessage(isSend:Bool){
            if isSend{
                sendBtn.loadIndicator(true)
                sendImg.image = nil
            }else{
                sendImg.image = #imageLiteral(resourceName: "send")
                sendBtn.loadIndicator(false)
            }
        }
    func SendStopMedia(isSend:Bool){
            if isSend{
                addMediaBtn.loadIndicator(true)
                addMediaImg.image = nil
            }else{
                addMediaImg.image = #imageLiteral(resourceName: "addMedia")
                addMediaBtn.loadIndicator(false)
            }
        }
    // MARK:- Get And Send Chat Data
    func getChatData(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.supportListing
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.msgArr.removeAll()
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        UserDefaults.standard.set("0", forKey: "support_count")
                        self.roomId = response["room_id"] as? String ?? ""
                        self.profileImg = response["profile_image"] as? String ?? ""
                        self.configureSocket()
                        if let allMsgs = response["all_message"] as? [[String:Any]]{
                            for i in 0..<allMsgs.count{
                                self.msgArr.append(SupportDataModel(id: allMsgs[i]["id"] as? Int ?? 0, sender_id: allMsgs[i]["sender_id"] as? Int ?? 0, receiver_id: allMsgs[i]["receiver_id"] as? Int ?? 0, room_id: allMsgs[i]["room_id"] as? String ?? "", message: allMsgs[i]["message"] as? String ?? "", document: allMsgs[i]["document"] as? String ?? "", seen: allMsgs[i]["seen"] as? Int ?? 0, role: allMsgs[i]["role"] as? Int ?? 0, is_draft: allMsgs[i]["is_draft"] as? Int ?? 0, is_schedule: allMsgs[i]["is_schedule"] as? Int ?? 0, schedule_date: allMsgs[i]["schedule_date"] as? String ?? "", schedule_time: allMsgs[i]["schedule_time"] as? String ?? "", message_time: allMsgs[i]["message_time"] as? String ?? "", created_at: allMsgs[i]["created_at"] as? String ?? "", updated_at: allMsgs[i]["updated_at"] as? String ?? "", thumbnail: UIImage(color: .lightGray), thumbnail_img: allMsgs[i]["thumbnail_img"] as? String ?? "",time: 0,playTime: "00:00",minTime: 0,maxTime: 0,isPlaying: false))
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    if self.msgArr.count == 0{
                        IJProgressView.shared.showToast(message: "No Messages Found", center: true)
                    }
                        self.chatTableView.reloadData()
                        self.view.layoutIfNeeded()
                        self.scrollToEnd()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func scrollToEnd(){
        if msgArr.count > 0 {
            chatTableView.scrollToRow(at: IndexPath(item:msgArr.count-1, section: 0), at: .bottom, animated: false)
        }
    }
    
    @IBAction func sendBtnAction(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() == true {
            let trimmedString = msgTV.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
            if trimmedString != ""{
                sendInfoData()
                msgTV.text = ""
                self.msgTVHeight.constant = 38
            }
        }else{
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func sendInfoData(){
        self.SendStopMessage(isSend: true)
        let signInUrl = Constants.baseURL + Constants.supportMessageSave
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            self.SendStopMessage(isSend: false)
            
            let message = response["message"] as? String ?? ""
          if let status = response["responseCode"] as? Int {
            if status == 200{
                if let data = response["data"] as? [String:Any]{
                    SocketManger.shared.socket.emit("newMessage",self.roomId,data)
                    self.msgArr.append(SupportDataModel(id: data["id"] as? Int ?? 0, sender_id: data["sender_id"] as? Int ?? 0, receiver_id: data["receiver_id"] as? Int ?? 0, room_id: data["room_id"] as? String ?? "", message: data["message"] as? String ?? "", document: data["document"] as? String ?? "", seen: data["seen"] as? Int ?? 0, role: data["role"] as? Int ?? 0, is_draft: data["is_draft"] as? Int ?? 0, is_schedule: data["is_schedule"] as? Int ?? 0, schedule_date: data["schedule_date"] as? String ?? "", schedule_time: data["schedule_time"] as? String ?? "", message_time: data["message_time"] as? String ?? "", created_at: data["created_at"] as? String ?? "", updated_at: data["updated_at"] as? String ?? "", thumbnail: UIImage(color: .lightGray), thumbnail_img: data["thumbnail_img"] as? String ?? "",time: 0,playTime: "00:00",minTime: 0,maxTime: 0,isPlaying: false))
                    self.chatTableView.reloadData()
                    self.scrollToEnd()
                }
            }else{
                self.SendStopMessage(isSend: false)
                alert(Constants.AppName, message: message, view: self)
            }
          }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        let trimmedString = msgTV.text?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        let trimmed = trimmedString?.replacingOccurrences(of: "\n", with: "@$^#*%&")
        print(trimmed)
        parameters["message"] = trimmed as AnyObject
        parameters["room_id"] = roomId as AnyObject
        parameters["document"] = "" as AnyObject
        parameters["thumbnail_img"] = "" as AnyObject
        
        return parameters
    }
    
    
    @IBAction func addMediaBtnAction(_ sender: UIButton) {
        
        
        let actionSheetAlertController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "           Camera", style: .default) { (action) in
            if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera)){
                self.imagePicker.delegate = self
                self.imagePicker.sourceType = UIImagePickerController.SourceType.camera
                self.imagePicker.allowsEditing = true
                self .present(self.imagePicker, animated: true, completion: nil)
            }else{
                alert(Constants.AppName, message: "Sorry, this device has no camera", view: self)
            }
        }
        let image = #imageLiteral(resourceName: "cam")
        let imageView = UIImageView()
        imageView.image = image
        imageView.contentMode = .scaleAspectFit
        imageView.frame =  CGRect(x: 25, y: 18, width: 24, height: 24)
        actionSheetAlertController.view.addSubview(imageView)
        cameraAction.setValue(UIColor.black, forKey: "titleTextColor")
        cameraAction.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        actionSheetAlertController.addAction(cameraAction)
        
        
        let galleryAction = UIAlertAction(title: "           Photo & Video Library", style: .default) { (action) in
            self.imagePicker.sourceType = .photoLibrary
            self.imagePicker.videoMaximumDuration = 30.00
            self.imagePicker.allowsEditing = true
            self.imagePicker.mediaTypes = [kUTTypeMovie as String, kUTTypeImage as String]
            self.imagePicker.delegate = self
            self.imagePicker.isEditing = true
            self.present(self.imagePicker, animated: true, completion: nil)
        }
        let image1 =  #imageLiteral(resourceName: "media")
        let imageView1 = UIImageView()
        imageView1.image = image1
        imageView1.contentMode = .scaleAspectFit
        imageView1.frame = CGRect(x: 25, y: 75, width: 24, height: 24)
        actionSheetAlertController.view.addSubview(imageView1)
        galleryAction.setValue(UIColor.black, forKey: "titleTextColor")
        galleryAction.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        actionSheetAlertController.addAction(galleryAction)
        let audioAction = UIAlertAction(title: "           Audio", style: .default) { (action) in
            let VC = RecordVC.instantiate(fromAppStoryboard: .SideMenu)
            VC.dismissedCurrentView =  { audioData in
                self.navigationController?.popViewController(animated: true)
                self.sendMediaData(data: audioData, type: "audio", thumbnailData: audioData)
            }
            self.navigationController?.pushViewController(VC, animated: true)
        }
        audioAction.setValue(UIColor.black, forKey: "titleTextColor")
        audioAction.setValue(CATextLayerAlignmentMode.left, forKey: "titleTextAlignment")
        actionSheetAlertController.addAction(audioAction)
        let image2 =  #imageLiteral(resourceName: "audio")
        let imageView2 = UIImageView()
        imageView2.image = image2
        imageView2.contentMode = .scaleAspectFit
        imageView2.frame = CGRect(x: 25, y: 132, width: 24, height: 24)
        actionSheetAlertController.view.addSubview(imageView2)

        let cancelActionButton = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        cancelActionButton.setValue(UIColor(named: "TextColor") , forKey: "titleTextColor")
        actionSheetAlertController.addAction(cancelActionButton)
        self.present(actionSheetAlertController, animated: true, completion: nil)
    }
    func sendMediaData(data: Data, type: String, thumbnailData: Data){
        if Reachability.isConnectedToNetwork() == true {
            
            self.SendStopMedia(isSend: true)
            IJProgressView.shared.showProgressView()
            let url = Constants.baseURL + Constants.supportMessageSave
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AF.upload(multipartFormData: { (multipartFormData) in
                for (key, value) in self.generatingMediaParameters() {
                    multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
                }
                print(data)
                if type == "image"{
                    multipartFormData.append(data, withName: "document", fileName: "document.jpg", mimeType: "")
                }else if type == "video"{
                    multipartFormData.append(data, withName: "document", fileName: "document.mp4", mimeType: "")
                    multipartFormData.append(thumbnailData, withName: "thumbnail_img", fileName: "thumbnail_img.jpg", mimeType: "")
                }else if type == "audio"{
                    multipartFormData.append(data, withName: "document", fileName: "document.mp3", mimeType: "")
                }
            }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers, interceptor: nil, fileManager: .default)
                
                .uploadProgress(closure: { (progress) in
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .responseJSON { (response) in
                    self.SendStopMedia(isSend: false)
                    IJProgressView.shared.hideProgressView()
                    switch response.result {
                    case .success(let value):
                        if let JSON = value as? [String: Any] {
                            let displayMessage = JSON["responseMessage"] as? String
                            if let code = JSON["responseCode"] as? Int {
                                if code == 200{
                                    if let data = JSON["data"] as? [String:Any]{
                                        SocketManger.shared.socket.emit("newMessage",self.roomId,data)
                                        self.msgArr.append(SupportDataModel(id: data["id"] as? Int ?? 0, sender_id: data["sender_id"] as? Int ?? 0, receiver_id: data["receiver_id"] as? Int ?? 0, room_id: data["room_id"] as? String ?? "", message: data["message"] as? String ?? "", document: data["document"] as? String ?? "", seen: data["seen"] as? Int ?? 0, role: data["role"] as? Int ?? 0, is_draft: data["is_draft"] as? Int ?? 0, is_schedule: data["is_schedule"] as? Int ?? 0, schedule_date: data["schedule_date"] as? String ?? "", schedule_time: data["schedule_time"] as? String ?? "", message_time: data["message_time"] as? String ?? "", created_at: data["created_at"] as? String ?? "", updated_at: data["updated_at"] as? String ?? "", thumbnail: UIImage(color: .lightGray), thumbnail_img: data["thumbnail_img"] as? String ?? "",time: 0,playTime: "00:00",minTime: 0,maxTime: 0,isPlaying: false))
                                        self.chatTableView.reloadData()
                                        self.scrollToEnd()
                                    }
                                }else{
                                    alert(Constants.AppName, message: displayMessage ?? "", view: self)
                                }
                            }
                            if let status = JSON["status"] as? Int, status == 401{
                                logOut(controller: self)
                            }
                        }
                    case .failure(let error):
                        // error handling
                    self.SendStopMedia(isSend: false)
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: error.localizedDescription, view: self)
                        break
                    }
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func generatingMediaParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["message"] = "" as AnyObject
        parameters["room_id"] = roomId as AnyObject
        
        return parameters
    }
   
}
//MARK:- TableView Delegate Method(s)

extension SupportVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return msgArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         var pathExt = ""
        if let document = msgArr[indexPath.row].document{
            if document != ""{
                let url = URL(string: document)
                pathExt = url?.pathExtension ?? ""
            }
        }
        if pathExt == ""{
            if msgArr[indexPath.row].role == 2{
                var cell: RightTextCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? RightTextCell
                if cell == nil {
                    tableView.register(UINib(nibName: "RightTextCell", bundle: nil), forCellReuseIdentifier: "Cell")
                    cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? RightTextCell
                }
                cell.msgTV.text = self.msgArr[indexPath.row].message?.replacingOccurrences(of: "@$^#*%&", with: "\n")
                cell.timeLbl.text = msgArr[indexPath.row].message_time?.convertDateToString()
                return cell
            }else{
                var cell: LeftTextCell! = tableView.dequeueReusableCell(withIdentifier: "Cell1") as? LeftTextCell
                if cell == nil {
                    tableView.register(UINib(nibName: "LeftTextCell", bundle: nil), forCellReuseIdentifier: "Cell1")
                    cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as? LeftTextCell
                }
                cell.msgTV.text = msgArr[indexPath.row].message?.htmlToString.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
                cell.timeLbl.text = msgArr[indexPath.row].message_time?.convertDateToString()
                return cell
            }
        }else{
            if pathExt == "mp4" || pathExt == "webm" || pathExt == "mpg" || pathExt == "m4v" {
                var cell: VideoCell! = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as? VideoCell
                if cell == nil {
                    tableView.register(UINib(nibName: "VideoCell", bundle: nil), forCellReuseIdentifier: "VideoCell")
                    cell = tableView.dequeueReusableCell(withIdentifier: "VideoCell") as? VideoCell
                }
                if msgArr[indexPath.row].role == 2{
                    cell.leadingConstraint.priority = UILayoutPriority(250)
                    cell.trailingConstraint.priority =  UILayoutPriority(999)
                    cell.timeLbl.textAlignment = .right
                }else{
                    cell.trailingConstraint.priority =  UILayoutPriority(250)
                    cell.leadingConstraint.priority =  UILayoutPriority(999)
                    cell.timeLbl.textAlignment = .left
                }
                
                cell.playBtn.tag = indexPath.row
                cell.playBtn.addTarget(self, action: #selector(playBtnAction(_:)), for: .touchUpInside)
                cell.timeLbl.text = msgArr[indexPath.row].message_time?.convertDateToString()
                cell.thumbImg.sd_setImage(with: URL(string: self.msgArr[indexPath.row].thumbnail_img ?? ""), placeholderImage: self.msgArr[indexPath.row].thumbnail, options: SDWebImageOptions.continueInBackground, completed: nil)
                return cell
            }else if pathExt == "mp3"{
                if msgArr[indexPath.row].role == 2{
                    var cell: RightAudioCell! = tableView.dequeueReusableCell(withIdentifier: "RAudioCell") as? RightAudioCell
                    if cell == nil {
                        tableView.register(UINib(nibName: "RightAudioCell", bundle: nil), forCellReuseIdentifier: "RAudioCell")
                        cell = tableView.dequeueReusableCell(withIdentifier: "RAudioCell") as? RightAudioCell
                    }
                    cell.timeLbl.text = msgArr[indexPath.row].message_time?.convertDateToString()
                    cell.profilePic.sd_setImage(with: URL(string: profileImg), placeholderImage: #imageLiteral(resourceName: "user_profile"))
                    cell.playBtn.setImage(UIImage(named:"play1"), for: .normal)
                    cell.playSlider.isUserInteractionEnabled = false
                    cell.playBtn.tag = indexPath.row
                    cell.playBtn.addTarget(self, action: #selector(playPauseTapped(sender:)), for: .touchUpInside)
                    return cell
                }else{
                    var cell: LeftAudioCell! = tableView.dequeueReusableCell(withIdentifier: "LAudioCell") as? LeftAudioCell
                    if cell == nil {
                        tableView.register(UINib(nibName: "LeftAudioCell", bundle: nil), forCellReuseIdentifier: "LAudioCell")
                        cell = tableView.dequeueReusableCell(withIdentifier: "LAudioCell") as? LeftAudioCell
                    }
                    cell.timeLbl.text = msgArr[indexPath.row].message_time?.convertDateToString()
                    cell.playSlider.isUserInteractionEnabled = false

                    cell.playBtn.tag = indexPath.row
                    cell.playBtn.addTarget(self, action: #selector(playPauseTapped(sender:)), for: .touchUpInside)
                    return cell
                }
            }else{
                var cell: ImageCell! = tableView.dequeueReusableCell(withIdentifier: "ImageCell") as? ImageCell
                if cell == nil {
                    tableView.register(UINib(nibName: "ImageCell", bundle: nil), forCellReuseIdentifier: "ImageCell")
                    cell = tableView.dequeueReusableCell(withIdentifier: "ImageCell") as? ImageCell
                }
                if msgArr[indexPath.row].role == 2{
                    cell.leadingConstraint.priority = UILayoutPriority(250)
                    cell.trailingConstraint.priority =  UILayoutPriority(999)
                    cell.timeLbl.textAlignment = .right
                }else{
                    cell.trailingConstraint.priority =  UILayoutPriority(250)
                    cell.leadingConstraint.priority =  UILayoutPriority(999)
                    cell.timeLbl.textAlignment = .left
                }
                cell.imgVw.sd_setImage(with: URL(string: msgArr[indexPath.row].document ?? ""), placeholderImage: #imageLiteral(resourceName: "imgPH"), options: SDWebImageOptions.continueInBackground, completed: nil)
                cell.timeLbl.text = msgArr[indexPath.row].message_time?.convertDateToString()
                cell.imageBtn.tag = indexPath.row
                cell.imageBtn.addTarget(self, action: #selector(imageShowBtnAction(_:)), for: .touchUpInside)
                return cell
            }
        }
    }
    @objc func imageShowBtnAction(_ sender : UIButton){
        var pathExt = ""
        if let document = msgArr[sender.tag].document{
            if document != ""{
                let url = URL(string: document)
                pathExt = url?.pathExtension ?? ""
            }
        }
        if pathExt != "mp4" && pathExt != "mp3" && pathExt != ""{
            let VC = ShowImageVC.instantiate(fromAppStoryboard: .SideMenu)
            VC.imageStr = msgArr[sender.tag].document ?? ""
            self.navigationController?.pushViewController(VC, animated: true)
        }
    }
    @objc func playBtnAction(_ sender : UIButton){
        if let videoURL = URL(string: self.msgArr[sender.tag].document ?? ""){
            let player = AVPlayer(url: videoURL)
            let playerViewController = AVPlayerViewController()
            playerViewController.player = player
            self.present(playerViewController, animated: true) {
                playerViewController.player!.play()
            }
        }
    }
    
    func createThumbnailOfVideoFromRemoteUrl(url: String) -> UIImage? {
        
        do {
            let asset = AVURLAsset(url:URL(string: url)! , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    func createThumbnailOfVideoFromUrl(url: URL) -> UIImage? {
        
        do {
            let asset = AVURLAsset(url:url , options: nil)
            let imgGenerator = AVAssetImageGenerator(asset: asset)
            imgGenerator.appliesPreferredTrackTransform = true
            let cgImage = try imgGenerator.copyCGImage(at: CMTimeMake(value: 0, timescale: 1), actualTime: nil)
            let thumbnail = UIImage(cgImage: cgImage)
            return thumbnail
        } catch let error {
            print("*** Error generating thumbnail: \(error.localizedDescription)")
            return nil
        }
    }
    //MARK: Private Functions
    
    // Create function for your button
    @objc func playPauseTapped(sender: UIButton) {
        let VC = AudioPlayerVC.instantiate(fromAppStoryboard: .SideMenu)
        VC.music = self.msgArr[sender.tag].document ?? ""
        VC.modalPresentationStyle = .overFullScreen
        self.present(VC, animated: false, completion: nil)
    }

    @objc func finishVideo()
    {
        isFinished = true
        self.msgArr[selectedIndex].time = 0
        self.msgArr[selectedIndex].playTime = "00:00"
        self.chatTableView.reloadData()
    }
    func stringFromTimeInterval(interval: TimeInterval) -> String {
    let interval = Int(interval)
    let seconds = interval % 60
    let minutes = (interval / 60) % 60
    let hours = (interval / 3600)
    return String(format: "%02d:%02d", minutes, seconds)
    }
    @objc func sliderChanged(sender: UISlider) {
        
        let seconds : Int64 = Int64(sender.value)
        let targetTime:CMTime = CMTimeMake(value: seconds, timescale: 1)
        
        player!.seek(to: targetTime)
        
        player!.addPeriodicTimeObserver(forInterval: CMTimeMakeWithSeconds(1, preferredTimescale: 1), queue: DispatchQueue.main) { (CMTime) -> Void in
            if self.player!.currentItem?.status == .readyToPlay {
                let time : Float64 = CMTimeGetSeconds(self.player!.currentTime());
                sender.value = Float ( time );
                
            }
        }
        if player?.rate == 0 {
            player?.play()
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableViewReload(){
        let loc = chatTableView.contentOffset
        UIView.performWithoutAnimation {
            chatTableView.reloadData()

            chatTableView.layoutIfNeeded()
            chatTableView.beginUpdates()
            chatTableView.endUpdates()

            chatTableView.layer.removeAllAnimations()
        }
        chatTableView.setContentOffset(loc, animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension SupportVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        SocketManger.shared.disconnect()
        stopPlayer()
        //  self.navigationController?.popViewController(animated: true)
        UserDefaults.standard.set("0", forKey: "support_count")
        if fromDelegate{
            appDelegate().redirectToHome()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    func stopPlayer() {
        if let play = player {
            print("stopped")
            play.pause()
            player = nil
            print("player deallocated")
        } else {
            print("player was already deallocated")
        }
    }
    func searchBtnTapped() {
        
    }
    
  
}
//MARK:- Textfield Delegate Method(s)

extension SupportVC : UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.contentSize.height <= 41 {
            msgTVHeight.constant = 38
        }else if textView.contentSize.height <= 100 && textView.contentSize.height > 41 {
            msgTVHeight.constant = textView.contentSize.height
        }else{
            msgTVHeight.constant = 100
        }
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if (text == "\n") {
        }
        return true
    }
}
//MARK:- PickerView Delegate Method(s)
extension SupportVC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]){
        let mediaType = info[UIImagePickerController.InfoKey.mediaType] as! CFString

        switch mediaType {
        case kUTTypeImage:
            if let chosenImage = info[UIImagePickerController.InfoKey.editedImage] as? UIImage{
                let data =  chosenImage.jpegData(compressionQuality: 0.3)!
                self.sendMediaData(data: data, type: "image", thumbnailData: data)
            }
            break
        case kUTTypeMovie:
            if let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL{
            print(videoURL)
                let thumbnail = self.createThumbnailOfVideoFromUrl(url: videoURL)
                let thumbnailData = thumbnail!.jpegData(compressionQuality: 0.3)!
                let data = NSData(contentsOf: videoURL as URL)! as Data
                self.sendMediaData(data: data, type: "video", thumbnailData: thumbnailData)
                print(data)
            }
            break
        case kUTTypeLivePhoto:
            
            break
        default:
            break
        }
        imagePicker .dismiss(animated: true, completion: nil)
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        print("Cancelled")
        imagePicker.dismiss(animated: true, completion: nil)
    }
}

