//
//  AudioPlayerVC.swift
//  Lmtlss
//
//  Created by apple on 29/12/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import MediaPlayer

class AudioPlayerVC: UIViewController {
    
    @IBOutlet weak var musicindicator: UIActivityIndicatorView!
    @IBOutlet weak var playPauseImage: UIImageView!
    @IBOutlet weak var pauseBtn: UIButton!
    @IBOutlet weak var backwardBtn: UIButton!
    @IBOutlet weak var forwardBtn: UIButton!
    @IBOutlet weak var timeProgress: UIProgressView!
    @IBOutlet weak var lblStopTIme: UILabel!
    @IBOutlet weak var lblStrtTime: UILabel!
    
    var music = String()
    var myAudioPlayer = AVAudioPlayer()
    var isPlaying = false
    var timer:Timer!
    override func viewDidLoad() {
        super.viewDidLoad()
        musicindicator.startAnimating()
        MediaPlayerManager.shared.delegate = self
        MediaPlayerManager.shared.stop()
        MediaPlayerManager.shared.play(withURL: music)
        self.playPauseImage.image = UIImage(named: "pauseB")
        
    }
    @objc func musicProgress()  {
        let normalizedTime = Float(self.myAudioPlayer.currentTime / (self.myAudioPlayer.duration ))
        self.timeProgress.progress = normalizedTime
        
    }

   
    @IBAction func backBtnAction(_ sender: UIButton) {
        MediaPlayerManager.shared.stop()
        self.dismiss(animated: false, completion: nil)
    }
    @IBAction func pausePlayBtnClicked(_ sender: UIButton) {
        if isPlaying == false {
            myAudioPlayer.pause()
            isPlaying = true
            playPauseImage.image = UIImage(named: "playB")
        } else {
            myAudioPlayer.play()
            isPlaying = false
            playPauseImage.image = UIImage(named: "pauseB")
        }
        let audioUrl = self.music
        if MediaPlayerManager.shared.isPause() && MediaPlayerManager.shared.isPlaying(on: audioUrl) {
            debugPrint("existing play")
            MediaPlayerManager.shared.resume()
            playPauseImage.image = UIImage(named: "pauseB")
            musicindicator.stopAnimating()
            musicindicator.isHidden = true
        }else if MediaPlayerManager.shared.isPlaying() && MediaPlayerManager.shared.isPlaying(on: audioUrl) {
            debugPrint("existing pause")
            MediaPlayerManager.shared.puase()
            musicindicator.stopAnimating()
            musicindicator.isHidden = true
            playPauseImage.image = UIImage(named: "playB")
        } else {
            debugPrint("new play")
            MediaPlayerManager.shared.play(withURL: audioUrl)
            playPauseImage.image = UIImage(named: "pauseB")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: PMNotificationName.startSleepTimer.rawValue), object: nil)
        }
    }
    @IBAction func forwardBtnClicked(_ sender: UIButton) {
        let player = MediaPlayerManager.shared.audioPlayer
        guard let duration = player.currentItem?.duration else { return }
        let currentTime = CMTimeGetSeconds(player.currentTime())
        let newTime = currentTime + 5.0
        if newTime < (CMTimeGetSeconds(duration)){
            let targetTime:CMTime = CMTimeMake(value: Int64(newTime), timescale: 1)
            MediaPlayerManager.shared.audioPlayer.seek(to: targetTime)
            if let durationCmtime = MediaPlayerManager.shared.audioPlayer.currentItem?.asset.duration{
                let duration = CMTimeGetSeconds(durationCmtime)
                let forwrardTime = CMTimeGetSeconds(targetTime)
                self.timeProgress.setProgress(Float(forwrardTime/duration), animated: false)
            }
            lblStrtTime.text = Mains().convertToHMS(number: Int(CMTimeGetSeconds(targetTime)))
        }
    }
    @IBAction func backwardBtnClicked(_ sender: UIButton) {
        let player = MediaPlayerManager.shared.audioPlayer
        let currentTime = CMTimeGetSeconds(player.currentTime())
        var newTime = currentTime - 5.0
        if newTime < 0{
            newTime = 0
        }
        let targetTime:CMTime = CMTimeMake(value: Int64(newTime), timescale: 1)
        MediaPlayerManager.shared.audioPlayer.seek(to: targetTime)
        if let durationCmtime = MediaPlayerManager.shared.audioPlayer.currentItem?.asset.duration{
            let duration = CMTimeGetSeconds(durationCmtime)
            let forwrardTime = CMTimeGetSeconds(targetTime)
            self.timeProgress.setProgress(Float(forwrardTime/duration), animated: false)
        }
        lblStrtTime.text = Mains().convertToHMS(number: Int(CMTimeGetSeconds(targetTime)))
    }
}
extension AudioPlayerVC: MediaPlayerManagerDelegate{
    func mediaPlayer(manager: MediaPlayerManager, didFailed error: ErrorModal) {
        print(error.message)
        musicindicator.stopAnimating()
        musicindicator.isHidden = true
    }
    
    func mediaPlayer(manager: MediaPlayerManager, currentPlayingAt second: TimeInterval) {
        if let durationCmtime = manager.audioPlayer.currentItem?.asset.duration{
            let duration = CMTimeGetSeconds(durationCmtime)
            self.timeProgress.setProgress(Float(second/duration), animated: false)
        }
        lblStrtTime.text = Mains().convertToHMS(number: Int(second))
    }
    
    func mediaPlayer(manager: MediaPlayerManager, didFinishPlayingSuccessfully flag: Bool) {
    }
    func completedPlaying(manager: MediaPlayerManager) {
        MediaPlayerManager.shared.stop()
        lblStrtTime.text = "00:00"
        self.timeProgress.setProgress(Float(0), animated: false)
        self.playPauseImage.image = UIImage(named: "playB")
    }
    
    func loadedAndReadyToPlay(manager: MediaPlayerManager) {
        musicindicator.stopAnimating()
        musicindicator.isHidden = true
        if let durationCmtime = manager.audioPlayer.currentItem?.asset.duration{
            let duration = CMTimeGetSeconds(durationCmtime)
//            cell.audioSlider.maximumValue = Float(duration)
            print(Mains().convertToHMS(number: Int(duration)))
            lblStopTIme.text = Mains().convertToHMS(number: Int(duration))
        }else {
          //  self.playPauseImage.image = UIImage(named: "playB")
        }
    }
    
    
}
