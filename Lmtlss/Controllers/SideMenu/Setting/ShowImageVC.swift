//
//  ShowImageVC.swift
//  Lmtlss
//
//  Created by apple on 15/09/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import SDWebImage

class ShowImageVC: UIViewController {
    var imageStr = ""
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var imageView: UIImageView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureUI()
    }
    
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = true
        myCustomView.logoImg.isHidden = false
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = nil
        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        imageView.sd_setImage(with: URL(string: imageStr), placeholderImage: UIImage(named: ""), options: SDWebImageOptions.continueInBackground, completed: nil)
        scrollView.delegate = self

    }
    
}
//MARK:- NavBar Delegate Method(s)

extension ShowImageVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        
    }
    
  
}
//MARK:- ScrollView Delegate Method(s)
extension ShowImageVC: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return imageView
    }
}
