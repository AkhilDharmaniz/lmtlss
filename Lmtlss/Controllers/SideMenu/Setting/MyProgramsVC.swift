//
//  MyProgramsVC.swift
//  Lmtlss
//
//  Created by apple on 20/10/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire
import QuickLook

class MyProgramsVC: UIViewController {
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var myProgramsTableView: UITableView!
    var programsArr = [MyProgramsField]()
    lazy var previewItem = NSURL()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.myProgramsTableView.delegate = self
        self.myProgramsTableView.dataSource = self
        self.getMyProgramsList()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "My Programs"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func getMyProgramsList(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.myProgrms
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [[String:Any]]{
                            for i in 0..<responseData.count{
                                var mealsArr = [MyProgramsCategoryField]()
                                if let mealItems = responseData[i]["meal"] as? [[String:Any]]{
                                    for l in 0..<mealItems.count{
                                        mealsArr.append(MyProgramsCategoryField(name: mealItems[l]["name"] as? String ?? "", url: mealItems[l]["url"] as? String ?? ""))
                                    }
                                }
                                self.programsArr.append(MyProgramsField(created_at: responseData[i]["created_at"] as? String ?? "", date: responseData[i]["date"] as? String ?? "", id: responseData[i]["id"] as? String ?? "", meal: mealsArr, order_id: responseData[i]["order_id"] as? String ?? "", order_status: responseData[i]["order_status"] as? String ?? "", plan_id: responseData[i]["plan_id"] as? String ?? "", plan_price: responseData[i]["plan_price"] as? String ?? "", title: responseData[i]["title"] as? String ?? "", updated_at: responseData[i]["updated_at"] as? String ?? "", user_id: responseData[i]["user_id"] as? String ?? "", selectedState: false))
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.myProgramsTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
}
//MARK:- NavBar Delegate Method(s)

extension MyProgramsVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
//MARK::- TableView Delegate,Datasource Method(s)
extension MyProgramsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return programsArr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell: MyProgramsCell! = tableView.dequeueReusableCell(withIdentifier: "MyProgramsCell") as? MyProgramsCell
        if cell == nil {
            tableView.register(UINib(nibName: "MyProgramsCell", bundle: nil), forCellReuseIdentifier: "MyProgramsCell")
            cell = tableView.dequeueReusableCell(withIdentifier: "MyProgramsCell") as? MyProgramsCell
        }
        cell.configureCategoryCell(data: programsArr[indexPath.row])
        cell.selectionStyle = .none
        cell.arrowBtn.tag = indexPath.row
        cell.arrowBtn.addTarget(self, action: #selector(addSubCategory(sender:)), for: .touchUpInside)
        if programsArr[indexPath.row].selectedState{
            cell.didSelectIndex = { name, url in
                if url == ""{
                    alert(Constants.AppName, message: "You have not obtained any meal plan in check-in.", view: self)
                }else{
                    let browserVC = BrowserVC.instantiate(fromAppStoryboard: .SideMenu)
                    browserVC.navTitle = name
                    browserVC.myBlog = url
                    self.navigationController?.pushViewController(browserVC, animated: true)
                }
            }
        }
        cell.downloadPDF = { name, url in
            if self.programsArr[indexPath.row].selectedState{
                if url == ""{
                    alert(Constants.AppName, message: "You have not obtained any meal plan in check-in.", view: self)
                }else{
                    IJProgressView.shared.showProgressView()
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1.0){
                        self.downloadPDF(name, url, self.programsArr[indexPath.row].title ?? "")
                    }
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    func downloadfile(originalString:String, name:String, completion: @escaping (_ success: Bool,_ fileLocation: URL?) -> Void){
        let fileManager = FileManager.default
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        print("Directory: \(paths)")
        do
        {
            let fileName = try fileManager.contentsOfDirectory(atPath: paths)
            
            for file in fileName {
                // For each file in the directory, create full path and delete the file
                let filePath = URL(fileURLWithPath: paths).appendingPathComponent(file).absoluteURL
                try fileManager.removeItem(at: filePath)
            }
        }catch let error {
            print(error.localizedDescription)
        }
        var escpaedStr = ""
        if originalString.contains("%20") {
            escpaedStr = originalString
        }else{
            escpaedStr = originalString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        }
        
        let itemUrl = URL(string: escpaedStr)
        // then lets create your document folder url
        let documentsDirectoryURL =  FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        
        // lets create your destination file url
        let destinationUrl = documentsDirectoryURL.appendingPathComponent("\(name).pdf")
        
        // to check if it exists before downloading it
        if FileManager.default.fileExists(atPath: destinationUrl.path) {
            
            debugPrint("The file already exists at path")
            completion(true, destinationUrl)
            
            // if the file doesn't exist
        } else {
            
            // you can use NSURLSession.sharedSession to download the data asynchronously
            URLSession.shared.downloadTask(with: itemUrl!, completionHandler: { (location, response, error) -> Void in
                AFWrapperClass.svprogressHudDismiss(view: self)
                
                guard let tempLocation = location, error == nil else { return }
                do {
                    // after downloading your file you need to move it to your destination url
                    try FileManager.default.moveItem(at: tempLocation, to: destinationUrl)
                    completion(true, destinationUrl)
                } catch let error as NSError {
                    print(error.localizedDescription)
                    completion(false, nil)
                }
            }).resume()
        }
    }
    func downloadPDF(_ name:String , _ url:String, _ plan: String){
        
            let url = URL(string: url)
            let pdfData = try? Data.init(contentsOf: url!)
            let resourceDocPath = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
            let pdfNameFromUrl = "\(plan)-\(name).pdf"
            let actualPath = resourceDocPath.appendingPathComponent(pdfNameFromUrl)
            do {
                try pdfData?.write(to: actualPath, options: .atomic)
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: "Downloaded Successfully!", view: self)
            } catch {
                IJProgressView.shared.hideProgressView()
            }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 0.0
        let data = programsArr[indexPath.row]
        if data.selectedState{
            height = Double(data.meal.count) * 45.0+15
        }
        return CGFloat(87 + height)
    }
    @objc func addSubCategory(sender: UIButton){
        if self.programsArr[sender.tag].meal.count != 0{
            self.programsArr[sender.tag].selectedState = !self.programsArr[sender.tag].selectedState
        }
        myProgramsTableView.beginUpdates()
        myProgramsTableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.none)
        myProgramsTableView.endUpdates()
    }
}
extension MyProgramsVC: QLPreviewControllerDataSource{
    func numberOfPreviewItems(in controller: QLPreviewController) -> Int {
        return 1
    }
    func previewController(_ controller: QLPreviewController, previewItemAt index: Int) -> QLPreviewItem {
        return self.previewItem as QLPreviewItem
    }
}
