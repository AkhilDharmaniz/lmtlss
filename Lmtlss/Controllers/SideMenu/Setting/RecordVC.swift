//
//  RecordVC.swift
//  Lmtlss
//
//  Created by apple on 16/09/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import AVFoundation

class RecordVC: UIViewController {

    @IBOutlet weak var recordBtn: UIButton!
    @IBOutlet weak var recordTimeLbl: UILabel!
    @IBOutlet weak var navView: UIView!
    var audioRecorder:AVAudioRecorder!
    var audioPlayer:AVAudioPlayer!
    var meterTimer:Timer!
    var isAudioRecordingGranted =  false
    var isRecording = false
    var dismissedCurrentView:((Data)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = true
        myCustomView.logoImg.isHidden = false
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = nil
        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        recordBtn.setImage(UIImage(named:"record"), for: .normal)
        recordTimeLbl.text = "00:00"
        checkRecordPermission()
    }
    
    //MARK:- Recording Method(s)
    func checkRecordPermission(){
        switch AVAudioSession.sharedInstance().recordPermission {
        case AVAudioSession.RecordPermission.granted:
            isAudioRecordingGranted = true
            break
        case AVAudioSession.RecordPermission.denied:
            isAudioRecordingGranted = false
            break
        case AVAudioSession.RecordPermission.undetermined:
            AVAudioSession.sharedInstance().requestRecordPermission() { [unowned self] allowed in
                DispatchQueue.main.async {
                    if allowed {
                        self.isAudioRecordingGranted = true
                    } else {
                        self.isAudioRecordingGranted = false
                    }
                }
            }
            break
        default:
            break
        }
    }
    func getDocumentsDirectory() -> URL{
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let documentsDirectory = paths[0]
        return documentsDirectory
    }
    func getFileUrl() -> URL{
        let filename = "myRecording.m4a"
        let filePath = getDocumentsDirectory().appendingPathComponent(filename)
        return filePath
    }
    func setupRecorder(successHandler: @escaping (() -> Void)){
        if isAudioRecordingGranted{
            let session = AVAudioSession.sharedInstance()
            do{
                try session.setCategory(AVAudioSession.Category.playAndRecord, options: .defaultToSpeaker)
                try session.setActive(true)
                let settings = [
                    AVFormatIDKey: Int(kAudioFormatMPEG4AAC),
                    AVSampleRateKey: 44100,
                    AVNumberOfChannelsKey: 2,
                    AVEncoderAudioQualityKey:AVAudioQuality.high.rawValue
                ]
                audioRecorder = try AVAudioRecorder(url: getFileUrl(), settings: settings)
                audioRecorder.delegate = self
                audioRecorder.isMeteringEnabled = true
                audioRecorder.prepareToRecord()
            }
            catch let error {
                
                showAlert(title:  "Error", message: error.localizedDescription)
            }
            successHandler()
        }
        else{
            showAlert(title:  "Error", message: "Don't have access to use your microphone.")
        }
    }
    @objc func updateAudioMeter(timer: Timer)
    {
        if audioRecorder.isRecording
        {
//            let hr = Int((audioRecorder.currentTime / 60) / 60)
            let min = Int(audioRecorder.currentTime / 60)
            let sec = Int(audioRecorder.currentTime.truncatingRemainder(dividingBy: 60))
            let secStr = String(format: "%02d", sec)
            let minStr = String(format: "%02d", min)
            recordTimeLbl.text = "\(minStr):\(secStr)"
            audioRecorder.updateMeters()
            if min == 2{
                recordBtn.imageView!.stopAnimating()
                recordBtn.setImage(UIImage(named:"stop"), for: .normal)
                isRecording = false
                finishAudioRecording(success: true)
            }
        }
    }
    func finishAudioRecording(success: Bool){
        if success{
            audioRecorder.stop()
            audioRecorder = nil
            meterTimer.invalidate()
            guard let data = try? Data(contentsOf:getFileUrl()) else { return }
            self.dismissedCurrentView?(data)
        }else{
            showAlert(title:  "Error", message: "Recording failed.")
        }
    }
    @IBAction func recordBtnAction(_ sender: UIButton) {
        if isRecording{
            recordBtn.imageView!.stopAnimating()
            recordBtn.setImage(UIImage(named:"stop"), for: .normal)
            isRecording = false
            finishAudioRecording(success: true)
            
        }else{
            setupRecorder{
                self.audioRecorder.record()
                self.meterTimer = Timer.scheduledTimer(timeInterval: 0.1, target:self, selector:#selector(self.updateAudioMeter(timer:)), userInfo:nil, repeats:true)
                //  recordBtn.setImage(UIImage(named:"stop"), for: .normal)
                let image1:UIImage = UIImage(named: "stop")!
                let image2:UIImage = UIImage(named: "stop2")!
                self.recordBtn.setImage(image1, for: .normal)
                self.recordBtn.imageView!.animationImages = [image1, image2]
                self.recordBtn.imageView!.animationDuration = 1.0
                self.recordBtn.imageView!.startAnimating()
                self.recordBtn.backgroundColor = UIColor(named: "BGColor")
                self.isRecording = true
            }
        }
    }
}
//MARK:- NavBar Delegate Method(s)

extension RecordVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        if isRecording{
            meterTimer.invalidate()
        }
        audioRecorder = nil
        self.navigationController?.popViewController(animated: true)
    }
    func searchBtnTapped() {
        
    }
}

//MARK:- Audio and Record Delegate Method(s)
extension RecordVC: AVAudioRecorderDelegate, AVAudioPlayerDelegate {
    func audioRecorderDidFinishRecording(_ recorder: AVAudioRecorder, successfully flag: Bool){
        if !flag{
            finishAudioRecording(success: false)
        }
    }
}
