//
//  BrowserVC.swift
//  Lmtlss
//
//  Created by apple on 30/09/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import WebKit

class BrowserVC: UIViewController {
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var webView: WKWebView!
    var navTitle = ""
    var myBlog = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.async {
            IJProgressView.shared.showProgressView()
        }
        if myBlog == ""{
            myBlog = navTitle == "About Us" ? Constants.aboutUS : Constants.terms
        }
        if let url = URL(string: myBlog){
            let request = URLRequest(url: url)
            webView.navigationDelegate = self
            webView.load(request as URLRequest)
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = navTitle
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
}

//MARK:- NavBar Delegate Method(s)

extension BrowserVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
//MARK:- WebView Delegate Method(s)
extension BrowserVC : WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        IJProgressView.shared.hideProgressView()
    }
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        IJProgressView.shared.hideProgressView()
        alert(Constants.AppName, message: error.localizedDescription, view: self)
    }
    func webView(_ webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: Error){
        IJProgressView.shared.hideProgressView()
        alert(Constants.AppName, message: error.localizedDescription, view: self)
    }
}
