//
//  NotificationVC.swift
//  Lmtlss
//
//  Created by Vivek Dharmani on 16/11/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire
class NotificationVC: UIViewController {

    @IBOutlet weak var navVw: UIView!
    @IBOutlet weak var notificationTableVw: UITableView!
    var notificationsListArray = [NotificationsListModel]()
    var arrCount = 0
    var pageNo = 1
    var dismissedCurrentView:((Bool)->Void)?
  
    override func viewDidLoad() {
        super.viewDidLoad()
        self.notificationTableVw.delegate = self
        self.notificationTableVw.dataSource = self
        getNotificationsList()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navVw.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Notifications"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navVw.addSubview(myCustomView)
       
    }
    func getNotificationsList(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.notificationList + "?page=\(pageNo)&per-page=\(100)"
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
            //    self.notificationsListArray.removeAll()
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        UserDefaults.standard.set("0", forKey: "notification_count")
                        if let responseData = response["responseData"] as? [[String:Any]]{
                            self.arrCount = responseData.count
                            for i in 0..<responseData.count{
                                self.notificationsListArray.append(NotificationsListModel(id: responseData[i]["id"] as? Int ?? 0, user_id: responseData[i]["user_id"] as? Int ?? 0, title: responseData[i]["title"] as? String ?? "", message: responseData[i]["message"] as? String ?? "", status: responseData[i]["status"] as? Int ?? 0, seen: responseData[i]["seen"] as? Int ?? 0, push_type: responseData[i]["push_type"] as? Int ?? 0, creation_at: responseData[i]["creation_at"] as? String ?? ""))
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.notificationTableVw.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
}
extension NotificationVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notificationsListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableCell", for: indexPath) as! NotificationTableCell
        
        cell.lblTitle.text = notificationsListArray[indexPath.row].title
        cell.lblDesp.text = notificationsListArray[indexPath.row].message
        switch notificationsListArray[indexPath.row].push_type {
        case 1:
            cell.iconImg.image = #imageLiteral(resourceName: "ReSend")
        case 2:
            cell.iconImg.image = #imageLiteral(resourceName: "Check")
        case 3:
            cell.iconImg.image = #imageLiteral(resourceName: "MealPlan")
        case 4:
            cell.iconImg.image = #imageLiteral(resourceName: "Plan")
        case 5:
            cell.iconImg.image = #imageLiteral(resourceName: "Support")
        case 6:
            cell.iconImg.image = #imageLiteral(resourceName: "Check")
        case 7:
            cell.iconImg.image = #imageLiteral(resourceName: "Questionnaire")
        case 8:
            cell.iconImg.image = #imageLiteral(resourceName: "NewForum")
        default:
            cell.iconImg.image = #imageLiteral(resourceName: "NewForum")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch notificationsListArray[indexPath.row].push_type {
        case 1:
            if notificationsListArray[indexPath.row].status == 1{
                let VC = QuestionnaireContainerVC.instantiate(fromAppStoryboard: .Questionnaire)
                self.navigationController?.pushViewController(VC, animated: true)
            }
        case 2:
            if notificationsListArray[indexPath.row].status == 1{
                let VC = CheckInContainerVC.instantiate(fromAppStoryboard: .CheckInQuestionnaire)
                self.navigationController?.pushViewController(VC, animated: true)
            }
        case 3,6,7:
            appDelegate().redirectToHome()
        case 4:
            appDelegate().redirectToExercisePlan()
        case 5:
            let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        case 8:
            let VC = ForumListingVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        default:
            return
        }
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = self.notificationsListArray.count - 1
        if indexPath.row == lastItem {
            if arrCount == 100 {
                pageNo += 1
                getNotificationsList()
            }
        }
    }
}
//MARK:- NavBar Delegate Method(s)

extension NotificationVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.dismissedCurrentView?(true)
        
    }
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
