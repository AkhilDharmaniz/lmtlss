//
//  VideoLibraryVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 04/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import AVKit

class VideoLibraryCell: UITableViewCell{
    
    @IBOutlet weak var thumbnailImg: UIImageView!
    @IBOutlet weak var videoNameLbl: UILabel!
    @IBOutlet weak var descriptionLbl: UILabel!
    @IBOutlet weak var viewVideoBtn: UIButton!
    @IBOutlet weak var shareBtn: UIButton!
    @IBOutlet weak var favUnfavBtn: UIButton!
    @IBOutlet weak var favUnfavImg: UIImageView!
    
}
class VideoLibraryVC: UIViewController {

    @IBOutlet weak var videoLibraryTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var noVideoView: UIView!
    @IBOutlet weak var noVideoLbl: UILabel!
    
    
    var category = ""
    var navigationTitle = ""
    var videoListArray = [VideoListField]()
    var videoListArr = [VideoListField]()
    var fromFavourite = false
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = fromFavourite == false ? "\(navigationTitle) Videos" : "Favorite Videos"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "search")
        myCustomView.searchBtn.isUserInteractionEnabled = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.isSupport = true
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        self.searchView.isHidden = true
        self.searchViewHeight.constant = 0
        self.videoLibraryTableView.delegate = self
        self.videoLibraryTableView.dataSource = self
        if fromFavourite == false{
            getVideoList()
        }else{
           getFacvouriteVideoList()
        }
    }

    
    func getVideoList(){
           if Reachability.isConnectedToNetwork() == true {
               
               DispatchQueue.main.async {
                   IJProgressView.shared.showProgressView()
               }
               let url = Constants.baseURL + Constants.videoList + category
               var token = UserDefaults.standard.value(forKey: "token") as? String
               token = "Bearer " + token!
               let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
               AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                   IJProgressView.shared.hideProgressView()
                   
                   self.videoListArray.removeAll()
                   let message = response["responseMessage"] as? String ?? ""
                   if let responseCode = response["responseCode"] as? Int{
                       if responseCode == 200{
                           if let responseData = response["responseData"] as? [[String:Any]]{
                            if responseData.count == 0{
                                self.noVideoView.isHidden = false
                                self.noVideoLbl.text = "No Videos Found"
                            }
                               for i in 0..<responseData.count{
                                self.videoListArray.append(VideoListField(category: responseData[i]["category"] as? String ?? "", created_at: responseData[i]["created_at"] as? String ?? "", fav_status: responseData[i]["fav_status"] as? String ?? "", id: responseData[i]["id"] as? String ?? "", sub_category: responseData[i]["sub_category"] as? String ?? "", title: responseData[i]["title"] as? String ?? "", updated_at: responseData[i]["updated_at"] as? String ?? "", video_thumbnail: responseData[i]["video_thumbnail"] as? String ?? "", video_url: responseData[i]["video_url"] as? String ?? ""))
                               }
                            self.videoListArr = self.videoListArray
                           }
                       }else{
                           IJProgressView.shared.hideProgressView()
                           alert(Constants.AppName, message: message, view: self)
                       }
                       self.videoLibraryTableView.reloadData()
                   }
                   if let status = response["status"] as? Int, status == 401{
                       logOut(controller: self)
                   }
               }) { (error) in
                   IJProgressView.shared.hideProgressView()
                   alert(Constants.AppName, message: error.localizedDescription, view: self)
                   
               }
           } else {
               
               alert(Constants.AppName, message: "Check internet connection", view: self)
           }
       }
    func getFacvouriteVideoList(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.favVideoList
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.videoListArray.removeAll()
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [[String:Any]]{
                            if responseData.count == 0{
                                self.noVideoView.isHidden = false
                                self.noVideoLbl.text = "No Favorite Videos Found"
                            }
                            for i in 0..<responseData.count{
                             self.videoListArray.append(VideoListField(category: responseData[i]["category"] as? String ?? "", created_at: responseData[i]["created_at"] as? String ?? "", fav_status: responseData[i]["fav_status"] as? String ?? "", id: responseData[i]["id"] as? String ?? "", sub_category: responseData[i]["sub_category"] as? String ?? "", title: responseData[i]["title"] as? String ?? "", updated_at: responseData[i]["updated_at"] as? String ?? "", video_thumbnail: responseData[i]["video_thumbnail"] as? String ?? "", video_url: responseData[i]["video_url"] as? String ?? ""))
                            }
                         self.videoListArr = self.videoListArray
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.videoLibraryTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    @IBAction func closeBtnAction(_ sender: UIButton) {
       self.searchView.isHidden = true
        self.searchViewHeight.constant = 0
        self.view.endEditing(true)
        self.videoListArray = videoListArr
        self.searchTF.text = ""
        self.videoLibraryTableView.reloadData()
        if videoListArray.count == 0{
            self.noVideoView.isHidden = false
            
            self.noVideoLbl.text = fromFavourite == false ? "No result found" : "No Favorite Videos Found"
        }else{
            self.noVideoView.isHidden = true
        }
    }
    
    @IBAction func searchTFAction(_ sender: UITextField) {
        if sender.text == ""{
            videoListArray = videoListArr
        }else{
            videoListArray = self.videoListArr.filter({ (text) -> Bool in
                let tmp: NSString = text.title! as NSString
                let range = tmp.range(of: sender.text!, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
        }
        self.videoLibraryTableView.reloadData()

        if videoListArray.count == 0{
            self.noVideoView.isHidden = false
            self.noVideoLbl.text = "No result found"
        }else{
            self.noVideoView.isHidden = true
        }
    }
    func favouriteUnfavouriteApi(from: String, index : Int){
          
          IJProgressView.shared.showProgressView()
          let signInUrl = Constants.baseURL + Constants.favUnfav
           
          var token = UserDefaults.standard.value(forKey: "token") as? String
          token = "Bearer " + token!
          let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(from : from, index: index), headers: headers, success: { (response) in
              IJProgressView.shared.hideProgressView()
              
              let message = response["message"] as? String ?? ""
              if let status = response["responseCode"] as? Int {
                  if status == 200{
                    if self.fromFavourite == true{
                        for i in 0..<self.videoListArr.count{
                            if self.videoListArray[index].id == self.videoListArr[i].id{
                                self.videoListArr.remove(at: i)
                                self.videoListArray.remove(at: index)
                                self.videoLibraryTableView.reloadData()
                                if self.videoListArr.count == 0{
                                  self.noVideoView.isHidden = false
                                    self.noVideoLbl.text = "No Favorite Videos Found"
                                }
                                return
                            }
                        }
                        
                    }else{
                        for i in 0..<self.videoListArr.count{
                            if self.videoListArray[index].id == self.videoListArr[i].id{
                                self.videoListArr[i].fav_status = from
                            }
                        }
                       self.videoListArray[index].fav_status = from
                        self.videoLibraryTableView.reloadData()
                    }
                  }else{
                      IJProgressView.shared.hideProgressView()
                      alert(Constants.AppName, message: message, view: self)
                  }
              }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
              IJProgressView.shared.hideProgressView()
              alert(Constants.AppName, message: error.localizedDescription, view: self)
              
          }
      }
    func generatingParameters(from: String, index : Int) -> [String:AnyObject] {
          var parameters:[String:AnyObject] = [:]
          parameters["video_id"] = videoListArray[index].id as AnyObject
          parameters["status"] = from as AnyObject
          
          return parameters
      }
}
extension VideoLibraryVC : UITableViewDelegate,UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return videoListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! VideoLibraryCell
        cell.videoNameLbl.text = videoListArray[indexPath.row].title
        cell.descriptionLbl.text = videoListArray[indexPath.row].sub_category
        cell.thumbnailImg.sd_setImage(with: URL(string: videoListArray[indexPath.row].video_thumbnail ?? ""), placeholderImage: UIImage(named: "ic_video_list"))
        cell.shareBtn.tag = indexPath.row
        cell.shareBtn.addTarget(self, action: #selector(shareBtnAction(_:)), for: .touchUpInside)
        cell.favUnfavBtn.tag = indexPath.row
        cell.favUnfavBtn.addTarget(self, action: #selector(favUnfavBtnAction(_:)), for: .touchUpInside)
        cell.favUnfavImg.image = videoListArray[indexPath.row].fav_status == "0" ? #imageLiteral(resourceName: "unfav") : #imageLiteral(resourceName: "fav")
        cell.viewVideoBtn.tag = indexPath.row
        cell.viewVideoBtn.addTarget(self, action: #selector(viewVideoBtnAction(_:)), for: .touchUpInside)
        return cell
    }
    @objc func favUnfavBtnAction(_ sender : UIButton){
        
        favouriteUnfavouriteApi(from: videoListArray[sender.tag].fav_status ?? "" == "0" ? "1" : "0", index: sender.tag)
    }
    @objc func shareBtnAction(_ sender : UIButton){
        
        let items = [URL(string: videoListArray[sender.tag].video_url ?? "")!]
     //   let items = [URL(string: "https://www.facebook.com")!]
        let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
        present(ac, animated: true)
    }
    @objc func viewVideoBtnAction(_ sender : UIButton){
        
        guard let videoURL = URL(string: videoListArray[sender.tag].video_url ?? "") else{return}
    //    let videoURL = URL(string: "http://62.171.142.108/RSF_Video/web/video/1591099111_BACKFLYE.mp4")
        let player = AVPlayer(url: videoURL)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let videoURL = URL(string: videoListArray[indexPath.row].video_url ?? "")
//        let player = AVPlayer(url: videoURL!)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        self.present(playerViewController, animated: true) {
//            playerViewController.player!.play()
//        }
////        let VC = VideoPlayerVC.instantiate(fromAppStoryboard: .SideMenu)
////        VC.videoUrl = videoListArray[indexPath.row].video_url ?? ""
////        VC.modalPresentationStyle = .fullScreen
////        self.navigationController?.pushViewController(VC, animated: true)
//    }
}
//MARK:- NavBar Delegate Method(s)

extension VideoLibraryVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        self.searchViewHeight.constant = 50
        self.searchView.isHidden = false
    }
}
    
  
