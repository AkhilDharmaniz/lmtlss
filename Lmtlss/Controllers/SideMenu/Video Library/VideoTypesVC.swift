//
//  VideoTypesVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 10/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class VideoTypesCell: UITableViewCell {
  @IBOutlet weak var bgView: UIView!
  @IBOutlet weak var imgView: UIImageView!
  @IBOutlet weak var titleLbl: UILabel!
}
class VideoTypesVC: UIViewController {
  //  var videoTypesArr = [("NUTRITION",#imageLiteral(resourceName: "nutrition"),#imageLiteral(resourceName: "nutri-text"),false),("TRAINING",#imageLiteral(resourceName: "training"),#imageLiteral(resourceName: "training-text"),false),("MOTIVATION",#imageLiteral(resourceName: "motivation"),#imageLiteral(resourceName: "motivation-text"),false)]
    @IBOutlet weak var videoTypesTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    var videoTypesArr = [VideoTypesField]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.videoTypesTableView.delegate = self
        self.videoTypesTableView.dataSource = self
        self.getVideoTypesList()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Video Library"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
   func getVideoTypesList(){
       if Reachability.isConnectedToNetwork() == true {
           
           DispatchQueue.main.async {
               IJProgressView.shared.showProgressView()
           }
           let url = Constants.baseURL + Constants.videoTypes
           var token = UserDefaults.standard.value(forKey: "token") as? String
           token = "Bearer " + token!
           let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
           AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
               IJProgressView.shared.hideProgressView()
               
               self.videoTypesArr.removeAll()
               let message = response["responseMessage"] as? String ?? ""
               if let responseCode = response["responseCode"] as? Int{
                   if responseCode == 200{
                       if let responseData = response["responseData"] as? [[String:Any]]{
                        self.videoTypesArr.append(VideoTypesField(color: "#f5c9c0", created_at: "", id: 0, image: "", name: "Favorites", updated_at: "", selected: false))
                           for i in 0..<responseData.count{
                            self.videoTypesArr.append(VideoTypesField(color: responseData[i]["color"] as? String ?? "", created_at: responseData[i]["created_at"] as? String ?? "", id: responseData[i]["id"] as? Int ?? 0, image: responseData[i]["image"] as? String ?? "", name: responseData[i]["name"] as? String ?? "", updated_at: responseData[i]["updated_at"] as? String ?? "", selected: false))
                           }
                        
                       }
                   }else{
                       IJProgressView.shared.hideProgressView()
                       alert(Constants.AppName, message: message, view: self)
                   }
                   self.videoTypesTableView.reloadData()
               }
               if let status = response["status"] as? Int, status == 401{
                   logOut(controller: self)
               }
           }) { (error) in
               IJProgressView.shared.hideProgressView()
               alert(Constants.AppName, message: error.localizedDescription, view: self)
               
           }
       } else {
           
           alert(Constants.AppName, message: "Check internet connection", view: self)
       }
   }
}
extension VideoTypesVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return videoTypesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! VideoTypesCell
        cell.titleLbl.text = videoTypesArr[indexPath.row].name?.uppercased()
        cell.titleLbl.textColor = UIColor(hexaRGB: videoTypesArr[indexPath.row].color ?? "")
        cell.bgView.backgroundColor = videoTypesArr[indexPath.row].selected == true ? #colorLiteral(red: 0.8470588235, green: 0.9450980392, blue: 1, alpha: 1) : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        if videoTypesArr[indexPath.row].name == "Favorites"{
            cell.imgView.image = #imageLiteral(resourceName: "favourite")
        }else{
            cell.imgView.sd_setImage(with: URL(string: videoTypesArr[indexPath.row].image ?? ""), placeholderImage: UIImage(named: "ic_video_cat"))
            DispatchQueue.main.async {
                cell.imgView.layer.cornerRadius = (cell.contentView.bounds.size.height * 0.58) / 2
                cell.imgView.layer.masksToBounds = true
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.size.height * 0.17
        //  return 130
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        for i in 0..<videoTypesArr.count {
            videoTypesArr[i].selected = false
        }
        videoTypesArr[indexPath.row].selected = true
        videoTypesTableView.reloadData()
        let VC = VideoLibraryVC.instantiate(fromAppStoryboard: .SideMenu)
        if videoTypesArr[indexPath.row].name != "Favorites"{
            VC.category = "\(videoTypesArr[indexPath.row].id ?? 0)"
            VC.fromFavourite = false
        }else{
            VC.fromFavourite = true
        }
        VC.navigationTitle = videoTypesArr[indexPath.row].name ?? ""
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension VideoTypesVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
//      let VC = VideoLibraryVC.instantiate(fromAppStoryboard: .SideMenu)
//        VC.category = "\(videoTypesArr[indexPath.row].id ?? 0)"
//        VC.navigationTitle = videoTypesArr[indexPath.row].name ?? ""
//        self.navigationController?.pushViewController(VC, animated: true)
    }
}
