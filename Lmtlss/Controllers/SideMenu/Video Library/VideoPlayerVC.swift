//
//  VideoPlayerVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 04/12/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import VersaPlayer

class VideoPlayerVC: UIViewController {

    @IBOutlet var controls: VersaPlayerControls!
    @IBOutlet weak var playerView: VersaPlayerView!
    @IBOutlet var navView: UIView!
    var videoUrl = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        playerView.layer.backgroundColor = UIColor.clear.cgColor
        playerView.use(controls: controls)
        
        if let url = URL.init(string: self.videoUrl) {
            let item = VersaPlayerItem(url: url)
            playerView.set(item: item)
        }
        controls.isHidden = false
    }
    


    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

