//
//  RecipeListVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage
class RecipeListCell: UITableViewCell {
    
    @IBOutlet weak var recipeImg: UIImageView!
    @IBOutlet weak var recipeName: UILabel!
    @IBOutlet weak var recipeCalories: UILabel!
    @IBOutlet weak var seeDetailsBtn: UIButton!
}
class RecipeListVC: UIViewController {

    @IBOutlet weak var recipeTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var categoryCollectionView: UICollectionView!
    var category = 0
    var recipesList = [RecipeListFiled]() {
        didSet {
            self.categoryCollectionView.reloadData()
            self.recipeTableView.reloadData()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        recipeTableView.delegate = self
        recipeTableView.dataSource = self
        categoryCollectionView.delegate = self
        categoryCollectionView.dataSource = self
        self.getRecipeList()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Recipes"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func getRecipeList(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.recipeListing
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                self.recipesList.removeAll()
                debugPrint(response)
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [[String:Any]]{
                            for obj in responseData{
                                var recipes = [RecipeField]()
                                if let allRecipes = obj["all_recipe"] as? [[String:Any]]{
                                    for i in 0..<allRecipes.count{
                                        recipes.append(RecipeField(calories: allRecipes[i]["calories"] as? String ?? "", carb: allRecipes[i]["carb"] as? String ?? "", fat: allRecipes[i]["fat"] as? String ?? "", created_at: allRecipes[i]["created_at"] as? String ?? "", ingredients: allRecipes[i]["ingredients"] as? [[String:Any]] ?? [], protein: allRecipes[i]["protein"] as? String ?? "", recipe_id: allRecipes[i]["recipe_id"] as? String ?? "", recipe_image: allRecipes[i]["recipe_image"] as? String ?? "", recipe_name: allRecipes[i]["recipe_name"] as? String ?? "", serves: allRecipes[i]["serves"] as? Int ?? 0, tip: allRecipes[i]["tip"] as? String ?? "", topping: allRecipes[i]["topping"] as? String ?? "", updated_at: allRecipes[i]["updated_at"] as? String ?? "", method: allRecipes[i]["method"] as? [String] ?? [], per_serve_calories: allRecipes[i]["per_serve_calories"] as? String ?? "", per_serve_carb: allRecipes[i]["per_serve_carb"] as? String ?? "", per_serve_fat: allRecipes[i]["per_serve_fat"] as? String ?? "", per_serve_protein: allRecipes[i]["per_serve_protein"] as? String ?? "", recipe_serve: "", total_serve_calories: "", total_serve_carb: "", total_serve_fat: "", total_serve_protein: "", recipe_weight: "", weight: allRecipes[i]["weight"] as? String ?? "", recipe_thumbnail_image: allRecipes[i]["recipe_thumbnail_image"] as? String ?? ""))
                                    }
                                }
                                self.recipesList.append(RecipeListFiled(catgory_name: obj["catgory_name"] as? String ?? "", all_recipe: recipes, selected: false))
                            }
                            if self.recipesList.count > 0 {
                                self.recipesList[0].selected = true
                            }
//                            for i in 0..<responseData.count{
//                                self.recipesList.append(RecipeField(calories: responseData[i]["calories"] as? String ?? "", carb: responseData[i]["carb"] as? String ?? "", fat: responseData[i]["fat"] as? String ?? "", created_at: responseData[i]["created_at"] as? String ?? "", ingredients: responseData[i]["ingredients"] as? [String] ?? [], protein: responseData[i]["protein"] as? String ?? "", recipe_id: responseData[i]["recipe_id"] as? String ?? "", recipe_image: responseData[i]["recipe_image"] as? String ?? "", recipe_name: responseData[i]["recipe_name"] as? String ?? "", serves: responseData[i]["serves"] as? Int ?? 0, tip: responseData[i]["tip"] as? String ?? "", topping: responseData[i]["topping"] as? String ?? "", updated_at: responseData[i]["updated_at"] as? String ?? "", method: responseData[i]["method"] as? [String] ?? [], per_serve_calories: responseData[i]["per_serve_calories"] as? String ?? "", per_serve_carb: responseData[i]["per_serve_carb"] as? String ?? "", per_serve_fat: responseData[i]["per_serve_fat"] as? String ?? "", per_serve_protein: responseData[i]["per_serve_protein"] as? String ?? "", recipe_serve: "", total_serve_calories: "", total_serve_carb: "", total_serve_fat: "", total_serve_protein: "", recipe_weight: "", weight: responseData[i]["weight"] as? String ?? "", recipe_thumbnail_image: responseData[i]["recipe_thumbnail_image"] as? String ?? ""))
//                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
                
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
}
extension RecipeListVC : UITableViewDelegate,UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return recipesList.count == 0 ? 0 : recipesList[category].all_recipe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecipeListCell
        cell.recipeName.text = recipesList[category].all_recipe[indexPath.row].recipe_name
        cell.recipeCalories.text = "\(recipesList[category].all_recipe[indexPath.row].calories ?? "") Calories"
   //     cell.recipeImg.sd_setImage(with: URL(string: recipesList[indexPath.row].recipe_image ?? ""), placeholderImage: UIImage(named: "ic_recipe_list_pp"), options: SDWebImageOptions.continueInBackground, completed: nil)
        
        let transformer = SDImageResizingTransformer(size: CGSize(width: 180, height: 200), scaleMode: .aspectFill)
        cell.recipeImg.sd_setImage(with:URL(string: recipesList[category].all_recipe[indexPath.row].recipe_thumbnail_image ?? ""), placeholderImage: UIImage(named: "ic_recipe_list_pp"), context: [.imageTransformer: transformer])
        cell.seeDetailsBtn.tag = indexPath.row
     //   cell.seeDetailsBtn.addTarget(self, action: #selector(seeDetailsBtnAction(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = NewRecipeDetailsVC.instantiate(fromAppStoryboard: .SideMenu)
        VC.optionId = recipesList[category].all_recipe[indexPath.row].recipe_id ?? ""
        self.navigationController?.pushViewController(VC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
}
extension RecipeListVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipesList.count == 0 ? 0 : recipesList.count
        
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MyTrainingMusclesCell
        cell.nameLbl.text = recipesList[indexPath.item].catgory_name
        cell.nameLbl.backgroundColor = recipesList[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        cell.nameLbl.textColor = recipesList[indexPath.item].selected == true ? .white : .black
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        category = indexPath.item
        for i in recipesList.indices { recipesList[i].selected = false }
        recipesList[indexPath.item].selected = true
        categoryCollectionView.reloadData()
        recipeTableView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         return CGSize(width: UIScreen.main.bounds.size.width * 0.23, height: categoryCollectionView.frame.size.height)
    }
}
//MARK:- NavBar Delegate Method(s)

extension RecipeListVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
