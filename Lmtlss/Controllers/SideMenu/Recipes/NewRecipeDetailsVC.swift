//
//  NewRecipeDetailsVC.swift
//  Lmtlss
//
//  Created by apple on 15/03/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit
import Alamofire

class NewRecipeDetailsVC: UIViewController {
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var recipeImg: UIImageView!
    @IBOutlet weak var recipeNameLbl: UILabel!
    @IBOutlet weak var servingCountLbl: UILabel!
    @IBOutlet weak var proteinLbl: UILabel!
    @IBOutlet weak var carbsLbl: UILabel!
    @IBOutlet weak var fatLbl: UILabel!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var ingradientsTableView: UITableView!
    @IBOutlet weak var ingredientsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var perServingProteinLbl: UILabel!
    @IBOutlet weak var perServingCarbsLbl: UILabel!
    @IBOutlet weak var perServingFatLbl: UILabel!
    @IBOutlet weak var perServingCaloriesLbl: UILabel!
    @IBOutlet weak var howToMakeTableView: UITableView!
    @IBOutlet weak var howToMakeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var emailRecipeBtn: UIButton!
    @IBOutlet weak var toppingLbl: UILabel!
    @IBOutlet weak var tipLbl: UILabel!
    @IBOutlet weak var toppingView: UIView!
    @IBOutlet weak var tipView: UIView!
    @IBOutlet weak var servingWeightLbl: UILabel!
    
    var ingredientsArr = [[String:Any]]()
    var howToMakearr = [String]()
    var optionId = ""
    var recipeDetails = [RecipeField]()
    var servings = 0.5
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Recipe Details"
        myCustomView.logoImg.isHidden = true
        //        myCustomView.searchImage.isHidden = true
        //        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        self.ingradientsTableView.delegate = self
        self.ingradientsTableView.dataSource = self
        self.howToMakeTableView.delegate = self
        self.howToMakeTableView.dataSource = self
        getRecipeDetails(servings: servings)
    }
    func getRecipeDetails(servings:Double){
        if internetValidation(controller: self){
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.recipeDetails + optionId
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            let params = ["serve" : "\(servings)"]
            AFWrapperClass.requestPostWithMultiFormData(url, params: params, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                debugPrint(response)
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.recipeImg.sd_setImage(with: URL(string: responseData["recipe_image"] as? String ?? ""), placeholderImage: UIImage(named: "ic_recipe_details_pp"))
                            self.proteinLbl.text = "\(responseData["protein"] as? String ?? "") g"
                            self.carbsLbl.text = "\(responseData["carb"] as? String ?? "") g"
                            self.fatLbl.text = "\(responseData["fat"] as? String ?? "") g"
                            self.caloriesLbl.text = "\(responseData["calories"] as? String ?? "") Cal"
                         //   self.servingCountLbl.text = "\(responseData["serves"] as? Int ?? 0)"
                            self.recipeNameLbl.text = responseData["recipe_name"] as? String ?? ""
                            self.perServingProteinLbl.text = "\(responseData["per_serve_protein"] as? String ?? "") g"
                            self.perServingCarbsLbl.text = "\(responseData["per_serve_carb"] as? String ?? "") g"
                            self.perServingFatLbl.text = "\(responseData["per_serve_fat"] as? String ?? "") g"
                            self.perServingCaloriesLbl.text = "\(responseData["per_serve_calories"] as? String ?? "") Cal"
                            self.toppingLbl.text = responseData["topping"] as? String ?? ""
                            self.tipLbl.text = responseData["tip"] as? String ?? ""
                            self.servingWeightLbl.text = "\(responseData["weight"] as? String ?? "") g"
//                            self.toppingView.isHidden = self.toppingLbl.text == "" ? true : false
//                            self.tipView.isHidden = self.tipLbl.text == "" ? true : false
                            if let ingredientsArray = responseData["ingredient_detail"] as? [[String:Any]] {
                                self.ingredientsArr = ingredientsArray
                                self.ingradientsTableView.reloadData()
                            }
                            if let howToMakeArray = responseData["method"] as? [String] {
                                self.howToMakearr = howToMakeArray
                                self.howToMakeTableView.reloadData()
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }
    @IBAction func incrementDecrementBtnAction(_ sender: UIButton) {
        if servings == 0.5 && sender.tag != 1{
         //   alert(Constants.AppName, message: "Quantity cannot be less than 10", view: self)
            return
        }
        servings = sender.tag == 1 ?  servings+0.5 : servings == 0.5 ? 0.5 : servings-0.5
        self.servingCountLbl.text = "\(servings)"
        
        getRecipeDetails(servings: servings)
    }
    
    @IBAction func emailRecipeBtnAction(_ sender: UIButton) {
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.recipeSendMail
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        let params = ["recipe_id" : optionId,"serve": "\(servings)"]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            let message = response["responseMessage"] as? String ?? ""
            alert(Constants.AppName, message: message, view: self)
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
}
//MARK:- TableView Delegate Method(s)
extension NewRecipeDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == ingradientsTableView{
            var cell: IngredientsHeaderCell! = tableView.dequeueReusableCell(withIdentifier: "header") as? IngredientsHeaderCell
            if cell == nil {
                tableView.register(UINib(nibName: "IngredientsHeaderCell", bundle: nil), forCellReuseIdentifier: "header")
                cell = tableView.dequeueReusableCell(withIdentifier: "header") as? IngredientsHeaderCell
            }
            return cell
        }
        return UITableViewCell()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView == ingradientsTableView ? 40 : 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == ingradientsTableView ? ingredientsArr.count : howToMakearr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ingradientsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! IngredientsCell
            cell.ingredientsLbl.text = ingredientsArr[indexPath.row]["ingredient_name"] as? String ?? ""
            cell.quantityLbl.text = ingredientsArr[indexPath.row]["quantity"] as? String ?? ""
            DispatchQueue.main.async {
                self.ingredientsTableViewHeight.constant = self.ingradientsTableView.contentSize.height
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HowToMakeCell
            cell.howToMakeLbl.text = howToMakearr[indexPath.row]
            cell.numberLbl.text = "\(indexPath.row+1)"
            DispatchQueue.main.async {
                self.howToMakeTableViewHeight.constant = self.howToMakeTableView.contentSize.height
            }
            return cell
        }
    }
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

//MARK:- NavBar Delegate Method(s)

extension NewRecipeDetailsVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
