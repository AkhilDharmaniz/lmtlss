//
//  RecipeDetailsVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage

class IngredientsCell: UITableViewCell {
    
    @IBOutlet weak var ingredientsLbl: UILabel!
    @IBOutlet weak var quantityLbl: UILabel!
}
class HowToMakeCell: UITableViewCell {
    
    @IBOutlet weak var numberLbl: UILabel!
    @IBOutlet weak var howToMakeLbl: UILabel!
}
protocol SelectionDelegate: class {
    func selectedRecipe(_: String)
}
class RecipeDetailsVC: UIViewController {

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var recipeImg: UIImageView!
    var recipeDetails = [RecipeField]()
    @IBOutlet weak var recipeNameLbl: UILabel!
    @IBOutlet weak var servingCountLbl: UILabel!
    @IBOutlet weak var proteinLbl: UILabel!
    @IBOutlet weak var carbsLbl: UILabel!
    @IBOutlet weak var fatLbl: UILabel!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var ingradientsTableView: UITableView!
    @IBOutlet weak var ingredientsTableViewHeight: NSLayoutConstraint!
    var ingredientsArr = [[String:Any]]()
    var howToMakearr = [String]()
    @IBOutlet weak var perServingProteinLbl: UILabel!
    @IBOutlet weak var perServingCarbsLbl: UILabel!
    @IBOutlet weak var perServingFatLbl: UILabel!
    @IBOutlet weak var perServingCaloriesLbl: UILabel!
    @IBOutlet weak var howToMakeTableView: UITableView!
    @IBOutlet weak var howToMakeTableViewHeight: NSLayoutConstraint!
    var fromQuestionnaire = false
    var optionId = ""
    @IBOutlet weak var addToMealPlanBtn: UIButton!
    @IBOutlet weak var toppingLbl: UILabel!
    @IBOutlet weak var tipLbl: UILabel!
    @IBOutlet weak var toppingView: UIView!
    @IBOutlet weak var tipView: UIView!
    @IBOutlet weak var servingWeightLbl: UILabel!
    
    weak var delegate: SelectionDelegate?
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        self.ingredientsTableViewHeight.constant = 0
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Recipe Details"
        myCustomView.logoImg.isHidden = true
//        myCustomView.searchImage.isHidden = true
//        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        self.ingradientsTableView.delegate = self
        self.ingradientsTableView.dataSource = self
        self.howToMakeTableView.delegate = self
        self.howToMakeTableView.dataSource = self
        if fromQuestionnaire == true{
          getrecipeDetails()
          addToMealPlanBtn.isHidden = false
        }else{
            addToMealPlanBtn.isHidden = true
            if recipeDetails.count != 0{
                DispatchQueue.main.async {
                    self.recipeImg.sd_setImage(with: URL(string: self.recipeDetails[0].recipe_image ?? ""), placeholderImage: UIImage(named: "ic_recipe_details_pp"))
                }
//                let transformer = SDImageResizingTransformer(size: CGSize(width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height * 0.35), scaleMode: .aspectFill)
//                self.recipeImg.sd_setImage(with:URL(string: self.recipeDetails[0].recipe_image ?? ""), placeholderImage: UIImage(named: "ic_recipe_details_pp"), context: [.imageTransformer: transformer])
                self.proteinLbl.text = "\(recipeDetails[0].protein ?? "") g"
                self.carbsLbl.text = "\(recipeDetails[0].carb ?? "") g"
                self.fatLbl.text = "\(recipeDetails[0].fat ?? "") g"
                self.caloriesLbl.text = "\(recipeDetails[0].calories ?? "") Cal"
                self.servingCountLbl.text = "\(recipeDetails[0].serves ?? 0)"
                self.recipeNameLbl.text = recipeDetails[0].recipe_name
                self.perServingProteinLbl.text = "\(recipeDetails[0].per_serve_protein ?? "") g"
                self.perServingCarbsLbl.text = "\(recipeDetails[0].per_serve_carb ?? "") g"
                self.perServingFatLbl.text = "\(recipeDetails[0].per_serve_fat ?? "") g"
                self.perServingCaloriesLbl.text = "\(recipeDetails[0].per_serve_calories ?? "") Cal"
                self.servingWeightLbl.text = recipeDetails[0].weight != "" ?  "\(recipeDetails[0].weight ?? "") g" : "\(recipeDetails[0].recipe_weight ?? "") g"
                self.toppingLbl.text = recipeDetails[0].topping
                self.toppingView.isHidden = recipeDetails[0].topping == "" ? true : false
                self.tipLbl.text = recipeDetails[0].tip
                self.tipView.isHidden = recipeDetails[0].tip == "" ? true : false
                if let ingredientsArray = recipeDetails[0].ingredients {
                    self.ingredientsArr = ingredientsArray
                    self.ingradientsTableView.reloadData()
                }
                if let howToMakeArray = recipeDetails[0].method {
                    self.howToMakearr = howToMakeArray
                    self.howToMakeTableView.reloadData()
                }
            }
        }
       
    }
    func getrecipeDetails(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.recipeDetails + optionId
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.recipeImg.sd_setImage(with: URL(string: responseData["recipe_image"] as? String ?? ""), placeholderImage: UIImage(named: "ic_recipe_details_pp"))
                            self.proteinLbl.text = "\(responseData["protein"] as? String ?? "") g"
                            self.carbsLbl.text = "\(responseData["carb"] as? String ?? "") g"
                            self.fatLbl.text = "\(responseData["fat"] as? String ?? "") g"
                            self.caloriesLbl.text = "\(responseData["calories"] as? String ?? "") Cal"
                            self.servingCountLbl.text = "\(responseData["serves"] as? Int ?? 0)"
                            self.recipeNameLbl.text = responseData["recipe_name"] as? String ?? ""
                            self.perServingProteinLbl.text = "\(responseData["per_serve_protein"] as? String ?? "") g"
                            self.perServingCarbsLbl.text = "\(responseData["per_serve_carb"] as? String ?? "") g"
                            self.perServingFatLbl.text = "\(responseData["per_serve_fat"] as? String ?? "") g"
                            self.perServingCaloriesLbl.text = "\(responseData["per_serve_calories"] as? String ?? "") Cal"
                            self.toppingLbl.text = responseData["topping"] as? String ?? ""
                            self.tipLbl.text = responseData["tip"] as? String ?? ""
                            self.servingWeightLbl.text = "\(responseData["weight"] as? String ?? "") g"
                            self.toppingView.isHidden = self.toppingLbl.text == "" ? true : false
                            self.tipView.isHidden = self.tipLbl.text == "" ? true : false
                            if let ingredientsArray = responseData["ingredient_detail"] as? [[String:Any]] {
                                self.ingredientsArr = ingredientsArray
                                self.ingradientsTableView.reloadData()
                            }
                            if let howToMakeArray = responseData["method"] as? [String] {
                                self.howToMakearr = howToMakeArray
                                self.howToMakeTableView.reloadData()
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    
    @IBAction func addToMealPlanBtnAction(_ sender: UIButton) {
        delegate?.selectedRecipe("")
      self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension RecipeDetailsVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
extension RecipeDetailsVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if tableView == ingradientsTableView{
            var cell: IngredientsHeaderCell! = tableView.dequeueReusableCell(withIdentifier: "header") as? IngredientsHeaderCell
            if cell == nil {
                tableView.register(UINib(nibName: "IngredientsHeaderCell", bundle: nil), forCellReuseIdentifier: "header")
                cell = tableView.dequeueReusableCell(withIdentifier: "header") as? IngredientsHeaderCell
            }
            return cell
        }
        return UITableViewCell()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableView == ingradientsTableView ? ingredientsArr.count == 0 ? 0 : 1 : 1
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return tableView == ingradientsTableView ? ingredientsArr.count == 0 ? 0 : 40 : 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableView == ingradientsTableView ? ingredientsArr.count : howToMakearr.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == ingradientsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! IngredientsCell
            cell.ingredientsLbl.text = ingredientsArr[indexPath.row]["ingredient_name"] as? String ?? ""
            cell.quantityLbl.text = ingredientsArr[indexPath.row]["quantity"] as? String ?? ""
            DispatchQueue.main.async {
                self.ingredientsTableViewHeight.constant = self.ingradientsTableView.contentSize.height
            }
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! HowToMakeCell
            cell.howToMakeLbl.text = howToMakearr[indexPath.row]
            cell.numberLbl.text = "\(indexPath.row+1)"
            DispatchQueue.main.async {
                self.howToMakeTableViewHeight.constant = self.howToMakeTableView.contentSize.height
            }
            return cell
        }
    }
   func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
