//
//  SupplementVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 22/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
class SupplementTableCell: UITableViewCell{
   @IBOutlet weak var supplementImg: UIImageView!
    @IBOutlet weak var supplementNameLbl: UILabel!
    @IBOutlet weak var howToTakeLbl: UILabel!
    
    @IBOutlet weak var viewDetailsBtn: UIButton!
}
class SupplementVC: UIViewController {

  
    @IBOutlet weak var supplementTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    var supplementList = [SupplementField]()
    override func viewDidLoad() {
        super.viewDidLoad()
        supplementTableView.delegate = self
        supplementTableView.dataSource = self
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        self.getSupplementList()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Supplement Shop"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func getSupplementList(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.allSupplements
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.supplementList.removeAll()
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [[String:Any]]{
                            for i in 0..<responseData.count{
                                self.supplementList.append(SupplementField(created_at: responseData[i]["created_at"] as? String ?? "", id: responseData[i]["id"] as? String ?? "", image: responseData[i]["image"] as? String ?? "", status: responseData[i]["status"] as? String ?? "", supplement_description: responseData[i]["supplement_description"] as? String ?? "", supplement_name: responseData[i]["supplement_name"] as? String ?? "", updated_at: responseData[i]["updated_at"] as? String ?? "", url: responseData[i]["url"] as? String ?? ""))
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.supplementTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }

}
extension SupplementVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return supplementList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SupplementTableCell
        cell.supplementNameLbl.text = supplementList[indexPath.row].supplement_name
        cell.howToTakeLbl.text = supplementList[indexPath.row].supplement_description
        cell.supplementImg.sd_setImage(with: URL(string: supplementList[indexPath.row].image ?? ""), placeholderImage: UIImage(named: "nutrition"))
        cell.viewDetailsBtn.tag = indexPath.row
        cell.viewDetailsBtn.addTarget(self, action: #selector(viewDetailsBtnAction(_:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    @objc func viewDetailsBtnAction(_ sender : UIButton){
        if supplementList[sender.tag].url != "" {
            if let url = URL(string: supplementList[sender.tag].url ?? "") {
                UIApplication.shared.open(url)
            }
        }
    }
}
//MARK:- NavBar Delegate Method(s)

extension SupplementVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
    
  
