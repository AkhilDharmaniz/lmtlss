//
//  ShoppingDaysVC.swift
//  Lmtlss
//
//  Created by apple on 30/03/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ShoppingDaysVC: UIViewController {

    @IBOutlet weak var restDaysTF: UITextField!
    @IBOutlet weak var trainingDaysTF: UITextField!
    @IBOutlet weak var navView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = true
        myCustomView.logoImg.isHidden = false
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func validateScreenData() -> Bool{
        if ValidationManager.shared.isEmpty(text: trainingDaysTF.text!){
            alert(Constants.AppName, message: "Please enter training days", view: self)
            return false
        }else if ValidationManager.shared.isEmpty(text: restDaysTF.text!){
            alert(Constants.AppName, message: "Please enter rest days", view: self)
            return false
        }
        return true
    }
    @IBAction func nextBtnAction(_ sender: UIButton) {
        self.view.endEditing(true)
        if validateScreenData() && internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.addShoppingDay
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            
            let params = ["training_day" : trainingDaysTF.text!,"rest_day": restDaysTF.text!]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                debugPrint(response)
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        let VC = MealsSectionVC.instantiate(fromAppStoryboard: .SideMenu)
                        self.navigationController?.pushViewController(VC, animated: true)
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
       
    }
}
//MARK:- NavBar Delegate Method(s)

extension ShoppingDaysVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
