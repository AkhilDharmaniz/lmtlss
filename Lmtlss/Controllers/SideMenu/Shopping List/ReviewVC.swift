//
//  ReviewVC.swift
//  Lmtlss
//
//  Created by apple on 08/04/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit
import Alamofire
class ReviewVC: UIViewController {
    
    @IBOutlet weak var reviewTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    var reviewModel =  [ReviewModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = true
        myCustomView.logoImg.isHidden = false
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        if #available(iOS 15.0, *) {
            UITableView.appearance().sectionHeaderTopPadding = CGFloat(0)
        }
        self.reviewTableView.delegate = self
        self.reviewTableView.dataSource = self
        getSummary()
    }
    func getSummary(){
        if internetValidation(controller: self){
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.shoppingListReview
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
               
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        
                        if let responseData = response["response_ios"] as? [[String:Any]]{
                            debugPrint(responseData)
                            self.reviewModel = responseData.map({ReviewModel(dict: $0)})
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                self.reviewTableView.reloadData()
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
                
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }
    @IBAction func emailShoppingListBtnAction(_ sender: UIButton) {
        IJProgressView.shared.showProgressView()
        let url = Constants.baseURL + Constants.shoppingListPdf
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
        AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            let message = response["responseMessage"] as? String ?? ""
            alert(Constants.AppName, message: message, view: self)
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
}

//MARK:- NavBar Delegate Method(s)

extension ReviewVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

//MARK:- TableView Delegate Method(s)
extension ReviewVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.reviewModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ReviewCell", for: indexPath) as! ReviewCell
        cell.titleLbl.text = self.reviewModel[indexPath.row].title
        cell.setupData(data: self.reviewModel[indexPath.row].detail)
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

