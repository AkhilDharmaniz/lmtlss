//
//  TrainingStatisticsVC.swift
//  Lmtlss
//
//  Created by Akhil on 11/01/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit
class TrainingStatisticsCell: UITableViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var titleLbl: UILabel!
}

class TrainingStatisticsVC: UIViewController {

    @IBOutlet weak var statisticsTableVeiw: UITableView!
    @IBOutlet weak var navView: UIView!
    var options = [("CURRENT STATISTICS",false,"1"),("ALL TIME STATISTICS",false,"2")]
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Training statistics"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.searchBtn.isUserInteractionEnabled = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        self.statisticsTableVeiw.delegate = self
        self.statisticsTableVeiw.dataSource = self
    }

}

extension TrainingStatisticsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! TrainingStatisticsCell
        cell.selectionStyle = .none
        let data = options[indexPath.row]
        cell.titleLbl.text = data.0
        cell.bgView.backgroundColor = data.1 == true ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in options.indices { options[i].1 = false }
        options[indexPath.row].1 = !options[indexPath.row].1
        statisticsTableVeiw.reloadData()
        let vc = MuscleGroupVC.instantiate(fromAppStoryboard: .SideMenu)
        vc.isStatistics = options[indexPath.row].2
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK:- NavBar Delegate Method(s)

extension TrainingStatisticsVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
