//
//  MuscleExerciseVC.swift
//  Lmtlss
//
//  Created by Akhil on 12/01/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit
import Alamofire
import AVKit


class MuscleExerciseCell: UITableViewCell {
    @IBOutlet weak var thumbnailImg: UIImageView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var exerciseNameLbl: UILabel!
    @IBOutlet weak var setsLbl: UILabel!
    @IBOutlet weak var avWeightLbl: UILabel!
    @IBOutlet weak var avRepsLbl: UILabel!
    @IBOutlet weak var avVolumeLbl: UILabel!
    @IBOutlet weak var viewVideoBtn: UIButton!
}
class MuscleExerciseVC: UIViewController {
    @IBOutlet weak var exerciseTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var noResultsView: UIView!
    
    var isStatistics = ""
    var muscleGroup: MuscleListModel?
    var exercisesArr = [MuscleExerciseModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        muscleExercisesApi()

    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "\(muscleGroup?.muscle_group_name ?? "") Exercise" 
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.searchBtn.isUserInteractionEnabled = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        self.exerciseTableView.delegate = self
        self.exerciseTableView.dataSource = self
    }

    //MARK:- API implementation Method(s)
        func muscleExercisesApi(){
            if internetValidation(controller: self){
                IJProgressView.shared.showProgressView()
                let signInUrl = Constants.baseURL + Constants.getAllMusclesAndExercise
                var token = UserDefaults.standard.value(forKey: "token") as? String
                token = "Bearer " + token!
                let headers : HTTPHeaders = ["Authorization":token ?? ""]
                 
                let params = ["search" : "","is_statistics": isStatistics,"muscle_group_id": "\(muscleGroup?.id ?? 0)"]
                AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                    IJProgressView.shared.hideProgressView()
                    
                    let message = response["responseMessage"] as? String ?? ""
                    if let responseCode = response["responseCode"] as? Int{
                        if responseCode == 200{
                            if let allExercises = response["all_exercise"] as? [[String:Any]]{
                                for exercise in allExercises{
                                    self.exercisesArr.append(MuscleExerciseModel(av_reps: exercise["av_reps"] as? String ?? "", av_weight: exercise["av_weight"] as? String ?? "", date: exercise["date"] as? String ?? "", exercise_name: exercise["exercise_name"] as? String ?? "", exercise_video_thumbnail: exercise["exercise_video_thumbnail"] as? String ?? "", exercise_video_url: exercise["exercise_video_url"] as? String ?? "", sets: exercise["sets"] as? String ?? "", volume: exercise["volume"] as? String ?? "", selected: false, exercise_id: exercise["exercise_id"] as? Int ?? 0))
                                }
                            }
                        }else{
                            IJProgressView.shared.hideProgressView()
                            alert(Constants.AppName, message: message, view: self)
                        }
                        if self.exercisesArr.count == 0{
                            self.noResultsView.isHidden = false
                        }else{
                            self.noResultsView.isHidden = true
                        }
                        self.exerciseTableView.reloadData()
                    }
                    if let status = response["status"] as? Int, status == 401{
                        logOut(controller: self)
                    }
                }) { (error) in
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: error.localizedDescription, view: self)
                    
                }
            }
        }
}
extension MuscleExerciseVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exercisesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MuscleExerciseCell
        cell.exerciseNameLbl.text = exercisesArr[indexPath.row].exercise_name
        cell.thumbnailImg.sd_setImage(with: URL(string: exercisesArr[indexPath.row].exercise_video_thumbnail ?? ""), placeholderImage: UIImage(named: "ic_video_list_pp"))
        cell.dateLbl.text = exercisesArr[indexPath.row].date
        cell.avRepsLbl.text = exercisesArr[indexPath.row].av_reps
        cell.setsLbl.text = exercisesArr[indexPath.row].sets
        cell.avWeightLbl.text = exercisesArr[indexPath.row].av_weight
        cell.avVolumeLbl.text = exercisesArr[indexPath.row].volume
//        cell.viewVideoBtn.tag = indexPath.row
//        cell.viewVideoBtn.addTarget(self, action: #selector(viewVideoBtnAction(_:)), for: .touchUpInside)

        return cell
    }
//    @objc func viewVideoBtnAction(_ sender : UIButton){
//        guard let videoURL = URL(string: exercisesArr[sender.tag].exercise_video_url ?? "") else{return}
//        let player = AVPlayer(url: videoURL)
//        let playerViewController = AVPlayerViewController()
//        playerViewController.player = player
//        self.present(playerViewController, animated: true) {
//            playerViewController.player!.play()
//        }
//    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = MuscleExerciseDetailsVC.instantiate(fromAppStoryboard: .SideMenu)
        vc.isStatistics = isStatistics
        vc.muscleGroup = muscleGroup
        vc.exerciseGroup = exercisesArr[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension MuscleExerciseVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
