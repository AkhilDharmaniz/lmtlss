//
//  MuscleGroupVC.swift
//  Lmtlss
//
//  Created by Akhil on 11/01/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MuscleGroupVC: UIViewController {

    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchTF: UITextField!
    @IBOutlet weak var musleTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    var muscles = [MuscleListModel]()
    var muscleListArr = [MuscleListModel]()
    var isStatistics = ""
    @IBOutlet weak var noResultView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Muscle Group"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "search")
        myCustomView.searchBtn.isUserInteractionEnabled = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.isSupport = true
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        musleTableView.register(nib, forCellReuseIdentifier: "cell")
        musleTableView.delegate = self
        musleTableView.dataSource = self
        self.searchView.isHidden = true
        self.searchViewHeight.constant = 0
        self.trainingStatisticsApi()
    }
    
//MARK:- API implementation Method(s)
    func trainingStatisticsApi(){
        if internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.getAllMusclesAndExercise
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
             
            let params = ["search" : "","is_statistics": isStatistics]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                debugPrint(response)
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let allMuscles = response["all_muscles"] as? [[String:Any]]{
                            if allMuscles.count == 0{
                                self.noResultView.isHidden = false
                            }
                            for muscle in allMuscles{
                                self.muscles.append(MuscleListModel(created_at: muscle["created_at"] as? String ?? "", gender: muscle["gender"] as? String ?? "", id: muscle["id"] as? Int ?? 0, muscle_group_name: muscle["muscle_group_name"] as? String ?? "", muscle_group_status: muscle["muscle_group_status"] as? Int ?? 0, updated_at: muscle["updated_at"] as? String ?? "",selected: false))
                            }
                            self.muscleListArr = self.muscles
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.musleTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }
    @IBAction func closeBtnAction(_ sender: UIButton) {
       self.searchView.isHidden = true
        self.searchViewHeight.constant = 0
        self.view.endEditing(true)
        self.muscleListArr = muscles
        self.searchTF.text = ""
        self.musleTableView.reloadData()
    }
    
    @IBAction func searchTFAction(_ sender: UITextField) {
        if sender.text == ""{
            muscleListArr = muscles
        }else{
            muscleListArr = self.muscles.filter({ (text) -> Bool in
                let tmp: NSString = text.muscle_group_name! as NSString
                let range = tmp.range(of: sender.text!, options: NSString.CompareOptions.caseInsensitive)
                return range.location != NSNotFound
            })
        }
        self.musleTableView.reloadData()
        if muscleListArr.count == 0{
            self.noResultView.isHidden = false
        }else{
            self.noResultView.isHidden = true
        }
    }
}
extension MuscleGroupVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return muscleListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        let data = muscleListArr[indexPath.row]
        cell.titleLbl.text = data.muscle_group_name
        cell.subTitleLbl.isHidden = true
        cell.exampleLbl.isHidden = true
        cell.bgImg.backgroundColor = data.selected == true ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in muscleListArr.indices { muscleListArr[i].selected = false }
        muscleListArr[indexPath.row].selected = !muscleListArr[indexPath.row].selected!
        musleTableView.reloadData()
        let vc = MuscleExerciseVC.instantiate(fromAppStoryboard: .SideMenu)
        vc.isStatistics = isStatistics
        vc.muscleGroup = muscleListArr[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension MuscleGroupVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        self.searchViewHeight.constant = 50
        self.searchView.isHidden = false
    }
}
