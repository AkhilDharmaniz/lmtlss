//
//  MuscleExerciseDetailsVC.swift
//  Lmtlss
//
//  Created by Akhil on 12/01/22.
//  Copyright © 2022 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MuscleExerciseDetailsCell: UITableViewCell {
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var setsLbl: UILabel!
    @IBOutlet weak var avWeightLbl: UILabel!
    @IBOutlet weak var avRepsLbl: UILabel!
    @IBOutlet weak var volumeLbl: UILabel!
}
class MuscleExerciseDetailsVC: UIViewController {
    @IBOutlet weak var exerciseTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    
    @IBOutlet weak var thumbnailImg: UIImageView!
    @IBOutlet weak var exerciseNameLbl: UILabel!
    var muscleGroup: MuscleListModel?
    var exerciseGroup: MuscleExerciseModel?
    var isStatistics = ""
    var exerciseArr = [ExerciseDetailsModel]()

    override func viewDidLoad() {
        super.viewDidLoad()
        exerciseDetailsApi()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "\(muscleGroup?.muscle_group_name ?? "") Exercise"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.searchBtn.isUserInteractionEnabled = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        self.exerciseTableView.delegate = self
        self.exerciseTableView.dataSource = self
    }
    func exerciseDetailsApi(){
        if internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.getExerciseStatisticDetailById
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
             
            let params = ["exercise_id" : "\(exerciseGroup?.exercise_id ?? 0)" ,"is_statistics": isStatistics,"muscle_group_id": "\(muscleGroup?.id ?? 0)"]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let exerciseDetails = response["exercise_detail"] as? [String:Any]{
                            self.exerciseNameLbl.text = exerciseDetails["exercise_name"] as? String ?? ""
                            self.thumbnailImg.sd_setImage(with: URL(string: exerciseDetails["exercise_video_thumbnail"] as? String ?? ""), placeholderImage: UIImage(named: "ic_video_list_pp"))
                        }
                        if let details = response["detail"] as? [[String:Any]]{
                            for detail in details{
                                self.exerciseArr.append(ExerciseDetailsModel(av_reps: detail["av_reps"] as? String ?? "", av_weight: detail["av_weight"] as? String ?? "", date: detail["date"] as? String ?? "", sets: detail["sets"] as? String ?? "", volume: detail["volume"] as? String ?? ""))
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.exerciseTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }

}

extension MuscleExerciseDetailsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return exerciseArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MuscleExerciseDetailsCell
        cell.dateLbl.text = self.exerciseArr[indexPath.row].date
        cell.setsLbl.text = self.exerciseArr[indexPath.row].sets
        cell.avWeightLbl.text = self.exerciseArr[indexPath.row].av_weight
        cell.avRepsLbl.text = self.exerciseArr[indexPath.row].av_reps
        cell.volumeLbl.text = self.exerciseArr[indexPath.row].volume
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}
//MARK:- NavBar Delegate Method(s)

extension MuscleExerciseDetailsVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
