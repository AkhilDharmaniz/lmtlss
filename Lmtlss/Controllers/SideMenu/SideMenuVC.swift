//
//  SideMenuVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 21/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class SideMenuCell: UITableViewCell{
    
    @IBOutlet weak var menuImg: UIImageView!
    @IBOutlet weak var menuTitleLbl: UILabel!
    @IBOutlet weak var badgeView: UIView!
    @IBOutlet weak var badgeLbl: UILabel!
}
class SideMenuVC: UIViewController {
    var menuArray = [(#imageLiteral(resourceName: "video"),"Video library"),(#imageLiteral(resourceName: "supp"),"Supplement shop"),(#imageLiteral(resourceName: "cloth"),"Clothing shop"),(#imageLiteral(resourceName: "setting"),"Settings"),(#imageLiteral(resourceName: "noti"),"Notifications"),(#imageLiteral(resourceName: "training"),"Training Statistics"),(#imageLiteral(resourceName: "recipe"),"Recipes"),(#imageLiteral(resourceName: "forum"),"Forum"),(#imageLiteral(resourceName: "shopping"),"Shopping List"),(#imageLiteral(resourceName: "check-mark"),"Check-in"),(#imageLiteral(resourceName: "supportB"),"Support"),(#imageLiteral(resourceName: "logout"),"Logout")]
       // ,(#imageLiteral(resourceName: "noti"),"Push notifications")
    @IBOutlet weak var menuTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBadge(notification:)), name: Notification.Name("updateBadge"), object: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        
    }
    // MARK:- configuring ui methods
    func configureUI(){
        menuTableView.delegate = self
        menuTableView.dataSource = self
    }
    @objc func updateBadge(notification: Notification) {
        let notification_count = UserDefaults.standard.value(forKey: "notification_count") as? String
        if notification_count != "0"{
            menuTableView.reloadData()
        }
    }
}
extension SideMenuVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SideMenuCell
        cell.menuTitleLbl.text = menuArray[indexPath.row].1
        let notification_count = UserDefaults.standard.value(forKey: "notification_count") as? String
        if notification_count != "0"{
            if menuArray[indexPath.row].1 == "Notifications"{
                cell.badgeView.isHidden = false
                cell.badgeLbl.text = notification_count
            }else{
                cell.badgeView.isHidden = true
            }
        }else{
            cell.badgeView.isHidden = true
        }
        cell.menuImg.image = menuArray[indexPath.row].0
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 65
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            let VC = VideoTypesVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 1 {
            let VC = SupplementVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 3 {
            let VC = SettingVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 4 {
            let VC = NotificationVC.instantiate(fromAppStoryboard: .SideMenu)
            VC.dismissedCurrentView = { showVal in
                VC.navigationController?.popViewController(animated: false)
                if showVal{
                    self.menuTableView.reloadData()
                }
            }
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 5 {
            let VC = TrainingStatisticsVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 6 {
            let VC = RecipeListVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 7 {
            let VC = ForumListingVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 8 {
            checkShopingListStatus()
        }else if indexPath.row == 9 {
            let VC = CheckInContainerVC.instantiate(fromAppStoryboard: .CheckInQuestionnaire)
            VC.dismissedCurrentView = { showVal in
                VC.navigationController?.popViewController(animated: false)
                if showVal{
                    self.sideMenuController?.hideLeftView()
                }
            }
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 10 {
            let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 11 {
            DispatchQueue.main.async {
                showMessage(title: Constants.AppName, message: "Do you want to Logout?", okButton: "OK", cancelButton: "Cancel", controller: self, okHandler: {
                    self.logOut()
                }) {}
            }
            
        }
    }
    func checkShopingListStatus(){
        if internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let url = Constants.baseURL + Constants.checkShoppingListStatus
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                debugPrint(response)
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        let status = response["status"] as? Int ?? 0
                        if status == 1{
                            let VC = ShoppingDaysVC.instantiate(fromAppStoryboard: .SideMenu)
                            self.navigationController?.pushViewController(VC, animated: true)
                        }else{
                            alert(Constants.AppName, message: message, view: self)
                        }

                    }else{
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    Lmtlss.logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }
    func logOut(){
        if Reachability.isConnectedToNetwork() == true {
            IJProgressView.shared.showProgressView()
            let url = Constants.baseURL + Constants.logout
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                appDelegate().logOutSuccess()
//                let message = response["responseMessage"] as? String ?? ""
//                if let responseCode = response["responseCode"] as? Int{
//                    if responseCode == 200{
//
//                    }else{
//                        alert(Constants.AppName, message: message, view: self)
//                    }
//                }
                if let status = response["status"] as? Int, status == 401{
                    Lmtlss.logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
}

