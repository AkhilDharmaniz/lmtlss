//
//  CheckInContainerVC.swift
//  Lmtlss
//
//  Created by apple on 21/07/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire

class CheckInContainerVC: UIViewController {
    @IBOutlet weak var progressLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var navView: UIView!
    private var currentVC:UIViewController?
    var fromDelegate = false
    private var checkInQuestionnaireScreens:[CheckInQuestionnaireScreensTypes] = [.CurrentWeight, .TargetWeight, .NewTargetWeight,.uploadImages,.PhysicalActivity, .Lifestyle, .Cardio,.TrainingProgram, .basedProgram, .TrainDays, .Muscles, .Exercises, .LoseFat, .Prefer,.PreferBeverage,.BeverageSelection,.AmendMeal,.ChangeMeal,.PreferMeal,.CaloriesDistribute, .LargerMeal,.CustomizeMealPlan,.IncorporateRecipe,.ReplaceRecipe,.SelectRecipe]
    var currentIndex = 0
    private var from = ""
    private var resultNextSelected = false
    private var results = ""
    var myCustomView:CustomHeaderView!
    var dismissedCurrentView:((Bool)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        from = "load"
        configure()
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideNextBtn(notification:)), name: Notification.Name("NextBtnHidden"), object: nil)
    }
    @objc func hideNextBtn(notification: Notification) {
        // Take Action on Notification
        if let hide = notification.userInfo?["hide"] as? Bool  {
            if hide == true{
                nextBtn.isHidden = true
            }else{
                nextBtn.isHidden = false
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        configureNavView()
    }
    func configureNavView(){
        myCustomView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = true
        myCustomView.logoImg.isHidden = false
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.searchBtn.isUserInteractionEnabled = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func configureUI(){
        //  currentIndex = 21
        //  currentIndex = UserDefaults.standard.value(forKey: "currentIndex") as? Int ?? 0
        self.setControllerQuestion(type: self.checkInQuestionnaireScreens[self.currentIndex])
        self.animateProgress()
    }
    private func animateProgress(){
        let singleUnit = UIScreen.main.bounds.width/CGFloat(checkInQuestionnaireScreens.count)
        view.layoutIfNeeded()
        progressWidthConstraint.constant = singleUnit * CGFloat(currentIndex+1)
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    private func dismissAnimation(){
        let singleUnit = UIScreen.main.bounds.width/CGFloat(checkInQuestionnaireScreens.count)
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.progressWidthConstraint.constant = singleUnit * CGFloat(self.currentIndex+1)
        }
    }
    private func setControllerQuestion(type: CheckInQuestionnaireScreensTypes){
        self.nextBtn.setImage(#imageLiteral(resourceName: "next-Question"), for: .normal)
        nextBtn.isHidden = false
        switch type {
        case .CurrentWeight:
            setVC(with: "CurrentWeightVC")
        case .TargetWeight:
            setVC(with: "TargetWeightVC")
        case .NewTargetWeight:
            setVC(with: "NewTargetWeightVC")
        case .uploadImages:
            setVC(with: "UploadImagesVC")
        case .PhysicalActivity:
            setVC(with: "PhysicalActivityCheckInVC")
        case .Lifestyle:
            setVC(with: "LifeStyleCheckInVC")
        case .Cardio:
            setVC(with: "CardioCheckInVC")
        case .TrainingProgram:
            setVC(with: "TraningProgramCheckInVC")
        case .basedProgram:
            setVC(with: "ProgramTypeCheckInVC")
        case .TrainDays:
            setVC(with: "TrainingDaysCheckInVC")
        case .Muscles:
            setVC(with: "MuscleCheckInVC")
        case .Exercises:
            setVC(with: "ExercisesCheckInVc")
        case .LoseFat:
            setVC(with: "LoseOrGainWeightCheckInVC")
        case .Prefer:
            setVC(with: "PreferCheckInVC")
        case .PreferBeverage:
            setVC(with: "PreferBeverageCheckInVC")
        case .BeverageSelection:
            setVC(with: "BeverageSelectionCheckInVC")
        case .AmendMeal:
            setVC(with: "AmendMealPlanCheckInVC")
        case .ChangeMeal:
            setVC(with: "ChangeMealsCheckInVC")
        case .PreferMeal:
            setVC(with: "PreferMealCheckInVC")
        case .CaloriesDistribute:
            setVC(with: "CaloriesDistributeCheckInVC")
        case .results:
            setVC(with: "ResultsCheckInVC")
            self.nextBtn.setImage(#imageLiteral(resourceName: "GTMP"), for: .normal)
         self.resultNextSelected = true
        case .LargerMeal:
            setVC(with: "LargerMealPreferCheckInVC")
        case .CustomizeMealPlan:
            setVC(with: "CustomizeMealPlanCheckInVC")
        case .IncorporateRecipe:
            setVC(with: "IncorporateRecipeCheckInVC")
        case .ReplaceRecipe:
            setVC(with: "ReplaceRecipeCheckInVC")
        case .SelectRecipe:
            setVC(with: "RecipeSelectionCheckInVC")
        }
    }
    private func setVC(with identifier: String){
        ViewEmbedder.embed(
            withIdentifier: identifier,
            parent: self,
            container: self.container){ vc in
            self.currentVC = vc
        }
    }
    private func setVC1(with identifier: String, storyboard: String){
        ViewEmbedder.embedVC(
            withIdentifier: identifier,
            parent: self,
            toStoryboard: storyboard,
            container: self.container){ vc in
            self.currentVC = vc
        }
    }
    //MARK:- IBAction Method(s)
    @IBAction func nextBtnAction(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork(){
            if resultNextSelected{
                saveTrainingPlan()
            }else{
                self.validateData(type: self.currentIndex)
            }
        }else{
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func configure(){
        if Reachability.isConnectedToNetwork() == true {
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let qa = Constants.baseURL + Constants.checkInQuestionnaire
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(qa, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["message"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        mealNumber = response["meal_no"] as? Int ?? 0
                        isPrevMealplan = response["is_prev_mealplan"] as? Int ?? 0
                        if let responseData = response["responseData"] as? [String:Any]{
                                checkInQuestionnaireDict = responseData
                            if let qa = responseData["qa"] as? [[String:Any]]{
                                checkInQuestionnaire = qa
                                if self.from == "load" {
                                    if let checkinStatus = response["checkin_status"] as? Int{
                                        if checkinStatus == 1{
                                            showAlertMessage(title: Constants.AppName, message: "Your Check-in is not active.", okButton: "OK", controller: self) {
                                                self.navigationController?.popViewController(animated: true)
                                            }
                                        }
                                    }
                                    self.configureUI()
                                }else{
                                    if self.results == "yes"{
                                        self.saveMealPlan(from: "results")
                                        self.currentIndex += 1
                                        self.setControllerQuestion(type: .results)
                                        self.results = ""
                                    }else{
                                        if self.currentIndex != 24{
                                            if self.currentIndex == 1{
                                                self.currentIndex += checkInQuestionnaire[2]["question_id"] as? Int == 32 ? 1 : 2
                                            }else if self.currentIndex == 6{
                                                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 39}){
                                                    self.currentIndex +=  1
                                                }else{
                                                    self.currentIndex +=  2
                                                }
                                            }else if self.currentIndex == 7{
                                                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 41}){
                                                    self.currentIndex +=  1
                                                }else{
                                                    self.currentIndex += 5
                                                }
                                            }else if self.currentIndex == 8{
                                                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 40}){
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += 4
                                                }
                                            }else if self.currentIndex == 14{
                                                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 48}){
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += isPrevMealplan == 0 ? 4 : 2
                                                }
                                            }else if self.currentIndex == 15{
                                                self.currentIndex += isPrevMealplan == 0 ? 3 : 1
                                            }else if self.currentIndex == 16{
                                                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 50}){
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += 6
                                                }
                                            }else if self.currentIndex == 17{
                                                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 51}){
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += 2
                                                }
                                            }else if self.currentIndex == 19{
                                                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 53}){
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += 2
                                                }
                                            }else{
                                                self.currentIndex += 1
                                            }
                                            self.setControllerQuestion(type: self.checkInQuestionnaireScreens[self.currentIndex])
                                            self.animateProgress()
                                            if self.currentIndex == 0{
                                                if let subView = self.navView.subviews[0] as? CustomHeaderView{
                                                    subView.menuImage.isHidden = true
                                                    subView.menuBtn.isUserInteractionEnabled = false
                                                }
                                            }else{
                                                if let subView = self.navView.subviews[0] as? CustomHeaderView{
                                                    subView.menuImage.isHidden = false
                                                    subView.menuBtn.isUserInteractionEnabled = true
                                                }
                                            }
                                        }else{
                                              self.saveMealPlan(from: "last")
                                        }
                                    }
                                }
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
              //  
            }
        } else {
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func saveMealPlan(from:String){
        DispatchQueue.main.async {
            IJProgressView.shared.showProgressView()
        }
        let signInUrl = Constants.baseURL + Constants.checkInMealPlan
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        let parameters:[String:Any]  = ["meal_no":"\(mealNumber)"]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: parameters, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            let status = response["responseCode"] as? Int ?? 0
            if status == 200{
                if from != "results"{
                    self.dismissedCurrentView?(true)
                }
            }else{
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: message, view: self)
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func saveTrainingPlan(){
        
        DispatchQueue.main.async {
            IJProgressView.shared.showProgressView()
        }
        let signInUrl = Constants.baseURL + Constants.checkInTrainingPlan
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        let parameters:[String:Any]  = ["meal_no":"\(mealNumber)"]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: parameters, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            let status = response["responseCode"] as? Int ?? 0
            if status == 200{
                self.resultNextSelected = false
                self.dismissedCurrentView?(true)
            }else{
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: message, view: self)
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func validateData(type: Int){
        //  print(type)
        switch type {
        case 0:
            if let vc = self.children.first as? CurrentWeightVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 1:
            if let vc = self.children.first as? TargetWeightVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 2:
            if let vc = self.children.first as? NewTargetWeightVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 3:
            if let vc = self.children.first as? UploadImagesVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 4:
            if let vc = self.children.first as? PhysicalActivityCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 5:
            if let vc = self.children.first as? LifeStyleCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 6:
            if let vc = self.children.first as? CardioCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 7:
            if let vc = self.children.first as? TraningProgramCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 8:
            if let vc = self.children.first as? ProgramTypeCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 9:
            if let vc = self.children.first as? TrainingDaysCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 10:
            if let vc = self.children.first as? MuscleCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 11:
            if let vc = self.children.first as? ExercisesCheckInVc {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 12:
            if let vc = self.children.first as? LoseOrGainWeightCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 13:
            if let vc = self.children.first as? PreferCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie(yesHandler: {
                        self.showPopUp(isValid: vc.validateScreenData())
                        self.results = ""
                        
                    }) {
                        self.results = "yes"
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 14:
            if let vc = self.children.first as? PreferBeverageCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 15:
            if let vc = self.children.first as? BeverageSelectionCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 16:
            if let vc = self.children.first as? AmendMealPlanCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 17:
            if let vc = self.children.first as? ChangeMealsCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 18:
            if let vc = self.children.first as? PreferMealCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 19:
            if let vc = self.children.first as? CaloriesDistributeCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 20:
            if let vc = self.children.first as? LargerMealPreferCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 21:
            if let vc = self.children.first as? CustomizeMealPlanCheckInVC {
                if !vc.validateScreenData(){
                    vc.saveFooditem {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 22:
            if let vc = self.children.first as? IncorporateRecipeCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie(yesHandler: {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }) {
                        self.saveMealPlan(from: "last")
                    }
                }
            }
        case 23:
            if let vc = self.children.first as? ReplaceRecipeCheckInVC {
                if !vc.validateScreenData(){
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        case 24:
            if let vc = self.children.first as? RecipeSelectionCheckInVC {
                if !vc.validateScreenData(){
                    print("validation completed")
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: vc.validateScreenData())
                    }
                }
            }
        default:
            showPopUp(isValid: false)
        }
    }
    func showPopUp(isValid : Bool){
        if !isValid{
            from = "next"
            configure()
        }
    }
    func validateScreens(vc: CheckInContainerVC){
    }
}

//MARK:- Nav bar delegate Method(s)
extension CheckInContainerVC: CustomHeaderViewDelegate{
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    func openSideMenu() {
        if currentIndex != 0{
            if resultNextSelected == true{
                resultNextSelected = false
            }
            if self.currentIndex == 3{
                self.currentIndex -= checkInQuestionnaire[2]["question_id"] as? Int == 32 ? 1 : 2
            }else if self.currentIndex == 8{
                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 39}){
                    self.currentIndex -= 1
                }else{
                    self.currentIndex -= 2
                }
            }else if self.currentIndex == 12{
                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 41}){
                    if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 40}){
                        self.currentIndex -= 1
                    }else{
                        self.currentIndex -= 4
                    }
                }else{
                    self.currentIndex -= 5
                }
            }else if self.currentIndex == 16{
                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 48}){
                    self.currentIndex -= 1
                }else{
                    self.currentIndex -= 2
                }
            }else if self.currentIndex == 18{
                if isPrevMealplan == 0{
                    if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 48}){
                        currentIndex -= 3
                    }else{
                        currentIndex -= 4
                    }
                }else{
                    currentIndex -= 1
                }
            }else if self.currentIndex == 19{
                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 51}){
                    self.currentIndex -= 1
                }else{
                    self.currentIndex -= 2
                }
            }else if self.currentIndex == 21{
                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 53}){
                    self.currentIndex -= 1
                }else{
                    self.currentIndex -= 2
                }
            }else if self.currentIndex == 22{
                if let _ = checkInQuestionnaire.firstIndex(where: {$0["question_id"] as? Int == 50}){
                    self.currentIndex -= 1
                }else{
                    self.currentIndex -= 6
                }
            }
            else{
                currentIndex -= 1
            }
            if currentIndex == -1{
                if fromDelegate{
                    appDelegate().redirectToHome()
                }else{
                    self.navigationController?.popViewController(animated: true)
                }
            }else{
                if let subView = self.navView.subviews[0] as? CustomHeaderView{
                    subView.menuBtn.isUserInteractionEnabled = true
                    setControllerQuestion(type: checkInQuestionnaireScreens[currentIndex])
                    dismissAnimation()
                }
            }
           
        }else{
            if fromDelegate{
                appDelegate().redirectToHome()
            }else{
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
