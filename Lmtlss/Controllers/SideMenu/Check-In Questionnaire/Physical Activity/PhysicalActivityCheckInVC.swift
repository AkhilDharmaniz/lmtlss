    //
//  PhysicalActivityCheckInVC.swift
//  Lmtlss
//
//  Created by apple on 22/07/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire

class PhysicalActivityCheckInVC: UIViewController {
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var activityTVHeightConstriant: NSLayoutConstraint!
    @IBOutlet weak var activityTableView: UITableView!
    var physicalActivityArr = [QuestionaryField]()
    var questionId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    private func configureUI(){
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        activityTableView.register(nib, forCellReuseIdentifier: "cell")
        activityTableView.delegate = self
        activityTableView.dataSource = self
        activityTableView.separatorStyle = .none
        for i in 0..<checkInQuestionnaire.count{
            if checkInQuestionnaire[i]["question_id"] as? Int == 36{
                questionTitleLbl.text = checkInQuestionnaire[i]["question"] as? String ?? ""
                questionId = checkInQuestionnaire[i]["question_id"] as? Int ?? 0
                if let option = checkInQuestionnaire[i]["option"] as? [[String:Any]] {
                    for i in 0..<option.count{
                        self.physicalActivityArr.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
                    }
                    activityTableView.reloadData()
                }
            }
        }
    }
    func validateScreenData() -> Bool{
      
        let selectedElement = physicalActivityArr.filter({$0.selected == 1})
        if selectedElement.count == 0{
            alert(Constants.AppName, message: "Please Select any 1 Option", view: self)
            return true
        }
        return false
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.saveCheckInQuestionnaire
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["question_id"] = questionId as AnyObject
        let options = getSelectedOptions()
        parameters["option_id"] = options as AnyObject
        parameters["meal_no"] = "\(mealNumber)" as AnyObject

        
        return parameters
    }
     func getSelectedOptions() -> String
     {
        let selectedArr : [String]  = self.physicalActivityArr.filter { (objField) -> Bool in
            return objField.selected == 1
        }.map { (objField) -> String in
            return "\(String(describing: objField.option_id!))"
        }
        return selectedArr.joined(separator: ",")
     }

}
extension PhysicalActivityCheckInVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return physicalActivityArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        let data = physicalActivityArr[indexPath.row]
        cell.titleLbl.text = data.option
        cell.subTitleLbl.text = data.description
        DispatchQueue.main.async {
            self.activityTVHeightConstriant.constant = self.activityTableView.contentSize.height
        }
        cell.bgImg.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<physicalActivityArr.count{
            physicalActivityArr[i].selected = i == indexPath.row ? 1 : 0
        }
        activityTableView.reloadData()
    }
}
