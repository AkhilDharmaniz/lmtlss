//
//  RecipeSelectionCheckInVC.swift
//  Lmtlss
//
//  Created by apple on 10/08/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire
import SDWebImage


class RecipeSelectionCheckInVC: UIViewController {

    @IBOutlet weak var recipeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var recipeTableView: UITableView!
    @IBOutlet weak var nameCollectionView: UICollectionView!
    var questionId = 0
    var recipeCollectionList = [QuestionaryRecipeResponseFiled]()
    var index = 0
    var selectedIndex = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    private func configureUI(){
        nameCollectionView.delegate = self
        nameCollectionView.dataSource = self
        scrollVw.delegate = self
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        recipeTableView.register(nib, forCellReuseIdentifier: "cell")
        recipeTableView.delegate = self
        recipeTableView.dataSource = self
        recipeTableView.separatorStyle = .none
        for i in 0..<checkInQuestionnaire.count{
            if checkInQuestionnaire[i]["question_id"] as? Int == 56{
                questionTitleLbl.text = checkInQuestionnaire[i]["question"] as? String ?? ""
                questionId = checkInQuestionnaire[i]["question_id"] as? Int ?? 0
                if let recipe = checkInQuestionnaire[i]["option"] as? [[String:Any]]{
                    for i in 0..<recipe.count{
                        var recipesArray = [QuestionaryRecipeField]()
                        if let optionsList = recipe[i]["recipe"] as? [[String:Any]]{
                            for j in 0..<optionsList.count{
                                recipesArray.append(QuestionaryRecipeField(option: optionsList[j]["option"] as? String ?? "", option_id: optionsList[j]["option_id"] as? Int ?? 0, selected: optionsList[j]["selected"] as? Int ?? 0, description: optionsList[j]["description"] as? String ?? "", example: optionsList[j]["example"] as? String ?? "", img: optionsList[j]["img"] as? String ?? "", thumbnail_img: optionsList[j]["thumbnail_img"] as? String ?? ""))
                            }
                        }
                        self.recipeCollectionList.append(QuestionaryRecipeResponseFiled(name: recipe[i]["name"] as? String ?? "", recipe: recipesArray, selected: false, id: recipe[i]["id"] as? String ?? ""))
                    }
                    DispatchQueue.main.async {
                        self.recipeCollectionList[0].selected = true
                        self.nameCollectionView.reloadData()
                        self.recipeTableViewHeight.constant = CGFloat(110 * self.recipeCollectionList[self.index].recipe.count)
                        self.recipeTableView.reloadData()
                    }
                }
            }
        }
    }
    func validateScreenData() -> Bool{
        
        for i in 0..<recipeCollectionList.count{
            let selectedElement = recipeCollectionList[i].recipe.filter({$0.selected == 1})
            if selectedElement.count == 0{
                alert(Constants.AppName, message: "Please select any 1 recipe from each meal", view: self)
                return true
            }
        }
        return false
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.saveRecipe
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
     //   parameters["question_id"] = questionId as AnyObject
        var mealOptions = ""
        var recipeOptions = ""
        for i in 0..<recipeCollectionList.count{
            let selectedElement = recipeCollectionList[i].recipe.filter({$0.selected == 1})
            if selectedElement.count != 0{
                mealOptions = mealOptions == "" ? "\(recipeCollectionList[i].id ?? "")" : "\(mealOptions),\(recipeCollectionList[i].id ?? "")"
                recipeOptions = recipeOptions == "" ? "\(selectedElement[0].option_id ?? 0)" : "\(recipeOptions),\(selectedElement[0].option_id ?? 0)"
            }
        }
        parameters["meal_id"] = mealOptions as AnyObject
        parameters["recipe_id"] = recipeOptions as AnyObject
        parameters["meal_no"] = "\(mealNumber)" as AnyObject
        
        return parameters
    }
}
extension RecipeSelectionCheckInVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return recipeCollectionList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ExercisesCollectionVeiwCell
        cell.nameLbl.text = recipeCollectionList[indexPath.item].name
        cell.selectedLbl.backgroundColor = recipeCollectionList[indexPath.item].selected == true ? UIColor(named: "BGColor") : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
        cell.nameLbl.textColor = recipeCollectionList[indexPath.item].selected == true ? .black : .lightGray
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<recipeCollectionList.count{
            recipeCollectionList[i].selected = i == indexPath.item ? true : false
        }
        DispatchQueue.main.async {
            self.nameCollectionView.reloadData()
            self.index = indexPath.item
            self.recipeTableViewHeight.constant = CGFloat(110 * self.recipeCollectionList[self.index].recipe.count)
            self.recipeTableView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.text = recipeCollectionList[indexPath.item].name
        label.sizeToFit()
        return CGSize(width: label.frame.size.width+10, height: nameCollectionView.frame.size.height)
    }
    
    
}
extension RecipeSelectionCheckInVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipeCollectionList[index].recipe.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! RecipeListTableCell
        cell.selectionStyle = .none
        let data = recipeCollectionList[index].recipe[indexPath.row]
        cell.recipeName.text = data.option
            let transformer = SDImageResizingTransformer(size: CGSize(width: 180, height: 200), scaleMode: .aspectFill)
            cell.recipeImg.sd_setImage(with:URL(string: self.recipeCollectionList[self.index].recipe[indexPath.row].thumbnail_img ?? ""), placeholderImage: UIImage(named: "ic_recipe_list_pp"), context: [.imageTransformer: transformer])
        cell.bgView.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        cell.viewDetailsBtn.tag = indexPath.row
        cell.viewDetailsBtn.addTarget(self, action: #selector(viewDetailsBtnAction(_:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<recipeCollectionList[index].recipe.count{
            recipeCollectionList[index].recipe[i].selected = i == indexPath.row ? 1 : 0
        }
        recipeTableView.reloadData()
    }
    @objc func viewDetailsBtnAction(_ sender : UIButton){
        selectedIndex = sender.tag
        let VC = RecipeDetailsVC.instantiate(fromAppStoryboard: .SideMenu)
        VC.optionId = "\(recipeCollectionList[index].recipe[sender.tag].option_id ?? 0)"
        VC.fromQuestionnaire = true
        VC.delegate = self
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
   
extension RecipeSelectionCheckInVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if #available(iOS 13, *) {
            (scrollView.subviews[(scrollView.subviews.count - 1)].subviews[0]).backgroundColor = UIColor(named: "BGColor") // #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1) //verticalIndicator
        } else {
            if let verticalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 1)] as? UIImageView) {
                verticalIndicator.backgroundColor = UIColor(named: "BGColor") // #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1)
            }
        }
    }
}
extension RecipeSelectionCheckInVC: SelectionDelegate {
    func selectedRecipe(_: String) {
      for i in 0..<recipeCollectionList[index].recipe.count{
            recipeCollectionList[index].recipe[i].selected = i == selectedIndex ? 1 : 0
        }
        DispatchQueue.main.async {
            self.recipeTableView.reloadData()
        }
    }
}
