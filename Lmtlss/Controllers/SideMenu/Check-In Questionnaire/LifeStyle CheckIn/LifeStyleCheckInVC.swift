//
//  LifeStyleCheckInVC.swift
//  Lmtlss
//
//  Created by apple on 23/07/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire

class LifeStyleCheckInVC: UIViewController {
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var activityTVHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var activityTableView: UITableView!
    var lifestyleActivitiesArr = [QuestionaryField]()
    var questionId = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    private func configureUI(){
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        activityTableView.register(nib, forCellReuseIdentifier: "cell")
        activityTableView.delegate = self
        activityTableView.dataSource = self
        activityTableView.separatorStyle = .none
        for i in 0..<checkInQuestionnaire.count{
            if checkInQuestionnaire[i]["question_id"] as? Int == 37{
                questionTitleLbl.text = checkInQuestionnaire[i]["question"] as? String ?? ""
                questionId = checkInQuestionnaire[i]["question_id"] as? Int ?? 0
                if let option = checkInQuestionnaire[i]["option"] as? [[String:Any]] {
                    for i in 0..<option.count{
                        self.lifestyleActivitiesArr.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
                    }
                    activityTableView.reloadData()
                }
            }
        }
    }
    func validateScreenData() -> Bool{
         
           let selectedElement = lifestyleActivitiesArr.filter({$0.selected == 1})
           if selectedElement.count == 0{
               alert(Constants.AppName, message: "Please Select any 1 Option", view: self)
            return true

           }
           return false
       }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.saveCheckInQuestionnaire
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["question_id"] = questionId as AnyObject
        let options = getSelectedOptions()
        parameters["option_id"] = options as AnyObject
        parameters["meal_no"] = "\(mealNumber)" as AnyObject
        
        
        return parameters
    }
     func getSelectedOptions() -> String
     {
        let selectedArr : [String]  = self.lifestyleActivitiesArr.filter { (objField) -> Bool in
            return objField.selected == 1
        }.map { (objField) -> String in
            return "\(String(describing: objField.option_id!))"
        }
        return selectedArr.joined(separator: ",")
     }

}

extension LifeStyleCheckInVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lifestyleActivitiesArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        let data = lifestyleActivitiesArr[indexPath.row]
        cell.titleLbl.text = data.option
        cell.subTitleLbl.text = data.description
        if data.example != ""{
            let boldAttribute = [
               NSAttributedString.Key.font: UIFont(name: "Poppins-SemiBold", size: 14.0)!, NSAttributedString.Key.foregroundColor : UIColor.black
            ]
            let regularAttribute = [
               NSAttributedString.Key.font: UIFont(name: "Poppins-Regular", size: 14.0)!
            ]
            let boldText = NSAttributedString(string: "E.g.", attributes: boldAttribute)
            let regularText = NSAttributedString(string: "- \(data.example ?? "")", attributes: regularAttribute)
            let newString = NSMutableAttributedString()
            newString.append(boldText)
            newString.append(regularText)
            cell.exampleLbl.attributedText = newString
        }
        cell.bgImg.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        DispatchQueue.main.async {
            self.activityTVHeightConstraint.constant = self.activityTableView.contentSize.height
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<lifestyleActivitiesArr.count{
            lifestyleActivitiesArr[i].selected = i == indexPath.row ? 1 : 0
        }
        activityTableView.reloadData()
    }
    
}
