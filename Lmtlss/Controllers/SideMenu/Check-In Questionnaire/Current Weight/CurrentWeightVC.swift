//
//  CurrentWeightVC.swift
//  Lmtlss
//
//  Created by apple on 21/07/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire

class CurrentWeightVC: UIViewController {
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var weightSlider: CustomSlider!
    @IBOutlet weak var lbsKgImg: UIImageView!
    var lbsKG = "kg"
    var questionId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    func configureUI(){
        for i in 0..<checkInQuestionnaire.count{
            if checkInQuestionnaire[i]["question_id"] as? Int == 30{
                questionId = checkInQuestionnaire[i]["question_id"] as? Int ?? 0
                questionTitleLbl.text = checkInQuestionnaire[i]["question"] as? String ?? ""
                if let option = checkInQuestionnaire[i]["option"] as? [[String:Any]] {
                    let weightUnit = option[0]["unit"] as? String ?? ""
                    lbsKgImg.image = weightUnit == "lbs" ? #imageLiteral(resourceName: "kg") : #imageLiteral(resourceName: "lbs")
                    lbsKG = weightUnit == "" ? "kg" : weightUnit
                    let weight = option[0]["option"] as? String ?? ""
                    weightSlider.value = NumberFormatter().number(from: weight)?.floatValue ?? 0.0
                    weightLbl.text = getWeight(slider: weightSlider)
                }
            }
        }
    }
    
    func validateScreenData() -> Bool{
        
        
        if weightLbl.text == "0" || weightLbl.text == "0.00"{
            alert(Constants.AppName, message: "Please Select weight", view: self)
            return true
        }
        return false
    }
    
    @IBAction func weightSliderValueChanged(_ sender: CustomSlider) {
        weightLbl.text = getWeight(slider: weightSlider)
    }
    @IBAction func lbsAndKgBtnAction(_ sender: UIButton) {
        //   lbsKgImg.image = #imageLiteral(resourceName: "lbs")
        if sender.tag == 1{
            lbsKgImg.image = #imageLiteral(resourceName: "kg")
            lbsKG = "lbs"
        }else if sender.tag == 2{
            lbsKgImg.image = #imageLiteral(resourceName: "lbs")
            lbsKG = "kg"
            
        }
        weightLbl.text = getWeight(slider: weightSlider)
        
    }
    func getWeight(slider : CustomSlider) -> String
    {
        if lbsKgImg.image == #imageLiteral(resourceName: "kg")
        {
            let myweight : Int = Int(slider.value)
            let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.kilograms)
            let lbWeight = kgWeight.converted(to: UnitMass.pounds)
            let weight = String(format: "%.2f", lbWeight.value)
            
            return weight
        }
        else
        {
            return "\(Int(slider.value))"
        }
    }
    
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
       // successHandler()
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.saveCheckInQuestionnaire
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
        
    }
    
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["question_id"] = questionId as AnyObject
        let weight = "\(Int(weightSlider.value))"
        parameters["option_id"] = "\(weight)" as AnyObject
        parameters["unit"] = "\(lbsKG)" as AnyObject
        parameters["meal_no"] = "\(mealNumber)" as AnyObject
        
        return parameters
    }
}
