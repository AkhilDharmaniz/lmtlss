//
//  UploadImagesVC.swift
//  Lmtlss
//
//  Created by apple on 22/07/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire

class UploadImagesVC: UIViewController {
    
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet var imgUploads: [UIImageView]!
    @IBOutlet var btnCancel: [UIButton]!
    @IBOutlet var imgCancel: [UIImageView]!
    var picker:ImagePicker?
    var selectedImg = 1
    var questionId = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    func configureUI(){
        picker = ImagePicker(presentationController: self, delegate: self)
        for i in 0..<imgUploads.count{
            if imgUploads[i].image == nil{
                btnCancel[i].isHidden = true
                imgCancel[i].isHidden = true
            }else{
                btnCancel[i].isHidden = false
                imgCancel[i].isHidden = false
            }
        }
        for i in 0..<checkInQuestionnaire.count{
            if checkInQuestionnaire[i]["question_id"] as? Int == 33{
                questionId = checkInQuestionnaire[i]["question_id"] as? Int ?? 0
                questionTitleLbl.text = checkInQuestionnaire[i]["question"] as? String ?? ""
                if let frontImg = checkInQuestionnaire[i]["front_img"] as? String {
                   self.imgUploads[0].sd_setImage(with: URL(string: frontImg))
                    if frontImg != ""{
                        btnCancel[0].isHidden = false
                        imgCancel[0].isHidden = false
                    }
                }
                if let sideImg = checkInQuestionnaire[i]["side_img"] as? String {
                   self.imgUploads[1].sd_setImage(with: URL(string: sideImg))
                    if sideImg != ""{
                        btnCancel[1].isHidden = false
                        imgCancel[1].isHidden = false
                    }
                }
                if let rearImg = checkInQuestionnaire[i]["rear_img"] as? String {
                   self.imgUploads[2].sd_setImage(with: URL(string: rearImg))
                    if rearImg != ""{
                        btnCancel[2].isHidden = false
                        imgCancel[2].isHidden = false
                    }
                    
                }
            }
        }
        
    }
    func validateScreenData() -> Bool{
        
        if imgCancel[0].isHidden == true {
            alert(Constants.AppName, message: "Please Upload Front image", view: self)
            return true
        }else if imgCancel[1].isHidden == true {
            alert(Constants.AppName, message: "Please Upload Side image", view: self)
            return true
        }else if imgCancel[2].isHidden == true {
            alert(Constants.AppName, message: "Please Upload Rear image", view: self)
            return true
        }
        return false
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        DispatchQueue.main.async {
        IJProgressView.shared.showProgressView()
        }
        let url = Constants.baseURL + Constants.checkInUploadImg
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in self.generatingParameters() {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if self.imgUploads[0].image != nil {
                let imageStr =  self.imgUploads[0].image!.jpegData(compressionQuality: 0.3)!
                multipartFormData.append(imageStr, withName: "front_img", fileName: "\(String.random(length: 10)).jpg", mimeType: "")
            }
            if self.imgUploads[1].image != nil {
                let imageStr =  self.imgUploads[1].image!.jpegData(compressionQuality: 0.3)!
                multipartFormData.append(imageStr, withName: "side_img", fileName: "\(String.random(length: 10)).jpg", mimeType: "")
            }
            if self.imgUploads[2].image != nil {
                let imageStr =  self.imgUploads[2].image!.jpegData(compressionQuality: 0.3)!
                multipartFormData.append(imageStr, withName: "rear_img", fileName: "\(String.random(length: 10)).jpg", mimeType: "")
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers, interceptor: nil, fileManager: .default)
            
            .uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON { (response) in
                IJProgressView.shared.hideProgressView()
                switch response.result {
                case .success(let value):
                    if let JSON = value as? [String: Any] {
                        let displayMessage = JSON["responseMessage"] as? String
                        if let code = JSON["responseCode"] as? Int{
                            if code == 200{
                                successHandler()
                            }else{
                                alert(Constants.AppName, message: displayMessage ?? "", view: self)
                            }
                        }
                        if let status = JSON["status"] as? Int, status == 401{
                            logOut(controller: self)
                        }
                    }
                case .failure(let error):
                    // error handling
                    alert(Constants.AppName, message: error.localizedDescription, view: self)
                    break
                }
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["meal_no"] = "\(mealNumber)" as AnyObject
        
        return parameters
    }
    @IBAction func addImageBtnAction(_ sender: UIButton) {
        selectedImg = sender.tag
        picker?.present(from: sender)
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.imgUploads[sender.tag-1].image = nil
        self.btnCancel[sender.tag-1].isHidden = true
        self.imgCancel[sender.tag-1].isHidden = true
    }
}
//MARK:- Image Picker Delegate

extension UploadImagesVC: ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        if image != nil{
            self.imgUploads[selectedImg-1].image = image
            self.btnCancel[selectedImg-1].isHidden = false
            self.imgCancel[selectedImg-1].isHidden = false
        }
      //  self.imageStr =  self.profileImageView.image!.jpegData(compressionQuality: 0.3)!
    }
}
