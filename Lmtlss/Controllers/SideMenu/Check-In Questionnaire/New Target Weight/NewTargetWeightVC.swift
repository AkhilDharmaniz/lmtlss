//
//  NewTargetWeightVC.swift
//  Lmtlss
//
//  Created by apple on 22/07/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire

class NewTargetWeightVC: UIViewController {
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var targetWeightSlider: CustomSlider!
    @IBOutlet weak var targetWeightLbl: UILabel!
    @IBOutlet weak var deficitNSuprlusImg: UIImageView!
    var surplusDeficit = "deficit"
    var questionId = 0
    var currentWeight = 0
    var targetWeight = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    func configureUI(){
        for i in 0..<checkInQuestionnaire.count{
            if checkInQuestionnaire[i]["question_id"] as? Int == 30{
                if let option = checkInQuestionnaire[i]["option"] as? [[String:Any]] {
                    let curentWeightStr = option[0]["option"] as? String ?? ""
                    currentWeight = NumberFormatter().number(from: curentWeightStr)?.intValue ?? 0
                }
            }
            if checkInQuestionnaire[i]["question_id"] as? Int == 32{
                questionId = checkInQuestionnaire[i]["question_id"] as? Int ?? 0
                questionTitleLbl.text = checkInQuestionnaire[i]["question"] as? String ?? ""
                if let option = checkInQuestionnaire[i]["option"] as? [[String:Any]] {
                    let targetWeightUnit = option[0]["unit"] as? String ?? ""
                    surplusDeficit = targetWeightUnit == "" ? "deficit" : targetWeightUnit
                    deficitNSuprlusImg.image = targetWeightUnit == "surplus" ? #imageLiteral(resourceName: "surplus") : #imageLiteral(resourceName: "deficit")
                    targetWeightLbl.text = option[0]["option"] as? String ?? "" == "" ? "0" : option[0]["option"] as? String ?? ""
                    targetWeightSlider.value = NumberFormatter().number(from: targetWeightLbl.text!)?.floatValue ?? 0.0
                    
                }
            }
        }
    }
    func validateScreenData() -> Bool{
        
        targetWeight = Int(targetWeightSlider.value)
        if targetWeightLbl.text == "0"{
            alert(Constants.AppName, message: "Please Select Target weight", view: self)
            return true
        }else if targetWeight == currentWeight{
            alert(Constants.AppName, message: "Current weight and the Target weight can't be same. Please change any one of both.", view: self)
            return true
        }
        return false
    }

    @IBAction func weightSliderValueChanged(_ sender: CustomSlider) {
        let currentValue = Int(sender.value)
         targetWeightLbl.text = "\(currentValue)"
        if currentWeight > currentValue{
           deficitNSuprlusImg.image = #imageLiteral(resourceName: "deficit")
           surplusDeficit = "deficit"
        }else{
            deficitNSuprlusImg.image = #imageLiteral(resourceName: "surplus")
            surplusDeficit = "surplus"
        }
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.saveCheckInQuestionnaire
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["question_id"] = questionId as AnyObject
        parameters["option_id"] = "\(targetWeightLbl.text ?? "0")" as AnyObject
        parameters["unit"] = "\(surplusDeficit)" as AnyObject
        parameters["meal_no"] = "\(mealNumber)" as AnyObject
        
        return parameters
    }
}
