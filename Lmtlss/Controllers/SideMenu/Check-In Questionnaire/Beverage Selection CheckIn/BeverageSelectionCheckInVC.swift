//
//  BeverageSelectionCheckInVC.swift
//  Lmtlss
//
//  Created by apple on 05/08/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire

class BeverageSelectionCheckInVC: UIViewController {
    
    @IBOutlet weak var beverageTableView: UITableView!
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var highCalorieBtn: UIButton!
    @IBOutlet weak var lowCalorieBtn: UIButton!
    
    var questionId = 0
    var highLowSelection = 1
    var options = [QuestionaryField]()
    var optionsArr = [BeverageQuestionnaireField]()
    var count = 0

    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    
    private func configureUI(){
        let nib = UINib(nibName: "BeverageCategoryCell", bundle: nil)
        beverageTableView.register(nib, forCellReuseIdentifier: "cell")
        beverageTableView.delegate = self
        beverageTableView.dataSource = self
        beverageTableView.separatorStyle = .none
        for i in 0..<checkInQuestionnaire.count{
            if checkInQuestionnaire[i]["question_id"] as? Int == 48{
                questionTitleLbl.text = checkInQuestionnaire[i]["question"] as? String ?? ""
                questionId = checkInQuestionnaire[i]["question_id"] as? Int ?? 0
                
                if let option = checkInQuestionnaire[i]["option"] as? [String:Any]{
                    var lowCalArr = [BeverageCategoryField]()
                    if let lowCal = option["low"] as? [[String:Any]]{
                        for i in 0..<lowCal.count{
                            var drinkItemsArr = [BeverageSubCategoryField]()
                            if let drinkItems = lowCal[i]["drink_items"] as? [[String:Any]]{
                                for l in 0..<drinkItems.count{
                                drinkItemsArr.append(BeverageSubCategoryField(option_id: drinkItems[l]["option_id"] as? Int ?? 0, option: drinkItems[l]["option"] as? String ?? "", description: drinkItems[l]["option"] as? String ?? "", example: drinkItems[l]["option"] as? String ?? "", selected: drinkItems[l]["selected"] as? Int ?? 0))
                                }
                            }
                            lowCalArr.append(BeverageCategoryField(drink_category_name: lowCal[i]["drink_category_name"] as? String ?? "", drink_items: drinkItemsArr, selectedState: false))
                        }
                    }
                    var highCalArr = [BeverageCategoryField]()
                    if let highCal = option["high"] as? [[String:Any]]{
                        for i in 0..<highCal.count{
                            var drinkItemsArr = [BeverageSubCategoryField]()
                            if let drinkItems = highCal[i]["drink_items"] as? [[String:Any]]{
                                for l in 0..<drinkItems.count{

                                drinkItemsArr.append(BeverageSubCategoryField(option_id: drinkItems[l]["option_id"] as? Int ?? 0, option: drinkItems[l]["option"] as? String ?? "", description: drinkItems[l]["option"] as? String ?? "", example: drinkItems[l]["option"] as? String ?? "", selected: drinkItems[l]["selected"] as? Int ?? 0))
                                }
                            }
                            highCalArr.append(BeverageCategoryField(drink_category_name: highCal[i]["drink_category_name"] as? String ?? "", drink_items: drinkItemsArr, selectedState: false))
                        }
                    }
                    optionsArr.append(BeverageQuestionnaireField(high: highCalArr, low: lowCalArr))
//                    for i in 0..<option.count{
//                        self.options.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
//                    }
                    beverageTableView.reloadData()
                }
            }
        }
    }
    func validateScreenData() -> Bool{
         
        var selectedHighCalArr = [BeverageSubCategoryField]()
        for i in 0..<self.optionsArr[0].high.count{
            for j in 0..<self.optionsArr[0].high[i].drink_items.count{
                if self.optionsArr[0].high[i].drink_items[j].selected == 1{
                    let selectedData = self.optionsArr[0].high[i].drink_items[j]
                    selectedHighCalArr.append(BeverageSubCategoryField(option_id: selectedData.option_id, option: selectedData.option, description: selectedData.description, example: selectedData.example, selected: selectedData.selected))
                }
            }
        }
        var selectedLowCalArr = [BeverageSubCategoryField]()
        for i in 0..<self.optionsArr[0].low.count{
            for j in 0..<self.optionsArr[0].low[i].drink_items.count{
                if self.optionsArr[0].low[i].drink_items[j].selected == 1{
                    let selectedData = self.optionsArr[0].low[i].drink_items[j]
                    selectedLowCalArr.append(BeverageSubCategoryField(option_id: selectedData.option_id, option: selectedData.option, description: selectedData.description, example: selectedData.example, selected: selectedData.selected))
                }
            }
        }
        count = selectedHighCalArr.count + selectedLowCalArr.count
        if count == 0{
            alert(Constants.AppName, message: "Please Select maximun 2 Options", view: self)
            return true
        }
           return false
       }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
          
          IJProgressView.shared.showProgressView()
          let signInUrl = Constants.baseURL + Constants.saveDrinks
           
          var token = UserDefaults.standard.value(forKey: "token") as? String
          token = "Bearer " + token!
          let headers : HTTPHeaders = ["Authorization":token ?? ""]
          AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
              IJProgressView.shared.hideProgressView()
              
              let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
              if status == 200{
                  successHandler()
              }else{
                  IJProgressView.shared.hideProgressView()
                  alert(Constants.AppName, message: message, view: self)
              }
            }
              if let status = response["status"] as? Int, status == 401{
                  logOut(controller: self)
              }
          }) { (error) in
              IJProgressView.shared.hideProgressView()
              alert(Constants.AppName, message: error.localizedDescription, view: self)
              
          }
      }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        //     parameters["question_id"] = questionId as AnyObject
        let optionsStr = getSelectedOptions()
        parameters["drink_id"] = optionsStr as AnyObject
        parameters["meal_no"] = "\(mealNumber)" as AnyObject
        
        
        return parameters
    }
       func getSelectedOptions() -> String
       {
        var selectedHighCalArr = [BeverageSubCategoryField]()
        for i in 0..<self.optionsArr[0].high.count{
            for j in 0..<self.optionsArr[0].high[i].drink_items.count{
                if self.optionsArr[0].high[i].drink_items[j].selected == 1{
                    let selectedData = self.optionsArr[0].high[i].drink_items[j]
                    selectedHighCalArr.append(BeverageSubCategoryField(option_id: selectedData.option_id, option: selectedData.option, description: selectedData.description, example: selectedData.example, selected: selectedData.selected))
                }
            }
        }
        var selectedLowCalArr = [BeverageSubCategoryField]()
        for i in 0..<self.optionsArr[0].low.count{
            for j in 0..<self.optionsArr[0].low[i].drink_items.count{
                if self.optionsArr[0].low[i].drink_items[j].selected == 1{
                    let selectedData = self.optionsArr[0].low[i].drink_items[j]
                    selectedLowCalArr.append(BeverageSubCategoryField(option_id: selectedData.option_id, option: selectedData.option, description: selectedData.description, example: selectedData.example, selected: selectedData.selected))
                }
            }
        }
        
        let selectedBothArr = selectedLowCalArr + selectedHighCalArr
        let selectedArr : [String]  = selectedBothArr.filter { (objField) -> Bool in
            return objField.selected == 1
        }.map { (objField) -> String in
            return "\(String(describing: objField.option_id!))"
        }
        return selectedArr.joined(separator: ",")
       }

   
    @IBAction func highLowCalorieBtnAction(_ sender: UIButton) {
        highLowSelection = sender.tag
        if sender.tag == 1{
            highCalorieBtn.setTitleColor(.white, for: .normal)
            highCalorieBtn.backgroundColor = UIColor(named: "BGColor") //#colorLiteral(red: 0.2081934214, green: 0.8793118, blue: 0.9418017268, alpha: 1)
            lowCalorieBtn.setTitleColor(.black, for: .normal)
            lowCalorieBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }else{
            lowCalorieBtn.setTitleColor(.white, for: .normal)
            lowCalorieBtn.backgroundColor = UIColor(named: "BGColor")//#colorLiteral(red: 0.2081934214, green: 0.8793118, blue: 0.9418017268, alpha: 1)
            highCalorieBtn.setTitleColor(.black, for: .normal)
            highCalorieBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        beverageTableView.reloadData()
    }
   

}
extension BeverageSelectionCheckInVC: UITableViewDelegate,UITableViewDataSource{
    
    //MARK::- TableView Delegate,Datasource Method(s)
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return highLowSelection == 1 ? optionsArr[0].high.count : optionsArr[0].low.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let mainData = highLowSelection == 1 ? optionsArr[0].high[indexPath.row] : optionsArr[0].low[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! BeverageCategoryCell
        cell.configureCategoryCell(data: mainData)
        cell.selectionStyle = .none
        cell.dropBtn.tag = indexPath.row
        cell.dropBtn.addTarget(self, action: #selector(addSubCategory(sender:)), for: .touchUpInside)
        cell.selectedCat = { mainCat, subCat, selectedIndex, selected in
            var selectedHighCalArr = [BeverageSubCategoryField]()
            for i in 0..<self.optionsArr[0].high.count{
                for j in 0..<self.optionsArr[0].high[i].drink_items.count{
                    if self.optionsArr[0].high[i].drink_items[j].selected == 1{
                        let selectedData = self.optionsArr[0].high[i].drink_items[j]
                        selectedHighCalArr.append(BeverageSubCategoryField(option_id: selectedData.option_id, option: selectedData.option, description: selectedData.description, example: selectedData.example, selected: selectedData.selected))
                    }
                }
            }
            var selectedLowCalArr = [BeverageSubCategoryField]()
            for i in 0..<self.optionsArr[0].low.count{
                for j in 0..<self.optionsArr[0].low[i].drink_items.count{
                    if self.optionsArr[0].low[i].drink_items[j].selected == 1{
                        let selectedData = self.optionsArr[0].low[i].drink_items[j]
                        selectedLowCalArr.append(BeverageSubCategoryField(option_id: selectedData.option_id, option: selectedData.option, description: selectedData.description, example: selectedData.example, selected: selectedData.selected))
                    }
                }
            }
            self.count = selectedHighCalArr.count + selectedLowCalArr.count
            if selected == false{
                if self.highLowSelection == 1{
                    self.optionsArr[0].high[indexPath.row].drink_items[selectedIndex].selected = 0
                }else{
                    self.optionsArr[0].low[indexPath.row].drink_items[selectedIndex].selected = 0
                }
                let mainData = self.highLowSelection == 1 ? self.optionsArr[0].high[indexPath.row] : self.optionsArr[0].low[indexPath.row]
                cell.configureCategoryCell(data: mainData)
            }else{
                if self.count < 2{
                    if self.highLowSelection == 1{
                        self.optionsArr[0].high[indexPath.row].drink_items[selectedIndex].selected = 1
                    }else{
                        self.optionsArr[0].low[indexPath.row].drink_items[selectedIndex].selected = 1
                    }
                    let mainData = self.highLowSelection == 1 ? self.optionsArr[0].high[indexPath.row] : self.optionsArr[0].low[indexPath.row]
                    cell.configureCategoryCell(data: mainData)
                }else{
                    DispatchQueue.main.async {
                        alert(Constants.AppName, message: "Please Select maximun 2 Options", view: self)
                    }
                    return
                }
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        var height = 0.0
        let data = highLowSelection == 1 ? optionsArr[0].high[indexPath.row] : optionsArr[0].low[indexPath.row]
        if data.selectedState{
            height = Double(data.drink_items.count) * 55.0
        }
        return CGFloat(75 + height)
    }
    
    
    @objc func addSubCategory(sender: UIButton){
        if highLowSelection == 1 {
            self.optionsArr[0].high[sender.tag].selectedState = !self.optionsArr[0].high[sender.tag].selectedState
        }else{
            self.optionsArr[0].low[sender.tag].selectedState = !self.optionsArr[0].low[sender.tag].selectedState
        }
       
        beverageTableView.beginUpdates()
        beverageTableView.reloadSections(NSIndexSet(index: 0) as IndexSet, with: UITableView.RowAnimation.none)
        beverageTableView.endUpdates()
    }
}
