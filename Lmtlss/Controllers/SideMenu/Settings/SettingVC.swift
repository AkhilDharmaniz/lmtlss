//
//  SettingVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 21/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class SettingCell: UITableViewCell{
    @IBOutlet weak var settingImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var pushSwitch: UISwitch!
    
}
class SettingVC: UIViewController {
    @IBOutlet weak var settingTableView: UITableView!
    @IBOutlet weak var navView: UIView!
    
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    var pushStatus = 1
    
    var settingListArray = [("My Referrals",#imageLiteral(resourceName: "referal")),("My Program's",#imageLiteral(resourceName: "program")),("Push notifications",#imageLiteral(resourceName: "noti1")),("About Us",#imageLiteral(resourceName: "about")),("Terms",#imageLiteral(resourceName: "term")),("Support",#imageLiteral(resourceName: "support"))]
    //,("Notifications",#imageLiteral(resourceName: "noti1"))
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getProfile()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
    }
    override func viewDidLayoutSubviews() {
        self.profileImageView.layer.cornerRadius = (UIScreen.main.bounds.size.width * 0.17) / 2
        self.profileImageView.layer.masksToBounds = true
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Settings"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.isHidden = true
        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.isSupport = true
 //       myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        settingTableView.delegate = self
        settingTableView.dataSource = self
        tableViewHeight.constant = CGFloat(60 * self.settingListArray.count)
    }
    func getProfile(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.getUserProfile
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.nameLbl.text = responseData["name"] as? String ?? ""
                            self.emailLbl.text = responseData["email"] as? String ?? ""
                            self.profileImageView.sd_setImage(with: URL(string: responseData["image"] as? String ?? ""), placeholderImage: UIImage(named: "user_profile"))
                            self.pushStatus = responseData["push_status"] as? Int ?? 0
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.settingTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    @IBAction func profileBtnAction(_ sender: UIButton) {
        let VC = ProfileVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    func pushSwitchChanged(){
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.pushNotificationStatus
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        let params = ["push_status": pushStatus]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params:params, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
          if let status = response["responseCode"] as? Int {
            if status == 200{
               
            }else{
                alert(Constants.AppName, message: message, view: self)
            }
          }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
}

extension SettingVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return settingListArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! SettingCell
        cell.titleLbl.text = settingListArray[indexPath.row].0
        cell.settingImg.image = settingListArray[indexPath.row].1
        cell.pushSwitch.isHidden = indexPath.row == 2 ? false : true
        cell.pushSwitch.setOn(pushStatus == 1 ? true : false, animated: false)
        cell.pushSwitch.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)

        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 1{
            let VC = MyProgramsVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }else if indexPath.row == 3 || indexPath.row == 4{
            let browserVC = BrowserVC.instantiate(fromAppStoryboard: .SideMenu)
            browserVC.navTitle = settingListArray[indexPath.row].0
            self.navigationController?.pushViewController(browserVC, animated: true)
        }else if indexPath.row == 5{
            let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }
    
    }
    @objc func switchChanged(mySwitch: UISwitch) {
        pushStatus = mySwitch.isOn == true ? 1 : 0
        pushSwitchChanged()
    }
}
//MARK:- NavBar Delegate Method(s)

extension SettingVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
//        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
//        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
