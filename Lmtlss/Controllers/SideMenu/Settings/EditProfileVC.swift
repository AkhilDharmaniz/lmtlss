//
//  EditProfileVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 21/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class EditProfileVC: UIViewController {

    @IBOutlet weak var cameraBtn: UIButton!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var countryTF: UITextField!
    @IBOutlet weak var heightTF: UITextField!
    @IBOutlet weak var startingWeightTF: UITextField!
    @IBOutlet weak var currentWeightTF: UITextField!
    @IBOutlet weak var navView: UIView!
    var picker:ImagePicker?
    var imageStr = Data()
    var flagsArray = [FlagDetailsField]()
    let countryPicker = UIPickerView()
    var heightUnit = ""
    var weightUnit = ""
    var countryStr = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        configureCountryPicker()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureNavUI()
    }
    override func viewDidLayoutSubviews() {
        self.profileImageView.layer.cornerRadius = (UIScreen.main.bounds.size.height * 0.17) / 2
        self.profileImageView.layer.masksToBounds = true
        self.cameraBtn.layer.cornerRadius = (UIScreen.main.bounds.size.width * 0.11) / 2
        self.cameraBtn.layer.masksToBounds = true
    }
    func configureNavUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Edit Profile"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func configureUI(){
        picker = ImagePicker(presentationController: self, delegate: self)
        self.getProfile()
        self.countryPicker.delegate = self
        self.countryPicker.dataSource = self
        self.countryTF.inputView = countryPicker
        
    }
    
    func configureCountryPicker(){
        let bundle = Bundle.main
        let path = bundle.path(forResource: "countriesList", ofType: "json")
        let  info = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
        let data = info!.data(using: String.Encoding.utf8)
        let json: AnyObject? = try? JSONSerialization.jsonObject(with: data!, options: []) as AnyObject
        self.flagsArray.removeAll()
        if let j: AnyObject = json {
            if let flagArray = j as? [[String:Any]]{
                for flag in flagArray{
                    self.flagsArray.append(FlagDetailsField(name: flag["name"] as? String ?? "", unicode: flag["unicode"] as? String ?? "", code: flag["code"] as? String ?? "", emoji: flag["emoji"] as? String ?? ""))
                }
                self.flagsArray.sort { $0.name! < $1.name! }
            }
        }
    }
    func getProfile(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.getUserProfile
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.nameTF.text = responseData["name"] as? String ?? ""
                        self.emailLbl.text = "\(responseData["email"] as? String ?? "")"

                            self.profileImageView.sd_setImage(with: URL(string: responseData["image"] as? String ?? ""), placeholderImage: UIImage(named: "user_profile"))
                           let country = responseData["country"] as? String ?? ""
                           if country != ""{
                               if self.flagsArray.count != 0{
                                   for i in 0..<self.flagsArray.count{
                                       if self.flagsArray[i].name == country{
                                           self.countryTF.text = "\(self.flagsArray[i].emoji ?? "") \(self.flagsArray[i].name ?? "")"
                                        self.countryStr = country
                                        self.countryPicker.selectRow(i, inComponent: 0, animated: false)
                                       }
                                   }
                               }
                           }
                            
                            self.heightUnit = responseData["height_unit"] as? String ?? ""
                            let height = responseData["height"] as? String ?? ""
                            self.heightTF.text = "\(self.getHeight(slider: height)) \(self.heightUnit)"
                            
                            self.weightUnit = responseData["weight_unit"] as? String ?? ""
                            let weight = responseData["starting_weight"] as? String ?? ""
                            self.startingWeightTF.text = "\(self.getWeight(slider: weight)) \(self.weightUnit)"
//                            self.heightTF.text = responseData["height"] as? String ?? ""
//                            self.startingWeightTF.text = responseData["starting_weight"] as? String ?? ""
                            let currentWeight = responseData["current_weight"] as? String ?? ""
                            if currentWeight == ""{
                                self.currentWeightTF.text = "\(self.getWeight(slider: weight)) \(self.weightUnit)"
                            }else{
                            //    self.currentWeightTF.text = responseData["current_weight"] as? String ?? ""
                                self.currentWeightTF.text = "\(self.getWeight(slider: currentWeight)) \(self.weightUnit)"
                            }
                       //     self.currentWeightTF.text = responseData["current_weight"] as? String ?? ""
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
                
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func getHeight(slider: String) -> String
         {
             if heightUnit == "ft"
             {
              let myheight : Int = Int(slider) ?? 0
                 let heightCm = Measurement(value: Double(myheight), unit: UnitLength.centimeters)
                 let heightFeet = heightCm.converted(to: UnitLength.feet)
                 let numInches = heightCm.converted(to: UnitLength.inches)
                 let heightInch = numInches.value.truncatingRemainder(dividingBy: 12);
                 
                 return "\(Int(heightFeet.value))" + "'" + " \(Int(round(heightInch)))" + "''"
             }
             else
             {
              return "\(Int(slider) ?? 0)"
             }
      }
      func getWeight(slider : String) -> String
      {
        if weightUnit == "lbs"
        {
            let myweight : Double = Double(slider) ?? 0.0
            let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.kilograms)
            let lbWeight = kgWeight.converted(to: UnitMass.pounds)
            let weight = String(format: "%.2f", lbWeight.value)
            
            return weight
//          if weightUnit == "lbs"
//          {
//              let myweight : Int = Int(slider) ?? 0
//              let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.kilograms)
//              let lbWeight = kgWeight.converted(to: UnitMass.pounds)
//              let weight = String(format: "%.2f", lbWeight.value)
//
//              return weight
          }
          else
          {
              return "\(Int(slider) ?? 0)"
          }
      }
    @IBAction func cameraBtnAction(_ sender: UIButton) {
        picker?.present(from: sender)
    }
    
    @IBAction func submitBtnAction(_ sender: UIButton) {
        let textCount = nameTF.text!.trimWhiteSpace
        if textCount.count == 0{
            alert(Constants.AppName, message: "Please enter name", view: self)
        }
//        else if countryTF.text!.isEmpty{
//            alert(Constants.AppName, message: "Please select country", view: self)
//        }
        else{
            if Reachability.isConnectedToNetwork() == true {
                editProfile()
            }else{
                alert(Constants.AppName, message: "Check internet connection", view: self)
            }
        }
    }
    func editProfile(){
        IJProgressView.shared.showProgressView()
        let url = Constants.baseURL + Constants.editUserProfle
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
        AF.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in self.generatingParameters() {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            if self.profileImageView.image != UIImage(named: "user_profile") {
                self.imageStr =  self.profileImageView.image!.jpegData(compressionQuality: 0.3)!
                multipartFormData.append(self.imageStr, withName: "image", fileName: "\(String.random(length: 10)).jpg", mimeType: "")
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers, interceptor: nil, fileManager: .default)
            
            .uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON { (response) in
                IJProgressView.shared.hideProgressView()
                switch response.result {
                case .success(let value):
                    if let JSON = value as? [String: Any] {
                        let displayMessage = JSON["responseMessage"] as? String
                        if let code = JSON["responseCode"] as? Int {
                            if code == 200{
                                showAlertMessage(title: Constants.AppName, message: "Profile edited successfully", okButton: "OK", controller: self) {
                                    self.navigationController?.popViewController(animated: true)
                                }
                            }else{
                                alert(Constants.AppName, message: displayMessage ?? "", view: self)
                            }
                        }
                        if let status = JSON["status"] as? Int, status == 401{
                            logOut(controller: self)
                        }                    }
                case .failure(let error):
                    // error handling
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: error.localizedDescription, view: self)
                    break
                }
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["country"] = self.countryStr as AnyObject
        parameters["name"] = nameTF.text as AnyObject
        
        return parameters
    }

}
//MARK:- NavBar Delegate Method(s)

extension EditProfileVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
//MARK:- Image Picker Delegate

extension EditProfileVC: ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        if image != nil{
        self.profileImageView.image = image
        }
      //  self.imageStr =  self.profileImageView.image!.jpegData(compressionQuality: 0.3)!
    }
}
extension EditProfileVC:UIPickerViewDataSource,UIPickerViewDelegate{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.flagsArray.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return flagsArray[row].name
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
      //  countryTF.text = flagsArray[row].name
        countryTF.text = "\(self.flagsArray[row].emoji ?? "") \(self.flagsArray[row].name ?? "")"
        self.countryStr = self.flagsArray[row].name ?? ""
    }
}
