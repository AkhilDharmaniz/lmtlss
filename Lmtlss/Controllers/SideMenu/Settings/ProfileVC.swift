//
//  ProfileVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 21/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ProfileVC: UIViewController {

    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var countryImg: UIImageView!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var startingWeightLbl: UILabel!
    @IBOutlet weak var currentWeightLbl: UILabel!
    var flagsArray = [FlagDetailsField]()
    var heightUnit = ""
    var weightUnit = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func viewDidLayoutSubviews() {
           self.profileImageView.layer.cornerRadius = (UIScreen.main.bounds.size.height * 0.17) / 2
           self.profileImageView.layer.masksToBounds = true
       }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.getProfile()
        configureNavUI()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       // self.getProfile()
    }
    @IBAction func editProfileBtnAction(_ sender: UIButton) {
        let VC = EditProfileVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    func configureNavUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Profile"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func configureUI(){
        configureCountryPicker()
    }
    func configureCountryPicker(){
           let bundle = Bundle.main
           let path = bundle.path(forResource: "countriesList", ofType: "json")
           let  info = try? String(contentsOfFile: path!, encoding: String.Encoding.utf8)
           let data = info!.data(using: String.Encoding.utf8)
           let json: AnyObject? = try? JSONSerialization.jsonObject(with: data!, options: []) as AnyObject
           self.flagsArray.removeAll()
           if let j: AnyObject = json {
               if let flagArray = j as? [[String:Any]]{
                   for flag in flagArray{
                       self.flagsArray.append(FlagDetailsField(name: flag["name"] as? String ?? "", unicode: flag["unicode"] as? String ?? "", code: flag["code"] as? String ?? "", emoji: flag["emoji"] as? String ?? ""))
                   }
               }
           }
       }
    func getProfile(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.getUserProfile
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.nameLbl.text = responseData["name"] as? String ?? ""
                            self.emailLbl.text = responseData["email"] as? String ?? ""
                            self.profileImageView.sd_setImage(with: URL(string: responseData["image"] as? String ?? ""), placeholderImage: UIImage(named: "user_profile"))
                            let country = responseData["country"] as? String ?? ""
                            if country != ""{
                                if self.flagsArray.count != 0{
                                    for i in 0..<self.flagsArray.count{
                                        if self.flagsArray[i].name == country{
                                            self.countryLbl.text = "\(self.flagsArray[i].emoji ?? "") \(self.flagsArray[i].name ?? "")"
                                        }
                                    }
                                }
                            }
                            self.heightUnit = responseData["height_unit"] as? String ?? ""
                            let height = responseData["height"] as? String ?? ""
                            self.heightLbl.text = "\(self.getHeight(slider: height)) \(self.heightUnit)"
                            
                            self.weightUnit = responseData["weight_unit"] as? String ?? ""
                            let weight = responseData["starting_weight"] as? String ?? ""
                            self.startingWeightLbl.text = "\(self.getWeight(weight: weight)) \(self.weightUnit)"
                            let currentWeight = responseData["current_weight"] as? String ?? ""
                            if currentWeight == ""{
                              self.currentWeightLbl.text = "\(self.getWeight(weight: weight)) \(self.weightUnit)"
                            }else{
                             //   self.currentWeightLbl.text = responseData["current_weight"] as? String ?? ""
                                self.currentWeightLbl.text = "\(self.getWeight(weight: currentWeight)) \(self.weightUnit)"
                            }
                            

                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func getHeight(slider: String) -> String
       {
           if heightUnit == "ft"
           {
            let myheight : Int = Int(slider) ?? 0
               let heightCm = Measurement(value: Double(myheight), unit: UnitLength.centimeters)
               let heightFeet = heightCm.converted(to: UnitLength.feet)
               let numInches = heightCm.converted(to: UnitLength.inches)
               let heightInch = numInches.value.truncatingRemainder(dividingBy: 12);
               
               return "\(Int(heightFeet.value))" + "'" + " \(Int(round(heightInch)))" + "''"
           }
           else
           {
            return "\(Int(slider) ?? 0)"
           }
    }
    func getWeight(weight : String) -> String
    {
        if weightUnit == "lbs"
        {
            let myweight : Double = Double(weight) ?? 0.0
            let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.kilograms)
            let lbWeight = kgWeight.converted(to: UnitMass.pounds)
            let weight = String(format: "%.2f", lbWeight.value)
            
            return weight
//        if weightUnit == "lbs"
//        {
//            let myweight : Int = Int(weight) ?? 0
//            let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.kilograms)
//            let lbWeight = kgWeight.converted(to: UnitMass.pounds)
//            let weight = String(format: "%.2f", lbWeight.value)
//            
//            return weight
        }
        else
        {
            return "\(Double(weight) ?? 0)"
        }
    }
}
//MARK:- NavBar Delegate Method(s)

extension ProfileVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
