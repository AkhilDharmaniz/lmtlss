//
//  ForumListingVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 11/01/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire
import AVKit

class ForumListingVC: UIViewController {

    @IBOutlet weak var navView: UIView!
    var forumListArray = [DayAndTime]()
    @IBOutlet weak var forumTableView: UITableView!
    var fromDelegate = false
    override func viewDidLoad() {
        super.viewDidLoad()
        self.forumTableView.delegate = self
        self.forumTableView.dataSource = self
        self.getForumList()
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(doSomething), for: .valueChanged)
        forumTableView.addSubview(refreshControl)
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "Forum Listing"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        
    }
    @objc func doSomething(refreshControl: UIRefreshControl) {
          self.getForumList()
          refreshControl.endRefreshing()
      }
    func getForumList(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.forumList
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.forumListArray.removeAll()
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            for (key, value) in responseData {
                                var forumArray = [ForumField]()
                                if let messages = value as? [[String:Any]]{
                                    for i in 0..<messages.count{
                                        forumArray.append(ForumField(caption: messages[i]["caption"] as? String ?? "", created_at: messages[i]["created_at"] as? String ?? "", id: messages[i]["id"] as? Int ?? 0, photo: messages[i]["photo"] as? String ?? "", title: messages[i]["title"] as? String ?? "", type: messages[i]["type"] as? String ?? "", updated_at: messages[i]["updated_at"] as? String ?? "", url: messages[i]["url"] as? String ?? "", video: messages[i]["video"] as? String ?? ""))
                                    }
                                    var date = Date()
                                    let dateFormatter = DateFormatter()
                                    dateFormatter.dateFormat = "MMMM dd, yyyy"
                                    date = dateFormatter.date(from: key)!
                                    forumArray.reverse()
                                    self.forumListArray.append(DayAndTime(day: key, forumData: forumArray, date: date))
                                }
                            }
                            self.forumListArray = self.forumListArray.sorted(by: { $0.date.compare($1.date) == .orderedDescending })
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.forumTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func getReadableDate(timeStamp: TimeInterval) -> String? {
        let date = Date(timeIntervalSince1970: timeStamp)
        let dateFormatter = DateFormatter()
        
        if Calendar.current.isDateInTomorrow(date) {
            return "Tomorrow"
        } else if Calendar.current.isDateInYesterday(date) {
            return "Yesterday"
        } else if dateFallsInCurrentWeek(date: date) {
            if Calendar.current.isDateInToday(date) {
                dateFormatter.dateFormat = "h:mm a"
                return dateFormatter.string(from: date)
            } else {
                dateFormatter.dateFormat = "EEEE"
                return dateFormatter.string(from: date)
            }
        } else {
            dateFormatter.dateFormat = "MMM d, yyyy"
            return dateFormatter.string(from: date)
        }
    }
    func dateFallsInCurrentWeek(date: Date) -> Bool {
        let currentWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: Date())
        let datesWeek = Calendar.current.component(Calendar.Component.weekOfYear, from: date)
        return (currentWeek == datesWeek)
    }
}
extension ForumListingVC : UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        var cell: ForumHeaderCell! = tableView.dequeueReusableCell(withIdentifier: "header") as? ForumHeaderCell
        if cell == nil {
            tableView.register(UINib(nibName: "ForumHeaderCell", bundle: nil), forCellReuseIdentifier: "header")
            cell = tableView.dequeueReusableCell(withIdentifier: "header") as? ForumHeaderCell
        }
        cell.dateLbl.text = forumListArray[section].day
        return cell
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.forumListArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return forumListArray[section].forumData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if self.forumListArray[indexPath.section].forumData[indexPath.row].type == "video"{
            var cell: ForumVideoCell! = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ForumVideoCell
            if cell == nil {
                tableView.register(UINib(nibName: "ForumVideoCell", bundle: nil), forCellReuseIdentifier: "Cell")
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell") as? ForumVideoCell
            }
            cell.titleLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].title
            cell.descriptionLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].caption
            cell.imgVw.sd_setImage(with: URL(string: self.forumListArray[indexPath.section].forumData[indexPath.row].photo ?? ""), placeholderImage: UIImage(named: ""))

            cell.playBtn.tag = indexPath.row
            cell.playBtn.addTarget(self, action: #selector(playBtnAction(_:)), for: .touchUpInside)
            addShadowToView(targetView: cell.bottomView, shadowOffset:CGSize(width: 1.0, height: 5.0),shadowOpacity : 0.7, shadowRadius : 4.0, shadowColor: #colorLiteral(red: 0.7764705882, green: 0.7764705882, blue: 0.7764705882, alpha: 0.6032656234))
            return cell
        }else if self.forumListArray[indexPath.section].forumData[indexPath.row].type == "photo"{
            var cell: ForumImageCell! = tableView.dequeueReusableCell(withIdentifier: "Cell3") as? ForumImageCell
            if cell == nil {
                tableView.register(UINib(nibName: "ForumImageCell", bundle: nil), forCellReuseIdentifier: "Cell3")
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell3") as? ForumImageCell
            }
            cell.titleLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].title
            cell.descriptionLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].caption
            cell.imgVw.sd_setImage(with: URL(string: self.forumListArray[indexPath.section].forumData[indexPath.row].photo ?? ""), placeholderImage: UIImage(named: ""))
            addShadowToView(targetView: cell.bottomView, shadowOffset:CGSize(width: 1.0, height: 5.0),shadowOpacity : 0.7, shadowRadius : 4.0, shadowColor: #colorLiteral(red: 0.7764705882, green: 0.7764705882, blue: 0.7764705882, alpha: 0.6032656234))
            return cell
        }else if self.forumListArray[indexPath.section].forumData[indexPath.row].type == "url"{
            var cell: ForumURLCell! = tableView.dequeueReusableCell(withIdentifier: "Cell2") as? ForumURLCell
            if cell == nil {
                tableView.register(UINib(nibName: "ForumURLCell", bundle: nil), forCellReuseIdentifier: "Cell2")
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell2") as? ForumURLCell
            }
            cell.titleLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].title
            cell.descriptionLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].url
            cell.captionLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].caption
            addShadowToView(targetView: cell.bottomView, shadowOffset:CGSize(width: 1.0, height: 5.0),shadowOpacity : 0.7, shadowRadius : 4.0, shadowColor: #colorLiteral(red: 0.7764705882, green: 0.7764705882, blue: 0.7764705882, alpha: 0.6032656234))
            return cell
            
        }else{
            var cell: ForumTextCell! = tableView.dequeueReusableCell(withIdentifier: "Cell1") as? ForumTextCell
            if cell == nil {
                tableView.register(UINib(nibName: "ForumTextCell", bundle: nil), forCellReuseIdentifier: "Cell1")
                cell = tableView.dequeueReusableCell(withIdentifier: "Cell1") as? ForumTextCell
            }
            cell.titleLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].title
            cell.descriptionLbl.text = self.forumListArray[indexPath.section].forumData[indexPath.row].caption
            
         //   addShadowToView(targetView: cell.bottomView, shadowOffset:CGSize(width: 1.0, height: 5.0),shadowOpacity : 1.0, shadowRadius : 3.0, shadowColor: #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.5))
            addShadowToView(targetView: cell.bottomView, shadowOffset:CGSize(width: 1.0, height: 5.0),shadowOpacity : 0.7, shadowRadius : 4.0, shadowColor: #colorLiteral(red: 0.7764705882, green: 0.7764705882, blue: 0.7764705882, alpha: 0.6032656234))
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    @objc func playBtnAction(_ sender : UIButton){
        let buttonPosition:CGPoint = sender.convert(CGPoint.zero, to:self.forumTableView)
        let indexPath = self.forumTableView.indexPathForRow(at: buttonPosition)
                let videoURL = URL(string: self.forumListArray[indexPath?.section ?? 0].forumData[indexPath?.row ?? 0].video ?? "")
                let player = AVPlayer(url: videoURL!)
                let playerViewController = AVPlayerViewController()
                playerViewController.player = player
                self.present(playerViewController, animated: true) {
                    playerViewController.player!.play()
                }
//        let VC = VideoPlayerVC.instantiate(fromAppStoryboard: .SideMenu)
//        VC.videoUrl = self.forumListArray[indexPath?.section ?? 0].forumData[indexPath?.row ?? 0].video ?? ""
//        VC.modalPresentationStyle = .fullScreen
//        self.navigationController?.pushViewController(VC, animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension ForumListingVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        if fromDelegate{
            appDelegate().redirectToHome()
        }else{
            self.navigationController?.popViewController(animated: true)
        }
    //    self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
struct DayAndTime {
    var day: String
    var forumData : [ForumField]
    var date: Date
    
    init(day: String, forumData : [ForumField], date: Date) {
        self.day = day
        self.forumData = forumData
        self.date = date
    }
}
