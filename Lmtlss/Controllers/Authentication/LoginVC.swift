//
//  LoginVC.swift
//  Lmtlss
//
//  Created by apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class LoginVC: UIViewController {

    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var centricStackView: UIStackView!
    @IBOutlet weak var eyeImg: UIImageView!
    var rememberMeSelected = false
    @IBOutlet weak var rememberMeImg: UIImageView!
    @IBOutlet weak var rememberMeBtn: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        eyeImg.image = #imageLiteral(resourceName: "eye1")
        emailTF.text = self.getEmail()
        passwordTF.text = self.getPassword()
        rememberMeBtn.isSelected = rememberMeSelected
        rememberMeImg.image = rememberMeSelected == true ? #imageLiteral(resourceName: "check") : #imageLiteral(resourceName: "uncheck")
    }
    
    private func configureUI(){
        centricStackView.spacing = UIScreen.main.bounds.height * 0.02
    }
    func validateTextFieldsBeforeSubmit()->Bool
    {
        var validate=true
        if emailTF.text!.isEmpty{
            alert(Constants.AppName, message: "Please enter email address", view: self)
             validate=false
        }else if !validateEmail(emailTF.text!){
           alert(Constants.AppName, message: "Please enter valid email address", view: self)
            validate=false
        }else if passwordTF.text!.isEmpty{
            alert(Constants.AppName, message: "Please enter password", view: self)
            validate=false
        }
        return validate
    }
    
    //MARK:- IBAction Method(s)
    @IBAction func loginBtnAction(_ sender: UIButton) {
        if validateTextFieldsBeforeSubmit(){
            if Reachability.isConnectedToNetwork() == true {
             
                
                IJProgressView.shared.showProgressView()
                let signInUrl = Constants.baseURL + Constants.logIn
                 
                AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: nil, success: { (response) in
                    IJProgressView.shared.hideProgressView()
                    
                    let message = response["message"] as? String ?? ""
                    if let status = response["status"] as? String{
                        if status == "1"{
                            if self.rememberMeSelected == true{
                                storeCredential(email: self.emailTF.text!, password: self.passwordTF.text!)
                            }else{
                                removeCredential()
                            }
                            
                            _ = [self.emailTF,self.passwordTF].map({$0?.resignFirstResponder()})
                            let access_token = response["access_token"] as? String ?? ""
                            print(access_token)
                            UserDefaults.standard.set(access_token, forKey: "token")
                            let isReply = response["is_reply"] as? Int ?? 0
                            UserDefaults.standard.set(isReply, forKey: "isReply")
                            if Reachability.isConnectedToNetwork() == true {
                                self.getUserData()
                            }else{
                                
                                alert(Constants.AppName, message: "Check internet connection", view: self)
                            }
                            
                        }else{
                            IJProgressView.shared.hideProgressView()
                             alert(Constants.AppName, message: message, view: self)
                        }
                    }
                }) { (error) in
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: error.localizedDescription, view: self)
                    
                }
                
            } else {
                
                alert(Constants.AppName, message: "Check internet connection", view: self)
            }
        }
        
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["username"] = emailTF.text as AnyObject
        parameters["password"] = passwordTF.text as AnyObject
        let deviceID = UserDefaults.standard.value(forKey: "device_token") as? String
        parameters["device_token"] = deviceID == nil ? "777" as AnyObject : deviceID as AnyObject
        parameters["device_type"] = 2 as AnyObject
        return parameters
    }
    func getUserData() {
        
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.userSettings
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any] {
                            let isQuestionnaire = responseData["is_questionnaire"] as? Int ?? 0
                            let supportCount = responseData["support_count"] as? Int ?? 0
                            UserDefaults.standard.set("\(supportCount)", forKey: "support_count")
                            let notificationCount = responseData["notification_count"] as? Int ?? 0
                            UserDefaults.standard.set("\(notificationCount)", forKey: "notification_count")
                            if isQuestionnaire == 1{
                                let baseVC = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
                                baseVC.tabBarController?.selectedIndex = 1
                                self.navigationController?.pushViewController(baseVC, animated: false)
                            }else{
                                let questionnaireVC = QuestionnaireContainerVC.instantiate(fromAppStoryboard: .Questionnaire)
                                self.navigationController?.pushViewController(questionnaireVC, animated: false)
                            }
                        }
                                       //                let homeTabBarVC = HomeTabBarVC.instantiate(fromAppStoryboard: .Home)
                                       //                self.navigationController?.pushViewController(homeTabBarVC, animated: false)
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
                
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
    }
    @IBAction func forgotPasswordBtnAction(_ sender: UIButton) {
        _ = [emailTF,passwordTF].map({$0?.resignFirstResponder()})
        let forgotVC = ForgotPasswordVC.instantiate(fromAppStoryboard: .Authentication)
        self.navigationController?.pushViewController(forgotVC, animated: true)
    }
    
    @IBAction func eyeButtonAction(_ sender: UIButton) {
        if eyeImg.image == #imageLiteral(resourceName: "eye1") {
            eyeImg.image = #imageLiteral(resourceName: "eye")
            passwordTF.isSecureTextEntry = false
        }else{
           eyeImg.image = #imageLiteral(resourceName: "eye1")
            passwordTF.isSecureTextEntry = true
        }
    }
    
    @IBAction func rememberMeBtnAction(_ sender: UIButton) {
        sender.isSelected = !sender.isSelected
        self.rememberMeSelected = sender.isSelected
        rememberMeImg.image = rememberMeSelected == true ? #imageLiteral(resourceName: "check") : #imageLiteral(resourceName: "uncheck")
    }
    func getEmail() -> String
       {
           if let email =  UserDefaults.standard.value(forKey:"userEmail")
           {
               rememberMeSelected = true
               return email as! String
           }
           else
           {
               rememberMeSelected = false
               return ""
           }
       }
       
       func getPassword() -> String
       {
           if let password =  UserDefaults.standard.value(forKey:"userPassword")
           {
               rememberMeSelected = true
               return password as! String
           }
           else
           {
               rememberMeSelected = false
               return ""

           }
       }
}
