//
//  ForgotPasswordVC.swift
//  Lmtlss
//
//  Created by apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class ForgotPasswordVC: UIViewController {

    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var centricStackView: UIStackView!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        // Do any additional setup after loading the view.
    }
    
    private func configureUI(){
        centricStackView.spacing = UIScreen.main.bounds.height * 0.035
    }
    
    
    
    //MARK:- IBAction Method(s)
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func validateTextFieldsBeforeSubmit()->Bool
    {
        var validate=true
        if emailTF.text!.isEmpty{
            alert(Constants.AppName, message: "Please enter email address", view: self)
            validate=false
        }else if !validateEmail(emailTF.text!){
            alert(Constants.AppName, message: "Please enter valid email address", view: self)
            validate=false
        }
        return validate
    }
    @IBAction func submitBtnAction(_ sender: UIButton) {
        if validateTextFieldsBeforeSubmit(){
            if Reachability.isConnectedToNetwork() == true {
             
                
                IJProgressView.shared.showProgressView()
                let signInUrl = Constants.baseURL + Constants.forgotPassword
                 
                AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: nil, success: { (response) in
                    IJProgressView.shared.hideProgressView()
                    
                    let message = response["message"] as? String ?? ""
                    if let status = response["status"] as? String{
                        if status == "1"{
                            
                            showAlertMessage(title: Constants.AppName, message: message, okButton: "OK", controller: self) {
                                self.navigationController?.popViewController(animated: true)
                            }
                        }else{
                            IJProgressView.shared.hideProgressView()
                             alert(Constants.AppName, message: message, view: self)
                        }
                    }
                }) { (error) in
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: error.localizedDescription, view: self)
                    
                }
                
            } else {
                
                alert(Constants.AppName, message: "Check internet connection", view: self)
            }
        }

    }
    func generatingParameters() -> [String:AnyObject] {
           var parameters:[String:AnyObject] = [:]
         parameters["username"] = emailTF.text as AnyObject
           return parameters
       }

}
