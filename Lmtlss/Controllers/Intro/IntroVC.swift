//
//  IntroVC.swift
//  Lmtlss
//
//  Created by apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class IntroVC: UIViewController {
    @IBOutlet weak var centricStakcView: UIStackView!
    
    @IBOutlet weak var title1Lbl: UILabel!
    @IBOutlet weak var subTitleLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var pageControlView: UIPageControl!
    @IBOutlet weak var getStartedBtn: UIButton!
    @IBOutlet weak var introCollectionView: UICollectionView!
    
    private var imageDataArray = [["SplashBG-1","SplashBG-2","SplashBG-3","SplashBG-4","SplashBG-5"],["getStartBtn-1","getStartBtn-2","getStartBtn-3","getStartBtn-4","getStartBtn-5"],["ACTIONS EXPRESS PRIORITIES","CUSTOMIZED MEAL PLANS","CUSTOMIZED MACROS","CUSTOMIZED TRAINING","TRACK PROGRESS"],["","","","",""],["We are excited to help fast track your fitness Oriented endeavors, let's work.","Not interested in counting calories? We have got you. Access to custom meal plans tailored in accordance to your preferences, ambitions and feedback.","Able to count calories but not sure how or when to change them? We have got you. Access to custom Macro targets amended in accordance to your feedback.","Looking to ensure your training compliments your ambitions? In need of some direction either at home or in the gym? Look no further, our custom training programs take into consideration the muscles you would like to prioritize, the trianing frequency best suited to you and any injuries you might have.","It’s hard to stay motivated without knowing for sure what you are doing is working, we understand. Ability to record and track all relevant data, graphed for your convenience."],["screen-1","intro-1","intro-2","intro-3","intro-4"]
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        // Do any additional setup after loading the view.
    }
    
    func configureUI(){
        introCollectionView.delegate = self
        introCollectionView.dataSource = self
        pageControlView.numberOfPages = imageDataArray[0].count
        pageControlView.currentPage = 0
        centricStakcView.spacing = UIScreen.main.bounds.height * 0.015
        if UIDevice().userInterfaceIdiom == .phone  {
            switch (UIScreen.main.nativeBounds.height) {
            case 1136:
                titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 40)
                subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 20)
                break;
                
            case 1334:
                titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 40)
                subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 20)
                break;
                
            case 1920, 2208:
                titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                break;
                
            case 2436:
                titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                break;
                
            case 2688:
                titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                break;
                
            case 1792:
                titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                break;
                
            default:
                break;
            }
        }
        titleLbl.text = imageDataArray[2][0]
        title1Lbl.text = imageDataArray[3][0]
        subTitleLbl.text = imageDataArray[4][0]
    }
    
    //MARK:- IBAction Method(s)
    
    @IBAction func pageControllerAction(_ sender: UIPageControl) {
        self.introCollectionView.scrollToItem(at: IndexPath(row: sender.currentPage, section: 0), at: .centeredHorizontally, animated: true)
        self.getStartedBtn.setImage(UIImage(named: imageDataArray[1][sender.currentPage]), for: .normal)
        titleLbl.text = imageDataArray[2][sender.currentPage]
        title1Lbl.text = imageDataArray[3][sender.currentPage]
        subTitleLbl.text = imageDataArray[4][sender.currentPage]
    }
    @IBAction func getStartedBtnAction(_ sender: UIButton) {
        let loginVC = LoginVC.instantiate(fromAppStoryboard: .Authentication)
        self.navigationController?.pushViewController(loginVC, animated: true)
    }
    
}

extension IntroVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return imageDataArray[0].count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! IntroImageCell
        
        if UIDevice().userInterfaceIdiom == .phone  {
            switch (UIScreen.main.nativeBounds.height) {
                case 1136:
                    let data = imageDataArray[0][indexPath.row]
                    cell.bgImageView.image = UIImage(named: data)
                        break;

                case 1334:
                    let data = imageDataArray[0][indexPath.row]
                    cell.bgImageView.image = UIImage(named: data)
                    break;

                case 1920, 2208:
                    let data = imageDataArray[0][indexPath.row]
                    cell.bgImageView.image = UIImage(named: data)
                    break;

               case 2436:
                    let data = imageDataArray[5][indexPath.row]
                    cell.bgImageView.image = UIImage(named: data)
                     break;

                case 2688:
                    let data = imageDataArray[5][indexPath.row]
                    cell.bgImageView.image = UIImage(named: data)
                     break;

                case 1792:
                    let data = imageDataArray[5][indexPath.row]
                    cell.bgImageView.image = UIImage(named: data)
                     break;

                default:
                    break;
            }
        }
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
    }

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let visibleRect = CGRect(origin: self.introCollectionView.contentOffset, size: self.introCollectionView.bounds.size)
        let visiblePoint = CGPoint(x: visibleRect.midX, y: visibleRect.midY)
        if let visibleIndexPath = self.introCollectionView.indexPathForItem(at: visiblePoint) {
            self.pageControlView.currentPage = visibleIndexPath.item
            self.getStartedBtn.setImage(UIImage(named: imageDataArray[1][visibleIndexPath.item]), for: .normal)
            if UIDevice().userInterfaceIdiom == .phone  {
                switch (UIScreen.main.nativeBounds.height) {
                case 1136:
                    if visibleIndexPath.item != 0{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 35)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 18)
                        
                    }else{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 40)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 20)
                    }
                    break;
                    
                case 1334:
                    if visibleIndexPath.item != 0{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 35)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 18)
                        
                    }else{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 40)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 20)
                    }
                    break;
                    
                case 1920, 2208:
                    if visibleIndexPath.item != 0{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 35)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 18)
                        
                    }else{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                    }
                    break;
                    
                case 2436:
                    if visibleIndexPath.item != 0{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 35)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 18)
                        
                    }else{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                    }
                    break;
                    
                case 2688:
                    if visibleIndexPath.item != 0{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 35)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 18)
                        
                    }else{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                    }
                    break;
                    
                case 1792:
                    if visibleIndexPath.item != 0{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 35)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 18)
                        
                    }else{
                        titleLbl.font = UIFont(name: "Poppins-SemiBold", size: 45)
                        subTitleLbl.font = UIFont(name: "Poppins-Regular", size: 23)
                    }
                    break;
                    
                default:
                    break;
                }
            }
            
            titleLbl.text = imageDataArray[2][visibleIndexPath.item]
            title1Lbl.text = imageDataArray[3][visibleIndexPath.item]
            subTitleLbl.text = imageDataArray[4][visibleIndexPath.item]
        }
    }
}
