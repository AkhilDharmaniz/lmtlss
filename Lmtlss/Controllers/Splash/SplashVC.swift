//
//  SplashVC.swift
//  Lmtlss
//
//  Created by apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import HealthKit


class SplashVC: UIViewController {
    
    @IBOutlet weak var imgVw: UIImageView!
    var healthScore = HKHealthStore()
    var footSteps = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if UIDevice().userInterfaceIdiom == .phone  {
            switch (UIScreen.main.nativeBounds.height) {
            case 1136,1334,1920, 2208:
                print("iPhone 5 or 5S or 5C");
                print("iPhone 6/6S/7/8");
                print("iPhone 6+/6S+/7+/8+");
                imgVw.image = UIImage(named: "SplashBG-1")
                break;
            case 2436,2688,1792:
                print("iPhone X/XS/11 Pro");
                print("iPhone XS Max/11 Pro Max");
                print("iPhone XR/ 11 ");
                imgVw.image = UIImage(named: "screen-1")
                break;
            default:
                print("Unknown");
                break;
            }
        }
        Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(navigateToLogin), userInfo: nil, repeats: false)
    }
    @objc func navigateToLogin(){
        if let token = UserDefaults.standard.value(forKey: "token") as? String{
            if token != ""{
                if Reachability.isConnectedToNetwork() == true {
                    getUserData()
                }else{
                    
                    alert(Constants.AppName, message: "Check internet connection", view: self)
                }
            }
        }else{
            if UserDefaults.standard.object(forKey: "isFirstTime") == nil{
                UserDefaults.standard.set("NO", forKey: "isFirstTime")
                let introVC = IntroVC.instantiate(fromAppStoryboard: .Intro)
                self.navigationController?.pushViewController(introVC, animated: false)
            }else{
                let introVC = LoginVC.instantiate(fromAppStoryboard: .Authentication)
                self.navigationController?.pushViewController(introVC, animated: false)
            }
        }
    }
    func getUserData() {
        let url = Constants.baseURL + Constants.userSettings
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
        AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["responseMessage"] as? String ?? ""
            if let responseCode = response["responseCode"] as? Int{
                if responseCode == 200{
                    if let responseData = response["responseData"] as? [String:Any] {
                        let isQuestionnaire = responseData["is_questionnaire"] as? Int ?? 0
                        let supportCount = responseData["support_count"] as? String ?? ""
                        UserDefaults.standard.set(supportCount, forKey: "support_count")
                        let notificationCount = responseData["notification_count"] as? String ?? ""
                        UserDefaults.standard.set(notificationCount, forKey: "notification_count")
                        self.trackSteps(qstnnaire: isQuestionnaire)
//                        if isQuestionnaire == 1{
//                            self.trackSteps()
//                        }else{
//                            let questionnaireVC = QuestionnaireContainerVC.instantiate(fromAppStoryboard: .Questionnaire)
//                            self.navigationController?.pushViewController(questionnaireVC, animated: false)
//                        }
                    }
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func trackSteps(qstnnaire: Int){
        let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)! ]
        // Check for Authorization
        healthScore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (bool, error) in
            if (bool) {
                // Authorization Successful
                self.getTodaysSteps { (result) in
                    DispatchQueue.main.async {
                        let stepCount = String(Int(result))
                        print(String(stepCount))
                        self.footSteps = String(stepCount)
                        self.saveSteps(qstnnaire: qstnnaire)
                    }
                }
            }
        }
    }
    func getTodaysSteps(completion: @escaping (Double) -> Void) {
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { _, result, _ in
            guard let result = result, let sum = result.sumQuantity() else {
                completion(0.0)
                return
            }
            completion(sum.doubleValue(for: HKUnit.count()))
        }
        healthScore.execute(query)
    }
    func saveSteps(qstnnaire: Int){
        if Reachability.isConnectedToNetwork() == true {
            let url = Constants.baseURL + Constants.recordSteps
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            AFWrapperClass.requestPostWithMultiFormData(url, params: generatingParameters(), headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.view.endEditing(true)
                if let _ = response["responseCode"] as? Int{
                    if qstnnaire == 1{
                        let baseVC = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
                        self.navigationController?.pushViewController(baseVC, animated: false)
                    }else{
                        let questionnaireVC = QuestionnaireContainerVC.instantiate(fromAppStoryboard: .Questionnaire)
                        self.navigationController?.pushViewController(questionnaireVC, animated: false)
                    }
                    
                }
            }) { (error) in
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["steps"] = footSteps as AnyObject
        
        return parameters
    }
}
