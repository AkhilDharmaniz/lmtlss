//
//  ProgressDetailsVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 20/02/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import UIKit
import Alamofire
import Charts

class DropDownCell: UITableViewCell{
    @IBOutlet weak var titleLbl: UILabel!
}
class ProgressCell: UITableViewCell{
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
}
class ProgressDetailsVC: UIViewController {
    @IBOutlet weak var entriesTableView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var dropDownTableView: UITableView!
    @IBOutlet weak var progressTypeLbl: UILabel!
    @IBOutlet weak var timeDropdownTableView: UITableView!
    @IBOutlet weak var timeLbl: UILabel!
    @IBOutlet weak var workoutsLbl: UILabel!
    @IBOutlet weak var volumeView: UIView!
    @IBOutlet weak var timeView: UIView!
    @IBOutlet weak var barChartView: BarChartView!
    @IBOutlet weak var volumeGraphView: UIView!
    @IBOutlet weak var scrollVw: UIScrollView!
    var volumeArr = [VolumeModel]()
    var workouts = 10
    
    var progressDropdownArr = [("All",false),("Weight",false),("Sleep",false),("Water Intake",false),("Steps",false),("Volume",false)]
    var timeDropdownArr = [("Weekly",false),("Monthly",false),("Custom",false),("LifeTime",false)]
    var progressArr = [("Apr 16,2015","Thursday","57 kg"), ("Apr 15,2015","Wednesday","60 kg"), ("Apr 14,2015","Tuesday","73 kg"), ("Apr 13,2015","Monday","77 kg"),("Apr 12,2015","Sunday","90 kg"),("Apr 11,2015","Saturday","82 kg")]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.scrollVw.isScrollEnabled = false
        self.volumeGraphView.isHidden = true
        self.dropDownTableView.isHidden = true
        self.timeDropdownTableView.isHidden = true
        self.entriesTableView.delegate = self
        self.entriesTableView.dataSource = self
        self.dropDownTableView.delegate = self
        self.dropDownTableView.dataSource = self
        self.timeDropdownTableView.delegate = self
        self.timeDropdownTableView.dataSource = self
        
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    
    func configureUI(){
        
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "My Progress"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image =  #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "menu")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
       
    }
    func configureVolumeGraph(){
        barChartView.noDataText = "You need to provide data for the chart."
        barChartView.xAxis.drawGridLinesEnabled = false
        barChartView.rightAxis.drawAxisLineEnabled = false
        barChartView.leftAxis.drawAxisLineEnabled = false
        barChartView.rightAxis.drawLabelsEnabled = false
        barChartView.xAxis.labelPosition = .bottom
 //       barChartView.xAxis.labelRotationAngle = volumeArr.count > 8 ? 60 : 0
        barChartView.xAxis.axisMinLabels = 1
        //     barChartView.xAxis.labelFont = .systemFont(ofSize: 7)
        barChartView.xAxis.setLabelCount(volumeArr.count, force: false)
        barChartView.legend.horizontalAlignment = .center
        
        var yValues = [Double]()
        var xValues = [String]()
        for volume in self.volumeArr{
            yValues.append(Double(volume.volume ?? "") ?? 0.0)
            xValues.append(volume.workout ?? "")
          //  xValues.append(volume.workout != "" ? "workout\n\(volume.workout ?? "")" : volume.workout ?? "")
        }
        barChartView.setBarChartData(xValues: xValues, yValues: yValues, label: "Volume (1000 kg)")
        
    }
    @IBAction func progressDropDownBtnAction(_ sender: UIButton) {
        if sender.tag == 1{
            self.dropDownTableView.isHidden = false
        }else if sender.tag == 2{
            self.timeDropdownTableView.isHidden = false
        }
    }
    
    @IBAction func incrementDecrementBtnAction(_ sender: UIButton) {
        if workouts == 10 && sender.tag != 1{
            alert(Constants.AppName, message: "Quantity cannot be less than 10", view: self)
            return
        }else if workouts == 100 && sender.tag == 1{
            return
        }
        workouts = sender.tag == 1 ? workouts == 100 ? 100 : workouts+10 : workouts == 10 ? 10 : workouts-10
        self.workoutsLbl.text = "\(workouts)"
        self.volumeAPI(workOut: "\(workouts)")
    }
    func volumeAPI(workOut: String){
        if internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.volumeGraph
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            let params = ["total_workout" : workOut]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                debugPrint(response)
                self.volumeArr.removeAll()
                IJProgressView.shared.hideProgressView()
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let details = response["detail"] as? [[String:Any]]{
                            for detail in details{
                                self.volumeArr.append(VolumeModel(volume: detail["volume"] as? String ?? "", workout: detail["workout"] as? String ?? ""))
                            }
//                            if self.volumeArr.count == 1{
//                                self.volumeArr.append(VolumeModel(volume: "0", workout: ""))
//                            }
                            self.configureVolumeGraph()
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }
}
    extension ProgressDetailsVC : UITableViewDelegate,UITableViewDataSource {
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return tableView == dropDownTableView ? progressDropdownArr.count : tableView == timeDropdownTableView ? timeDropdownArr.count : progressArr.count
        }
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            if tableView == dropDownTableView || tableView == timeDropdownTableView{
                let cell = tableView.dequeueReusableCell(withIdentifier: "DropDownCell", for: indexPath) as! DropDownCell
                cell.titleLbl.text = tableView == dropDownTableView ? progressDropdownArr[indexPath.row].0 : timeDropdownArr[indexPath.row].0
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ProgressCell
                cell.dateLbl.text = progressArr[indexPath.row].0
                cell.dayLbl.text = progressArr[indexPath.row].1
                cell.weightLbl.text = progressArr[indexPath.row].2
                DispatchQueue.main.async {
                    self.tableViewHeight.constant = self.entriesTableView.contentSize.height
                }
                return cell
            }
        }
        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
            return tableView == dropDownTableView || tableView == timeDropdownTableView ? 30 : UITableView.automaticDimension
        }
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            if tableView == dropDownTableView {
                self.dropDownTableView.isHidden = true
                self.progressTypeLbl.text = progressDropdownArr[indexPath.row].0
                for i in progressDropdownArr.indices { progressDropdownArr[i].1 = false }
                self.progressDropdownArr[indexPath.row].1 = true
                if indexPath.row == 5{
                    self.timeView.isHidden = true
                    self.volumeView.isHidden = false
                    self.timeDropdownTableView.isHidden = true
                    self.volumeGraphView.isHidden = false
                    self.scrollVw.isScrollEnabled = false
                    self.volumeAPI(workOut: workoutsLbl.text!)
                }else{
                    self.volumeGraphView.isHidden = true
                    self.timeView.isHidden = false
                    self.scrollVw.isScrollEnabled = true
                    self.volumeView.isHidden = true
                }
                
            }else if tableView == timeDropdownTableView {
                self.timeDropdownTableView.isHidden = true
                self.timeLbl.text = timeDropdownArr[indexPath.row].0
            }
        }
    }
    //MARK:- NavBar Delegate Method(s)

    extension ProgressDetailsVC: CustomHeaderViewDelegate {
        func openSideMenu() {
            self.sideMenuController?.showLeftView(animated: true, completionHandler: {
                if let leftVC = self.sideMenuController?.leftViewController as? SideMenuVC {
                    leftVC.menuTableView.reloadData()
                }
            })
        }
        
        func searchBtnTapped() {
            let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
            self.navigationController?.pushViewController(VC, animated: true)
        }
        
    }

public struct VolumeModel {
    public var volume: String?
    public var workout: String?
    public init(volume: String?,workout: String?) {
        self.volume = volume
        self.workout = workout
    }
}
