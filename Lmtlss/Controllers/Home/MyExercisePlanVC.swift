//
//  MyExercisePlanVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 09/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MyExerciseCell: UITableViewCell {
    @IBOutlet weak var repsLbl: UILabel!
    @IBOutlet weak var setsLbl: UILabel!
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var exerciseLbl: UILabel!
    @IBOutlet weak var thumbnailImg: UIImageView!
}
class MyTrainingMusclesCell: UICollectionViewCell {
    @IBOutlet weak var nameLbl: UILabel!
}
class MyExercisePlanVC: UIViewController {
    @IBOutlet weak var homeGymViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var gymHomeView: UIView!
    @IBOutlet weak var gymBtn: UIButton!
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var exerciseLbl: UILabel!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var daysCollectionView: UICollectionView!
    @IBOutlet weak var exerciseTableView: UITableView!
    @IBOutlet weak var musclesCollectionView: UICollectionView!
    @IBOutlet weak var noTrainingView: UIView!
    @IBOutlet weak var holdTightView: UIView!
    @IBOutlet weak var trainingPlanLbl: UILabel!
    @IBOutlet weak var goToQtnBtn: UIButton!
    @IBOutlet weak var imFinishedBtn: UIButton!
    
    var trainingPlanArr = [TrainingPlanResponse]()
    var dayIndex = 0
    var muscleIndex = 0
    var goingToDeatails = false
    var responseDataDict = [String:Any]()
    var gymHome = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        configureNavUI()
        if goingToDeatails == true{
            goingToDeatails = false
        }else{
            self.getTrainingPlanList(from: "load", option: "")
        }
    }
    func configureUI(){
        self.daysCollectionView.delegate = self
        self.daysCollectionView.dataSource = self
        self.musclesCollectionView.delegate = self
        self.musclesCollectionView.dataSource = self
        self.exerciseTableView.delegate = self
        self.exerciseTableView.dataSource = self
    }
    func configureNavUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "My Exercise Plan"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "menu")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func getTrainingPlanList(from: String, option: String){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.getTrainingPlan
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.trainingPlanArr.removeAll()
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.responseDataDict = responseData
                            let type = responseData["type"] as? String ?? ""
                            self.holdTightView.isHidden = true
                            
                            if type == "gym" || type == "home"{
                                self.gymHomeView.isHidden = true
                                self.homeGymViewHeightConstraint.constant = 0
                                self.getData(type: type,fromApi: true,from : from)
                            }else if type == "both"{
                                self.gymBtn.backgroundColor = from == "load" ? UIColor(named: "BGColor") : option == "gym" ? UIColor(named: "BGColor") : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                                self.homeBtn.backgroundColor = option == "home" ? UIColor(named: "BGColor") : #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                                self.gymBtn.setTitleColor(from == "load" ? .white : option == "gym" ? .white : .black , for: .normal)
                                self.homeBtn.setTitleColor(option == "home" ? .white : .black, for: .normal)
                                self.gymHomeView.isHidden = false
                                self.getData(type: from == "load" ? "gym" : option ,fromApi: true, from: from)
                            }else if type == "no"{
                                self.noTrainingView.isHidden = false
                                self.gymHomeView.isHidden = true
                                self.homeGymViewHeightConstraint.constant = 0
                                if let mealPlanSend = self.responseDataDict["meal_plan_send"] as? String{
                                    if mealPlanSend == "1"{
                                        self.trainingPlanLbl.text = "Currently you don't have any training plan. if you want training plan please wait until check-in."
                                        self.goToQtnBtn.isHidden = true
                                    }else{
                                        self.trainingPlanLbl.text = "Currently you don't have any training plan. if you want training plan please go to questionnaire and change the option and resubmit questionnaire."
                                        self.goToQtnBtn.isHidden = false
                                    }
                                }
                            }else if type == ""{
                                self.holdTightView.isHidden = false
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    
    @IBAction func homeGymBtnAction(_ sender: UIButton) {
        if sender.tag == 1 {
            homeBtn.backgroundColor = UIColor(named: "BGColor")
            homeBtn.setTitleColor(.white, for: .normal)
            gymBtn.setTitleColor(.black, for: .normal)
            gymBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            gymHome = "home"
            getData(type: "home",fromApi: false, from: "")
            
        }else if sender.tag == 2{
            gymBtn.backgroundColor = UIColor(named: "BGColor")
            homeBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            gymBtn.setTitleColor(.white, for: .normal)
            homeBtn.setTitleColor(.black, for: .normal)
            gymHome = "gym"
            getData(type: "gym",fromApi: false, from: "")
        }
    }
    func getData(type: String,fromApi: Bool,from: String){
        self.trainingPlanArr.removeAll()
        if let option = responseDataDict["\(type)"] as? [[String:Any]] {
            for i in 0..<option.count{
                
                var muscleDataList = [MuscleData]()
                for (key, value) in option[i] {
                    var muscleList = [MuscleListData]()
                    if let muscles = value as? [[String:Any]]{
                        for j in 0..<muscles.count{
                            var volumeArr = [VolumeData]()
                            if let volumeData = muscles[j]["volume"] as? [[String:Any]]{
                                for volume in volumeData{
                                    volumeArr.append(VolumeData(exercise_id: volume["exercise_id"] as? String ?? "", id: volume["id"] as? String ?? "", muscle_group_id: volume["muscle_group_id"] as? String ?? "", reps: volume["reps"] as? String ?? "", set_id: volume["set_id"] as? String ?? "", user_id: volume["user_id"] as? String ?? "", weight: volume["weight"] as? String ?? "",enable: false,selected: false, is_prev: volume["is_prev"] as? String ?? ""))
                                }
                            }
                            muscleList.append(MuscleListData(day: muscles[j]["day"] as? String ?? "", exercise_id: muscles[j]["exercise_id"] as? String ?? "", exercise_name: muscles[j]["exercise_name"] as? String ?? "", exercise_place: muscles[j]["exercise_place"] as? String ?? "", exercise_video_thumbnail: muscles[j]["exercise_video_thumbnail"] as? String ?? "", exercise_video_url: muscles[j]["exercise_video_url"] as? String ?? "", id: muscles[j]["id"] as? String ?? "", is_priority: muscles[j]["is_priority"] as? String ?? "", muscle_group_id: muscles[j]["muscle_group_id"] as? String ?? "", muscle_group_name: muscles[j]["muscle_group_name"] as? String ?? "", reps: muscles[j]["reps"] as? String ?? "", sets: muscles[j]["sets"] as? String ?? "", user_id: muscles[j]["user_id"] as? String ?? "", selected: false, main_generic_note: muscles[j]["main_generic_note"] as? String ?? "", specific_note: muscles[j]["specific_note"] as? String ?? "", volume: volumeArr, is_log: muscles[j]["is_log"] as? String ?? "", is_complete: muscles[j]["is_complete"] as? String ?? ""))
                        }
                        muscleDataList.append(MuscleData(muscleName: key, muscleData: muscleList, selected: false, is_complete: muscleList.count > 0 ? muscleList[0].is_complete : "0"))
                    }
                }
                
                muscleDataList =  muscleDataList.sorted { $0.muscleName < $1.muscleName }
                //    muscleDataList[self.muscleIndex].selected = true
                self.trainingPlanArr.append(TrainingPlanResponse(day: "Day \(i+1)", selected: false, muscleList: muscleDataList, is_day_complete: option[i]["is_day_complete"] as? String ?? ""))
            }
            if fromApi == true{
                if from == "load"{
                    self.dayIndex = 0
                    self.muscleIndex = 0
                }
            }
            self.trainingPlanArr[self.dayIndex].selected = true
            if self.trainingPlanArr[self.dayIndex].muscleList.count != 0{
                self.trainingPlanArr[self.dayIndex].muscleList[self.muscleIndex].selected = true
            }
            self.daysCollectionView.reloadData()
            self.musclesCollectionView.reloadData()
            self.exerciseTableView.reloadData()
            self.exerciseLbl.text = "Exercises for \(self.trainingPlanArr[self.dayIndex].muscleList.count == 0 ? "" : self.trainingPlanArr[self.dayIndex].muscleList[self.muscleIndex].muscleName)"
            self.imFinishedBtn.isUserInteractionEnabled =  self.trainingPlanArr[self.dayIndex].muscleList[muscleIndex].is_complete == "1" ? false : true
        }
    }
    
    @IBAction func goToQuestionnaireBtnAction(_ sender: UIButton) {
        if let mealNumber = responseDataDict["meal_no"] as? String{
            if mealNumber == "1"{
                let questionnaireVC = QuestionnaireContainerVC.instantiate(fromAppStoryboard: .Questionnaire)
                questionnaireVC.currentIndex = 5
                questionnaireVC.backStopIndex = 5
                UserDefaults.standard.set(5, forKey: "currentIndex")
                //        questionnaireVC.currentIndex = 15
                //        UserDefaults.standard.set(15, forKey: "currentIndex")
                self.navigationController?.pushViewController(questionnaireVC, animated: false)
            }else{
                let questionnaireVC = CheckInContainerVC.instantiate(fromAppStoryboard: .CheckInQuestionnaire)
                //     questionnaireVC.currentIndex = 5
                //     questionnaireVC.backStopIndex = 5
                self.navigationController?.pushViewController(questionnaireVC, animated: false)
            }
        }
    }
    
    @IBAction func imFinishedBtnAction(_ sender: UIButton) {
        if self.trainingPlanArr.count > dayIndex && self.trainingPlanArr[dayIndex].muscleList.count > muscleIndex {
            if self.trainingPlanArr[dayIndex].muscleList[muscleIndex].is_complete == "1"{
                self.imFinishedBtn.isUserInteractionEnabled = false
            }else{
                self.imFinishedBtn.isUserInteractionEnabled = true
                let selectedArr = self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData.filter({$0.is_log == "1"})
                if selectedArr.count == self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData.count{
                    finishedExercise(muscleGrpId: self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[0].muscle_group_id,  day_id: self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[0].day, meal_no: responseDataDict["meal_no"] as? String ?? "")
                }else{
                    alert(Constants.AppName, message: "Please log all exercises.", view: self)
                }
            }
        }
    }
    func finishedExercise(muscleGrpId: String, day_id: String, meal_no: String){
        if internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.volumnFinishedExercise
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
             
            let params = ["muscle_group_id" : muscleGrpId,"day_id": day_id,"meal_no":meal_no]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        showAlertMessage(title: Constants.AppName, message: message, okButton: "OK", controller: self) {
                            self.getTrainingPlanList(from: "finish", option: self.gymHome)
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }
}

extension MyExercisePlanVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == musclesCollectionView {
            return trainingPlanArr.count == 0 ? 0 : trainingPlanArr[dayIndex].muscleList.count
        }else{
            return trainingPlanArr.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == musclesCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MyTrainingMusclesCell
            cell.nameLbl.text = trainingPlanArr[dayIndex].muscleList[indexPath.item].muscleName
            cell.nameLbl.backgroundColor = trainingPlanArr[dayIndex].muscleList[indexPath.item].selected == true ? trainingPlanArr[dayIndex].muscleList[indexPath.item].is_complete == "1" ? .systemGreen : #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : trainingPlanArr[dayIndex].muscleList[indexPath.item].is_complete == "1" ? .systemGreen : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            cell.nameLbl.textColor = trainingPlanArr[dayIndex].muscleList[indexPath.item].selected == true ? .white : .black
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MealListCollectionCell
            cell.nameLbl.text = trainingPlanArr[indexPath.item].day
            cell.selectedLbl.backgroundColor = trainingPlanArr[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            cell.nameLbl.textColor = trainingPlanArr[indexPath.item].selected == true ? trainingPlanArr[indexPath.item].is_day_complete == "1" ? .systemGreen : #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : trainingPlanArr[indexPath.item].is_day_complete == "1" ? .systemGreen : .black
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == musclesCollectionView {
            muscleIndex = indexPath.item
            for i in 0..<trainingPlanArr[dayIndex].muscleList.count{
                trainingPlanArr[dayIndex].muscleList[i].selected = i == indexPath.item ? true : false
            }
            musclesCollectionView.reloadData()
            exerciseTableView.reloadData()
        }else if collectionView == daysCollectionView{
            dayIndex = indexPath.item
            muscleIndex = 0
            for i in 0..<trainingPlanArr.count{
                trainingPlanArr[i].selected = i == indexPath.item ? true : false
            }
            for i in 0..<trainingPlanArr[dayIndex].muscleList.count{
                trainingPlanArr[dayIndex].muscleList[i].selected = i == muscleIndex ? true : false
            }
            daysCollectionView.reloadData()
            musclesCollectionView.reloadData()
            exerciseTableView.reloadData()
        }
        self.imFinishedBtn.isUserInteractionEnabled =  self.trainingPlanArr[self.dayIndex].muscleList[muscleIndex].is_complete == "1" ? false : true
        self.exerciseLbl.text = "Exercises for \(self.trainingPlanArr[self.dayIndex].muscleList.count == 0 ? "" : self.trainingPlanArr[self.dayIndex].muscleList[self.muscleIndex].muscleName)"
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == musclesCollectionView {
         return CGSize(width: UIScreen.main.bounds.size.width * 0.23, height: musclesCollectionView.frame.size.height)
        }
        else{
            if trainingPlanArr.count < 4{
                return CGSize(width: UIScreen.main.bounds.size.width * 0.31, height: daysCollectionView.frame.size.height)
            }
            return CGSize(width: UIScreen.main.bounds.size.width * 0.26, height: daysCollectionView.frame.size.height)
        }
    }
}

extension MyExercisePlanVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.trainingPlanArr.count == 0 ? 0 : self.trainingPlanArr[dayIndex].muscleList.count == 0 ? 0 : self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyExerciseCell
        cell.exerciseLbl.text = self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[indexPath.row].exercise_name
        cell.repsLbl.text = self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[indexPath.row].reps
        cell.setsLbl.text = self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[indexPath.row].sets
        
        cell.thumbnailImg.sd_setImage(with: URL(string: self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[indexPath.row].exercise_video_thumbnail ), placeholderImage: UIImage(named: "ic_video_list_pp"))
    //    cell.thumbnailImg.image = UIImage(named:"img\(indexPath.row+1)")
        cell.bgView.backgroundColor = self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[indexPath.row].is_log == "1" ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        for i in 0..<self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData.count{
//            self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[i].selected = i == indexPath.row ? true : false
//        }
//        exerciseTableView.reloadData()
        goingToDeatails = true
        let VC = ExerciseDetailVC.instantiate(fromAppStoryboard: .Home)
        VC.muscleData = [self.trainingPlanArr[dayIndex].muscleList[muscleIndex].muscleData[indexPath.row]]
        VC.mealNumber = responseDataDict["meal_no"] as? String ?? ""
        VC.dismissedCurrentView  = { showVal in
            VC.navigationController?.popViewController(animated: false)
            if showVal{
                self.getTrainingPlanList(from: "select", option: self.gymHome)
            }
        }
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension MyExercisePlanVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.sideMenuController?.showLeftView(animated: true, completionHandler: {
            if let leftVC = self.sideMenuController?.leftViewController as? SideMenuVC {
                leftVC.menuTableView.reloadData()
            }
        })
      // self.sideMenuController?.showLeftViewAnimated()
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
struct TrainingPlanResponse {
    var day: String
    var is_day_complete : String
    var muscleList : [MuscleData]
    var selected : Bool
    
    init(day: String, selected : Bool,muscleList : [MuscleData],is_day_complete: String) {
        self.day = day
        self.muscleList = muscleList
        self.selected = selected
        self.is_day_complete = is_day_complete
    }
}
struct MuscleData {
    var muscleName : String
    var muscleData : [MuscleListData]
    var selected : Bool
    var is_complete : String
    init(muscleName: String, muscleData : [MuscleListData], selected : Bool,is_complete : String) {
        self.muscleName = muscleName
        self.muscleData = muscleData
        self.selected = selected
        self.is_complete = is_complete
    }
}
struct MuscleListData {
    var day : String
    var exercise_id : String
    var exercise_name : String
    var exercise_place : String
    var exercise_video_thumbnail : String
    var exercise_video_url : String
    var id : String
    var is_priority : String
    var muscle_group_id : String
    var muscle_group_name : String
    var reps : String
    var sets : String
    var user_id : String
    var main_generic_note : String
    var specific_note : String
    var selected : Bool
    var is_log : String
    var is_complete : String
    var volume : [VolumeData]
    init(day : String, exercise_id : String, exercise_name : String, exercise_place : String, exercise_video_thumbnail : String,exercise_video_url : String,id : String,is_priority : String,muscle_group_id : String,muscle_group_name : String,reps : String,sets:String,user_id:String,selected:Bool,main_generic_note : String,specific_note : String,volume : [VolumeData],is_log : String,is_complete : String) {
        self.day = day
        self.exercise_id = exercise_id
        self.exercise_name = exercise_name
        self.exercise_place = exercise_place
        self.exercise_video_thumbnail = exercise_video_thumbnail
        self.exercise_video_url = exercise_video_url
        self.id = id
        self.is_priority = is_priority
        self.muscle_group_id = muscle_group_id
        self.muscle_group_name = muscle_group_name
        self.reps = reps
        self.sets = sets
        self.user_id = user_id
        self.selected = selected
        self.main_generic_note = main_generic_note
        self.specific_note = specific_note
        self.volume = volume
        self.is_log = is_log
        self.is_complete = is_complete
    }
}
struct VolumeData {
    var exercise_id : String
    var id : String
    var muscle_group_id : String
    var reps : String
    var set_id : String
    var user_id : String
    var weight : String
    var enable : Bool
    var selected : Bool
    var is_prev : String
    init(exercise_id : String, id : String, muscle_group_id : String,reps : String,set_id : String,user_id : String,weight : String, enable : Bool, selected : Bool, is_prev : String) {
        self.exercise_id = exercise_id
        self.id = id
        self.muscle_group_id = muscle_group_id
        self.reps = reps
        self.set_id = set_id
        self.user_id = user_id
        self.weight = weight
        self.enable = enable
        self.selected = selected
        self.is_prev = is_prev
    }
}
