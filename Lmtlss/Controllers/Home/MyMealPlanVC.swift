//
//  MyMealPlanVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 20/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MealPlanCell: UITableViewCell {
    @IBOutlet weak var foodNameLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    @IBOutlet weak var protienLbl: UILabel!
    @IBOutlet weak var fatLbl: UILabel!
    @IBOutlet weak var carbsLbl: UILabel!
    @IBOutlet weak var totalCalLbl: UILabel!
}
class MyMealPlanVC: UIViewController {

    @IBOutlet weak var proteinLbl: UILabel!
    @IBOutlet weak var carbsLbl: UILabel!
    @IBOutlet weak var fatLbl: UILabel!
    @IBOutlet weak var caloriesLbl: UILabel!
    @IBOutlet weak var trainingDaysBtn: UIButton!
    @IBOutlet weak var restDaysBtn: UIButton!
    @IBOutlet weak var swapBtn: UIButton!
    @IBOutlet weak var mealTableView: UITableView!
    @IBOutlet weak var mealListCollectionView: UICollectionView!
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var macroView: UIView!
    @IBOutlet weak var resultsTableView: UITableView!
    @IBOutlet weak var resultsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var recipeView: UIView!
    @IBOutlet weak var recipeImg: UIImageView!
    @IBOutlet weak var recipeName: UILabel!
    @IBOutlet weak var servingSizeLbl: UILabel!
    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var MMADBtn: UIButton!
    @IBOutlet weak var holdTightView: UIView!
    @IBOutlet weak var bottomMealView: UIView!
    @IBOutlet weak var swapView: UIView!
    @IBOutlet var stokeLbl: [UILabel]!
    @IBOutlet weak var BGLbl: UILabel!
    
    var mealPlanListArray = [MyMealPlanResponseField]()
    var mealPlanTableArray = [MyMealPlanField]()
    var drinksArray = [BeverageField]()
    var trainNrestdays = "0"
    var swapTraining = false
    var swapRest = false
    var microDict = [String:Any]()
    var index = 0
    var recipeDetails = false
    var swap = 0
    var drinksSelected = false
    var drinksTotal = [String:Any]()
    var mealNumber = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    func configureNavUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "My Meal Plan"
        myCustomView.logoImg.isHidden = true
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "menu")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
    }
    func configureUI(){
        self.mealListCollectionView.delegate = self
        self.mealListCollectionView.dataSource = self
        self.mealTableView.delegate = self
        self.mealTableView.dataSource = self
        self.macroView.isHidden = true
        self.resultsTableView.delegate = self
        self.resultsTableView.dataSource = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.BGLbl.isHidden = recipeDetails == false ? false : true
        configureNavUI()
        if recipeDetails == false{
          self.getMealPlanList()
        }
        recipeDetails = false
    }
    @IBAction func trainingDaysAndRestDaysBtnAction(_ sender: UIButton) {
        if sender.tag == 1 {
            trainingDaysBtn.backgroundColor = UIColor(named: "BGColor")
            trainingDaysBtn.setTitleColor(.white, for: .normal)
            restDaysBtn.setTitleColor(.black, for: .normal)
            restDaysBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            trainNrestdays = "0"
            swapBtn.setImage(#imageLiteral(resourceName: "swap-un"), for: .normal)
            self.getMealPlanList()
        }else if sender.tag == 2{
            restDaysBtn.backgroundColor = UIColor(named: "BGColor")
            trainingDaysBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            restDaysBtn.setTitleColor(.white, for: .normal)
            trainingDaysBtn.setTitleColor(.black, for: .normal)
            trainNrestdays = "1"
            swapBtn.setImage(#imageLiteral(resourceName: "swap-un"), for: .normal)
            self.getMealPlanList()
        }
    }
    func getMealPlanList(){
          if Reachability.isConnectedToNetwork() == true {
              DispatchQueue.main.async {
                  IJProgressView.shared.showProgressView()
              }
              let url = Constants.baseURL + Constants.myMealPlan + trainNrestdays
              var token = UserDefaults.standard.value(forKey: "token") as? String
              token = "Bearer " + token!
              let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { [self] (response) in
                  IJProgressView.shared.hideProgressView()
                self.holdTightView.isHidden = true
                self.BGLbl.isHidden = true
                  
                  self.mealPlanListArray.removeAll()
                  let message = response["responseMessage"] as? String ?? ""
                  mealNumber = response["meal_no"] as? String ?? ""
                  if let responseCode = response["responseCode"] as? Int{
                      if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            if let micro = responseData["bm_micro"] as? [String:Any]{
                                self.microDict = micro
                                self.macroView.isHidden = false
                                self.resultsTableView.reloadData()
                            }
                            if let drinksData = responseData["beverages"] as? [String:Any]{
                                if let dirnksTotalData = drinksData["drinks_total"] as? [String:Any]{
                                    drinksTotal = dirnksTotalData
                                }
                                if let drinks = drinksData["drinks"] as? [[String:Any]]{
                                    self.drinksArray.removeAll()
                                    if drinks.count != 0{
                                        for i in 0..<drinks.count{
                                            self.drinksArray.append(BeverageField(calorie_category: drinks[i]["calorie_category"] as? String ?? "", calories: drinks[i]["calories"] as? String ?? "", carb: drinks[i]["carb"] as? String ?? "", created_at: drinks[i]["created_at"] as? String ?? "", dietary_fibre: drinks[i]["dietary_fibre"] as? String ?? "", drink_calories: drinks[i]["drink_calories"] as? String ?? "", drink_carbs: drinks[i]["drink_carbs"] as? String ?? "", drink_dietary_fibre: drinks[i]["drink_dietary_fibre"] as? String ?? "", drink_fat: drinks[i]["drink_fat"] as? String ?? "", drink_id: drinks[i]["drink_id"] as? Int ?? 0, drink_name: drinks[i]["drink_name"] as? String ?? "", drink_protein: drinks[i]["drink_protein"] as? String ?? "", drink_serving_unit: drinks[i]["drink_serving_unit"] as? String ?? "", drink_serving_size: drinks[i]["drink_serving_size"] as? String ?? "", fat: drinks[i]["fat"] as? String ?? "", id: drinks[i]["id"] as? Int ?? 0, meal_id: drinks[i]["meal_id"] as? String ?? "", meal_no: drinks[i]["meal_no"] as? String ?? "", protein: drinks[i]["protein"] as? String ?? "", serving_quantity: drinks[i]["serving_quantity"] as? String ?? "", serving_size: drinks[i]["serving_size"] as? String ?? "", serving_unit: drinks[i]["serving_unit"] as? Int ?? 0, sub_category: drinks[i]["sub_category"] as? Int ?? 0, sub_category_name: drinks[i]["sub_category_name"] as? String ?? "", updated_at: drinks[i]["updated_at"] as? String ?? "", user_id: drinks[i]["user_id"] as? Int ?? 0))
                                        }
                                    }
                                }
                            }
                            if let mealPlan = responseData["meal_plan"] as? [[String:Any]]{
                                if mealPlan.count != 0{
                                    self.holdTightView.isHidden = true
                                    for i in 0..<mealPlan.count{
                                        var aFoodItem = [MyMealPlanField]()
                                        var foodItem = [MyMealPlanField]()
                                        var foodItemTotal = FoodItemTotal(total_cal: "", total_carb: "", total_fat: "", total_protein: "")
                                        var aFoodItemTotal = FoodItemTotal(total_cal: "", total_carb: "", total_fat: "", total_protein: "")
                                        var recipeData = RecipeField(calories: "", carb: "", fat: "", created_at: "", ingredients: [], protein: "", recipe_id: "", recipe_image: "", recipe_name: "", serves: 0, tip: "", topping: "", updated_at: "", method: [], per_serve_calories: "", per_serve_carb: "", per_serve_fat: "", per_serve_protein: "", recipe_serve: "", total_serve_calories: "", total_serve_carb: "", total_serve_fat: "", total_serve_protein: "", recipe_weight: "", weight: "", recipe_thumbnail_image: "")
                                        var swapCount = 2
                                        if let aFoodItemList = mealPlan[i]["a_food_item"] as? [[String:Any]]{
                                            for j in 0..<aFoodItemList.count{
                                                aFoodItem.append(MyMealPlanField(food_carb: aFoodItemList[j]["food_carb"] as? String ?? "", food_fat: aFoodItemList[j]["food_fat"] as? String ?? "", food_item_id: aFoodItemList[j]["food_item_id"] as? Int ?? 0, food_name: aFoodItemList[j]["food_name"] as? String ?? "", food_protien: aFoodItemList[j]["food_protien"] as? String ?? "", food_cal: aFoodItemList[j]["food_cal"] as? String ?? "", serving_size: aFoodItemList[j]["serving_size"] as? String ?? "", serving_unit: aFoodItemList[j]["serving_unit"] as? String ?? ""))
                                            }
                                        }
                                        if let foodItemList = mealPlan[i]["food_item"] as? [[String:Any]]{
                                            for j in 0..<foodItemList.count{
                                                foodItem.append(MyMealPlanField(food_carb: foodItemList[j]["food_carb"] as? String ?? "", food_fat: foodItemList[j]["food_fat"] as? String ?? "", food_item_id: foodItemList[j]["food_item_id"] as? Int ?? 0, food_name: foodItemList[j]["food_name"] as? String ?? "", food_protien: foodItemList[j]["food_protien"] as? String ?? "", food_cal: foodItemList[j]["food_cal"] as? String ?? "", serving_size: foodItemList[j]["serving_size"] as? String ?? "", serving_unit: foodItemList[j]["serving_unit"] as? String ?? ""))
                                            }
                                        }
                                        if let foodItemTot = mealPlan[i]["food_item_total"] as? [String:Any]{
                                            foodItemTotal = FoodItemTotal(total_cal: foodItemTot["total_cal"] as? String ?? "", total_carb: foodItemTot["total_carb"] as? String ?? "", total_fat: foodItemTot["total_fat"] as? String ?? "", total_protein: foodItemTot["total_protein"] as? String ?? "")
                                        }
                                        if let aFoodItemTot = mealPlan[i]["a_food_item_total"] as? [String:Any]{
                                            aFoodItemTotal = FoodItemTotal(total_cal: aFoodItemTot["total_cal"] as? String ?? "", total_carb: aFoodItemTot["total_carb"] as? String ?? "", total_fat: aFoodItemTot["total_fat"] as? String ?? "", total_protein: aFoodItemTot["total_protein"] as? String ?? "")
                                        }
                                        if let recipeDataDict = mealPlan[i]["recipe_data"] as? [String:Any]{
                                            recipeData = RecipeField(calories: recipeDataDict["calories"] as? String ?? "", carb: recipeDataDict["carb"] as? String ?? "", fat: recipeDataDict["fat"] as? String ?? "", created_at: recipeDataDict["created_at"] as? String ?? "", ingredients: recipeDataDict["ingredients"] as? [[String:Any]] ?? [], protein: recipeDataDict["protein"] as? String ?? "", recipe_id: recipeDataDict["recipe_id"] as? String ?? "", recipe_image: recipeDataDict["recipe_image"] as? String ?? "", recipe_name: recipeDataDict["recipe_name"] as? String ?? "", serves: recipeDataDict["serves"] as? Int ?? 0, tip: recipeDataDict["tip"] as? String ?? "", topping: recipeDataDict["topping"] as? String ?? "", updated_at: recipeDataDict["updated_at"] as? String ?? "", method: recipeDataDict["method"] as? [String] ?? [], per_serve_calories: recipeDataDict["per_serve_calories"] as? String ?? "", per_serve_carb: recipeDataDict["per_serve_carb"] as? String ?? "", per_serve_fat: recipeDataDict["per_serve_fat"] as? String ?? "", per_serve_protein: recipeDataDict["per_serve_protein"] as? String ?? "", recipe_serve: recipeDataDict["recipe_serve"] as? String ?? "", total_serve_calories: recipeDataDict["total_serve_calories"] as? String ?? "", total_serve_carb: recipeDataDict["total_serve_carb"] as? String ?? "", total_serve_fat: recipeDataDict["total_serve_fat"] as? String ?? "", total_serve_protein: recipeDataDict["total_serve_protein"] as? String ?? "", recipe_weight: recipeDataDict["recipe_weight"] as? String ?? "", weight: "", recipe_thumbnail_image: recipeDataDict["recipe_thumbnail_image"] as? String ?? "")
                                            swapCount = 3
                                        }
                                        self.mealPlanListArray.append(MyMealPlanResponseField(meal_name: mealPlan[i]["meal_name"] as? String ?? "", a_food_item: aFoodItem, food_item: foodItem, selected: false, swap: mealPlan[i]["swap"] as? Int ?? 0, food_item_total: foodItemTotal, a_food_item_total: aFoodItemTotal, recipe_data: recipeData, swapCount: swapCount, mark_meal_as_done: mealPlan[i]["mark_meal_as_done"] as? String ?? "", meal_plan_no: mealPlan[i]["meal_plan_no"] as? String ?? "",select_meal: 0,other_meal: 0, meal_id: 0))
                                    }
                                    self.index = 0
                                    self.drinksSelected = false
                                    self.updateDrinksUI()
                                    self.mealPlanListArray[self.index].selected = true
                                    mealListCollectionView.setContentOffset(.zero, animated: false)
                                }else{
                                    self.holdTightView.isHidden = false
                                }
                            }
                        }
                      }else{
                          IJProgressView.shared.hideProgressView()
                          alert(Constants.AppName, message: message, view: self)
                      }
                    self.updateMealUI()
                  }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                  IJProgressView.shared.hideProgressView()
                  alert(Constants.AppName, message: error.localizedDescription, view: self)
                  
              }
          } else {
              
              alert(Constants.AppName, message: "Check internet connection", view: self)
          }
      }
    func updateDrinksUI(){
        stokeLbl[1].backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8196078431, alpha: 1)
        swapView.isHidden = false
    }
    func updateMealUI(){
        if self.mealPlanListArray.count != 0 {
            MMADBtn.setImage(self.mealPlanListArray[index].mark_meal_as_done == "0" ? #imageLiteral(resourceName: "mark-meal-un") : #imageLiteral(resourceName: "mark-meal"), for: .normal)
            MMADBtn.isUserInteractionEnabled = self.mealPlanListArray[index].mark_meal_as_done == "0" ? true : false
            if self.mealPlanListArray[index].swap == 3{
                self.recipeView.isHidden = false
                self.showRecipeData()
                self.mealTableView.isHidden = true
                self.totalRecipeValuesUpdation()
            }else if self.mealPlanListArray[index].swap == 1{
                self.mealPlanTableArray = self.mealPlanListArray[index].food_item
                self.mealTableView.reloadData()
                self.totalValuesUpdation()
                self.recipeView.isHidden = true
                self.mealTableView.isHidden = false
            }else if self.mealPlanListArray[index].swap == 2{
                self.mealPlanTableArray = self.mealPlanListArray[index].a_food_item
                self.mealTableView.reloadData()
                self.totalAValuesUpdation()
                self.recipeView.isHidden = true
                self.mealTableView.isHidden = false
            }
            self.mealListCollectionView.reloadData()
        }
    }
    
    @IBAction func markMealAsDoneBtnAction(_ sender: UIButton) {
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.markMealAsDone
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(from : "mmad"), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    self.mealPlanListArray[self.index].mark_meal_as_done = "1"
                    self.updateMealUI()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    @IBAction func swapBtnAction(_ sender: UIButton) {
        if mealPlanListArray[index].swapCount == 2{
            if mealPlanListArray[index].swap == 1{
                self.swap = 2
            }else{
                self.swap = 1
            }
        }else if mealPlanListArray[index].swapCount == 3{
            if mealPlanListArray[index].swap == 1{
                self.swap = 2
            }else if mealPlanListArray[index].swap == 2{
                self.swap = 3
            }else if mealPlanListArray[index].swap == 3{
                self.swap = 1
            }
        }
        self.swapApi()
    }
    func showRecipeData(){
        self.recipeImg.sd_setImage(with: URL(string: mealPlanListArray[index].recipe_data?.recipe_image ?? ""), placeholderImage: UIImage(named: "ic_recipe_details_pp"))
        self.recipeName.text = mealPlanListArray[index].recipe_data?.recipe_name
        self.servingSizeLbl.text = "Serving size: \(mealPlanListArray[index].recipe_data?.recipe_serve ?? "")g"
        self.weightLbl.text = mealPlanListArray[index].recipe_data?.recipe_weight != "" ? "Serving Weight: \(mealPlanListArray[index].recipe_data?.recipe_weight ?? "")g" : ""
    }
    func swapApi(){
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.doSwap
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(from : "swap"), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    self.mealPlanListArray[self.index].swap = self.swap
                    self.updateMealUI()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
        }
    }
    func generatingParameters(from: String) -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["meal_plan_no"] = mealPlanListArray[index].meal_plan_no as AnyObject
        parameters["meal_name"] = mealPlanListArray[index].meal_name as AnyObject
        parameters["meal_type"] = trainNrestdays as AnyObject
        if from == "swap"{
            parameters["swap_no"] = "\(swap)" as AnyObject
        }
        return parameters
    }
    @IBAction func goToQuestionnaireBtnAction(_ sender: UIButton) {
            if mealNumber == "1"{
                let questionnaireVC = QuestionnaireContainerVC.instantiate(fromAppStoryboard: .Questionnaire)
                questionnaireVC.currentIndex = 11
                questionnaireVC.backStopIndex = 11
                UserDefaults.standard.set(11, forKey: "currentIndex")
                self.navigationController?.pushViewController(questionnaireVC, animated: false)
            }else{
                let questionnaireVC = CheckInContainerVC.instantiate(fromAppStoryboard: .CheckInQuestionnaire)
                self.navigationController?.pushViewController(questionnaireVC, animated: false)
            }
    }
    @IBAction func viewDetailsBtnAction(_ sender: UIButton) {
      let VC = RecipeDetailsVC.instantiate(fromAppStoryboard: .SideMenu)
        VC.recipeDetails = [mealPlanListArray[index].recipe_data!]
        VC.fromQuestionnaire = false
        recipeDetails = true
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

extension MyMealPlanVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return drinksArray.count != 0 ? mealPlanListArray.count+1 : mealPlanListArray.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MealListCollectionCell
        if indexPath.item == mealPlanListArray.count {
            cell.nameLbl.text = "Beverages"
            cell.selectedLbl.backgroundColor = index == indexPath.item ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            cell.nameLbl.textColor =  index == indexPath.item  ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .black
        }else{
            cell.nameLbl.text = mealPlanListArray[indexPath.item].meal_name
            cell.selectedLbl.backgroundColor = mealPlanListArray[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            cell.nameLbl.textColor = mealPlanListArray[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .black
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for i in 0..<mealPlanListArray.count{
            mealPlanListArray[i].selected = i == indexPath.item ? true : false
        }
        index = indexPath.item
        mealListCollectionView.reloadData()
        if drinksArray.count != 0 && indexPath.item == mealPlanListArray.count{
            mealTableView.isHidden = false
            recipeView.isHidden = true
            swapView.isHidden = true
            stokeLbl[1].backgroundColor = .clear
            drinksSelected = true
            mealTableView.reloadData()
            updateDrinksTotalValuesUpdation()
        }else{
            stokeLbl[1].backgroundColor = #colorLiteral(red: 0.8196078431, green: 0.8196078431, blue: 0.8196078431, alpha: 1)
            swapView.isHidden = false
            drinksSelected = false
            updateMealUI()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont(name:"Poppins-Regular", size: 16.0)
        if indexPath.item == mealPlanListArray.count{
            label.text = "Beverages"
        }else{
            label.text = self.mealPlanListArray[indexPath.item].meal_name
        }
        label.sizeToFit()
        return CGSize(width: label.frame.size.width+30, height: mealListCollectionView.frame.size.height)
    }
    func totalValuesUpdation(){
        self.proteinLbl.text = self.mealPlanListArray[index].food_item_total?.total_protein == "" ? "-" : "\(self.mealPlanListArray[index].food_item_total?.total_protein ?? "") g"
        self.carbsLbl.text = self.mealPlanListArray[index].food_item_total?.total_carb == "" ? "-" : "\(self.mealPlanListArray[index].food_item_total?.total_carb ?? "") g"
        self.fatLbl.text = self.mealPlanListArray[index].food_item_total?.total_fat == "" ? "-" : "\(self.mealPlanListArray[index].food_item_total?.total_fat ?? "") g"
        self.caloriesLbl.text = self.mealPlanListArray[index].food_item_total?.total_cal == "" ? "-" : "\(self.mealPlanListArray[index].food_item_total?.total_cal ?? "") kCal"
    }
    func totalAValuesUpdation(){
        self.proteinLbl.text = self.mealPlanListArray[index].a_food_item_total?.total_protein == "" ? "-" : "\(self.mealPlanListArray[index].a_food_item_total?.total_protein ?? "") g"
        self.carbsLbl.text = self.mealPlanListArray[index].a_food_item_total?.total_carb == "" ? "-" : "\(self.mealPlanListArray[index].a_food_item_total?.total_carb ?? "") g"
        self.fatLbl.text = self.mealPlanListArray[index].a_food_item_total?.total_fat == "" ? "-" : "\(self.mealPlanListArray[index].a_food_item_total?.total_fat ?? "") g"
        self.caloriesLbl.text = self.mealPlanListArray[index].a_food_item_total?.total_cal == "" ? "-" : "\(self.mealPlanListArray[index].a_food_item_total?.total_cal ?? "") kCal"
    }
    func totalRecipeValuesUpdation(){
           self.proteinLbl.text = self.mealPlanListArray[index].recipe_data?.total_serve_protein == "" ? "-" : "\(self.mealPlanListArray[index].recipe_data?.total_serve_protein ?? "") g"
           self.carbsLbl.text = self.mealPlanListArray[index].recipe_data?.total_serve_carb == "" ? "-" : "\(self.mealPlanListArray[index].recipe_data?.total_serve_carb ?? "") g"
           self.fatLbl.text = self.mealPlanListArray[index].recipe_data?.total_serve_fat == "" ? "-" : "\(self.mealPlanListArray[index].recipe_data?.total_serve_fat ?? "") g"
           self.caloriesLbl.text = self.mealPlanListArray[index].recipe_data?.total_serve_calories == "" ? "-" : "\(self.mealPlanListArray[index].recipe_data?.total_serve_calories ?? "") kCal"
       }
    func updateDrinksTotalValuesUpdation(){
        if let totalProtein = drinksTotal["total_protein"] as? String{
            self.proteinLbl.text =  totalProtein == "" ? "-" : "\(totalProtein) g"
        }
        if let totalCal = drinksTotal["total_cal"] as? String{
            self.caloriesLbl.text =  totalCal == "" ? "-" : "\(totalCal) kCal"
        }
        if let totalCarb = drinksTotal["total_carb"] as? String{
            self.carbsLbl.text =  totalCarb == "" ? "-" : "\(totalCarb) g"
        }
        if let totalFat = drinksTotal["total_fat"] as? String{
            self.fatLbl.text = totalFat == "" ? "-" : "\(totalFat) g"
        }
    }
    
}
extension MyMealPlanVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == resultsTableView{
            return 4
        }else{
            return drinksSelected == true ? drinksArray.count : mealPlanTableArray.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if tableView == resultsTableView{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ResultsCell
            cell.selectionStyle = .none
            cell.progressBGLbl.backgroundColor = indexPath.row == 0 ? #colorLiteral(red: 0.7458475828, green: 0.899774611, blue: 0.9934951663, alpha: 1) : indexPath.row == 1 ? #colorLiteral(red: 0.9912576079, green: 0.7879117727, blue: 0.6135630012, alpha: 1) : indexPath.row == 2 ? #colorLiteral(red: 0.6887137294, green: 1, blue: 0.6693044305, alpha: 1) : #colorLiteral(red: 0.9906267524, green: 0.7774583697, blue: 0.8127933741, alpha: 1)
            cell.progressLbl.backgroundColor = indexPath.row == 0 ? #colorLiteral(red: 0.1690995097, green: 0.7477579713, blue: 0.9476557374, alpha: 1) : indexPath.row == 1 ? #colorLiteral(red: 0.9287672639, green: 0.6092590094, blue: 0.3520768881, alpha: 1) : indexPath.row == 2 ? #colorLiteral(red: 0.3553128839, green: 0.790861547, blue: 0.338606596, alpha: 1) : #colorLiteral(red: 0.9580395818, green: 0.3819159865, blue: 0.4740775824, alpha: 1)
            cell.imgVw.image =  indexPath.row == 0 ? #imageLiteral(resourceName: "calories") : indexPath.row == 1 ? #imageLiteral(resourceName: "protein") : indexPath.row == 2 ? #imageLiteral(resourceName: "carb") : #imageLiteral(resourceName: "fat")
            cell.titleLbl.text = indexPath.row == 0 ? "Calories" : indexPath.row == 1 ? "Protein" : indexPath.row == 2 ? "Carbs" : "Fat"
            cell.titleLbl.textColor = indexPath.row == 0 ? #colorLiteral(red: 0.2666666667, green: 0.737254902, blue: 0.9294117647, alpha: 1) : indexPath.row == 1 ? #colorLiteral(red: 0.8823529412, green: 0.568627451, blue: 0.3960784314, alpha: 1) : indexPath.row == 2 ? #colorLiteral(red: 0.4941176471, green: 0.7764705882, blue: 0.4274509804, alpha: 1) : #colorLiteral(red: 0.8901960784, green: 0.4196078431, blue: 0.4823529412, alpha: 1)
            cell.resultsLbl.textColor = indexPath.row == 0 ? #colorLiteral(red: 0.2666666667, green: 0.737254902, blue: 0.9294117647, alpha: 1) : indexPath.row == 1 ? #colorLiteral(red: 0.8823529412, green: 0.568627451, blue: 0.3960784314, alpha: 1) : indexPath.row == 2 ? #colorLiteral(red: 0.4941176471, green: 0.7764705882, blue: 0.4274509804, alpha: 1) : #colorLiteral(red: 0.8901960784, green: 0.4196078431, blue: 0.4823529412, alpha: 1)
            cell.resultsLbl.text = indexPath.row == 0 ? String(format: "%.02f kCal", microDict["calories"] as? Double ?? 0.0) : indexPath.row == 1 ? String(format: "%.02f g", microDict["protein"] as? Double ?? 0.0) : indexPath.row == 2 ? String(format: "%.02f g", microDict["carb"] as? Double ?? 0.0) : String(format: "%.02f g", microDict["fat"] as? Double ?? 0.0)
            cell.progressLblWidth.constant =  indexPath.row == 0 ? cell.progressBGLbl.frame.size.width * 0.3 : indexPath.row == 1 ? cell.progressBGLbl.frame.size.width * 0.6 : indexPath.row == 2 ? cell.progressBGLbl.frame.size.width * 0.9 : cell.progressBGLbl.frame.size.width * 0.5
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MealPlanCell
            if drinksSelected == false{
                cell.foodNameLbl.text = mealPlanTableArray[indexPath.row].food_name
                cell.carbsLbl.text = mealPlanTableArray[indexPath.row].food_carb == "" ? "-" : "\(mealPlanTableArray[indexPath.row].food_carb ?? "") g"
                cell.fatLbl.text = mealPlanTableArray[indexPath.row].food_fat == "" ? "-" : "\(mealPlanTableArray[indexPath.row].food_fat ?? "") g"
                cell.protienLbl.text = mealPlanTableArray[indexPath.row].food_protien == "" ? "-" : "\(mealPlanTableArray[indexPath.row].food_protien ?? "") g"
                cell.totalCalLbl.text = mealPlanTableArray[indexPath.row].food_cal == "" ? "-" : "\(mealPlanTableArray[indexPath.row].food_cal ?? "") kCal"
                cell.totalLbl.text = "Serving size:\(mealPlanTableArray[indexPath.row].serving_size ?? "")\(mealPlanTableArray[indexPath.row].serving_unit ?? "")"
            }else{
                cell.foodNameLbl.text = drinksArray[indexPath.row].drink_name
                cell.carbsLbl.text = drinksArray[indexPath.row].carb == "" ? "-" : "\(drinksArray[indexPath.row].carb ?? "") g"
                cell.fatLbl.text = drinksArray[indexPath.row].fat == "" ? "-" : "\(drinksArray[indexPath.row].fat ?? "") g"
                cell.protienLbl.text = drinksArray[indexPath.row].protein == "" ? "-" : "\(drinksArray[indexPath.row].protein ?? "") g"
                cell.totalCalLbl.text = drinksArray[indexPath.row].calories == "" ? "-" : "\(drinksArray[indexPath.row].calories ?? "") kCal"
                cell.totalLbl.text = "Serving size:\(drinksArray[indexPath.row].serving_quantity ?? "")\(drinksArray[indexPath.row].drink_serving_unit ?? "")"
            }
            return cell
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == resultsTableView{
            return 75
        }else{
            return UITableView.automaticDimension
        }
    }
}

//MARK:- NavBar Delegate Method(s)

extension MyMealPlanVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.sideMenuController?.showLeftView(animated: true, completionHandler: {
            if let leftVC = self.sideMenuController?.leftViewController as? SideMenuVC {
                leftVC.menuTableView.reloadData()
            }
        })
    }
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}


