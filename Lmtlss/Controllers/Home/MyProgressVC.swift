//
//  MyProgressVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 16/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MyProgressCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var resultLbl: UILabel!
}
class MyProgressVC: UIViewController {
    var progressArray = [("Track Weight",""), ("Track Sleep",""), ("Track Water Intake",""),("Track Steps","")]
    
    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var progressTableView: UITableView!
    var weightUnit = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.updateBtn(notification:)), name: Notification.Name("updateProgress"), object: nil)
        
    }
    @objc func updateBtn(notification: Notification) {
       configureUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "My Progress"
        myCustomView.logoImg.isHidden = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "menu")
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        getRecords()
        self.progressTableView.delegate = self
        self.progressTableView.dataSource = self
    }
   
    func getRecords(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.getRecord
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.progressArray[0].1 = responseData["weight"] as? String ?? ""
                            self.progressArray[1].1 = responseData["total_sleep"] as? String ?? "0"
                            self.progressArray[2].1 = responseData["total_water_intake"] as? String ?? "0"
                           // self.progressArray[3].1 = "0"
                            self.weightUnit = responseData["weight_unit"] as? String ?? ""
                            self.progressArray[3].1 = responseData["total_steps"] as? String ?? "0"
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.progressTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func timeString(time: TimeInterval) -> String {
        let hour = Int(time) / 3600
        let minute = Int(time) / 60 % 60
        let second = Int(time) % 60

        // return formated string
     //   return String(format: "%02i:%02i:%02i", hour, minute, second)
        return String(format: "%02i:%02i", hour, minute)

    }
}
extension MyProgressVC : UITableViewDelegate,UITableViewDataSource {
   func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return progressArray.count
    
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! MyProgressCell
        cell.titleLbl.text = progressArray[indexPath.row].0
        if indexPath.row == 0{
            cell.resultLbl.text = "\(self.getWeight(weight: progressArray[indexPath.row].1, weightUnit: self.weightUnit)) \(self.weightUnit)"
        }else if indexPath.row == 1{
            if progressArray[indexPath.row].1 != ""{
                let hrs =  Int(progressArray[indexPath.row].1)
                var minutes = Int(progressArray[indexPath.row].1)
                minutes = minutes! * 60
               let timeStr = timeString(time: TimeInterval(minutes ?? 0))
                cell.resultLbl.text = hrs ?? 0 <= 60 ? "\(timeStr) hr" : "\(timeStr) hrs"
            }
        }else if indexPath.row == 2{
            cell.resultLbl.text = "\(progressArray[indexPath.row].1) Ltr"
        }
//        else if indexPath.row == 3{
//            cell.resultLbl.text = "\(progressArray[indexPath.row].1)"
//        }
        else if indexPath.row == 3{
            cell.resultLbl.text = "\(progressArray[indexPath.row].1)"
        }
        
        return cell
    }
    func getWeight(weight : String, weightUnit : String) -> String
    {
        if weightUnit == "lbs"
        {
            let myweight : Double = Double(weight) ?? 0.0
            let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.kilograms)
            let lbWeight = kgWeight.converted(to: UnitMass.pounds)
            let weight = String(format: "%.2f", lbWeight.value)
            
            return weight
        }
        else
        {
            return "\(Int(weight) ?? 0)"
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      let VC = ProgressDetailsVC.instantiate(fromAppStoryboard: .Home)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}
//MARK:- NavBar Delegate Method(s)

extension MyProgressVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.sideMenuController?.showLeftView(animated: true, completionHandler: {
            if let leftVC = self.sideMenuController?.leftViewController as? SideMenuVC {
                leftVC.menuTableView.reloadData()
            }
        })
     //  self.sideMenuController?.showLeftViewAnimated()
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
