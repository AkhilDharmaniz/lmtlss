//
//  MiddleTabVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 15/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import LKAWaveCircleProgressBar
import JKCircularProgess
import HealthKit
import Alamofire

class MiddleTabVC: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var waterView: LKAWaveCircleProgressBar!
    @IBOutlet weak var stepsView: JKCircleView!
    @IBOutlet weak var sleepTF: UITextField!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var quaterLiterLbl: UILabel!
    @IBOutlet weak var halfLiterLbl: UILabel!
    @IBOutlet weak var footSteps: UILabel!
    @IBOutlet weak var totalFootSteps: UILabel!
    @IBOutlet weak var literLbl: UILabel!
    @IBOutlet weak var waterTakenLbl: UILabel!
    @IBOutlet weak var totalWaterLbl: UILabel!
    @IBOutlet weak var weightUnitLbl: UILabel!
    
    var healthScore = HKHealthStore()
    let timePicker = UIDatePicker()
    let sleepPicker = UIPickerView()
    var hour:Int = 0
    var minutes:Int = 0
    var seconds:Int = 0
    var water = ""
    var totalWater = ""
    var totalSteps = ""
    var waterTaken = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    func configureUI(){
        self.weightTF.delegate = self
        timePicker.datePickerMode = .time
        timePicker.locale = Locale(identifier: "en_GB")
        if #available(iOS 13.4, *) {
            self.timePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
        }
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        if let date = dateFormatter.date(from: "00:00") {
            print(date) // 2000-01-01 22:00:00 +0000
            timePicker.date = date
        }
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(secondDonePressed))
        toolBar.setItems([doneButton], animated: true)
        sleepTF.inputAccessoryView = toolBar
        sleepTF.inputView = timePicker
        self.waterView.progressAnimationDuration = 0.7
        self.waterView.waveRollingDuration = 2
        self.waterView.progressTintColor = #colorLiteral(red: 0.1725120544, green: 0.7595852017, blue: 0.9552707076, alpha: 0.7003331097)
        self.waterView.startWaveRollingAnimation()
        stepsView.animationDuration = 3
        stepsView.lineWidth = 13
        stepsView.isToRemoveLayerAfterCompletion = false
        //        DispatchQueue.main.async {
        //            self.addFullCircleView()
        //        }
        let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)! ]
        // Check for Authorization
        healthScore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (bool, error) in
            if (bool) {
                // Authorization Successful
                self.getTodaysSteps { (result) in
                    DispatchQueue.main.async {
                        let stepCount = String(Int(result))
                        print(String(stepCount))
                        self.footSteps.text = String(stepCount)
                        self.getRecords()
                        self.saveSteps()
                        //   self.stepLbl.text = String(stepCount)
                    }
                }
            }
        }
        
    }
    func getRecords(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.getRecord
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.waterTaken = responseData["total_water_intake"] as? String ?? "0"
                            self.waterTakenLbl.text = self.waterTaken
                            self.totalSteps = responseData["steps_per_day"] as? String ?? "0"
                            self.weightUnitLbl.text = responseData["weight_unit"] as? String ?? "" == "kg" ? "KG" : "LBS"
                            if self.totalSteps != ""{
                                self.totalFootSteps.text = "/\(responseData["steps_per_day"] as? String ?? "0") steps"
                                let maxSteps = Double(self.totalSteps)
                                let walkedSteps = Double(self.footSteps.text!)
                                let steps = walkedSteps! / maxSteps!
                                //  let stepsPercentage = steps * 100
                                DispatchQueue.main.async {
                                    self.stepsView.animateCircle(angle: CGFloat(steps))
                                }
                            }else{
                                DispatchQueue.main.async {
                                    self.stepsView.animateCircle(angle: 0.0)
                                }
                            }
                            self.totalWater = responseData["water_per_day"] as? String ?? "0"
                            if self.totalWater != ""{
                                self.totalWaterLbl.text = "/\(responseData["water_per_day"] as? String ?? "0") litres"
                            }
                            let maxWater = Double(self.totalWater)
                            let totalWaterTaken = Double(self.waterTaken)
                            let avgWater = totalWaterTaken! / maxWater!
                            //   let waterPercentage = avgWater * 100
                            self.waterView.progress = CGFloat(avgWater)
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func saveSteps(){
        if Reachability.isConnectedToNetwork() == true {
            let url = Constants.baseURL + Constants.recordSteps
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            AFWrapperClass.requestPostWithMultiFormData(url, params: generatingParameters(savedItem : "steps"), headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.view.endEditing(true)
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        NotificationCenter.default.post(name: Notification.Name("updateProgress"), object: nil, userInfo: nil)
                    }else{
                        //  alert(Constants.AppName, message: message, view: self)
                    }
                }
                //                if let status = response["status"] as? Int{
                //                    if status == 401{
                //                        showAlertMessage(title: Constants.AppName, message: "User not found", okButton: "OK", controller: self) {
                //                            DispatchQueue.main.async {
                //                                appDelegate().logOutSuccess()
                //                            }
                //                        }
                //                    }
                //                }
            }) { (error) in
                //                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func getTodaysSteps(completion: @escaping (Double) -> Void) {
        let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!
        
        let now = Date()
        let startOfDay = Calendar.current.startOfDay(for: now)
        let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)
        
        let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { _, result, _ in
            guard let result = result, let sum = result.sumQuantity() else {
                completion(0.0)
                return
            }
            completion(sum.doubleValue(for: HKUnit.count()))
        }
        
        healthScore.execute(query)
    }
    @objc func secondDonePressed(){
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm"
        let dateSelect = dateFormatter.string(from: timePicker.date)
        self.sleepTF.text = dateSelect
        self.view.endEditing(true)
    }
    func addFullCircleView() {
        stepsView.animateCircle(angle: 0.0)
    }
    override func viewDidLayoutSubviews() {
        mainView.layer.cornerRadius = 25
        stepsView.layer.cornerRadius = stepsView.frame.size.height/2
        waterView.layer.cornerRadius = waterView.frame.size.height/2
        if #available(iOS 11, *) {
            mainView.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        }
    }
    
    @IBAction func backBtnAction(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
        
    }
    
    @IBAction func sleepSaveBtnAction(_ sender: UIButton) {
        if sleepTF.text == ""{
            alert(Constants.AppName, message: "Please enter sleeping hours", view: self)
        }else{
            saveInfo(savedItem: "sleep")
        }
    }
    
    @IBAction func weightSaveBtnAction(_ sender: UIButton) {
        if weightTF.text == ""{
            alert(Constants.AppName, message: "Please enter weight", view: self)
        }else{
            saveInfo(savedItem: "weight")
        }
    }
    
    @IBAction func waterAddBtnAction(_ sender: UIButton) {
        if water == ""{
            alert(Constants.AppName, message: "Please select water quantity", view: self)
        }else{
            saveInfo(savedItem: "water")
        }
    }
    func saveInfo(savedItem: String){
        if Reachability.isConnectedToNetwork() == true {
            IJProgressView.shared.showProgressView()
            var url = ""
            if savedItem == "weight"{
                url = Constants.baseURL + Constants.recordWeight
            }
            else if savedItem == "water"{
                url = Constants.baseURL + Constants.recordWaterIntake
            }
            else if savedItem == "sleep" {
                url = Constants.baseURL + Constants.recordSleep
            }
            //            else if savedItem == "steps" {
            //                url = Constants.baseURL + Constants.recordSteps
            //            }
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            AFWrapperClass.requestPostWithMultiFormData(url, params: generatingParameters(savedItem : savedItem), headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                self.view.endEditing(true)
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        NotificationCenter.default.post(name: Notification.Name("updateProgress"), object: nil, userInfo: nil)
                        showAlertMessage(title: Constants.AppName, message: message, okButton: "OK", controller: self) {
                            
                            if savedItem == "sleep"{
                                self.sleepTF.text = ""
                            }else if savedItem == "weight"{
                                self.weightTF.text = ""
                            }else if savedItem == "water"{
                                self.water = ""
                                self.quaterLiterLbl.backgroundColor = .clear
                                self.quaterLiterLbl.textColor = .darkGray
                                self.halfLiterLbl.backgroundColor = .clear
                                self.halfLiterLbl.textColor = .darkGray
                                self.literLbl.backgroundColor = .clear
                                self.literLbl.textColor = .darkGray
                            }
                            self.getRecords()
                            //  self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func generatingParameters(savedItem : String) -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        if savedItem == "weight" {
            parameters["weight"] = getWeight(weight: weightTF.text!) as AnyObject
            
        }
        else if savedItem == "water"{
            parameters["water_intake"] = water as AnyObject
        }
        else if savedItem == "sleep" {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm" //Your date format
            dateFormatter.timeZone = TimeZone.current //Current time zone
            let date = dateFormatter.date(from: sleepTF.text!) //according to date format your date string
            let calendar = Calendar.current
            let comp = calendar.dateComponents([.hour, .minute], from: date!)
            let hour = comp.hour ?? 0
            let minute = comp.minute ?? 0
            let finalMinut:Int = (hour * 60) + minute
            parameters["sleep"] = "\(finalMinut)" as AnyObject
        }
        else if savedItem == "steps"{
            parameters["steps"] = footSteps.text as AnyObject
        }
        
        
        return parameters
    }
    func getWeight(weight : String) -> String
    {
        if weightUnitLbl.text == "LBS"{
            let myweight : Double = Double(weight) ?? 0.0
            let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.pounds)
            let lbWeight = kgWeight.converted(to: UnitMass.kilograms)
            let weight = String(format: "%.8f", lbWeight.value)
            
            return weight
        }else{
            return "\(Int(weight) ?? 0)"
        }
    }
    @IBAction func selectedBottleBtnAction(_ sender: UIButton) {
        if sender.tag == 1{
            water = "0.25"
            quaterLiterLbl.backgroundColor = #colorLiteral(red: 0.2079787254, green: 0.875400126, blue: 0.9378625751, alpha: 1)
            quaterLiterLbl.textColor = .white
            halfLiterLbl.backgroundColor = .clear
            halfLiterLbl.textColor = .darkGray
            literLbl.backgroundColor = .clear
            literLbl.textColor = .darkGray
        }else if sender.tag == 2{
            water = "0.5"
            halfLiterLbl.backgroundColor = #colorLiteral(red: 0.2079787254, green: 0.875400126, blue: 0.9378625751, alpha: 1)
            halfLiterLbl.textColor = .white
            quaterLiterLbl.backgroundColor = .clear
            quaterLiterLbl.textColor = .darkGray
            literLbl.backgroundColor = .clear
            literLbl.textColor = .darkGray
        }else if sender.tag == 3{
            water = "1"
            literLbl.backgroundColor = #colorLiteral(red: 0.2079787254, green: 0.875400126, blue: 0.9378625751, alpha: 1)
            literLbl.textColor = .white
            quaterLiterLbl.backgroundColor = .clear
            quaterLiterLbl.textColor = .darkGray
            halfLiterLbl.backgroundColor = .clear
            halfLiterLbl.textColor = .darkGray
        }
    }
}
extension MiddleTabVC : UITextFieldDelegate {
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {
        
        let countdots =  (textField.text?.components(separatedBy: (".")).count)! - 1
        
        if (countdots > 0 && string == "."){
            return false
        }
        let dotString = "."
        if let text = textField.text {
            let isDeleteKey = string.isEmpty
            if !isDeleteKey {
                if text.contains(dotString) {
                    if text.components(separatedBy: dotString)[1].count == 3 {
                        return false
                    }
                }
            }
        }
        return true
    }
}
