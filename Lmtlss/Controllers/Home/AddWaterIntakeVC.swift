//
//  AddWaterIntakeVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 15/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import HealthKit

enum AddRecords {
    case trackWeight, trackSleep, trackWaterIntake, trackCalories, trackSteps
}
class AddWaterIntakeVC: UIViewController {
    var addRecords:AddRecords?
    let timePicker = UIDatePicker()
    let sleepPicker = UIPickerView()
    var hour:Int = 0
    var minutes:Int = 0
    var seconds:Int = 0
    @IBOutlet weak var waterTF: UITextField!
    @IBOutlet weak var navView: UIView!
    var healthScore = HKHealthStore()

    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = true
        myCustomView.titleLbl.text = ""
        myCustomView.logoImg.isHidden = false
        myCustomView.searchImage.isHidden = true
        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        if addRecords == .trackWeight{
            waterTF.placeholder = "Enter Weight (Kg)"
            waterTF.keyboardType = .decimalPad
            waterTF.delegate = self
        }else if addRecords == .trackWaterIntake{
            waterTF.placeholder = "Enter Water (Liters)"
            waterTF.keyboardType = .decimalPad
            waterTF.delegate = self
        }else if addRecords == .trackSleep {
            waterTF.placeholder = "Select Sleep"
            timePicker.datePickerMode = .time
            timePicker.locale = Locale(identifier: "en_GB")
            let toolBar = UIToolbar()
            toolBar.sizeToFit()
            let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(secondDonePressed))
            toolBar.setItems([doneButton], animated: true)
            waterTF.inputAccessoryView = toolBar
            waterTF.inputView = timePicker
            
//            self.sleepPicker.delegate = self
//            self.sleepPicker.dataSource = self
//            waterTF.inputView = sleepPicker
        }else if addRecords == .trackSteps{
            //            waterTF.text = "Enter Water (Liters)"
            //            waterTF.keyboardType = .decimalPad
            //            waterTF.delegate = self
            let healthKitTypes: Set = [ HKObjectType.quantityType(forIdentifier: HKQuantityTypeIdentifier.stepCount)! ]
            // Check for Authorization
            healthScore.requestAuthorization(toShare: healthKitTypes, read: healthKitTypes) { (bool, error) in
                if (bool) {
                    // Authorization Successful
                    self.getTodaysSteps { (result) in
                        DispatchQueue.main.async {
                            let stepCount = String(Int(result))
                            self.waterTF.text = String(stepCount)
                            self.waterTF.isUserInteractionEnabled = false
                            //   self.stepLbl.text = String(stepCount)
                        }
                    }
                }
            }
            
        }
    }
    func getSteps(completion: @escaping (Double) -> Void){
           let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!

           let now = Date()
           let exactlySevenDaysAgo = Calendar.current.date(byAdding: DateComponents(day: -7), to: now)!
           let predicate = HKQuery.predicateForSamples(withStart: exactlySevenDaysAgo, end: now, options: .strictStartDate)

           let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { (_, result, error) in
               var resultCount = 0.0

               guard let result = result else {
                   completion(resultCount)
                   return
               }

               if let sum = result.sumQuantity() {
                   resultCount = sum.doubleValue(for: HKUnit.count())
               }

               DispatchQueue.main.async {
                   completion(resultCount)
               }
           }

           healthScore.execute(query)
       }
       func getTodaysSteps(completion: @escaping (Double) -> Void) {
           let stepsQuantityType = HKQuantityType.quantityType(forIdentifier: .stepCount)!

           let now = Date()
           let startOfDay = Calendar.current.startOfDay(for: now)
           let predicate = HKQuery.predicateForSamples(withStart: startOfDay, end: now, options: .strictStartDate)

           let query = HKStatisticsQuery(quantityType: stepsQuantityType, quantitySamplePredicate: predicate, options: .cumulativeSum) { _, result, _ in
               guard let result = result, let sum = result.sumQuantity() else {
                   completion(0.0)
                   return
               }
               completion(sum.doubleValue(for: HKUnit.count()))
           }

           healthScore.execute(query)
       }
    @objc func secondDonePressed(){
        let dateFormatter = DateFormatter()
       dateFormatter.dateFormat = "HH:mm"
        let dateSelect = dateFormatter.string(from: timePicker.date)
        self.waterTF.text = dateSelect
        self.view.endEditing(true)
    }
    @IBAction func submitBtnAction(_ sender: UIButton) {
        let isValid : Bool = validateScreenData()
        if isValid == false{
            saveWaterIntake()
        }
    }
    func validateScreenData() -> Bool{
        
        if waterTF.text == ""{
            if addRecords == .trackWeight {
             alert(Constants.AppName, message: "Please enter weight", view: self)
            }else if addRecords == .trackWaterIntake {
                alert(Constants.AppName, message: "Please enter water quantity", view: self)
            }
            return true
        }
        return false
    }

    func saveWaterIntake(){
        if Reachability.isConnectedToNetwork() == true {
            IJProgressView.shared.showProgressView()
            var url = ""
            if addRecords == .trackWeight{
              url = Constants.baseURL + Constants.recordWeight
            }else if addRecords == .trackWaterIntake{
              url = Constants.baseURL + Constants.recordWaterIntake
            }else if addRecords == .trackSleep {
              url = Constants.baseURL + Constants.recordSleep
            }else if addRecords == .trackSteps {
              url = Constants.baseURL + Constants.recordSteps
            }
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            AFWrapperClass.requestPostWithMultiFormData(url, params: generatingParameters(), headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        showAlertMessage(title: Constants.AppName, message: "Saved successfully", okButton: "OK", controller: self) {
                            self.navigationController?.popViewController(animated: true)
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        if addRecords == .trackWeight {
           parameters["weight"] = waterTF.text as AnyObject
        }else if addRecords == .trackWaterIntake{
           parameters["water_intake"] = waterTF.text as AnyObject
        }else if addRecords == .trackSleep {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm" //Your date format
            dateFormatter.timeZone = TimeZone.current //Current time zone
            let date = dateFormatter.date(from: waterTF.text!) //according to date format your date string
            let calendar = Calendar.current
            let comp = calendar.dateComponents([.hour, .minute], from: date!)
            let hour = comp.hour ?? 0
            let minute = comp.minute ?? 0
            let finalMinut:Int = (hour * 60) + minute
            parameters["sleep"] = "\(finalMinut)" as AnyObject
        }else if addRecords == .trackSteps{
           parameters["steps"] = waterTF.text as AnyObject
        }
        
        
        return parameters
    }
    
}
//MARK:- Nav bar delegate Method(s)
extension AddWaterIntakeVC: CustomHeaderViewDelegate{
    func searchBtnTapped() {
        
    }
    
    func openSideMenu() {
        self.navigationController?.popViewController(animated: true)
    }
}
extension AddWaterIntakeVC : UITextFieldDelegate {
    func textField(_ textField: UITextField,shouldChangeCharactersIn range: NSRange,replacementString string: String) -> Bool
    {

        let countdots =  (textField.text?.components(separatedBy: (".")).count)! - 1

        if (countdots > 0 && string == "."){
            return false
        }
        let dotString = "."
        if let text = textField.text {
            let isDeleteKey = string.isEmpty
            if !isDeleteKey {
                if text.contains(dotString) {
                    if text.components(separatedBy: dotString)[1].count == 3 {
                        return false
                    }
                }
            }
        }
        return true
    }
}
//extension AddWaterIntakeVC:UIPickerViewDataSource,UIPickerViewDelegate{
//   func numberOfComponents(in pickerView: UIPickerView) -> Int {
//        return 3
//    }
//    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
//        switch component {
//        case 0:
//            return 25
//        case 1,2:
//            return 60
//
//        default:
//            return 0
//        }
//    }
//
//    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
//        return pickerView.frame.size.width/3
//    }
//
//    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
//        switch component {
//        case 0:
//            return "\(row) Hour"
//        case 1:
//            return "\(row) Minute"
//        case 2:
//            return "\(row) Second"
//        default:
//            return ""
//        }
//    }
//    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
//        switch component {
//        case 0:
//            hour = row
//        case 1:
//            minutes = row
//        case 2:
//            seconds = row
//        default:
//            break;
//        }
//    }
//}
