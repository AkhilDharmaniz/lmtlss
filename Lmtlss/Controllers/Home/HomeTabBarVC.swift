//
//  HomeTabBarVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 15/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class HomeTabBarVC: UITabBarController,UITabBarControllerDelegate {
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        // Do any additional setup after loading the view.
        
        
    }
    
   
    func tabBarController(_ tabBarController: UITabBarController, shouldSelect viewController: UIViewController) -> Bool {
        let tabbarIndex = tabBarController.viewControllers!.firstIndex(of: viewController)!
            if  tabbarIndex == 2 {
              let vc = self.storyboard?.instantiateViewController(withIdentifier: "MiddleTabVC") as! MiddleTabVC
                let nav = UINavigationController(rootViewController: vc)
                nav.modalPresentationStyle = .overCurrentContext
                nav.setNavigationBarHidden(true, animated: false)
               self.present(nav, animated: false, completion: nil)
                return false
            } else {
                return true
            }
    }
}
