//
//  MyMacrosVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 09/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MyMacrosVC: UIViewController {

    @IBOutlet weak var navView: UIView!
    @IBOutlet weak var macroTableView: UITableView!
    var microDict = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.macroTableView.delegate = self
        self.macroTableView.dataSource = self
    }
    func configureUI(){
        let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = false
        myCustomView.titleLbl.text = "My Macros"
        myCustomView.logoImg.isHidden = true
//        myCustomView.searchImage.isHidden = true
//        myCustomView.searchBtn.isUserInteractionEnabled = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.menuImage.image = #imageLiteral(resourceName: "menu")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
        myCustomView.delegate = self
        self.navView.addSubview(myCustomView)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
       getMacroList()
    }
    func getMacroList(){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let url = Constants.baseURL + Constants.macroDetail
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            self.microDict = responseData
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                    self.macroTableView.reloadData()
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
}
extension MyMacrosVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ResultsCell
        cell.selectionStyle = .none
        if indexPath.row == 0{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.7458475828, green: 0.899774611, blue: 0.9934951663, alpha: 1)
            cell.progressLbl.backgroundColor = #colorLiteral(red: 0.1690995097, green: 0.7477579713, blue: 0.9476557374, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "calories")
            cell.titleLbl.text = "Calories"
            cell.titleLbl.textColor = #colorLiteral(red: 0.2666666667, green: 0.737254902, blue: 0.9294117647, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.2666666667, green: 0.737254902, blue: 0.9294117647, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f kCal", microDict["calories"] as? Double ?? 0.0)
            cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.3
        }else if indexPath.row == 1{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.9912576079, green: 0.7879117727, blue: 0.6135630012, alpha: 1)
            cell.progressLbl.backgroundColor = #colorLiteral(red: 0.9287672639, green: 0.6092590094, blue: 0.3520768881, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "protein")
            cell.titleLbl.text = "Protein"
            cell.titleLbl.textColor = #colorLiteral(red: 0.8823529412, green: 0.568627451, blue: 0.3960784314, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.8823529412, green: 0.568627451, blue: 0.3960784314, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f g", microDict["protein"] as? Double ?? 0.0)
            cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.6
        }else if indexPath.row == 2{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.6887137294, green: 1, blue: 0.6693044305, alpha: 1)
            cell.progressLbl.backgroundColor = #colorLiteral(red: 0.3553128839, green: 0.790861547, blue: 0.338606596, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "carb")
            cell.titleLbl.text = "Carbs"
            cell.titleLbl.textColor = #colorLiteral(red: 0.4941176471, green: 0.7764705882, blue: 0.4274509804, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.4941176471, green: 0.7764705882, blue: 0.4274509804, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f g", microDict["carb"] as? Double ?? 0.0)
            cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.9
        }else if indexPath.row == 3{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.9906267524, green: 0.7774583697, blue: 0.8127933741, alpha: 1)
            cell.progressLbl.backgroundColor = #colorLiteral(red: 0.9580395818, green: 0.3819159865, blue: 0.4740775824, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "fat")
            cell.titleLbl.text = "Fat"
            cell.titleLbl.textColor = #colorLiteral(red: 0.8901960784, green: 0.4196078431, blue: 0.4823529412, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.8901960784, green: 0.4196078431, blue: 0.4823529412, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f g", microDict["fat"] as? Double ?? 0.0)
            cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.5
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
}
//MARK:- NavBar Delegate Method(s)

extension MyMacrosVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        self.sideMenuController?.showLeftView(animated: true, completionHandler: {
            if let leftVC = self.sideMenuController?.leftViewController as? SideMenuVC {
                leftVC.menuTableView.reloadData()
            }
        })
      // self.sideMenuController?.showLeftViewAnimated()
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
    
  
}
