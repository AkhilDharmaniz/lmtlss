//
//  ExerciseDetailVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 24/11/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import GPVideoPlayer
import Alamofire

class VolumeTableViewCell: UITableViewCell {
    
    @IBOutlet weak var setLbl: UILabel!
    @IBOutlet weak var repsTF: UITextField!
    @IBOutlet weak var repsLbl: UILabel!
    @IBOutlet weak var weightTF: UITextField!
    @IBOutlet weak var kgLbl: UILabel!
    @IBOutlet weak var lastLbl: UILabel!
}
class ExerciseDetailVC: UIViewController {

    @IBOutlet weak var navView: UIView!
    var muscleData = [MuscleListData]()
    @IBOutlet weak var exerciseNameLbl: UILabel!
    @IBOutlet weak var repsLbl: UILabel!
    @IBOutlet weak var setsLbl: UILabel!
    @IBOutlet weak var specialNotesLbl: UILabel!
    @IBOutlet weak var genericNotesLbl: UILabel!
    @IBOutlet weak var thumbnailImg: UIImageView!
    @IBOutlet weak var exerciseLbl: UILabel!
    @IBOutlet weak var mediaView: UIView!
    @IBOutlet weak var volumeTableView: UITableView!
    @IBOutlet weak var volumeTableViewHeight: NSLayoutConstraint!
    
    @IBOutlet weak var logExerciseBtn: UIButton!
    var volumeArr = [VolumeData]()
    var videoPlayer = GPVideoPlayer()
    var isPlaying = false
    var mealNumber = ""
    var dismissedCurrentView:((Bool)->Void)?
    override func viewDidLoad() {
        super.viewDidLoad()
        if muscleData.count != 0 {
            exerciseNameLbl.text = muscleData[0].exercise_name
            repsLbl.text = muscleData[0].reps
            setsLbl.text = muscleData[0].sets
            thumbnailImg.sd_setImage(with: URL(string: muscleData[0].exercise_video_thumbnail ), placeholderImage: UIImage(named: "thumb_vid_det"))
            specialNotesLbl.text = "Workout will only be considered complete if all sets for all exercises are performed. \n\(muscleData[0].specific_note)"
            genericNotesLbl.text = muscleData[0].main_generic_note
            exerciseLbl.text = muscleData[0].muscle_group_name
            volumeArr = muscleData[0].volume
            self.volumeTableView.delegate = self
            self.volumeTableView.dataSource = self
            if muscleData[0].is_log == "1"{
                self.logExerciseBtn.isUserInteractionEnabled = false
                for i in volumeArr.indices { volumeArr[i].enable = false }
            }else{
                self.logExerciseBtn.isUserInteractionEnabled = true
                let selectedArr = self.volumeArr.filter({$0.weight != "" && $0.reps != ""})
                if selectedArr.count == volumeArr.count{
                    for i in volumeArr.indices { volumeArr[i].enable = true }
                }else{
                    for i in 0..<volumeArr.count{
                        if volumeArr[i].weight == "" && volumeArr[i].reps == ""{
                            volumeArr[i].selected = true
                            volumeArr[i].enable = true
                            self.volumeTableView.reloadData()
                            return
                        }
                    }
                }
            }
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        configureUI()
    }
    func configureUI(){
          let myCustomView:CustomHeaderView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomHeaderView
          myCustomView.frame = navView.frame
          myCustomView.titleLbl.isHidden = false
          myCustomView.titleLbl.text = "Exercise Detail"
          myCustomView.logoImg.isHidden = true
         myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
          myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            myCustomView.badgeView.isHidden = false
            myCustomView.badgeLbl.text = support_count
        }
          myCustomView.delegate = self
          self.navView.addSubview(myCustomView)
        
        
      }
    

    @IBAction func playVideoBtnAction(_ sender: UIButton) {
        if muscleData[0].exercise_video_url != ""{
            mediaView.isHidden = false
            if let player = GPVideoPlayer.initialize(with: self.mediaView.bounds) {
                videoPlayer = player
                videoPlayer.isToShowPlaybackControls = true
                self.mediaView.addSubview(videoPlayer)
                let url1 = URL(string: muscleData[0].exercise_video_url)!
                videoPlayer.loadVideo(with: url1)
       //         videoPlayer.loadVideos(with: [url1])
                videoPlayer.isToShowPlaybackControls = true
                videoPlayer.isMuted = false
                videoPlayer.playVideo()
                isPlaying = true
            }
        }
    }
    
    @IBAction func logExerciseBtnAction(_ sender: UIButton) {
        let filterArr = volumeArr.filter ({$0.reps == "" && $0.weight == ""})
        if filterArr.count == 0{
            logExerciseApi()
        }else{
            alert(Constants.AppName, message: "Please Complete all Sets", view: self)
        }
    }
}
//MARK:- NavBar Delegate Method(s)

extension ExerciseDetailVC: CustomHeaderViewDelegate {
    func openSideMenu() {
        if isPlaying == true {
            self.videoPlayer.pauseVideo()
        }
        self.navigationController?.popViewController(animated: true)
    }
    
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
    }
}

//MARK:- TableView Delegate Method(s)
extension ExerciseDetailVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return volumeArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! VolumeTableViewCell
        DispatchQueue.main.async {
            self.volumeTableViewHeight.constant = self.volumeTableView.contentSize.height
        }
        cell.repsTF.tag = indexPath.row
        cell.repsTF.addTarget(self, action: #selector(repsTextFieldDidEndEditing(sender:)), for: .editingDidEnd)
        cell.repsTF.addTarget(self, action: #selector(repsTextFieldDidBeginEditing(sender:)), for: .editingDidBegin)
        cell.weightTF.tag = indexPath.row
        cell.weightTF.addTarget(self, action: #selector(weightTextFieldDidEndEditing(sender:)), for: .editingDidEnd)
        cell.weightTF.addTarget(self, action: #selector(weightTextFieldDidBeginEditing(sender:)), for: .editingDidBegin)
        cell.setLbl.text = "Set\(indexPath.row+1)"
        cell.weightTF.text = volumeArr[indexPath.row].weight
        cell.repsTF.text = volumeArr[indexPath.row].reps
        cell.lastLbl.text = volumeArr[indexPath.row].is_prev
        cell.setLbl.textColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .darkGray
        _ = [cell.repsLbl,cell.kgLbl,cell.lastLbl].map({$0?.textColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .lightGray})
        _ = [cell.weightTF,cell.repsTF].map({$0?.textColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .lightGray})
        _ = [cell.weightTF,cell.repsTF].map({$0?.borderColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .lightGray})
        _ = [cell.weightTF,cell.repsTF].map({$0?.isUserInteractionEnabled = volumeArr[indexPath.row].enable == true ? true : false})
        return cell
    }
    @objc func repsTextFieldDidEndEditing(sender: UITextField){
        self.volumeArr[sender.tag].reps = sender.text!
        validateFiledsData(index: sender.tag)
    }
    @objc func weightTextFieldDidEndEditing(sender: UITextField){
        self.volumeArr[sender.tag].weight = sender.text!
        validateFiledsData(index: sender.tag)
        
    }
    @objc func repsTextFieldDidBeginEditing(sender: UITextField){
        if volumeArr[sender.tag].enable == true{
            for i in volumeArr.indices { volumeArr[i].selected = false }
            self.volumeArr[sender.tag].selected = true
         //   self.volumeTableView.reloadData()
        }
        validateDidBegin(index: sender.tag)
    }
    @objc func weightTextFieldDidBeginEditing(sender: UITextField){
        if volumeArr[sender.tag].enable == true{
            for i in volumeArr.indices { volumeArr[i].selected = false }
            self.volumeArr[sender.tag].selected = true
        }
        validateDidBegin(index: sender.tag)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func validateFiledsData(index: Int){
        if self.volumeArr[index].weight != "" && self.volumeArr[index].reps != ""{
            setApi(index: index)
            //  self.volumeArr[index].enable = false
            self.volumeArr[index].selected = false
            if index == volumeArr.count-1{
                self.volumeTableView.reloadData()
            }
            for i in 0..<volumeArr.count{
                if volumeArr[i].weight != "" && volumeArr[i].reps != ""{
                    volumeArr[i].enable = true
                    self.volumeTableView.reloadData()
                    validateDidBegin(index: index)
                }
                if volumeArr[i].weight == "" && volumeArr[i].reps == ""{
                    volumeArr[i].enable = true
                    self.volumeTableView.reloadData()
                    validateDidBegin(index: index)
                    return
                }
            }
        }
    }
    func validateDidBegin(index: Int){
        let indexPath = IndexPath.init(row: index, section: 0)
        let cell = volumeTableView.cellForRow(at: indexPath) as! VolumeTableViewCell
        cell.setLbl.textColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .darkGray
        _ = [cell.repsLbl,cell.kgLbl,cell.lastLbl].map({$0?.textColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .lightGray})
        _ = [cell.weightTF,cell.repsTF].map({$0?.textColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .lightGray})
        _ = [cell.weightTF,cell.repsTF].map({$0?.borderColor = volumeArr[indexPath.row].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : .lightGray})
        _ = [cell.weightTF,cell.repsTF].map({$0?.isUserInteractionEnabled = volumeArr[indexPath.row].enable == true ? true : false})
        
    }
    func setApi(index: Int){
        if internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.volumnSave
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
             
            let params = ["muscle_group_id" : muscleData[0].muscle_group_id,"exercise_id": muscleData[0].exercise_id,"day_id": muscleData[0].day,"set_id": "\(index+1)","reps": volumeArr[index].reps,"weight": volumeArr[index].weight,"meal_no":mealNumber,"exercise_place": muscleData[0].exercise_place == "gym" ? "1" : "0"]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["message"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        
                    }else{
                        IJProgressView.shared.hideProgressView()
                         alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
            
        }
    }
    func logExerciseApi(){
        if internetValidation(controller: self){
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.volumnLogExercise
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
             
            let params = ["muscle_group_id" : muscleData[0].muscle_group_id,"exercise_id": muscleData[0].exercise_id,"day_id": muscleData[0].day,"meal_no":mealNumber]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: params, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        showAlertMessage(title: Constants.AppName, message: message, okButton: "OK", controller: self) {
                            DispatchQueue.main.async {
                                self.dismissedCurrentView?(true)
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    }
   
}
