//
//  BMRQuestionVC.swift
//  Lmtlss
//
//  Created by apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire
import AVKit

class BMRQuestionVC: UIViewController {

    @IBOutlet weak var weightLbl: UILabel!
    @IBOutlet weak var weightSlider: CustomSlider!
    @IBOutlet weak var monthTF: UITextField!
    @IBOutlet weak var dayTF: UITextField!
    @IBOutlet weak var yearTF: UITextField!
    @IBOutlet weak var heightSlider: CustomSlider!
    @IBOutlet weak var heightLbl: UILabel!
    @IBOutlet weak var targetWeightSlider: CustomSlider!
    @IBOutlet weak var targetWeightLbl: UILabel!
    
    @IBOutlet weak var lbsKgImg: UIImageView!
    @IBOutlet weak var feetCMImg: UIImageView!
    @IBOutlet weak var deficitNSuprlusImg: UIImageView!
    @IBOutlet var imgUploads: [UIImageView]!
    @IBOutlet var btnCancel: [UIButton]!
    @IBOutlet var imgCancel: [UIImageView]!
    var picker:ImagePicker?

    let datePicker = UIDatePicker()
    var questionId = 0
    var lbsKG = "kg"
    var cmFeet = "cm"
    var surplusDeficit = "deficit"
    var selectedImg = 1
    override func viewDidLoad() {
        super.viewDidLoad()
        creatDatePicker()
        for i in 0..<imgUploads.count{
            if imgUploads[i].image == nil{
                btnCancel[i].isHidden = true
                imgCancel[i].isHidden = true
            }else{
                btnCancel[i].isHidden = false
                imgCancel[i].isHidden = false
            }
        }
        configureUI()
    }
    func configureUI(){
        AVCaptureDevice.authorizeVideo(completion: { (status) in
            
        })
        picker = ImagePicker(presentationController: self, delegate: self)
        for i in 0..<questionnaire.count{
            if questionnaire[i]["question_id"] as? Int == 2{
                questionId = questionnaire[i]["question_id"] as? Int ?? 0
                if let option = questionnaire[1]["option"] as? [[String:Any]] {
                    let weightUnit = option[0]["unit"] as? String ?? ""
                    lbsKgImg.image = weightUnit == "lbs" ? #imageLiteral(resourceName: "kg") : #imageLiteral(resourceName: "lbs")
                    lbsKG = weightUnit == "" ? "kg" : weightUnit
                    let weight = option[0]["option"] as? String ?? ""
                    weightSlider.value = NumberFormatter().number(from: weight)?.floatValue ?? 0.0
                    weightLbl.text = getWeight(slider: weightSlider)
                    
                    let heightUnit = option[2]["unit"] as? String ?? ""
                    cmFeet = heightUnit == "" ? "cm" : heightUnit
                    feetCMImg.image = heightUnit == "ft" ? #imageLiteral(resourceName: "feet") : #imageLiteral(resourceName: "cm")
                    let height = option[2]["option"] as? String ?? ""
                    heightSlider.value = NumberFormatter().number(from: height)?.floatValue ?? 0.0
                    heightLbl.text = getHeight(slider: heightSlider)
                    
                    let targetWeightUnit = option[3]["unit"] as? String ?? ""
                    surplusDeficit = targetWeightUnit == "" ? "deficit" : targetWeightUnit
                    deficitNSuprlusImg.image = targetWeightUnit == "surplus" ? #imageLiteral(resourceName: "surplus") : #imageLiteral(resourceName: "deficit")
                    targetWeightLbl.text = option[3]["option"] as? String ?? ""
                    targetWeightSlider.value = NumberFormatter().number(from: targetWeightLbl.text!)?.floatValue ?? 0.0
                    
                    let age = option[1]["option"] as? String ?? ""
                    if age != ""{
                        let listItems = age.components(separatedBy: "-")
                        dayTF.text = listItems[2]
                        monthTF.text = listItems[1]
                        yearTF.text = listItems[0]
                    }
                    
                }
                if let frontImg = questionnaire[i]["front_img"] as? String {
                   self.imgUploads[0].sd_setImage(with: URL(string: frontImg), placeholderImage: UIImage(named: ""))
                    if frontImg != ""{
                        btnCancel[0].isHidden = false
                        imgCancel[0].isHidden = false
                    }
                }
                if let sideImg = questionnaire[i]["side_img"] as? String {
                   self.imgUploads[1].sd_setImage(with: URL(string: sideImg), placeholderImage: UIImage(named: ""))
                    if sideImg != ""{
                        btnCancel[1].isHidden = false
                        imgCancel[1].isHidden = false
                    }
                }
                if let rearImg = questionnaire[i]["rear_img"] as? String {
                   self.imgUploads[2].sd_setImage(with: URL(string: rearImg), placeholderImage: UIImage(named: ""))
                    if rearImg != ""{
                        btnCancel[2].isHidden = false
                        imgCancel[2].isHidden = false
                    }
                    
                }
            }
        }
        
    }
    func validateScreenData() -> Bool{
        if weightLbl.text == "0" || weightLbl.text == "0.00"{
               alert(Constants.AppName, message: "Please Select weight", view: self)
               return true
        }else if heightLbl.text == "0" || heightLbl.text == "0\' 0\'\'"{
            alert(Constants.AppName, message: "Please Select height", view: self)
            return true
        }else if dayTF.text == ""{
            alert(Constants.AppName, message: "Please Select Age", view: self)
            return true
        }else if targetWeightLbl.text == "0"{
            alert(Constants.AppName, message: "Please Select target weight", view: self)
            return true
        }else if imgCancel[0].isHidden == true {
            alert(Constants.AppName, message: "Please Upload Front image", view: self)
            return true
        }else if imgCancel[1].isHidden == true {
            alert(Constants.AppName, message: "Please Upload Side image", view: self)
            return true
        }else if imgCancel[2].isHidden == true {
            alert(Constants.AppName, message: "Please Upload Rear image", view: self)
            return true
        }
           return false
       }

    func creatDatePicker(){
        datePicker.datePickerMode = .date
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: nil, action: #selector(secondDonePressed))
        toolBar.setItems([doneButton], animated: true)
      //  datePicker.maximumDate = Date()
        datePicker.maximumDate = Calendar.current.date(byAdding: .year, value: -10, to: Date())
        if #available(iOS 13.4, *) {
            self.datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
        }
        dayTF.inputAccessoryView = toolBar
        dayTF.inputView = datePicker
        monthTF.inputAccessoryView = toolBar
        monthTF.inputView = datePicker
        yearTF.inputAccessoryView = toolBar
        yearTF.inputView = datePicker
    }
    @objc func secondDonePressed(){
        let dataFormatrer = DateFormatter()
        dataFormatrer.dateFormat = "dd-MM-yyyy"
        let fullNameArr = dataFormatrer.string(from: datePicker.date).components(separatedBy: "-")
        dayTF.text = fullNameArr[0]
        monthTF.text = fullNameArr[1]
        yearTF.text = fullNameArr[2]
        self.view.endEditing(true)
    }
    @IBAction func weightSliderValueChanged(_ sender: CustomSlider) {
        let currentValue = Int(sender.value)
        if sender.tag == 1{
       // weightLbl.text = "\(currentValue)"
        weightLbl.text = getWeight(slider: weightSlider)
        }else if sender.tag == 2{
        // heightLbl.text = "\(currentValue)"
        heightLbl.text = getHeight(slider: heightSlider)
        }else if sender.tag == 3{
         targetWeightLbl.text = "\(currentValue)"
        }
        if weightSlider.value > targetWeightSlider.value{
           deficitNSuprlusImg.image = #imageLiteral(resourceName: "deficit")
           surplusDeficit = "deficit"
        }else{
            deficitNSuprlusImg.image = #imageLiteral(resourceName: "surplus")
            surplusDeficit = "surplus"
        }
    }
    @IBAction func lbsAndKgBtnAction(_ sender: UIButton) {
     //   lbsKgImg.image = #imageLiteral(resourceName: "lbs")
        if sender.tag == 1{
            lbsKgImg.image = #imageLiteral(resourceName: "kg")
            lbsKG = "lbs"
        }else if sender.tag == 2{
            lbsKgImg.image = #imageLiteral(resourceName: "lbs")
            lbsKG = "kg"

        }
        weightLbl.text = getWeight(slider: weightSlider)

    }
    func getWeight(slider : CustomSlider) -> String
       {
           if lbsKgImg.image == #imageLiteral(resourceName: "kg")
           {
               let myweight : Int = Int(slider.value)
               let kgWeight = Measurement(value: Double(myweight), unit: UnitMass.kilograms)
               let lbWeight = kgWeight.converted(to: UnitMass.pounds)
               let weight = String(format: "%.2f", lbWeight.value)
               
               return weight
           }
           else
           {
               return "\(Int(slider.value))"
           }
       }
    func getHeight(slider : CustomSlider) -> String
    {
        if feetCMImg.image == #imageLiteral(resourceName: "feet")
        {
            let myheight : Int = Int(slider.value)
            let heightCm = Measurement(value: Double(myheight), unit: UnitLength.centimeters)
            let heightFeet = heightCm.converted(to: UnitLength.feet)
            let numInches = heightCm.converted(to: UnitLength.inches)
            let heightInch = numInches.value.truncatingRemainder(dividingBy: 12);
            
            return "\(Int(heightFeet.value))" + "'" + " \(Int(round(heightInch)))" + "''"
        }
        else
        {
            return "\(Int(slider.value))"
        }
    }
    @IBAction func feetCMBtnAction(_ sender: UIButton) {
   //     feetCMImg.image = #imageLiteral(resourceName: "cm")
        if sender.tag == 1{
            feetCMImg.image = #imageLiteral(resourceName: "feet")
            cmFeet = "ft"
        }else if sender.tag == 2{
            feetCMImg.image = #imageLiteral(resourceName: "cm")
            cmFeet = "cm"

        }
        heightLbl.text = getHeight(slider: heightSlider)
    }
    
    @IBAction func deficitNSurplusBtnAction(_ sender: UIButton) {
        if sender.tag == 1{
            deficitNSuprlusImg.image = #imageLiteral(resourceName: "deficit")
            surplusDeficit = "deficit"
        }else if sender.tag == 2{
            deficitNSuprlusImg.image = #imageLiteral(resourceName: "surplus")
            surplusDeficit = "surplus"

        }
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.questionnaireSave
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    self.submitImages{
                        successHandler()
                    }
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
        }
    }
    func generatingParameters() -> [String:AnyObject] {
              var parameters:[String:AnyObject] = [:]
            parameters["question_id"] = questionId as AnyObject
        let weight = "\(Int(weightSlider.value))"
        let height = "\(Int(heightSlider.value))"
        parameters["option_id"] = "\(weight),\(yearTF.text ?? "")-\(monthTF.text ?? "")-\(dayTF.text ?? ""),\(height),\(targetWeightLbl.text ?? "0")" as AnyObject
        parameters["unit"] = "\(lbsKG),date,\(cmFeet),\(surplusDeficit)" as AnyObject        
              return parameters
          }
//    func getSelectedOptions() -> String
//    {
//        let selectedArr : [String]  = self.physicalActivityArr.filter { (objField) -> Bool in
//            return objField.selected == 1
//            }.map { (objField) -> String in
//                return "\(String(describing: objField.option_id!))"
//        }
//        return selectedArr.joined(separator: ",")
//    }
    func submitImages(successHandler: @escaping (() -> Void)){
        DispatchQueue.main.async {
        IJProgressView.shared.showProgressView()
        }
        let url = Constants.baseURL + Constants.uploadimg
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
        AF.upload(multipartFormData: { (multipartFormData) in
//            for (key, value) in self.generatingParameters() {
//                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
//            }
            if self.imgUploads[0].image != nil {
                let imageStr =  self.imgUploads[0].image!.jpegData(compressionQuality: 0.3)!
                multipartFormData.append(imageStr, withName: "front_img", fileName: "\(String.random(length: 10)).jpg", mimeType: "")
            }
            if self.imgUploads[1].image != nil {
                let imageStr =  self.imgUploads[1].image!.jpegData(compressionQuality: 0.3)!
                multipartFormData.append(imageStr, withName: "side_img", fileName: "\(String.random(length: 10)).jpg", mimeType: "")
            }
            if self.imgUploads[2].image != nil {
                let imageStr =  self.imgUploads[2].image!.jpegData(compressionQuality: 0.3)!
                multipartFormData.append(imageStr, withName: "rear_img", fileName: "\(String.random(length: 10)).jpg", mimeType: "")
            }
            
        }, to: url, usingThreshold: UInt64.init(), method: .post, headers: headers, interceptor: nil, fileManager: .default)
            
            .uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            .responseJSON { (response) in
                IJProgressView.shared.hideProgressView()
                switch response.result {
                case .success(let value):
                    if let JSON = value as? [String: Any] {
                        let displayMessage = JSON["responseMessage"] as? String
                        if let code = JSON["responseCode"] as? Int{
                            if code == 200{
                                successHandler()
                            }else{
                                alert(Constants.AppName, message: displayMessage ?? "", view: self)
                            }
                        }
                        if let status = JSON["status"] as? Int, status == 401{
                            logOut(controller: self)
                        }
                    }
                case .failure(let error):
                    // error handling
                    alert(Constants.AppName, message: error.localizedDescription, view: self)
                    break
                }
        }
        
    }
    @IBAction func addImageBtnAction(_ sender: UIButton) {
        AVCaptureDevice.authorizeVideo(completion: { (status) in
            if status == .alreadyDenied || status == .justDenied{
                showMessage(title: "Unable to access the Camera", message: "To enable access, go to Settings > Privacy > Camera and turn on Camera access for this app.", okButton: "Settings", cancelButton: "Cancel", controller: self) {
                    if let url = URL(string: UIApplication.openSettingsURLString) {
                               UIApplication.shared.open(url, options: [:], completionHandler: { _ in
                                   // Handle
                               })
                           }
                } cancelHandler: {
                    
                }
            }else{
                self.selectedImg = sender.tag
                self.picker?.present(from: sender)
            }
        })
        
    }
    
    @IBAction func cancelBtnAction(_ sender: UIButton) {
        self.imgUploads[sender.tag-1].image = nil
        self.btnCancel[sender.tag-1].isHidden = true
        self.imgCancel[sender.tag-1].isHidden = true
    }
}
//MARK:- Image Picker Delegate

extension BMRQuestionVC: ImagePickerDelegate{
    func didSelect(image: UIImage?) {
        if image != nil{
            self.imgUploads[selectedImg-1].image = image
            self.btnCancel[selectedImg-1].isHidden = false
            self.imgCancel[selectedImg-1].isHidden = false
        }
      //  self.imageStr =  self.profileImageView.image!.jpegData(compressionQuality: 0.3)!
    }
}
