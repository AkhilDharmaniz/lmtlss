//
//  ReplaceRecipeVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 29/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ReplaceRecipeVC: UIViewController {
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var recipeTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var recipeTableView: UITableView!
    @IBOutlet weak var questionTitleLbl: UILabel!
    var options = [QuestionaryField]()
    var questionId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    private func configureUI(){
        scrollVw.delegate = self
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        recipeTableView.register(nib, forCellReuseIdentifier: "cell")
        recipeTableView.delegate = self
        recipeTableView.dataSource = self
        recipeTableView.separatorStyle = .none
        for i in 0..<questionnaire.count{
            if questionnaire[i]["question_id"] as? Int == 21{
                questionId = questionnaire[i]["question_id"] as? Int ?? 0
                questionTitleLbl.text = questionnaire[i]["question"] as? String ?? ""
                if let option = questionnaire[i]["option"] as? [[String:Any]] {
                    for i in 0..<option.count{
                        self.options.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
                    }
                    recipeTableViewHeight.constant = CGFloat(80 * self.options.count)
                    recipeTableView.reloadData()
                }
            }
        }
    }
    func validateScreenData() -> Bool{
      
        let selectedElement = options.filter({$0.selected == 1})
        if selectedElement.count == 0{
            alert(Constants.AppName, message: "Please Select atleast 1 Option", view: self)
            return true
        }
        return false
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.questionnaireSave
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
              var parameters:[String:AnyObject] = [:]
            parameters["question_id"] = questionId as AnyObject
        let optionsStr = getSelectedOptions()
        
           parameters["option_id"] = optionsStr as AnyObject
           
              return parameters
          }
     func getSelectedOptions() -> String
       {
           let selectedArr : [String]  = self.options.filter { (objField) -> Bool in
               return objField.selected == 1
               }.map { (objField) -> String in
                   return "\(String(describing: objField.option_id!))"
           }
           return selectedArr.joined(separator: ",")
       }
}
extension ReplaceRecipeVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        let data = options[indexPath.row]
        cell.titleLbl.text = data.option
      //  cell.subTitleLbl.text = ""
        cell.subTitleLbl.isHidden = true
        cell.exampleLbl.isHidden = true
        cell.bgImg.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if options[indexPath.row].selected == 0{
            options[indexPath.row].selected = 1
        }else{
            options[indexPath.row].selected = 0
        }
      //  muscles[indexPath.row].selected = !muscles[indexPath.row].selected
//        if var option = questionnaire[7]["option"] as? [[String:Any]] {
//           let optionSelected = option[indexPath.row]["selected"] as? Int ?? 0
//            if optionSelected == 0{
//                option[indexPath.row]["selected"]  = 1
//            }else{
//                option[indexPath.row]["selected"]  = 0
//            }
//            questionnaire[7]["option"] = option
//        }
        recipeTableView.reloadData()
    }
}
extension ReplaceRecipeVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if #available(iOS 13, *) {
            (scrollView.subviews[(scrollView.subviews.count - 1)].subviews[0]).backgroundColor = UIColor(named: "BGColor") // #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1)  //verticalIndicator
            (scrollView.subviews[(scrollView.subviews.count - 2)].subviews[0]).backgroundColor =  UIColor(named: "BGColor") //#colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1) //horizontalIndicator
        } else {
            if let verticalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 1)] as? UIImageView) {
                verticalIndicator.backgroundColor =  UIColor(named: "BGColor") //#colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1)
            }
            
            if let horizontalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 2)] as? UIImageView) {
                horizontalIndicator.backgroundColor = UIColor(named: "BGColor")// #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1)
            }
        }
    }
}


