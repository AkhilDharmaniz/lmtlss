//
//  GainWeightQuestionVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 18/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class GainWeightQuestionVC: UIViewController {

    @IBOutlet weak var gainWeightTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var gainWeightTableView: UITableView!
    var gainWeight = [QuestionaryField]()
    
    @IBOutlet weak var questionTitleLbl: UILabel!
    //   fileprivate var gainWeight = [Options(option: "Steady Weight Gain",selected: false),Options(option: "Moderate Weight Gain",selected: false),Options(option: "Drastic Weight Gain",selected: false)]
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    private func configureUI(){
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        gainWeightTableView.register(nib, forCellReuseIdentifier: "cell")
        gainWeightTableView.delegate = self
        gainWeightTableView.dataSource = self
        gainWeightTableView.separatorStyle = .none
     //   gainWeightTableViewHeight.constant = CGFloat(70.0 * 3)
        for i in 0..<questionnaire.count{
            if questionnaire[i]["question_id"] as? Int == 14{
                questionTitleLbl.text = questionnaire[i]["question"] as? String ?? ""
                if let option = questionnaire[i]["option"] as? [[String:Any]] {
                    for i in 0..<option.count{
                        self.gainWeight.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
                    }
                    
                    gainWeightTableViewHeight.constant = CGFloat(80 * self.gainWeight.count)
                    gainWeightTableView.reloadData()
                }
            }
        }
//        questionTitleLbl.text = questionnaire[11]["question"] as? String ?? ""
//        if let option = questionnaire[11]["option"] as? [[String:Any]] {
//            for i in 0..<option.count{
//                self.gainWeight.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0))
//            }
//
//            gainWeightTableViewHeight.constant = CGFloat(70 * self.gainWeight.count)
//            gainWeightTableView.reloadData()
//        }
    }
    func validateScreenData() -> Bool{
        
        let selectedElement = gainWeight.filter({$0.selected == 1})
        if selectedElement.count == 0{
            alert(Constants.AppName, message: "Please Select atleast 1 Option", view: self)
            return true
        }
        return false
    }
   

}
extension GainWeightQuestionVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gainWeight.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        let data = gainWeight[indexPath.row]
        cell.titleLbl.text = data.option
        cell.subTitleLbl.text = data.description
        cell.bgImg.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<gainWeight.count{
            gainWeight[i].selected = i == indexPath.row ? 1 : 0
        }
        if var option = questionnaire[11]["option"] as? [[String:Any]] {
            for i in 0..<option.count{
                option[i]["selected"] = i == indexPath.row ? 1 : 0
            }
            questionnaire[11]["option"] = option
        }
        gainWeightTableView.reloadData()
    }
}
