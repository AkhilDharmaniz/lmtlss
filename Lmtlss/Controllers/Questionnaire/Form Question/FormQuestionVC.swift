//
//  FormQuestionVC.swift
//  Lmtlss
//
//  Created by apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class FormQuestionVC: UIViewController {
    @IBOutlet weak var maleSelBtn: UIButton!
    
    @IBOutlet weak var femaleSelBtn: UIButton!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var orderNumberTF: UITextField!
    var gender = ""
    var questionnaireField = [QuestionaryField]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        configure()
    }
    func configure(){
        if Reachability.isConnectedToNetwork() == true {
            IJProgressView.shared.showProgressView()
            let qa = Constants.baseURL + Constants.questionnaire
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(qa, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                let message = response["message"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            if let pesronalInformation = responseData["personal_information"] as? [String:Any]{
                                self.nameTF.text = pesronalInformation["name"] as? String ?? ""
                                
                                self.orderNumberTF.text = pesronalInformation["order_number"] as? String ?? ""
                                self.emailTF.text = pesronalInformation["username"] as? String ?? ""
                            }
                            if let qa = responseData["qa"] as? [[String:Any]]{
                                questionnaire = qa
                                if let option = questionnaire[0]["option"] as? [[String:Any]]{
                                    for i in 0..<option.count{
                                        self.questionnaireField.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
                                    }
                                    if self.questionnaireField[0].selected == 1{
                                        self.gender = "male"
                                        self.maleSelBtn.backgroundColor = #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1)
                                        self.femaleSelBtn.backgroundColor = .white
                                    }
                                    if self.questionnaireField[1].selected == 1{
                                        self.gender = "female"
                                        self.femaleSelBtn.backgroundColor = #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1)
                                        self.maleSelBtn.backgroundColor = .white
                                    }
                                }
                               // questionnaire[1]["option"] = [[String:Any]]()
                                if var BMROption = questionnaire[1]["option"] as? [[String:Any]]{
                                    if BMROption.count == 0{
                                       let weight = ["option": "0","question":"Weight","unit":"kg"] as [String : Any]
                                        let age = ["option": "","question":"Age","unit":"date"] as [String : Any]
                                        let height = ["option": "0","question":"Height","unit":"cm"] as [String : Any]
                                        let targetWeight = ["option": "0","question":"Target Weight","unit":"deficit"] as [String : Any]
                                        BMROption.append(weight)
                                        BMROption.append(age)
                                        BMROption.append(height)
                                        BMROption.append(targetWeight)
                                        questionnaire[1]["option"] = BMROption
                                    }
                                }
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
            }
            
        } else {
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func validateScreenData() -> Bool{
        if orderNumberTF.text!.isEmpty{
            alert(Constants.AppName, message: "Please enter order number", view: self)
            return true
        }
        else if nameTF.text!.isEmpty{
            alert(Constants.AppName, message: "Please enter name", view: self)
            return true
        }
        else if emailTF.text!.isEmpty{
            alert(Constants.AppName, message: "Please enter eamil", view: self)
            return true
        }else if gender == ""{
            alert(Constants.AppName, message: "Please select your gender", view: self)
            return true
        }else{
            return false
        }
    }
  
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.questionnaireSave
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
        }
    }
    func generatingParameters() -> [String:AnyObject] {
              var parameters:[String:AnyObject] = [:]
            parameters["question_id"] = 1 as AnyObject
           parameters["option_id"] = self.questionnaireField[0].selected == 1 ? "\(self.questionnaireField[0].option_id ?? 0)" as AnyObject : "\(self.questionnaireField[1].option_id ?? 0)" as AnyObject
              return parameters
          }
    //MARK:- IBAction Method(s)
    @IBAction func genderSelectBtnAction(_ sender: UIButton) {
        if sender.tag == 0{
            gender = "male"
            self.questionnaireField[0].selected = 1
            self.questionnaireField[1].selected = 0
            self.maleSelBtn.backgroundColor = #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1)
            self.femaleSelBtn.backgroundColor = .white
        }else{
            gender = "female"
            self.questionnaireField[0].selected = 0
            self.questionnaireField[1].selected = 1
            self.femaleSelBtn.backgroundColor = #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1)
            self.maleSelBtn.backgroundColor = .white
        }
        if var option = questionnaire[0]["option"] as? [[String:Any]]{
            if gender == "male"{
              option[0]["selected"] = 1
              option[1]["selected"] = 0
            }else if gender == "female"{
                 option[1]["selected"] = 1
                option[0]["selected"] = 0

            }
            questionnaire[0]["option"] = option
        }
    }
    
}
