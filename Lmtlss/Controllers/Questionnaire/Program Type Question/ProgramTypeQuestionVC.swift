//
//  ProgramTypeQuestionVC.swift
//  Lmtlss
//
//  Created by apple on 23/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ProgramTypeQuestionVC: UIViewController {

   
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var programTypeTableView: UITableView!
    var options = [QuestionaryField]()
    var questionId = 0

   // fileprivate var options = [Options(option: "Gym", selected: false),Options(option: "Home", selected: false)]
    
    @IBOutlet weak var programTableViewHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        // Do any additional setup after loading the view.
    }
    

    private func configureUI(){
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        programTypeTableView.register(nib, forCellReuseIdentifier: "cell")
        programTypeTableView.delegate = self
        programTypeTableView.dataSource = self
        programTypeTableView.separatorStyle = .none
        for i in 0..<questionnaire.count{
            if questionnaire[i]["question_id"] as? Int == 9{
                questionTitleLbl.text = questionnaire[i]["question"] as? String ?? ""
                questionId = questionnaire[i]["question_id"] as? Int ?? 0
                if let option = questionnaire[i]["option"] as? [[String:Any]] {
                    for i in 0..<option.count{
                        self.options.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
                    }
                    
                    //    programTableViewHeight.constant = CGFloat(80 * self.options.count)
                    programTypeTableView.reloadData()
                }
                //  programTableViewHeight.constant = CGFloat(70.0 * 2)
            }
        }
    }
    func validateScreenData() -> Bool{
           
           let selectedElement = options.filter({$0.selected == 1})
           if selectedElement.count == 0{
               alert(Constants.AppName, message: "Please Select Gym or Home", view: self)
               return true
           }
           return false
       }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
            
            IJProgressView.shared.showProgressView()
            let signInUrl = Constants.baseURL + Constants.questionnaireSave
             
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Authorization":token ?? ""]
            AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["message"] as? String ?? ""
                if let status = response["responseCode"] as? Int {
                    if status == 200{
                        successHandler()
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["question_id"] = questionId as AnyObject
        let optionsStr = getSelectedOptions()
        parameters["option_id"] = optionsStr as AnyObject
        
        return parameters
    }
         func getSelectedOptions() -> String
           {
               let selectedArr : [String]  = self.options.filter { (objField) -> Bool in
                   return objField.selected == 1
                   }.map { (objField) -> String in
                       return "\(String(describing: objField.option_id!))"
               }
               return selectedArr.joined(separator: ",")
           }
}



extension ProgramTypeQuestionVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        let data = options[indexPath.row]
        cell.titleLbl.text = data.option
        cell.subTitleLbl.text = data.description
        cell.bgImg.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        DispatchQueue.main.async {
            self.programTableViewHeight.constant = self.programTypeTableView.contentSize.height
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<options.count{
            options[i].selected = i == indexPath.row ? 1 : 0
        }
        if var option = questionnaire[5]["option"] as? [[String:Any]] {
            for i in 0..<option.count{
                option[i]["selected"] = i == indexPath.row ? 1 : 0
            }
            questionnaire[5]["option"] = option
        }
        programTypeTableView.reloadData()
    }
    
}
