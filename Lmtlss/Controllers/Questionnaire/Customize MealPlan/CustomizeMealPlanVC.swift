//
//  CustomizeMealPlanVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 07/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class MealListCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectedLbl: UILabel!
    
}
class ListCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var bgView: UIView!
    @IBOutlet weak var nameLbl: UILabel!
}
class MealPlanCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var nameLbl: UILabel!
}
class CustomizeMealPlanVC: UIViewController {
    var protienResponseArray = [MealPlanResponseField]()
    var carbResponseArray = [MealPlanResponseField]()
    var fatResponseArray = [MealPlanResponseField]()
    var condimentsResponseArray = [MealPlanResponseField]()
    var mainMealArr: MainMealPlanField?
    
    var listArray = [("Protein",true,0),("Carb",false,1),("Fat",false,2),("Condiments",false,3)]
    var index = 0
    @IBOutlet weak var mealListCollectionView: UICollectionView!
    @IBOutlet weak var listCollectionView: UICollectionView!
    @IBOutlet weak var mealPlanCollectionView: UICollectionView!
    @IBOutlet weak var alternatePlanCollectionView: UICollectionView!
    var mealArr = [MealPlanField]()
    var alternateArr = [MealPlanField]()
    var selectedMeal = ""
    @IBOutlet weak var mealPlanTitle: UILabel!
    @IBOutlet weak var alternatePlanTitle: UILabel!
    var mealOptionId = 0
    var alternateOptionId = 0
    var mealNumber = 0
    var isFirstTime = false
    override func viewDidLoad() {
        super.viewDidLoad()
        mealListCollectionView.delegate = self
        mealListCollectionView.dataSource = self
        listCollectionView.delegate = self
        listCollectionView.dataSource = self
        mealPlanCollectionView.delegate = self
        mealPlanCollectionView.dataSource = self
        alternatePlanCollectionView.delegate = self
        alternatePlanCollectionView.dataSource = self
        selectedMeal = "protein"
        NotificationCenter.default.post(name: Notification.Name("NextBtnHidden"), object: nil, userInfo: ["hide":true])
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        isFirstTime = true
        configure(meal: "protein")
        
    }
    
    func configure(meal : String){
        if Reachability.isConnectedToNetwork() == true {
            
            DispatchQueue.main.async {
            IJProgressView.shared.showProgressView()
            }
            var url = ""
            if meal == "protein"{
                url = Constants.baseURL + Constants.protienItem
            }else if meal == "carb"{
                url = Constants.baseURL + Constants.carbItem
            }else if meal == "fat"{
                url = Constants.baseURL + Constants.fatItem
            }else if meal == "condiment"{
                url = Constants.baseURL + Constants.condimentsItem
            }
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(url, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
                
                let message = response["responseMessage"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                        if let responseData = response["responseData"] as? [String:Any]{
                            if let qa = responseData["qa"] as? [String:Any]{
                                if let option = qa["option"] as? [[String:Any]] {
                                    if meal == "protein"{
                                        self.protienResponseArray.removeAll()
                                    }else if meal == "carb"{
                                        self.carbResponseArray.removeAll()
                                    }else if meal == "fat"{
                                        self.fatResponseArray.removeAll()
                                    }else if meal == "condiment"{
                                        self.condimentsResponseArray.removeAll()
                                    }
                                    for i in 0..<option.count{
                                        var alternateMeal = [MealPlanField]()
                                        var meal1 = [MealPlanField]()
                                        if let alternateMealListList = option[i]["alternate_meal"] as? [[String:Any]]{
                                            for j in 0..<alternateMealListList.count{
                                                alternateMeal.append(MealPlanField(food_name: alternateMealListList[j]["food_name"] as? String ?? "", id: alternateMealListList[j]["id"] as? Int ?? 0, selected: alternateMealListList[j]["selected"] as? Int ?? 0, selectedLocal: alternateMealListList[j]["selected"] as? Int ?? 0))
                                            }
                                        }
                                        if let mealList = option[i]["meal"] as? [[String:Any]]{
                                            for j in 0..<mealList.count{
                                                meal1.append(MealPlanField(food_name: mealList[j]["food_name"] as? String ?? "", id: mealList[j]["id"] as? Int ?? 0, selected: mealList[j]["selected"] as? Int ?? 0, selectedLocal: mealList[j]["selected"] as? Int ?? 0))
                                            }
                                        }
                                        if meal == "protein"{
                                            let selectedMeal = meal1.filter({$0.selected == 1})
                                            let selectedAleternateMeal = alternateMeal.filter({$0.selected == 1})
                                            self.protienResponseArray.append(MealPlanResponseField(name: option[i]["name"] as? String ?? "", id: option[i]["id"] as? Int ?? 0, alternate_meal: alternateMeal, meal: meal1, selected: false, enabled: selectedMeal.count == 0 && selectedAleternateMeal.count == 0 ? false : true))
                                        }else if meal == "carb"{
                                            let selectedMeal = meal1.filter({$0.selected == 1})
                                            let selectedAleternateMeal = alternateMeal.filter({$0.selected == 1})
                                            self.carbResponseArray.append(MealPlanResponseField(name: option[i]["name"] as? String ?? "", id: option[i]["id"] as? Int ?? 0, alternate_meal: alternateMeal, meal: meal1, selected: false, enabled: selectedMeal.count == 0 && selectedAleternateMeal.count == 0 ? false : true))
                                        }else if meal == "fat"{
                                            let selectedMeal = meal1.filter({$0.selected == 1})
                                            let selectedAleternateMeal = alternateMeal.filter({$0.selected == 1})
                                            self.fatResponseArray.append(MealPlanResponseField(name: option[i]["name"] as? String ?? "", id: option[i]["id"] as? Int ?? 0, alternate_meal: alternateMeal, meal: meal1, selected: false, enabled: selectedMeal.count == 0 && selectedAleternateMeal.count == 0 ? false : true))
                                        }else if meal == "condiment"{
                                            let selectedMeal = meal1.filter({$0.selected == 1})
                                            let selectedAleternateMeal = alternateMeal.filter({$0.selected == 1})
                                            self.condimentsResponseArray.append(MealPlanResponseField(name: option[i]["name"] as? String ?? "", id: option[i]["id"] as? Int ?? 0, alternate_meal: alternateMeal, meal: meal1, selected: false, enabled: selectedMeal.count == 0 && selectedAleternateMeal.count == 0 ? false : true))
                                        }
                                    }
                                   
                                    if meal == "protein"{
                                        if self.isFirstTime{
                                            self.mainMealArr = MainMealPlanField(protein: self.protienResponseArray, carb: self.carbResponseArray, fat: self.fatResponseArray, condiments: self.condimentsResponseArray, selected: 1)
                                        }
                                        self.mainMealArr?.protein = self.protienResponseArray
                                    }else if meal == "carb"{
                                        self.mainMealArr?.carb = self.carbResponseArray
                                    }else if meal == "fat"{
                                        self.mainMealArr?.fat = self.fatResponseArray
                                    }else if meal == "condiment"{
                                        self.mainMealArr?.condiments = self.condimentsResponseArray
                                    }
                                    if self.selectedMeal == "protein"{
                                        self.mealPlanTitle.text = "Primary protein source"
                                        self.alternatePlanTitle.text = "Proteins for alternate meal"
                                    }else if self.selectedMeal == "carb"{
                                        self.mealPlanTitle.text = "Primary carb source"
                                        self.alternatePlanTitle.text = "Carb for alternate meal"
                                    }else if self.selectedMeal == "fat"{
                                        self.mealPlanTitle.text = "Primary fat source"
                                        self.alternatePlanTitle.text = "Fat for alternate meal"
                                    }else if self.selectedMeal == "condiment"{
                                        self.mealPlanTitle.text = "Primary condiments source"
                                        self.alternatePlanTitle.text = "Condiments for alternate meal"
//                                        if self.condimentsResponseArray[self.index].name == "Snacks"{
//                                            NotificationCenter.default.post(name: Notification.Name("NextBtnHidden"), object: nil, userInfo: ["hide":false])
//                                        }
//                                        else{
//                                            NotificationCenter.default.post(name: Notification.Name("NextBtnHidden"), object: nil, userInfo: ["hide":true])
//                                        }
                                    }
                                    if self.selectedMeal == "protein"{
                                        self.mainMealArr?.protein[self.index].selected = true
                                        self.mainMealArr?.protein[self.index].enanled = true
                                    }else if self.selectedMeal == "carb"{
                                        self.mainMealArr?.carb[self.index].selected = true
                                        self.mainMealArr?.carb[self.index].enanled = true
                                    }else if self.selectedMeal == "fat"{
                                        self.mainMealArr?.fat[self.index].selected = true
                                        self.mainMealArr?.fat[self.index].enanled = true
                                    }else if self.selectedMeal == "condiment"{
                                        self.mainMealArr?.condiments[self.index].selected = true
                                        self.mainMealArr?.condiments[self.index].enanled = true
                                        let forIndex = self.mainMealArr?.condiments[self.index].name != "Snacks" ? self.index+2 : self.index+1
                                        for i in 0..<forIndex{
                                            self.mainMealArr?.protein[i].enanled = true
                                            self.mainMealArr?.carb[i].enanled = true
                                            self.mainMealArr?.fat[i].enanled = true
                                            self.mainMealArr?.condiments[i].enanled = true
                                        }
                                    }

                                    if let condiments = self.mainMealArr?.condiments{
                                        for c in 0..<condiments.count{
                                            let mealArr = condiments[c].meal
                                            var mealSel = false
                                            for m in 0..<mealArr.count{
                                                if mealArr[m].selected == 1{
                                                    mealSel = true
                                                    break
                                                }
                                            }
                                            let alterMealArr = condiments[c].alternate_meal
                                            var alternateMealSel = false
                                            for m in 0..<mealArr.count{
                                                if alterMealArr[m].selected == 1{
                                                    alternateMealSel = true
                                                    break
                                                }
                                            }
                                            if mealSel == true && alternateMealSel == true{
                                               if c != condiments.count - 1{
                                                self.mainMealArr?.condiments[c+1].enanled = true
                                                self.mainMealArr?.carb[c+1].enanled = true
                                                self.mainMealArr?.fat[c+1].enanled = true
                                                self.mainMealArr?.protein[c+1].enanled = true
                                                }
                                            }
                                        }
                                    }
                                    let prEnableArr  = self.mainMealArr?.protein.filter({$0.enanled == true})
                                    let carbEnableArr  = self.mainMealArr?.carb.filter({$0.enanled == true})
                                    let fatEnableArr  = self.mainMealArr?.fat.filter({$0.enanled == true})
                                    let conEnableArr  = self.mainMealArr?.condiments.filter({$0.enanled == true})
                                    if prEnableArr?.count  == self.mainMealArr?.protein.count &&
                                        carbEnableArr?.count == self.mainMealArr?.carb.count && fatEnableArr?.count == self.mainMealArr?.fat.count && conEnableArr?.count == self.mainMealArr?.condiments.count{
                                        let conEnArr = self.mainMealArr?.condiments.last
                                        let selMealArr = conEnArr?.meal.filter({$0.selected == 1})
                                        let selAltMealArr = conEnArr?.alternate_meal.filter({$0.selected == 1})
                                        if selMealArr?.count != 0 && selAltMealArr?.count != 0{
                                            NotificationCenter.default.post(name: Notification.Name("NextBtnHidden"), object: nil, userInfo: ["hide":false])
                                        }else{
                                            let condiArr = self.mainMealArr?.condiments
                                            if self.selectedMeal == "condiment" && condiArr?[self.index].name == "Snacks"{
                                                NotificationCenter.default.post(name: Notification.Name("NextBtnHidden"), object: nil, userInfo: ["hide":false])
                                            }else{
                                                NotificationCenter.default.post(name: Notification.Name("NextBtnHidden"), object: nil, userInfo: ["hide":true])
                                            }
                                        }
                                        
                                        
                                    }
                                    self.mealListCollectionView.reloadData()
                                    self.mealPlanCollectionView.reloadData()
                                    self.alternatePlanCollectionView.reloadData()
                                }
                            }
                        }
                        if self.isFirstTime == true{
                            if meal == "protein" {
                                self.configure(meal: "carb")
                            }else if meal == "carb"{
                                self.configure(meal: "fat")
                            }else if meal == "fat"{
                                self.configure(meal: "condiment")
                            }else if meal == "condiment"{
                                self.isFirstTime = false
                            }
                            
                        }
                        if self.selectedMeal == "carb"{
                            if meal != "fat"{
                                self.configure(meal: "fat")
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
                
            }
        } else {
            
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func saveFooditem(successHandler: @escaping (() -> Void)){
        DispatchQueue.main.async {
            IJProgressView.shared.showProgressView()
        }
        let signInUrl = Constants.baseURL + Constants.saveFoodItem
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["meal_option_id"] = mealOptionId as AnyObject
        parameters["meal_number"] = mealNumber as AnyObject
        parameters["alternate_option_id"] = alternateOptionId as AnyObject
        
        return parameters
    }
    func validateScreenData() -> Bool{
        
        let conResArr = mainMealArr?.condiments
        
//        condimentsResponseArray[index].alternate_meal = alternateArr
//        condimentsResponseArray[index].meal = mealArr
        if conResArr?.count == 0{
            return true
        }
        let conArr = conResArr?.last
        let selectedAlternateMeal = conArr?.alternate_meal.filter({$0.selected == 1})
        let selectedMeal = conArr?.meal.filter({$0.selected == 1})
        if selectedAlternateMeal?.count != 0 && selectedMeal?.count != 0 {
            // alert(Constants.AppName, message: "Please Select atleast 1 Option", view: self)
            mealOptionId = selectedMeal?[0].id ?? 0
            alternateOptionId = selectedAlternateMeal?[0].id ?? 0
            mealNumber = conArr?.id ?? 0
            return false
        }else{
            return true
        }
    }
}
extension CustomizeMealPlanVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == mealListCollectionView{
            if self.selectedMeal == "protein"{
                return mainMealArr?.protein.count ?? 0
            }else if self.selectedMeal == "carb"{
                return mainMealArr?.carb.count ?? 0
            }else if self.selectedMeal == "fat"{
                return mainMealArr?.fat.count ?? 0
            }else if self.selectedMeal == "condiment"{
                return mainMealArr?.condiments.count ?? 0
            }else{
                return 0
            }
        }else if collectionView == mealPlanCollectionView{
            if self.selectedMeal == "protein"{
                return mainMealArr?.protein[index].meal.count ?? 0
            }else if self.selectedMeal == "carb"{
                return mainMealArr?.carb[index].meal.count ?? 0
            }else if self.selectedMeal == "fat"{
                return mainMealArr?.fat[index].meal.count ?? 0
            }else if self.selectedMeal == "condiment"{
                return mainMealArr?.condiments[index].meal.count ?? 0
            }else{
                return 0
            }
        }else if collectionView == alternatePlanCollectionView{
            if self.selectedMeal == "protein"{
                return mainMealArr?.protein[index].alternate_meal.count ?? 0
            }else if self.selectedMeal == "carb"{
                return mainMealArr?.carb[index].alternate_meal.count ?? 0
            }else if self.selectedMeal == "fat"{
                return mainMealArr?.fat[index].alternate_meal.count ?? 0
            }else if self.selectedMeal == "condiment"{
                return mainMealArr?.condiments[index].alternate_meal.count ?? 0
            }else{
                return 0
            }
        }else{
            return listArray.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == mealListCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MealListCollectionCell
            if self.selectedMeal == "protein"{
                cell.nameLbl.text = mainMealArr?.protein[indexPath.item].name
                cell.nameLbl.textColor = mainMealArr?.protein[indexPath.item].enanled == true ? .black : .lightGray
                cell.selectedLbl.backgroundColor = mainMealArr?.protein[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            }else if self.selectedMeal == "carb"{
                cell.nameLbl.text = mainMealArr?.carb[indexPath.item].name
                cell.nameLbl.textColor = mainMealArr?.carb[indexPath.item].enanled == true ? .black : .lightGray
                cell.selectedLbl.backgroundColor = mainMealArr?.carb[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            }else if self.selectedMeal == "fat"{
                cell.nameLbl.text = mainMealArr?.fat[indexPath.item].name
                cell.nameLbl.textColor = mainMealArr?.fat[indexPath.item].enanled == true ? .black : .lightGray
                cell.selectedLbl.backgroundColor = mainMealArr?.fat[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            }else if self.selectedMeal == "condiment"{
                cell.nameLbl.text = mainMealArr?.condiments[indexPath.item].name
                cell.nameLbl.textColor = mainMealArr?.condiments[indexPath.item].enanled == true ? .black : .lightGray
                cell.selectedLbl.backgroundColor = mainMealArr?.condiments[indexPath.item].selected == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            }
            return cell
        }else if collectionView == mealPlanCollectionView{
            if self.selectedMeal == "protein"{
                if let meal = mainMealArr?.protein[index].meal{
                self.mealArr = meal
                }
            }else if self.selectedMeal == "carb"{
                if let meal = mainMealArr?.carb[index].meal{
                    self.mealArr = meal
                }
            }else if self.selectedMeal == "fat"{
                if let meal = mainMealArr?.fat[index].meal{
                    self.mealArr = meal
                }
            }else if self.selectedMeal == "condiment"{
                if let meal = mainMealArr?.condiments[index].meal{
                    self.mealArr = meal
                }
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MealPlanCollectionCell
            cell.nameLbl.text = self.mealArr[indexPath.item].food_name
            cell.nameLbl.textColor = mealArr[indexPath.item].selected == 1 ? .white : .black
            cell.contentView.backgroundColor = mealArr[indexPath.item].selected == 1 ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            return cell
        }else if collectionView == alternatePlanCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! MealPlanCollectionCell
            if self.selectedMeal == "protein"{
                if let meal = mainMealArr?.protein[index].alternate_meal{
                self.alternateArr = meal
                }
            }else if self.selectedMeal == "carb"{
                if let meal = mainMealArr?.carb[index].alternate_meal{
                    self.alternateArr = meal
                }
            }else if self.selectedMeal == "fat"{
                if let meal = mainMealArr?.fat[index].alternate_meal{
                    self.alternateArr = meal
                }
            }else if self.selectedMeal == "condiment"{
                if let meal = mainMealArr?.condiments[index].alternate_meal{
                    self.alternateArr = meal
                }
            }
            cell.nameLbl.text = self.alternateArr[indexPath.item].food_name
            cell.nameLbl.textColor = alternateArr[indexPath.item].selected == 1 ? .white : .black
            cell.contentView.backgroundColor = alternateArr[indexPath.item].selected == 1 ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ListCollectionCell
            cell.nameLbl.text = listArray[indexPath.item].0
            cell.nameLbl.textColor = listArray[indexPath.item].1 == true ? .white : .black
            cell.bgView.backgroundColor = listArray[indexPath.item].1 == true ? #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == mealListCollectionView{
            var selectedEnabled = false
            if self.selectedMeal == "protein"{
                selectedEnabled = mainMealArr?.protein[indexPath.item].enanled ?? false
            }else if self.selectedMeal == "carb"{
                selectedEnabled = mainMealArr?.carb[indexPath.item].enanled ?? false
            }else if self.selectedMeal == "fat"{
                selectedEnabled = mainMealArr?.fat[indexPath.item].enanled ?? false
            }else if self.selectedMeal == "condiment"{
                selectedEnabled = mainMealArr?.condiments[indexPath.item].enanled ?? false
            }
            if selectedEnabled == true{
                var selectedIndex = 0
                if let protArr = mainMealArr?.protein{
                    for i in 0..<protArr.count{
                        if protArr[i].selected == true{
                            selectedIndex = i
                        }
                    }
                }
                if selectedIndex < indexPath.item{
                    let selectedAlternateMeal = self.mainMealArr?.condiments[index].alternate_meal.filter({$0.selected == 1})
                    let selectedMeal = self.mainMealArr?.condiments[index].meal.filter({$0.selected == 1})
                    if selectedAlternateMeal?.count != 0 && selectedMeal?.count != 0 {
                        mealOptionId = selectedMeal?[0].id ?? 0
                        alternateOptionId = selectedAlternateMeal?[0].id ?? 0
                        mealNumber = self.mainMealArr?.condiments[index].id ?? 0
                        saveFooditem {
                            self.index = indexPath.item
                            self.selectedMeal = "protein"
                            for i in 0..<self.listArray.count{
                                self.listArray[i].1 = false
                            }
                            self.listArray[0].1 = true
                            self.listTableReload()
                            self.listCollectionView.reloadData()
                            if self.selectedMeal == "protein"{
                                if var protienArr = self.mainMealArr?.protein{
                                    for i in 0..<protienArr.count{
                                        protienArr[i].selected = i == indexPath.item ? true : false
                                    }
                                    self.mainMealArr?.protein = protienArr
                                }
                            }else if self.selectedMeal == "carb"{
                                if var carbArr = self.mainMealArr?.carb{
                                    for i in 0..<carbArr.count{
                                        carbArr[i].selected = i == indexPath.item ? true : false
                                    }
                                    self.mainMealArr?.carb = carbArr
                                }
                            }else if self.selectedMeal == "fat"{
                                if var fatArr = self.mainMealArr?.fat{
                                    for i in 0..<fatArr.count{
                                        fatArr[i].selected = i == indexPath.item ? true : false
                                    }
                                    self.mainMealArr?.fat = fatArr
                                }
                            }else if self.selectedMeal == "condiment"{
                                if var condimentArr = self.mainMealArr?.condiments{
                                    for i in 0..<condimentArr.count{
                                        condimentArr[i].selected = i == indexPath.item ? true : false
                                    }
                                    self.mainMealArr?.condiments = condimentArr
                                }
                            }
                            self.mealListCollectionView.reloadData()
                            self.mealPlanCollectionView.reloadData()
                            self.alternatePlanCollectionView.reloadData()
                        }
                   }else{
                    // not selected fat meal and alternate meal
                    showToast(message: "Please select previous Food Items")
                    return
                    }
                }else{
                    index = indexPath.item
                    self.selectedMeal = "protein"
                    for i in 0..<listArray.count{
                        listArray[i].1 = false
                    }
                    listArray[0].1 = true
                    listTableReload()
                    self.listCollectionView.reloadData()
                    if self.selectedMeal == "protein"{
                        if var protienArr = self.mainMealArr?.protein{
                            for i in 0..<protienArr.count{
                                protienArr[i].selected = i == indexPath.item ? true : false
                            }
                            self.mainMealArr?.protein = protienArr
                        }
                    }else if self.selectedMeal == "carb"{
                        if var carbArr = self.mainMealArr?.carb{
                            for i in 0..<carbArr.count{
                                carbArr[i].selected = i == indexPath.item ? true : false
                            }
                            self.mainMealArr?.carb = carbArr
                        }
                    }else if self.selectedMeal == "fat"{
                        if var fatArr = self.mainMealArr?.fat{
                            for i in 0..<fatArr.count{
                                fatArr[i].selected = i == indexPath.item ? true : false
                            }
                            self.mainMealArr?.fat = fatArr
                        }
                    }else if self.selectedMeal == "condiment"{
                        if var condimentArr = self.mainMealArr?.condiments{
                            for i in 0..<condimentArr.count{
                                condimentArr[i].selected = i == indexPath.item ? true : false
                            }
                            self.mainMealArr?.condiments = condimentArr
                        }
                    }
                    mealListCollectionView.reloadData()
                    mealPlanCollectionView.reloadData()
                    self.alternatePlanCollectionView.reloadData()
                }
            }else{
                showToast(message: "Please select All Food Items of Previous Meals")
            }
        }else if collectionView == listCollectionView{
            if indexPath.item == 0{
                for i in 0..<listArray.count{
                    listArray[i].1 = i == indexPath.item ? true : false
                }
            }
            if indexPath.item == 1{
                let selectedIndex = listArray.filter({$0.1 == true})
                if selectedIndex[0].2 < indexPath.item{
                    let selectedAlternateMeal = self.mainMealArr?.protein[index].alternate_meal.filter({$0.selected == 1})
                    let selectedMeal = self.mainMealArr?.protein[index].meal.filter({$0.selected == 1})
                    if selectedAlternateMeal?.count != 0 && selectedMeal?.count != 0 {
                        mealOptionId = selectedMeal?[0].id ?? 0
                        alternateOptionId = selectedAlternateMeal?[0].id ?? 0
                        mealNumber = self.mainMealArr?.protein[index].id ?? 0
                        saveFooditem {
                            if var meal = self.mainMealArr?.protein[self.index].meal{
                                for i in 0..<meal.count{
                                    meal[i].selectedLocal = meal[i].selected
                                }
                                self.mainMealArr?.protein[self.index].meal = meal
                            }
                            if var altMeal = self.mainMealArr?.protein[self.index].alternate_meal{
                                for i in 0..<altMeal.count{
                                    altMeal[i].selectedLocal = altMeal[i].selected
                                }
                                self.mainMealArr?.protein[self.index].alternate_meal = altMeal
                            }
                            for i in 0..<self.listArray.count{
                                self.listArray[i].1 = false
                            }
                            self.listArray[indexPath.item].1 = true
                            self.listTableReload()
                        }
                    }else{
                        // not selected protien meal and alternate meal
                        showToast(message: "Please select previous Food Items")
                        return
                    }
                }else{
                    for i in 0..<listArray.count{
                        listArray[i].1 = false
                    }
                    listArray[indexPath.item].1 = true
                    listTableReload()
                }
            }else if indexPath.item == 2{
                let selectedIndex = listArray.filter({$0.1 == true})
                if selectedIndex[0].2 < indexPath.item{
                    if let carbsArr = self.mainMealArr?.carb{
                        if carbsArr.count != 0{
                            let selectedAlternateMeal = self.mainMealArr?.protein[index].alternate_meal.filter({$0.selectedLocal == 1})
                            let selectedMeal = self.mainMealArr?.protein[index].meal.filter({$0.selectedLocal == 1})
                            if selectedAlternateMeal?.count != 0 && selectedMeal?.count != 0 {
                                if carbsArr[index].alternate_meal.count == 0 && carbsArr[index].meal.count == 0{
                                    for i in 0..<self.listArray.count{
                                        self.listArray[i].1 = false
                                    }
                                    self.listArray[indexPath.item].1 = true
                                    self.listTableReload()
                                }else if carbsArr[index].alternate_meal.count != 0 && carbsArr[index].meal.count == 0{
                                     let selectedAlternateMeal = carbsArr[index].alternate_meal.filter({$0.selected == 1})
                                    if selectedAlternateMeal.count != 0{
                                        mealOptionId = 0
                                        alternateOptionId = selectedAlternateMeal[0].id!
                                        mealNumber = carbsArr[index].id!
                                        saveFooditem {
                                            for i in 0..<self.listArray.count{
                                                self.listArray[i].1 = false
                                            }
                                            self.listArray[indexPath.item].1 = true
                                            self.listTableReload()
                                        }
                                    }else{
                                        // not selected carb alternate meal (meal data is empty)
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                    
                                }else if carbsArr[index].alternate_meal.count == 0 && carbsArr[index].meal.count != 0{
                                     let selectedMeal = carbsArr[index].meal.filter({$0.selected == 1})
                                    if selectedMeal.count != 0{
                                        mealOptionId = selectedMeal[0].id!
                                        alternateOptionId = 0
                                        mealNumber = carbsArr[index].id!
                                        saveFooditem {
                                            for i in 0..<self.listArray.count{
                                                self.listArray[i].1 = false
                                            }
                                            self.listArray[indexPath.item].1 = true
                                            self.listTableReload()
                                        }
                                    }else{
                                        // not selected carb meal (alternate meal data is empty)
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                }else{
                                    let selectedAlternateMeal = carbsArr[index].alternate_meal.filter({$0.selected == 1})
                                    let selectedMeal = carbsArr[index].meal.filter({$0.selected == 1})
                                    if selectedAlternateMeal.count != 0 && selectedMeal.count != 0 {
                                        mealOptionId = selectedMeal[0].id!
                                        alternateOptionId = selectedAlternateMeal[0].id!
                                        mealNumber = carbsArr[index].id!
                                        saveFooditem {
                                            for i in 0..<self.listArray.count{
                                                self.listArray[i].1 = false
                                            }
                                            self.listArray[indexPath.item].1 = true
                                            self.listTableReload()
                                        }
                                    }else{
                                        // not selected carb meal and alternate meal)
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                }
                            }else{
                                showToast(message: "Please select previous Food Items")
                                return
                        }
                        }else{
                            showToast(message: "Please select previous Food Items")
                            return
                        }
                    }
                    
                }else{
                    for i in 0..<listArray.count{
                        listArray[i].1 = false
                    }
                    listArray[indexPath.item].1 = true
                    listTableReload()
                }
            }else if indexPath.item == 3{
               
                let selectedIndex = listArray.filter({$0.1 == true})
                if selectedIndex[0].2 < indexPath.item{
                    if let fatArr = self.mainMealArr?.fat{
                        if fatArr.count != 0{
                            let selectedAlternateMeal1 = self.mainMealArr?.protein[index].alternate_meal.filter({$0.selectedLocal == 1})
                            let selectedMeal1 = self.mainMealArr?.protein[index].meal.filter({$0.selectedLocal == 1})
                            if selectedAlternateMeal1?.count == 0 && selectedMeal1?.count == 0{
                                showToast(message: "Please select previous Food Items")
                                return
                            }else{
                                if self.mainMealArr?.carb[index].meal.count == 0 && self.mainMealArr?.carb[index].alternate_meal.count == 0{
                                    
                                }else if self.mainMealArr?.carb[index].meal.count == 0 && self.mainMealArr?.carb[index].alternate_meal.count != 0{
                                    let selectedAlternateMeal = self.mainMealArr?.carb[index].alternate_meal.filter({$0.selected == 1})
                                    if selectedAlternateMeal?.count == 0{
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                }else if self.mainMealArr?.carb[index].meal.count != 0 && self.mainMealArr?.carb[index].alternate_meal.count == 0{
                                    let selectedMeal = self.mainMealArr?.carb[index].meal.filter({$0.selected == 1})
                                    if selectedMeal?.count == 0{
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                }else{
                                    let selectedAlternateMeal = self.mainMealArr?.carb[index].alternate_meal.filter({$0.selected == 1})
                                    let selectedMeal = self.mainMealArr?.carb[index].meal.filter({$0.selected == 1})
                                    if selectedAlternateMeal?.count == 0 && selectedMeal?.count == 0 {
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                }
                                if fatArr[index].alternate_meal.count == 0 && fatArr[index].meal.count == 0{
                                    saveFooditem {
                                        for i in 0..<self.listArray.count{
                                            self.listArray[i].1 = false
                                        }
                                        self.listArray[indexPath.item].1 = true
                                        self.listTableReload()
                                    }
                                }else if fatArr[index].alternate_meal.count != 0 && fatArr[index].meal.count == 0{
                                    let selectedAlternateMeal = fatArr[index].alternate_meal.filter({$0.selected == 1})
                                    if selectedAlternateMeal.count != 0{
                                        mealOptionId = 0
                                        alternateOptionId = selectedAlternateMeal[0].id!
                                        mealNumber = fatArr[index].id!
                                        saveFooditem {
                                            for i in 0..<self.listArray.count{
                                                self.listArray[i].1 = false
                                            }
                                            self.listArray[indexPath.item].1 = true
                                            self.listTableReload()
                                        }
                                    }else{
                                        // not selected fat alternate meal (meal data is empty)
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                    
                                }else if fatArr[index].alternate_meal.count == 0 && fatArr[index].meal.count != 0{
                                    let selectedMeal = fatArr[index].meal.filter({$0.selected == 1})
                                    if selectedMeal.count != 0{
                                        mealOptionId = selectedMeal[0].id!
                                        alternateOptionId = 0
                                        mealNumber = fatArr[index].id!
                                        saveFooditem {
                                            for i in 0..<self.listArray.count{
                                                self.listArray[i].1 = false
                                            }
                                            self.listArray[indexPath.item].1 = true
                                            self.listTableReload()
                                        }
                                    }else{
                                        // not selected fat meal (alternate meal data is empty)
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                    
                                }else{
                                    let selectedAlternateMeal = fatArr[index].alternate_meal.filter({$0.selected == 1})
                                    let selectedMeal = fatArr[index].meal.filter({$0.selected == 1})
                                    if selectedAlternateMeal.count != 0 && selectedMeal.count != 0 {
                                        mealOptionId = selectedMeal[0].id!
                                        alternateOptionId = selectedAlternateMeal[0].id!
                                        mealNumber = fatArr[index].id!
                                        saveFooditem {
                                            for i in 0..<self.listArray.count{
                                                self.listArray[i].1 = false
                                            }
                                            self.listArray[indexPath.item].1 = true
                                            self.listTableReload()
                                        }
                                    }else{
                                        // not selected fat meal and alternate meal)
                                        showToast(message: "Please select previous Food Items")
                                        return
                                    }
                                }
                            }
                        }else{
                            showToast(message: "Please select previous Food Items")
                            return
                        }
                    }
                   
                }
            }else{
                for i in 0..<listArray.count{
                    listArray[i].1 = false
                }
                listArray[indexPath.item].1 = true
                listTableReload()
            }
        }else if collectionView == mealPlanCollectionView{
            if selectedMeal == "protein"{
                if var meal = mainMealArr?.protein[index].meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.protein[index].meal = meal
                }
            }else if selectedMeal == "carb"{
                if var meal = mainMealArr?.carb[index].meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.carb[index].meal = meal
                }
            }else if selectedMeal == "fat"{
                if var meal = mainMealArr?.fat[index].meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.fat[index].meal = meal
                }
            }else if selectedMeal == "condiment"{
                if var meal = mainMealArr?.condiments[index].meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.condiments[index].meal = meal
                }
            }
            mealPlanCollectionView.reloadData()
            mealListCollectionView.reloadData()

        }else if collectionView == alternatePlanCollectionView{
            if selectedMeal == "protein"{
                if var meal = mainMealArr?.protein[index].alternate_meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.protein[index].alternate_meal = meal
                }
            }else if selectedMeal == "carb"{
                if var meal = mainMealArr?.carb[index].alternate_meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.carb[index].alternate_meal = meal
                }
            }else if selectedMeal == "fat"{
                if var meal = mainMealArr?.fat[index].alternate_meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.fat[index].alternate_meal = meal
                }
            }else if selectedMeal == "condiment"{
                if var meal = mainMealArr?.condiments[index].alternate_meal{
                    for i in 0..<meal.count{
                        meal[i].selected = i == indexPath.item ? meal[i].selected == 1 ? 0 : 1 : 0
                    }
                    mainMealArr?.condiments[index].alternate_meal = meal
                }
            }
            alternatePlanCollectionView.reloadData()
            mealListCollectionView.reloadData()
        }
    }
    func showToast(message : String) {
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont(name: "Poppins-Regular", size: 12.0)
        label.text = message
        label.sizeToFit()
        let toastLabel = UILabel(frame: CGRect(x:label.frame.size.width > UIScreen.main.bounds.size.width * 0.8 ? self.view.frame.size.width/2 - UIScreen.main.bounds.size.width * 0.4 : self.view.frame.size.width/2 - (label.frame.size.width/2+10), y: self.view.frame.size.height-50, width: label.frame.size.width > UIScreen.main.bounds.size.width * 0.8 ? UIScreen.main.bounds.size.width * 0.8 : label.frame.size.width+20, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Poppins-Medium", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
    func listTableReload(){
        if listArray[0].1 == true{
            selectedMeal = "protein"
            configure(meal: selectedMeal)
        }else if listArray[1].1 == true{
            selectedMeal = "carb"
            configure(meal: selectedMeal)
        }else if listArray[2].1 == true{
            selectedMeal = "fat"
            configure(meal: selectedMeal)
        }else if listArray[3].1 == true{
            selectedMeal = "condiment"
            configure(meal: selectedMeal)
        }
        listCollectionView.reloadData()
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == mealListCollectionView{
            let label = UILabel(frame: CGRect.zero)
            if self.selectedMeal == "protein"{
                label.text = mainMealArr?.protein[indexPath.item].name
            }else if self.selectedMeal == "carb"{
                label.text = mainMealArr?.carb[indexPath.item].name
            }else if self.selectedMeal == "fat"{
                label.text = mainMealArr?.fat[indexPath.item].name
            }else if self.selectedMeal == "condiment"{
                label.text = mainMealArr?.condiments[indexPath.item].name
            }
            label.sizeToFit()
            return CGSize(width: label.frame.size.width+10, height: mealListCollectionView.frame.size.height)
        }else if collectionView == mealPlanCollectionView{
            let label = UILabel(frame: CGRect.zero)
            label.font = UIFont(name:"Poppins-Regular", size: 15.0)
            if self.selectedMeal == "protein"{
                label.text = mainMealArr?.protein[index].meal[indexPath.item].food_name
            }else if self.selectedMeal == "carb"{
                label.text = mainMealArr?.carb[index].meal[indexPath.item].food_name
            }else if self.selectedMeal == "fat"{
                label.text = mainMealArr?.fat[index].meal[indexPath.item].food_name
            }else if self.selectedMeal == "condiment"{
                label.text = mainMealArr?.condiments[index].meal[indexPath.item].food_name
            }
            
            label.sizeToFit()
            return CGSize(width: label.frame.size.width+10, height: 30)
        }else if collectionView == alternatePlanCollectionView{
            let label = UILabel(frame: CGRect.zero)
            if self.selectedMeal == "protein"{
                label.text = mainMealArr?.protein[index].alternate_meal[indexPath.item].food_name
            }else if self.selectedMeal == "carb"{
                label.text = mainMealArr?.carb[index].alternate_meal[indexPath.item].food_name
            }else if self.selectedMeal == "fat"{
                label.text = mainMealArr?.fat[index].alternate_meal[indexPath.item].food_name
            }else if self.selectedMeal == "condiment"{
                label.text = mainMealArr?.condiments[index].alternate_meal[indexPath.item].food_name
            }
            label.sizeToFit()
            return CGSize(width: label.frame.size.width+10, height: 30)
        }else{
            let label = UILabel(frame: CGRect.zero)
            label.text = listArray[indexPath.item].0
            label.sizeToFit()
            return CGSize(width: self.listCollectionView.bounds.size.width * 0.25, height: listCollectionView.frame.size.height)
        }
    }
    
}
extension CustomizeMealPlanVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if #available(iOS 13, *) {
            (scrollView.subviews[(scrollView.subviews.count - 1)].subviews[0]).backgroundColor = #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1)  //verticalIndicator
        } else {
            if let verticalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 1)] as? UIImageView) {
                verticalIndicator.backgroundColor = #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1) 
            }
        }
    }
}
