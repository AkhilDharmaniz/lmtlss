//
//  ResultsVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 01/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
class ResultsCell: UITableViewCell {
  @IBOutlet weak var titleLbl: UILabel!
  @IBOutlet weak var resultsLbl: UILabel!
    @IBOutlet weak var imgVw: UIImageView!
    @IBOutlet weak var progressBGLbl: UILabel!
    
    @IBOutlet weak var progressLbl: UILabel!
    @IBOutlet weak var progressLblWidth: NSLayoutConstraint!
}
class ResultsVC: UIViewController {
    
    @IBOutlet weak var resultsTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var resultsTableView: UITableView!
    var microDict = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }
    private func configureUI(){
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        resultsTableView.register(nib, forCellReuseIdentifier: "cell")
        resultsTableView.delegate = self
        resultsTableView.dataSource = self
        resultsTableView.separatorStyle = .none
        resultsTableViewHeight.constant = CGFloat(80 * 4)
        if let micro = questionnaireDict["bm_micro"] as? [String:Any]{
            microDict = micro
        }
        resultsTableView.reloadData()
        //  preferTableViewHeight.constant = CGFloat(70.0 * 2)
        
        
    }
}
extension ResultsVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as! ResultsCell
        cell.selectionStyle = .none
        if indexPath.row == 0{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.7458475828, green: 0.899774611, blue: 0.9934951663, alpha: 1)
            cell.progressLbl.backgroundColor =  #colorLiteral(red: 0.1690995097, green: 0.7477579713, blue: 0.9476557374, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "calories")
            cell.titleLbl.text = "Calories"
            cell.titleLbl.textColor = #colorLiteral(red: 0.2666666667, green: 0.737254902, blue: 0.9294117647, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.2666666667, green: 0.737254902, blue: 0.9294117647, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f kCal", microDict["calories"] as? Double ?? 0.0)
            cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.3
        }else if indexPath.row == 1{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.9912576079, green: 0.7879117727, blue: 0.6135630012, alpha: 1)
            cell.progressLbl.backgroundColor = #colorLiteral(red: 0.9287672639, green: 0.6092590094, blue: 0.3520768881, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "protein")
            cell.titleLbl.text = "Protein"
            cell.titleLbl.textColor = #colorLiteral(red: 0.8823529412, green: 0.568627451, blue: 0.3960784314, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.8823529412, green: 0.568627451, blue: 0.3960784314, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f g", microDict["protein"] as? Double ?? 0.0)
            cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.6
        }else if indexPath.row == 2{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.6887137294, green: 1, blue: 0.6693044305, alpha: 1)
            cell.progressLbl.backgroundColor = #colorLiteral(red: 0.3553128839, green: 0.790861547, blue: 0.338606596, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "carb")
            cell.titleLbl.text = "Carbs"
            cell.titleLbl.textColor = #colorLiteral(red: 0.4941176471, green: 0.7764705882, blue: 0.4274509804, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.4941176471, green: 0.7764705882, blue: 0.4274509804, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f g", microDict["carb"] as? Double ?? 0.0)
           cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.9
        }else if indexPath.row == 3{
            cell.progressBGLbl.backgroundColor = #colorLiteral(red: 0.9906267524, green: 0.7774583697, blue: 0.8127933741, alpha: 1)
            cell.progressLbl.backgroundColor = #colorLiteral(red: 0.9580395818, green: 0.3819159865, blue: 0.4740775824, alpha: 1)
            cell.imgVw.image = #imageLiteral(resourceName: "fat")
            cell.titleLbl.text = "Fat"
            cell.titleLbl.textColor = #colorLiteral(red: 0.8901960784, green: 0.4196078431, blue: 0.4823529412, alpha: 1)
            cell.resultsLbl.textColor = #colorLiteral(red: 0.8901960784, green: 0.4196078431, blue: 0.4823529412, alpha: 1)
            cell.resultsLbl.text = String(format: "%.02f g", microDict["fat"] as? Double ?? 0.0)
            cell.progressLblWidth.constant = cell.progressBGLbl.frame.size.width * 0.5
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    
}
