//
//  QuestionnaireContainerVC.swift
//  Lmtlss
//
//  Created by apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class QuestionnaireContainerVC: UIViewController {
    @IBOutlet weak var progressLbl: UILabel!
    @IBOutlet weak var nextBtn: UIButton!
    @IBOutlet weak var progressWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var container: UIView!
    @IBOutlet weak var navView: UIView!
    private var currentVC:UIViewController?
    private var questionnaireScreens:[QuestionnaireScreensTypes] = [.Form, .BMR, .PhysicalActivity, .Lifestyle, .Cardio, .basedProgram, .TrainDays, .Muscles, .Exercises, .Ambition, .LoseFat, .Prefer,.PreferBeverage,.BeverageSelection,.PreferMeal,.CaloriesDistribute, .LargerMeal,.CustomizeMealPlan,.IncorporateRecipe,.ReplaceRecipe,.SelectRecipe]
    var currentIndex = 0
    private var from = ""
    private var results = ""
    private var resultNextSelected = false
    var backStopIndex = 0
    var myCustomView:CustomHeaderView!
    override func viewDidLoad() {
        super.viewDidLoad()
        from = "load"
        configure()
        NotificationCenter.default.addObserver(self, selector: #selector(self.hideNextBtn(notification:)), name: Notification.Name("NextBtnHidden"), object: nil)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        configureNavView()
    }
    @objc func hideNextBtn(notification: Notification) {
        // Take Action on Notification
        if let hide = notification.userInfo?["hide"] as? Bool  {
            if hide == true{
                nextBtn.isHidden = true
            }else{
                nextBtn.isHidden = false
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        let support_count = UserDefaults.standard.value(forKey: "support_count") as? String
        if support_count != "0"{
            self.myCustomView.badgeView.isHidden = false
            self.myCustomView.badgeLbl.text = support_count
        }else{
            self.myCustomView.badgeView.isHidden = true
            self.myCustomView.badgeLbl.text = ""
        }
    }
    func configureNavView(){
        myCustomView = UINib(nibName: "CustomHeaderView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? CustomHeaderView
        myCustomView.frame = navView.frame
        myCustomView.titleLbl.isHidden = true
     //   myCustomView.titleLbl.text = "Questionnaire"
        myCustomView.logoImg.isHidden = false
        myCustomView.searchImage.isHidden = false
        myCustomView.searchImage.image = #imageLiteral(resourceName: "supportB")
        myCustomView.searchBtn.isUserInteractionEnabled = true
        myCustomView.menuImage.image = #imageLiteral(resourceName: "back")
        myCustomView.delegate = self
        if currentIndex == 0{
            myCustomView.menuBtn.isUserInteractionEnabled = false
            myCustomView.menuImage.isHidden = true
        }else{
            myCustomView.menuBtn.isUserInteractionEnabled = true
            myCustomView.menuImage.isHidden = false
        }         
        self.navView.addSubview(myCustomView)
    }
    func configureUI(){
      //  currentIndex = UserDefaults.standard.value(forKey: "currentIndex") as? Int ?? 0
     //   currentIndex = 17
        if currentIndex == 0{
            myCustomView.menuBtn.isUserInteractionEnabled = false
            myCustomView.menuImage.isHidden = true
        }else{
            myCustomView.menuBtn.isUserInteractionEnabled = true
            myCustomView.menuImage.isHidden = false
        }
        self.setControllerQuestion(type: self.questionnaireScreens[self.currentIndex])
        self.animateProgress()
    }
    
    private func animateProgress(){
        let singleUnit = UIScreen.main.bounds.width/CGFloat(questionnaireScreens.count)
        view.layoutIfNeeded()
        progressWidthConstraint.constant = singleUnit * CGFloat(currentIndex+1)
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.view.layoutIfNeeded()
        }
    }
    
    private func dismissAnimation(){
        let singleUnit = UIScreen.main.bounds.width/CGFloat(questionnaireScreens.count)
        UIView.animate(withDuration: 0.3) { () -> Void in
            self.progressWidthConstraint.constant = singleUnit * CGFloat(self.currentIndex+1)
        }
    }
    func showPOPUp(){
        alert(Constants.AppName, message: "Please select atleast 1 recipe for each meal.", view: self)
    }
    private func setControllerQuestion(type: QuestionnaireScreensTypes){
         self.nextBtn.setImage(#imageLiteral(resourceName: "next-Question"), for: .normal)
        nextBtn.isHidden = false
        switch type {
        case .Form:
            setVC(with: "FormQuestionVC")
        case .BMR:
            setVC(with: "BMRQuestionVC")
        case .PhysicalActivity:
            setVC(with: "PhysicalActivityVC")
        case .Lifestyle:
            setVC(with: "LifestyleQuestionVC")
        case .Cardio:
            setVC(with: "CardioQuestionVC")
        case .basedProgram:
            setVC(with: "ProgramTypeQuestionVC")
        case .TrainDays:
            setVC(with: "TrainDaysQuestionVC")
        case .Muscles:
            setVC(with: "MusclesQuestionVC")
        case .Exercises:
            setVC(with: "ExercisesQuestionVC")
        case .Ambition:
            setVC(with: "AmbitionQuestionVC")
        case .LoseFat:
            setVC(with: "LoseFatVC")
        case .Prefer:
            setVC(with: "PreferQuestionVC")
        case .PreferBeverage:
            setVC(with: "PreferBeverageVC")
        case .BeverageSelection:
            setVC(with: "BeverageSelectionVC")
        case .PreferMeal:
            setVC(with: "PreferMealVC")
        case .CaloriesDistribute:
            setVC(with: "CaloriesDistributeVC")
        case .results:
            setVC(with: "ResultsVC")
          //  nextBtn.isHidden = true
            self.nextBtn.setImage(#imageLiteral(resourceName: "GTMP"), for: .normal)
            self.resultNextSelected = true
        case .LargerMeal:
            setVC(with: "LargerMealPreferVC")
        case .CustomizeMealPlan:
            setVC(with: "CustomizeMealPlanVC")
        case .IncorporateRecipe:
            setVC(with: "IncorporateRecipeVC")
        case .ReplaceRecipe:
            setVC(with: "ReplaceRecipeVC")
        case .SelectRecipe:
            setVC(with: "RecipeSelectionVC")
        }
        
    }
    private func setVC(with identifier: String){
        ViewEmbedder.embed(
            withIdentifier: identifier,
            parent: self,
            container: self.container){ vc in
                self.currentVC = vc
        }
    }
    
    //MARK:- IBAction Method(s)
    @IBAction func nextBtnAction(_ sender: UIButton) {
        if Reachability.isConnectedToNetwork() == true {
            if resultNextSelected == true{
//                resultNextSelected = false
//                currentIndex -= 1
//                setControllerQuestion(type: questionnaireScreens[currentIndex])
//                dismissAnimation()
                saveTrainingPlan()
//                let baseVC = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
//                baseVC.tabBarController?.selectedIndex = 1
//                self.navigationController?.pushViewController(baseVC, animated: false)
            }else{
                self.validateData(type: self.currentIndex)
            }
        }else{
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func configure(){
        if Reachability.isConnectedToNetwork() == true {
            DispatchQueue.main.async {
                IJProgressView.shared.showProgressView()
            }
            let qa = Constants.baseURL + Constants.questionnaire
            var token = UserDefaults.standard.value(forKey: "token") as? String
            token = "Bearer " + token!
            let headers : HTTPHeaders = ["Content-Type":"application/json" , "Authorization":token ?? ""]
            AFWrapperClass.requestGETURL(qa, params: nil, headers: headers, success: { (response) in
                IJProgressView.shared.hideProgressView()
              //  
                let message = response["message"] as? String ?? ""
                if let responseCode = response["responseCode"] as? Int{
                    if responseCode == 200{
                      //  
                        if let responseData = response["responseData"] as? [String:Any]{
                            questionnaireDict = responseData
                            if let qa = responseData["qa"] as? [[String:Any]]{
                                questionnaire = qa
                                if var BMROption = questionnaire[1]["option"] as? [[String:Any]]{
                                    if BMROption.count == 0{
                                        let weight = ["option": "0","question":"Weight","unit":"kg"] as [String : Any]
                                        let age = ["option": "","question":"Age","unit":"date"] as [String : Any]
                                        let height = ["option": "0","question":"Height","unit":"cm"] as [String : Any]
                                        let targetWeight = ["option": "0","question":"Target Weight","unit":"deficit"] as [String : Any]
                                        BMROption.append(weight)
                                        BMROption.append(age)
                                        BMROption.append(height)
                                        BMROption.append(targetWeight)
                                        questionnaire[1]["option"] = BMROption
                                    }
                                }
                                if self.from == "load" {
                                    self.configureUI()
                                }else{
                                    if self.results == "yes"{
                                        self.saveMealPlan(from: "results")
                                        self.currentIndex += 1
                                        self.setControllerQuestion(type: .results)
                                        self.results = ""
                                    }else{
                                       
                                        if self.currentIndex != 20{
                                            if self.currentIndex == 5{
                                                if questionnaire[6]["question_id"] as? Int == 23{
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += 4
                                                }
                                            }else if self.currentIndex == 9{
                                                //   if questionnaire[7]["question_id"] as? Int == 15 || questionnaire[8]["question_id"] as? Int == 15 || questionnaire[10]["question_id"] as? Int == 15{
                                                if questionnaire[6]["question_id"] as? Int == 23 && questionnaire[10]["question_id"] as? Int == 15 || questionnaire[7]["question_id"] as? Int == 15 {
                                                    self.currentIndex += 2
                                                }else{
                                                    self.currentIndex += 1
                                                }
                                            }else if self.currentIndex == 12{
                                                var index = 0
                                                for i in 0..<questionnaire.count{
                                                    if questionnaire[i]["question_id"] as? Int == 29{
                                                        index = i
                                                    }
                                                }
                                                if questionnaire[index]["question_id"] as? Int == 29{
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += 2
                                                }
                                            }else if self.currentIndex == 15 {
                                                var index = 0
                                                for i in 0..<questionnaire.count{
                                                    if questionnaire[i]["question_id"] as? Int == 18{
                                                        index = i
                                                    }
                                                }
                                                if questionnaire[index]["question_id"] as? Int == 18{
                                                    self.currentIndex += 1
                                                }else{
                                                    self.currentIndex += 2
                                                }
                                            }
                                            else{
                                                self.currentIndex += 1
                                            }
                                            self.setControllerQuestion(type: self.questionnaireScreens[self.currentIndex])
                                            UserDefaults.standard.set(self.currentIndex, forKey: "currentIndex")
                                            self.animateProgress()
                                            if self.currentIndex == 0{
                                                if let subView = self.navView.subviews[0] as? CustomHeaderView{
                                                    subView.menuImage.isHidden = true
                                                    subView.menuBtn.isUserInteractionEnabled = false
                                                }
                                            }else{
                                                if let subView = self.navView.subviews[0] as? CustomHeaderView{
                                                    subView.menuImage.isHidden = false
                                                    subView.menuBtn.isUserInteractionEnabled = true
                                                }
                                            }
                                        }else{
                                        self.saveMealPlan(from: "last")
                                        }
                                }
                                }
                            }
                        }
                    }else{
                        IJProgressView.shared.hideProgressView()
                        alert(Constants.AppName, message: message, view: self)
                    }
                }
                if let status = response["status"] as? Int, status == 401{
                    logOut(controller: self)
                }
            }) { (error) in
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: error.localizedDescription, view: self)
              //  
            }
        } else {
            alert(Constants.AppName, message: "Check internet connection", view: self)
        }
    }
    func saveMealPlan(from:String){
        DispatchQueue.main.async {
            IJProgressView.shared.showProgressView()
        }
        let signInUrl = Constants.baseURL + Constants.mealPlan
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: nil, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            let message = response["message"] as? String ?? ""
            let status = response["responseCode"] as? Int ?? 0
            if status == 200{
                if from != "results"{
                    let baseVC = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
                    baseVC.tabBarController?.selectedIndex = 1
                    self.navigationController?.pushViewController(baseVC, animated: false)
                }
//                let homeTabBarVC = HomeTabBarVC.instantiate(fromAppStoryboard: .Home)
//                self.navigationController?.pushViewController(homeTabBarVC, animated: true)
            }else{
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: message, view: self)
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func saveTrainingPlan(){
        DispatchQueue.main.async {
            IJProgressView.shared.showProgressView()
        }
        let signInUrl = Constants.baseURL + Constants.trainingPlan
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: nil, headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            let message = response["message"] as? String ?? ""
            let status = response["responseCode"] as? Int ?? 0
            if status == 200{
                self.resultNextSelected = false
                let baseVC = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
                baseVC.tabBarController?.selectedIndex = 1
                self.navigationController?.pushViewController(baseVC, animated: false)
            }else{
                IJProgressView.shared.hideProgressView()
                alert(Constants.AppName, message: message, view: self)
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func validateData(type: Int){
        switch type {
        case 0:
            if let vc = self.children.first as? FormQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 1:
            if let vc = self.children.first as? BMRQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 2:
            if let vc = self.children.first as? PhysicalActivityVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 3:
            if let vc = self.children.first as? LifestyleQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 4:
            if let vc = self.children.first as? CardioQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 5:
            if let vc = self.children.first as? ProgramTypeQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 6:
            if let vc = self.children.first as? TrainDaysQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 7:
            if let vc = self.children.first as? MusclesQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 8:
            if let vc = self.children.first as? ExercisesQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 9:
            if let vc = self.children.first as? AmbitionQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 10:
            if let vc = self.children.first as? LoseFatVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 11:
            if let vc = self.children.first as? PreferQuestionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie(yesHandler: {
                        self.showPopUp(isValid: isValid)
                        self.results = ""
                        
                    }) {
                        self.results = "yes"
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 12:
            if let vc = self.children.first as? PreferBeverageVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 13:
            if let vc = self.children.first as? BeverageSelectionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 14:
            if let vc = self.children.first as? PreferMealVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 15:
            if let vc = self.children.first as? CaloriesDistributeVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 16:
            if let vc = self.children.first as? LargerMealPreferVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 17:
            if let vc = self.children.first as? CustomizeMealPlanVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.saveFooditem {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 18:
            if let vc = self.children.first as? IncorporateRecipeVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie(yesHandler: {
                        self.showPopUp(isValid: isValid)
                    }) {
                        self.saveMealPlan(from: "last")
                    }
                }
            }
        case 19:
            if let vc = self.children.first as? ReplaceRecipeVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        case 20:
            if let vc = self.children.first as? RecipeSelectionVC {
                let isValid : Bool = vc.validateScreenData()
                if isValid == false{
                    vc.submitQuestionnarie {
                        self.showPopUp(isValid: isValid)
                    }
                }
            }
        default:
            showPopUp(isValid: false)
        }
    }
    func showPopUp(isValid : Bool){
        if isValid == false{
            from = "next"
            configure()
        }
    }
}

//MARK:- Nav bar delegate Method(s)
extension QuestionnaireContainerVC: CustomHeaderViewDelegate{
    func searchBtnTapped() {
        let VC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
        self.navigationController?.pushViewController(VC, animated: true)
//        showMessage(title: Constants.AppName, message: "Do you want to Logout?", okButton: "OK", cancelButton: "Cancel", controller: self, okHandler: {
//            DispatchQueue.main.async {
//                appDelegate().logOutSuccess()
//            }
//        }) {}
    }
    func openSideMenu() {
        if currentIndex != 0{
            if resultNextSelected == true{
                resultNextSelected = false
            }
            //  currentIndex -= 1
            if self.backStopIndex != 0 && self.backStopIndex == self.currentIndex{
                self.navigationController?.popViewController(animated: true)
            }
            if self.currentIndex == 9{
                if questionnaire[6]["question_id"] as? Int == 23{
                    self.currentIndex -= 1
                }else{
                    self.currentIndex -= 4
                }
            }else if self.currentIndex == 11{
//                if questionnaire[7]["question_id"] as? Int == 15 || questionnaire[8]["question_id"] as? Int == 15 ||  questionnaire[10]["question_id"] as? Int == 15{
//                    currentIndex -= 2
//                }else{
//                    currentIndex -= 1
//                }
                if questionnaire[6]["question_id"] as? Int == 23 && questionnaire[10]["question_id"] as? Int == 15 || questionnaire[7]["question_id"] as? Int == 15 { 
                    currentIndex -= 2
                }else{
                    currentIndex -= 1
                }
            }else if self.currentIndex == 14{
                var index = 0
                for i in 0..<questionnaire.count{
                    if questionnaire[i]["question_id"] as? Int == 29{
                        index = i
                    }
                }
                if questionnaire[index]["question_id"] as? Int == 29{
                    currentIndex -= 1
                }else{
                    currentIndex -= 2
                }
            } else if self.currentIndex == 17{
                var index = 0
                for i in 0..<questionnaire.count{
                    if questionnaire[i]["question_id"] as? Int == 18{
                        index = i
                    }
                }
                if questionnaire[index]["question_id"] as? Int == 18{
                    currentIndex -= 1
                }else{
                    currentIndex -= 2
                }
            }
            else{
                currentIndex -= 1
            }
            if currentIndex == 0{
                if let subView = self.navView.subviews[0] as? CustomHeaderView{
                    subView.menuImage.isHidden = true
                    subView.menuBtn.isUserInteractionEnabled = false
                }
            }else{
                if let subView = self.navView.subviews[0] as? CustomHeaderView{
                    subView.menuImage.isHidden = false
                    subView.menuBtn.isUserInteractionEnabled = true
                }
            }
            setControllerQuestion(type: questionnaireScreens[currentIndex])
            dismissAnimation()
        }else{
            if let subView = self.navView.subviews[0] as? CustomHeaderView{
                subView.menuImage.isHidden = true
                subView.menuBtn.isUserInteractionEnabled = false
            }
        }
    }
}
extension UIResponder {
  
  func getOwningViewController() -> UIViewController? {
    var nextResponser = self
    while let next = nextResponser.next {
      nextResponser = next
      if let viewController = nextResponser as? UIViewController {
        return viewController
      }
    }
    return nil
  }
}
