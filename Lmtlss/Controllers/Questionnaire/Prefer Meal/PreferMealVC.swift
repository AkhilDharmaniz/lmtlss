//
//  PreferMealVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 06/10/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class PreferMealVC: UIViewController {
    
    @IBOutlet weak var preferMealTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var preferMealTableView: UITableView!
    @IBOutlet weak var questionTitleLbl: UILabel!
    var options = [QuestionaryField]()
    var questionId = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
    }
    private func configureUI(){
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        preferMealTableView.register(nib, forCellReuseIdentifier: "cell")
        preferMealTableView.delegate = self
        preferMealTableView.dataSource = self
        preferMealTableView.separatorStyle = .none
        //  preferTableViewHeight.constant = CGFloat(70.0 * 2)
        for i in 0..<questionnaire.count{
            if questionnaire[i]["question_id"] as? Int == 16{
                questionId = questionnaire[i]["question_id"] as? Int ?? 0
                questionTitleLbl.text = questionnaire[i]["question"] as? String ?? ""
                if let option = questionnaire[i]["option"] as? [[String:Any]] {
                    for i in 0..<option.count{
                        self.options.append(QuestionaryField(option: option[i]["option"] as? String ?? "", option_id: option[i]["option_id"] as? Int ?? 0, selected: option[i]["selected"] as? Int ?? 0, description: option[i]["description"] as? String ?? "", example: option[i]["example"] as? String ?? "", title: option[i]["title"] as? String ?? ""))
                    }
                    
             //       preferMealTableViewHeight.constant = CGFloat(80 * self.options.count)
                    preferMealTableView.reloadData()
                }
            }
        }
    }
    func validateScreenData() -> Bool{
        
        let selectedElement = options.filter({$0.selected == 1})
        if selectedElement.count == 0{
            alert(Constants.AppName, message: "Please Select any 1 Option", view: self)
            return true
        }
        return false
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.questionnaireSave
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
        var parameters:[String:AnyObject] = [:]
        parameters["question_id"] = questionId as AnyObject
        let optionsStr = getSelectedOptions()
        parameters["option_id"] = optionsStr as AnyObject
        
        return parameters
    }
    func getSelectedOptions() -> String
    {
        let selectedArr : [String]  = self.options.filter { (objField) -> Bool in
            return objField.selected == 1
        }.map { (objField) -> String in
            return "\(String(describing: objField.option_id!))"
        }
        return selectedArr.joined(separator: ",")
    }
    
}
extension PreferMealVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        let data = options[indexPath.row]
        cell.titleLbl.text = "\(data.option ?? "") Meals"
       // cell.subTitleLbl.text = data.description
        cell.subTitleLbl.isHidden = true
        cell.exampleLbl.isHidden = true
        cell.bgImg.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        DispatchQueue.main.async {
            self.preferMealTableViewHeight.constant = self.preferMealTableView.contentSize.height
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        for i in 0..<options.count{
            options[i].selected = i == indexPath.row ? 1 : 0
        }
        preferMealTableView.reloadData()
    }
    
}
