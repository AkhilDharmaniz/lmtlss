      //
//  ExercisesQuestionVC.swift
//  Lmtlss
//
//  Created by Dharmani Apps on 18/09/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import Alamofire

class ExercisesCollectionVeiwCell : UICollectionViewCell{
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var selectedLbl: UILabel!
}
class ExercisesQuestionVC: UIViewController {

    @IBOutlet weak var homeGymView: UIView!
    @IBOutlet weak var homeGymViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var questionTitleLbl: UILabel!
    @IBOutlet weak var exercisesTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var exercisesTableView: UITableView!
    @IBOutlet weak var scrollVw: UIScrollView!
    @IBOutlet weak var exercisesCollectionView: UICollectionView!
    var execrciesCollectionList = [ExerciseResponseField]()
    var homeExecrciesCollectionList = [ExerciseResponseField]()
    var gymExecrciesCollectionList = [ExerciseResponseField]()
    var gymHomeSelected = ""
    var questionId = 0
    @IBOutlet weak var homeBtn: UIButton!
    @IBOutlet weak var gymBtn: UIButton!
    
  //   fileprivate var  execrciesCollectionList = [Options(option: "Biceps", selected: true),Options(option: "Chest", selected: false),Options(option: "Triceps", selected: false),Options(option: "Shoulders", selected: false),Options(option: "ABS", selected: false),Options(option: "Hamstring", selected: false),Options(option: "Quads", selected: false),Options(option: "Back", selected: false)]
  //  var execrciesTableList = [ExerciseResponseField]()
    var index = 0
//    fileprivate var  execrciesTableList = [Options(option: "Getting Started", selected: false),Options(option: "Barbell bench press", selected: false),Options(option: "Pec deck", selected: false),Options(option: "Cable Crossover", selected: false),Options(option: "Chest press", selected: false),Options(option: "Dips", selected: false)]
    override func viewDidLoad() {
        super.viewDidLoad()

        configureUI()
    }
    private func configureUI(){
        
        exercisesCollectionView.delegate = self
        exercisesCollectionView.dataSource = self
        scrollVw.delegate = self
        let nib = UINib(nibName: "PhysicalActivityCell", bundle: nil)
        exercisesTableView.register(nib, forCellReuseIdentifier: "cell")
        exercisesTableView.delegate = self
        exercisesTableView.dataSource = self
        exercisesTableView.separatorStyle = .none
        for i in 0..<questionnaire.count{
            if questionnaire[i]["question_id"] as? Int == 11{
                questionTitleLbl.text = questionnaire[i]["question"] as? String ?? ""
                questionId = questionnaire[i]["question_id"] as? Int ?? 0
                
                if let exercises = questionnaire[i]["option"] as? [[String:Any]]{
                    self.gymHomeSelected = "single"
                    self.homeGymViewHeightConstraint.constant = 0
                    self.homeGymView.isHidden = true
                    for i in 0..<exercises.count{
                        var exerciseArray = [ExerciseField]()
                        if let optionsList = exercises[i]["option_list"] as? [[String:Any]]{
                            for j in 0..<optionsList.count{
                                exerciseArray.append(ExerciseField(exercise_name: optionsList[j]["exercise_name"] as? String ?? "", id: optionsList[j]["id"]
                                    as? Int ?? 0, selected: optionsList[j]["selected"] as? Int ?? 0))
                                
                            }
                        }
                        self.execrciesCollectionList.append(ExerciseResponseField(option_name: exercises[i]["option_name"] as? String ?? "", exerciseList: exerciseArray,selected: false))
                    }
                    self.execrciesCollectionList[0].selected = true
                    self.exercisesCollectionView.reloadData()
                    exercisesTableViewHeight.constant = CGFloat(80 * execrciesCollectionList[index].exerciseList.count)
                    self.exercisesTableView.reloadData()
                }else{
                    if let exercises = questionnaire[i]["option"] as? [String:Any]{
                        self.gymHomeSelected = "gym"
                        if let gymExercises = exercises["gym"] as? [[String:Any]]{
                            for i in 0..<gymExercises.count{
                                var exerciseArray = [ExerciseField]()
                                if let optionsList = gymExercises[i]["option_list"] as? [[String:Any]]{
                                    for j in 0..<optionsList.count{
                                        exerciseArray.append(ExerciseField(exercise_name: optionsList[j]["exercise_name"] as? String ?? "", id: optionsList[j]["id"]
                                            as? Int ?? 0, selected: optionsList[j]["selected"] as? Int ?? 0))
                                        
                                    }
                                }
                                self.gymExecrciesCollectionList.append(ExerciseResponseField(option_name: gymExercises[i]["option_name"] as? String ?? "", exerciseList: exerciseArray,selected: false))
                            }
                            self.gymExecrciesCollectionList[0].selected = true
                            self.exercisesCollectionView.reloadData()
                            exercisesTableViewHeight.constant = CGFloat(80 * gymExecrciesCollectionList[index].exerciseList.count)
                            self.exercisesTableView.reloadData()
                        }
                        if let homeExercises = exercises["home"] as? [[String:Any]]{
                            for i in 0..<homeExercises.count{
                                var exerciseArray = [ExerciseField]()
                                if let optionsList = homeExercises[i]["option_list"] as? [[String:Any]]{
                                    for j in 0..<optionsList.count{
                                        exerciseArray.append(ExerciseField(exercise_name: optionsList[j]["exercise_name"] as? String ?? "", id: optionsList[j]["id"]
                                            as? Int ?? 0, selected: optionsList[j]["selected"] as? Int ?? 0))
                                        
                                    }
                                }
                                self.homeExecrciesCollectionList.append(ExerciseResponseField(option_name: homeExercises[i]["option_name"] as? String ?? "", exerciseList: exerciseArray,selected: false))
                            }
                        }
                    }
                }
            }
        }
    }
    func validateScreenData() -> Bool{
        
        //              let selectedElement = execrciesTableList.filter({$0.selected == true})
        //              if selectedElement.count < 2{
        //                  alert(Constants.AppName, message: "Please Select any 2 Options", view: self)
        //                  return true
        //              }
        return false
    }
    
    @IBAction func homeGymBtnAction(_ sender: UIButton) {
        if sender.tag == 1 {
            self.gymHomeSelected = "home"
            homeBtn.backgroundColor = UIColor(named: "BGColor") // #colorLiteral(red: 0.2081934214, green: 0.8793118, blue: 0.9418017268, alpha: 1)
            homeBtn.setTitleColor(.white, for: .normal)
            gymBtn.setTitleColor(.black, for: .normal)
            gymBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            //  getData(data: "home")
            self.homeExecrciesCollectionList[index].selected = true
            self.exercisesCollectionView.reloadData()
            exercisesTableViewHeight.constant = CGFloat(80 * homeExecrciesCollectionList[index].exerciseList.count)
            self.exercisesTableView.reloadData()
        }else if sender.tag == 2{
            self.gymHomeSelected = "gym"
            gymBtn.backgroundColor = UIColor(named: "BGColor") // #colorLiteral(red: 0.2081934214, green: 0.8793118, blue: 0.9418017268, alpha: 1)
            homeBtn.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            gymBtn.setTitleColor(.white, for: .normal)
            homeBtn.setTitleColor(.black, for: .normal)
            //   getData(data: "gym")
            self.gymExecrciesCollectionList[index].selected = true
            self.exercisesCollectionView.reloadData()
            exercisesTableViewHeight.constant = CGFloat(80 * gymExecrciesCollectionList[index].exerciseList.count)
            self.exercisesTableView.reloadData()
        }
    }
    func getData(data: String){
        for i in 0..<questionnaire.count{
            if questionnaire[i]["question_id"] as? Int == 11{
                self.execrciesCollectionList.removeAll()
                if let exercises = questionnaire[i]["option"] as? [String:Any]{
                    if let homeExercises = exercises[data] as? [[String:Any]]{
                        for i in 0..<homeExercises.count{
                            var exerciseArray = [ExerciseField]()
                            if let optionsList = homeExercises[i]["option_list"] as? [[String:Any]]{
                                for j in 0..<optionsList.count{
                                    exerciseArray.append(ExerciseField(exercise_name: optionsList[j]["exercise_name"] as? String ?? "", id: optionsList[j]["id"]
                                        as? Int ?? 0, selected: optionsList[j]["selected"] as? Int ?? 0))
                                    
                                }
                            }
                            self.execrciesCollectionList.append(ExerciseResponseField(option_name: homeExercises[i]["option_name"] as? String ?? "", exerciseList: exerciseArray,selected: false))
                        }
                        self.execrciesCollectionList[index].selected = true
                        self.exercisesCollectionView.reloadData()
                        exercisesTableViewHeight.constant = CGFloat(80 * execrciesCollectionList[index].exerciseList.count)
                        self.exercisesTableView.reloadData()
                    }
                }
                
            }
            
        }
    }
    func submitQuestionnarie(successHandler: @escaping (() -> Void)){
        
        IJProgressView.shared.showProgressView()
        let signInUrl = Constants.baseURL + Constants.questionnaireSave
         
        var token = UserDefaults.standard.value(forKey: "token") as? String
        token = "Bearer " + token!
        let headers : HTTPHeaders = ["Authorization":token ?? ""]
        AFWrapperClass.requestPostWithMultiFormData(signInUrl, params: generatingParameters(), headers: headers, success: { (response) in
            IJProgressView.shared.hideProgressView()
            
            let message = response["message"] as? String ?? ""
            if let status = response["responseCode"] as? Int {
                if status == 200{
                    successHandler()
                }else{
                    IJProgressView.shared.hideProgressView()
                    alert(Constants.AppName, message: message, view: self)
                }
            }
            if let status = response["status"] as? Int, status == 401{
                logOut(controller: self)
            }
        }) { (error) in
            IJProgressView.shared.hideProgressView()
            alert(Constants.AppName, message: error.localizedDescription, view: self)
            
        }
    }
    func generatingParameters() -> [String:AnyObject] {
              var parameters:[String:AnyObject] = [:]
            parameters["question_id"] = questionId as AnyObject
        let optionsStr = getSelectedOptions()
           parameters["option_id"] = optionsStr as AnyObject
           
              return parameters
          }
     func getSelectedOptions() -> String
       {
        var selectedElement = [ExerciseField]()
        if gymHomeSelected == "single"{
            for i in 0..<execrciesCollectionList.count{
                let selectedElementArr = execrciesCollectionList[i].exerciseList.filter({$0.selected == 1})
                for obj in selectedElementArr{
                    selectedElement.append(obj)
                }
            }
        }else{
            for i in 0..<gymExecrciesCollectionList.count{
                let selectedElementArr = gymExecrciesCollectionList[i].exerciseList.filter({$0.selected == 1})
                for obj in selectedElementArr{
                    selectedElement.append(obj)
                }
            }
            for i in 0..<homeExecrciesCollectionList.count{
                let selectedElementArr = homeExecrciesCollectionList[i].exerciseList.filter({$0.selected == 1})
                for obj in selectedElementArr{
                    selectedElement.append(obj)
                }
            }
        }
           let selectedArr : [String]  = selectedElement.filter { (objField) -> Bool in
               return objField.selected == 1
               }.map { (objField) -> String in
                   return "\(String(describing: objField.id!))"
           }
           return selectedArr.joined(separator: ",")
       }
}
extension ExercisesQuestionVC: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if self.gymHomeSelected == "gym"{
          return gymExecrciesCollectionList.count
        }else if self.gymHomeSelected == "home"{
          return homeExecrciesCollectionList.count
        }else{
          return execrciesCollectionList.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if self.gymHomeSelected == "gym"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ExercisesCollectionVeiwCell
            cell.nameLbl.text = gymExecrciesCollectionList[indexPath.item].option_name
            cell.selectedLbl.backgroundColor = gymExecrciesCollectionList[indexPath.item].selected == true ? UIColor(named: "BGColor") : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0) // #colorLiteral(red: 0.2025949061, green: 0.8596807122, blue: 0.9263890386, alpha: 1) : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            return cell
        }else if self.gymHomeSelected == "home"{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ExercisesCollectionVeiwCell
            cell.nameLbl.text = homeExecrciesCollectionList[indexPath.item].option_name
            cell.selectedLbl.backgroundColor = homeExecrciesCollectionList[indexPath.item].selected == true ? UIColor(named: "BGColor") : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! ExercisesCollectionVeiwCell
            cell.nameLbl.text = execrciesCollectionList[indexPath.item].option_name
            cell.selectedLbl.backgroundColor = execrciesCollectionList[indexPath.item].selected == true ? UIColor(named: "BGColor") : #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0)
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.gymHomeSelected == "gym" || self.gymHomeSelected == "home"{
            for i in 0..<gymExecrciesCollectionList.count{
                gymExecrciesCollectionList[i].selected = i == indexPath.item ? true : false
            }
            for i in 0..<homeExecrciesCollectionList.count{
                homeExecrciesCollectionList[i].selected = i == indexPath.item ? true : false
            }
            exercisesCollectionView.reloadData()
            index = indexPath.item
            exercisesTableViewHeight.constant = self.gymHomeSelected == "gym" ? CGFloat(80 * gymExecrciesCollectionList[index].exerciseList.count) : CGFloat(80 * homeExecrciesCollectionList[index].exerciseList.count)
            exercisesTableView.reloadData()
        }else{
            for i in 0..<execrciesCollectionList.count{
                execrciesCollectionList[i].selected = i == indexPath.item ? true : false
            }
            exercisesCollectionView.reloadData()
            index = indexPath.item
//            exercisesTableViewHeight.constant = CGFloat(80 * execrciesCollectionList[index].exerciseList.count)
            exercisesTableView.reloadData()
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let label = UILabel(frame: CGRect.zero)
        if self.gymHomeSelected == "gym"{
            label.text = gymExecrciesCollectionList[indexPath.item].option_name
        }else if self.gymHomeSelected == "home"{
            label.text = homeExecrciesCollectionList[indexPath.item].option_name
        }else{
            label.text = execrciesCollectionList[indexPath.item].option_name
        }
        label.sizeToFit()
        return CGSize(width: label.frame.size.width+10, height: exercisesCollectionView.frame.size.height)
    }
    
    
}
extension ExercisesQuestionVC: UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.gymHomeSelected == "gym"{
            return gymExecrciesCollectionList[index].exerciseList.count
        }else if self.gymHomeSelected == "home"{
            return homeExecrciesCollectionList[index].exerciseList.count
        }else{
            return execrciesCollectionList[index].exerciseList.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PhysicalActivityCell
        cell.selectionStyle = .none
        var data = ExerciseField(exercise_name: "", id: 0, selected: 0)
        if self.gymHomeSelected == "gym"{
            data = gymExecrciesCollectionList[index].exerciseList[indexPath.row]
        }else if self.gymHomeSelected == "home"{
            data = homeExecrciesCollectionList[index].exerciseList[indexPath.row]
        }else{
            data = execrciesCollectionList[index].exerciseList[indexPath.row]
        }
        DispatchQueue.main.async {
            self.exercisesTableViewHeight.constant = self.exercisesTableView.contentSize.height
        }
        cell.titleLbl.text = data.exercise_name
        cell.subTitleLbl.isHidden = true
        cell.exampleLbl.isHidden = true
        cell.bgImg.backgroundColor = data.selected == 1 ? #colorLiteral(red: 0.8680323958, green: 0.967669785, blue: 0.9996747375, alpha: 1) : .white
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      //  return 80
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.gymHomeSelected == "gym"{
            let selectedElement = gymExecrciesCollectionList[index].exerciseList.filter({$0.selected == 1})
            if selectedElement.count < 2{
                if gymExecrciesCollectionList[index].exerciseList[indexPath.row].selected == 0{
                    gymExecrciesCollectionList[index].exerciseList[indexPath.row].selected = 1
                }else{
                    gymExecrciesCollectionList[index].exerciseList[indexPath.row].selected = 0
                }
            }else{
                gymExecrciesCollectionList[index].exerciseList[indexPath.row].selected = 0
            }
            exercisesTableView.reloadData()
        }else if self.gymHomeSelected == "home"{
            let selectedElement = homeExecrciesCollectionList[index].exerciseList.filter({$0.selected == 1})
            if selectedElement.count < 2{
                if homeExecrciesCollectionList[index].exerciseList[indexPath.row].selected == 0{
                    homeExecrciesCollectionList[index].exerciseList[indexPath.row].selected = 1
                }else{
                    homeExecrciesCollectionList[index].exerciseList[indexPath.row].selected = 0
                }
            }else{
                homeExecrciesCollectionList[index].exerciseList[indexPath.row].selected = 0
            }
            exercisesTableView.reloadData()
            
        }else{
            let selectedElement = execrciesCollectionList[index].exerciseList.filter({$0.selected == 1})
            if selectedElement.count < 2{
                if execrciesCollectionList[index].exerciseList[indexPath.row].selected == 0{
                    execrciesCollectionList[index].exerciseList[indexPath.row].selected = 1
                }else{
                    execrciesCollectionList[index].exerciseList[indexPath.row].selected = 0
                }
            }else{
                execrciesCollectionList[index].exerciseList[indexPath.row].selected = 0
            }
            
            //        execrciesCollectionList[index].exerciseList[indexPath.row].selected = !execrciesCollectionList[index].exerciseList[indexPath.row].selected
            exercisesTableView.reloadData()
        }
      
    }
}
extension ExercisesQuestionVC: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if #available(iOS 13, *) {
            (scrollView.subviews[(scrollView.subviews.count - 1)].subviews[0]).backgroundColor = UIColor(named: "BGColor") // #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1) //verticalIndicator
//            (scrollView.subviews[(scrollView.subviews.count - 2)].subviews[0]).backgroundColor = #colorLiteral(red: 0.2023728192, green: 0.8594731688, blue: 0.9392046928, alpha: 1) //horizontalIndicator
        } else {
            if let verticalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 1)] as? UIImageView) {
                verticalIndicator.backgroundColor = UIColor(named: "BGColor") // #colorLiteral(red: 0.02352941176, green: 0.8588235294, blue: 0.9411764706, alpha: 1)
            }
            
//            if let horizontalIndicator: UIImageView = (scrollView.subviews[(scrollView.subviews.count - 2)] as? UIImageView) {
//                horizontalIndicator.backgroundColor = #colorLiteral(red: 0.2023728192, green: 0.8594731688, blue: 0.9392046928, alpha: 1)
//            }
        }
    }
}
