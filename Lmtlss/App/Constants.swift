//

//  Created by Vivek Dharmani on 04/12/17.
//  Copyright © 2017 Vivek Dharmani. All rights reserved.

//

//

import Foundation
import UIKit
class Constants {
    static let AppName = "Lmtlss"
    static let baseURL = "http://161.97.91.153/api/web/v1/"
    static let aboutUS = "http://161.97.91.153/site/about"
    static let terms = "http://161.97.91.153/site/term-and-condition"
    
    
    static let logIn = "user/login"
    static let forgotPassword = "user/forgotpassword"
    static let questionnaire = "questionnaire/all-qa"
    static let questionnaireSave = "questionnaire/qa-save"
    static let protienItem = "questionnaire/protien-item"
    static let fatItem = "questionnaire/fat-item"
    static let carbItem = "questionnaire/carb-item"
    static let condimentsItem = "questionnaire/condiments-item"
    static let saveFoodItem = "questionnaire/food-item-save"
    static let recordWaterIntake = "record/water-intake"
    static let recordWeight = "record/weight"
    static let recordSleep = "record/sleep"
    static let mealPlan = "questionnaire/meal-plan"
    static let trainingPlan = "questionnaire/training-plan"
    static let getRecord = "record/get-record"
    static let getUserProfile = "user/user-profile"
    static let editUserProfle = "user/edit-profile"
    static let allRecipe = "recipe/all-recipe"
    static let allSupplements = "supplement/all-supplements"
    static let uploadimg = "questionnaire/uploadimg"
    static let saveRecipe = "questionnaire/save-recipe"
    static let myMealPlan = "mealplan/user-meal?meal_type="
    static let userSettings = "user/user-settings"
    static let recordSteps = "record/steps-count"
    static let recipeDetails = "recipe/recipe-detail?recipe_id="
    static let videoList = "video/video-list?category="
    static let videoTypes = "video/category-list"
    static let getTrainingPlan = "mealplan/get-training-plan"
    static let doSwap = "swap/do-swap"
    static let markMealAsDone = "mark-meal-as-done/mark-done"
    static let forumList = "forum/forum-list"
    static let macroDetail = "mealplan/macro-detail"
    static let favUnfav = "video-fav-unfav/fav-unfav"
    static let favVideoList = "video/fav-video-list"
    static let saveDrinks = "questionnaire/save-drinks"
    static let checkInQuestionnaire = "checkin-questionnaire/all-qa"
    static let saveCheckInQuestionnaire = "checkin-questionnaire/qa-save"
    static let checkInUploadImg = "checkin-questionnaire/uploadimg"
    static let checkInMealPlan = "checkin-questionnaire/meal-plan"
    static let checkInTrainingPlan = "checkin-questionnaire/training-plan"
    static let supportListing = "support-chat/support-message"
    static let supportMessageSave = "support-chat/support-message-save"
    static let pushNotificationStatus = "user/push-notification-status"
    static let myProgrms = "mealplan/my-program"
    static let notificationList = "user/notification-detail"
    static let logout = "user/logout"
    static let volumnSave = "volumn/volumn-save"
    static let volumnLogExercise = "volumn/volumn-log-exercise"
    static let volumnFinishedExercise = "volumn/volumn-finished-exercise"
    static let getAllMusclesAndExercise = "volumn/get-all-muscles-and-exercise"
    static let getExerciseStatisticDetailById = "volumn/get-exercise-statistic-detail-by-id"
    static let volumeGraph = "volumn/volume-graph"
    static let recipeListing = "recipe/recipe-listing"
    static let recipeSendMail = "recipe/send-mail"
    static let addShoppingDay = "shopping-list/add-shopping-day"
    static let shoppingListDetail = "shopping-list/shopping-list-detail?meal_type="
    static let addShoppingList = "shopping-list/add-shopping-list"
    static let shoppingListReview = "shopping-list/shopping-list-review"
    static let shoppingListPdf = "shopping-list/shopping-list-pdf"
    static let checkShoppingListStatus = "shopping-list/check-shopping-list-status"
}
struct Colors {
    
    static var gradient1 = UIColor(red: 244.0/255.0, green: 92.0/255.0, blue: 67.0/255.0, alpha: 1)
    static var gradient2 = UIColor(red: 235.0/255.0, green: 51.0/255.0, blue: 73.0/255.0, alpha: 1)
    static var gradient3 = UIColor(red: 211.0/255.0, green: 16.0/255.0, blue: 39.0/255.0, alpha: 1)
    
    static var appbgColor = UIColor.init(red: 247.0/255.0, green: 248.0/255.0, blue: 249.0/255.0, alpha: 1)
    static var appRedColor = UIColor.init(red: 224.0/255.0, green: 29.0/255.0, blue: 48.0/255.0, alpha: 1)
    static var shadowColor = UIColor.init(red: 225.0/255.0, green: 225.0/255.0, blue: 225.0/255.0, alpha: 1)
    static var sliderGrayColor = UIColor.init(red: 133.0/255.0, green: 133.0/255.0, blue: 133.0/255.0, alpha: 0.5)
    static var borderColor = UIColor.init(red: 151.0/255.0, green: 151.0/255.0, blue: 151.0/255.0, alpha: 0.18)
    static var sepratorColor = UIColor.init(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 0.08)
    static var readStatusColor = UIColor.init(red: 200.0/255.0, green: 200.0/255.0, blue: 200.0/255.0, alpha: 1)
    static var readDateColor = UIColor.init(red: 172.0/255.0, green: 172.0/255.0, blue: 173.0/255.0, alpha: 1)
    static var slateGrayColor = UIColor.init(red: 78.0/255.0, green: 78.0/255.0, blue: 86.0/255.0, alpha: 1)
    static var slateGrayColorAlpha = UIColor.init(red: 78.0/255.0, green: 78.0/255.0, blue: 86.0/255.0, alpha: 0.5)


}
