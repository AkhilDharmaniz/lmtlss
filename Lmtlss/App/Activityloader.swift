//
//  Activityloader.swift
//  Colert App
//
//  Created by Dharmani Apps on 26/11/19.
//  Copyright © 2019 Dharmani Apps. All rights reserved.
//
import Foundation
import UIKit

open class IJProgressView {
    public static let shared = IJProgressView()
    
    var containerView = UIView()
    var progressView = UIView()
    var activityIndicator = UIActivityIndicatorView()
    
    open func showProgressView() {
        let window = UIWindow(frame: UIScreen.main.bounds)
        
        containerView.frame = window.frame
        containerView.center = window.center
        containerView.backgroundColor = UIColor(hex: 0xffffff, alpha: 0.3)
        
        progressView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        progressView.center = window.center
//        progressView.backgroundColor = UIColor(hex: 0x444444, alpha: 0.7)
        progressView.backgroundColor = UIColor.lightGray
        progressView.clipsToBounds = true
        progressView.layer.cornerRadius = 10
        
        activityIndicator.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        activityIndicator.style = .whiteLarge
        activityIndicator.color = UIColor(named: "BGColor")
            // #colorLiteral(red: 0.2078431373, green: 0.8784313725, blue: 0.9411764706, alpha: 1)
        activityIndicator.center = CGPoint(x: progressView.bounds.width / 2, y: progressView.bounds.height / 2)
        
        progressView.addSubview(activityIndicator)
        containerView.addSubview(progressView)
        UIApplication.shared.keyWindow?.addSubview(containerView)
        activityIndicator.startAnimating()
    }
    
    open func hideProgressView() {
        DispatchQueue.main.async {
            self.activityIndicator.stopAnimating()
            self.containerView.removeFromSuperview()
        }
    }
    open func showToast(message : String, center : Bool) {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let label = UILabel(frame: CGRect.zero)
        label.font = UIFont(name: "Poppins-Regular", size: 12.0)
        label.text = message
        label.sizeToFit()
        let toastLabel = UILabel(frame: CGRect(x:label.frame.size.width > UIScreen.main.bounds.size.width * 0.8 ? window.frame.size.width/2 - UIScreen.main.bounds.size.width * 0.4 : window.frame.size.width/2 - (label.frame.size.width/2+10), y: center == true ? window.frame.size.height/2 : window.frame.size.height-50, width: label.frame.size.width > UIScreen.main.bounds.size.width * 0.8 ? UIScreen.main.bounds.size.width * 0.8 : label.frame.size.width+20, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.8)
        toastLabel.textColor = UIColor.white
        toastLabel.numberOfLines = 0
        toastLabel.textAlignment = .center;
        toastLabel.font = UIFont(name: "Poppins-Medium", size: 12.0)
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        UIApplication.shared.keyWindow?.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
            toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
}

extension UIColor {
    convenience init(hex: UInt32, alpha: CGFloat) {
        let red = CGFloat((hex & 0xFF0000) >> 16)/256.0
        let green = CGFloat((hex & 0xFF00) >> 8)/256.0
        let blue = CGFloat(hex & 0xFF)/256.0
        
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }
}
