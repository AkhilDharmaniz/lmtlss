//
//  AppDelegate.swift
//  Lmtlss
//
//  Created by apple on 21/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import SDWebImage
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        UITabBar.appearance().tintColor = #colorLiteral(red: 0.3601126671, green: 0.8402963877, blue: 0.9105961919, alpha: 1)
        UITabBar.appearance().unselectedItemTintColor = .black
        configureNotification(application: application)
        return true
    }
    
    func logOutSuccess(){
        if let _ = UserDefaults.standard.value(forKey: "token") as? String{
            UserDefaults.standard.removeObject(forKey: "token")
        }
        if let _ = UserDefaults.standard.value(forKey: "support_count") as? String{
            UserDefaults.standard.removeObject(forKey: "support_count")
        }
        if let _ = UserDefaults.standard.value(forKey: "notification_count") as? String{
            UserDefaults.standard.removeObject(forKey: "notification_count")
        }
        if let _ = UserDefaults.standard.value(forKey: "currentIndex") as? Int{
            UserDefaults.standard.removeObject(forKey: "currentIndex")
        }
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let homeViewController = LoginVC.instantiate(fromAppStoryboard: .Authentication)
        let nav = UINavigationController(rootViewController: homeViewController)
        nav.setNavigationBarHidden(true, animated: true)
        appdelegate.window!.rootViewController = nav
    }
    func application(_ application: UIApplication, performFetchWithCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
    }
    // MARK: UISceneSession Lifecycle
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    @available(iOS 13.0, *)
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "Lmtlss")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

}
//MARK:- Push notifications method(s)
extension AppDelegate: UNUserNotificationCenterDelegate{
    
    func configureNotification(application: UIApplication) {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.delegate = self
            center.requestAuthorization(options:[.badge, .alert, .sound]){ (granted, error) in
                DispatchQueue.main.async {
                    application.registerForRemoteNotifications()
                }
            }
        }else{
            UIApplication.shared.registerUserNotificationSettings(UIUserNotificationSettings(types: [.badge, .sound, .alert], categories: nil))
        }
        UIApplication.shared.registerForRemoteNotifications()
    }

    
   
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        UIApplication.shared.applicationIconBadgeNumber = 0
        
        if let userInfo = response.notification.request.content.userInfo as? [String:Any]{
            if let apnsData = userInfo["aps"] as? [String:Any]{
                if let dataObj = apnsData["data"] as? [String:Any]{
                    let supportCount = dataObj["support_count"] as? String ?? ""
                    UserDefaults.standard.set(supportCount, forKey: "support_count")
                    let notificationCount = dataObj["notification_count"] as? String ?? ""
                    UserDefaults.standard.set(notificationCount, forKey: "notification_count")
                    let notificationType = dataObj["push_type"] as? String
                    if notificationType == "1"{
                        let baseVC = QuestionnaireContainerVC.instantiate(fromAppStoryboard: .Questionnaire)
                        let nav = UINavigationController(rootViewController: baseVC)
                        nav.isNavigationBarHidden = true
                        self.window?.rootViewController = nav
                    }else if notificationType == "2" || notificationType == "9"{
                        let baseVC = CheckInContainerVC.instantiate(fromAppStoryboard: .CheckInQuestionnaire)
                        baseVC.fromDelegate = true
                        let nav = UINavigationController(rootViewController: baseVC)
                        nav.isNavigationBarHidden = true
                        self.window?.rootViewController = nav
                    }else if notificationType == "3" || notificationType == "6" || notificationType == "7"{
                        let baseVC = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
                        let nav = UINavigationController(rootViewController: baseVC)
                        nav.isNavigationBarHidden = true
                        self.window?.rootViewController = nav
                    }else if notificationType == "4"{
                        let baseVC = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
                        let tabBar = baseVC.rootViewController as? HomeTabBarVC
                        tabBar?.selectedIndex = 1
                        let nav = UINavigationController(rootViewController: baseVC)
                        nav.isNavigationBarHidden = true
                        self.window?.rootViewController = nav
                    }else if notificationType == "5"{
                        let baseVC = SupportVC.instantiate(fromAppStoryboard: .SideMenu)
                        baseVC.fromDelegate = true
                        let nav = UINavigationController(rootViewController: baseVC)
                        nav.isNavigationBarHidden = true
                        self.window?.rootViewController = nav
                    }else if notificationType == "8"{
                        let baseVC = ForumListingVC.instantiate(fromAppStoryboard: .SideMenu)
                        baseVC.fromDelegate = true
                        let nav = UINavigationController(rootViewController: baseVC)
                        nav.isNavigationBarHidden = true
                        self.window?.rootViewController = nav
                    }
                }
            }
        }
        completionHandler()
    }
    func redirectToExercisePlan(){
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let viewController = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
        let tabBar = viewController.rootViewController as? HomeTabBarVC
        tabBar?.selectedIndex = 1
        let nav = UINavigationController(rootViewController: viewController)
        nav.setNavigationBarHidden(true, animated: true)
        appdelegate.window!.rootViewController = nav
    }
    func redirectToHome(){
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        let viewController = LGSideBaseVC.instantiate(fromAppStoryboard: .Home)
        let nav = UINavigationController(rootViewController: viewController)
        nav.setNavigationBarHidden(true, animated: true)
        appdelegate.window!.rootViewController = nav
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print(deviceTokenString)
        UserDefaults.standard.set(deviceTokenString as String, forKey: "device_token")
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("APNs registration failed: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        let userDict = userInfo as! [String:Any]
        print("received", userDict)
        if application.applicationState == .active{
            if let apnsData = userDict["aps"] as? [String:Any]{
                if let dataObj = apnsData["data"] as? [String:Any]{
                    let supportCount = dataObj["support_count"] as? String ?? ""
                    UserDefaults.standard.set(supportCount, forKey: "support_count")
                    let notificationCount = dataObj["notification_count"] as? String ?? ""
                    UserDefaults.standard.set(notificationCount, forKey: "notification_count")
                    if supportCount != "0" || notificationCount != "0"{
                        NotificationCenter.default.post(name: Notification.Name("updateBadge"), object: nil, userInfo: nil)
                    }
                }
            }
        }
        completionHandler(.newData)
    }
    
    
    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
       
        if let userInfo = notification.request.content.userInfo as? [String:Any]{
            if let apnsData = userInfo["aps"] as? [String:Any]{
                if let dataObj = apnsData["data"] as? [String:Any]{
                    let supportCount = dataObj["support_count"] as? String ?? ""
                    UserDefaults.standard.set(supportCount, forKey: "support_count")
                    let notificationCount = dataObj["notification_count"] as? String ?? ""
                    UserDefaults.standard.set(notificationCount, forKey: "notification_count")
                    if supportCount != "0" || notificationCount != "0"{
                        NotificationCenter.default.post(name: Notification.Name("updateBadge"), object: nil, userInfo: nil)
                    }
                }
            }
        }
        completionHandler([.sound,.alert])
    }
}
func appDelegate() -> AppDelegate {
    return UIApplication.shared.delegate as! AppDelegate
}
