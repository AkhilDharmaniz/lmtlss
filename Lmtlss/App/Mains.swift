//
//  Mains.swift
//  Lmtlss
//
//  Created by apple on 29/12/21.
//  Copyright © 2021 Apple. All rights reserved.
//

import Foundation

struct Mains {
    static var AppTitle = "Lmtlss"
    func convertToHMS(number: Int) -> String {
//        let minute  = (number % 3600) / 60;
        let minute = (number / 60) % 3600
        let second = (number % 3600) % 60 ;
        var m = String(minute);
        var s = String(second);
        if m.count == 1{
            m = "0\(minute)";
        }
        if s.count == 1{
            s = "0\(second)";
        }
        return "\(m):\(s)"
    }
}
