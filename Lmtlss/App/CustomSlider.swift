//
//  CustomSlider.swift
//  Lmtlss
//
//  Created by apple on 22/07/20.
//  Copyright © 2020 Apple. All rights reserved.
//

import UIKit

class CustomSlider: UISlider{
    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        let point = CGPoint(x: bounds.minX, y: bounds.midY)
        return CGRect(origin: point, size: CGSize(width: bounds.width, height: 15))
    }
}
